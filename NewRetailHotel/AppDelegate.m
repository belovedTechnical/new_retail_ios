//
//  AppDelegate.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/1.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "AppDelegate.h"
#import "RootTool.h"
#import "SaveTool.h"
#import <AlipaySDK/AlipaySDK.h>
#import "WXApi.h"

@interface AppDelegate ()<WXApiDelegate>

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = [RootTool choosRootVC];
    [self.window makeKeyAndVisible];
    
    // 获取网络状态
    [[BDNetworkTools sharedInstance] startMonitoringNetworkState];
    NSString *userKey = [SaveTool objectForKey:apikey];
    if (userKey.length < 8) {
        [[BDNetworkTools sharedInstance] getLoginSignInBasicAuthWithBlock:^(NSDictionary *responseObject, NSError *error) {
            
        }];
    }
    // 注册微信
    [WXApi registerApp:kWeChatURLScheme];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([url.host isEqualToString:@"safepay"]) {
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
//            NSLog(@"result = %@",resultDic);
            //支付返回结果，实际支付结果需要去自己的服务器端查询
            NSNotification *notification = [NSNotification notificationWithName:PAY_NOTIFICATION object:@"success"]; [[NSNotificationCenter defaultCenter] postNotification:notification];
        }];
        
    }else {
        BOOL isSuc = [WXApi handleOpenURL:url delegate:self];
        return isSuc;
    }
    return YES;
}

// NOTE: 9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
{
    if ([url.host isEqualToString:@"safepay"]) {
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
//            NSLog(@"result = %@",resultDic);
            //支付返回结果，实际支付结果需要去自己的服务器端查询
            NSNotification *notification = [NSNotification notificationWithName:PAY_NOTIFICATION object:nil]; [[NSNotificationCenter defaultCenter] postNotification:notification];
        }];
        
    }else {
        BOOL isSuc = [WXApi handleOpenURL:url delegate:self];
        return isSuc;
    }
    return YES;
}

//微信回调,有支付结果的时候会回调这个方法
- (void)onResp:(BaseResp *)resp {
    // 支付结果回调
    if([resp isKindOfClass:[PayResp class]]){
        switch (resp.errCode) {
            case WXSuccess:{
                //支付返回结果，实际支付结果需要去自己的服务器端查询
                NSNotification *notification = [NSNotification notificationWithName:PAY_NOTIFICATION object:@"success"]; [[NSNotificationCenter defaultCenter] postNotification:notification]; break;
            }
            default:{
                NSNotification *notification = [NSNotification notificationWithName:PAY_NOTIFICATION object:@"fail"]; [[NSNotificationCenter defaultCenter] postNotification:notification]; break;
            }
        }
    }
}

#pragma mark -判断旧版本更新
- (void)oldVersionsUpdata {
    // 获取线上APP版本号
    NSString *appStoreInfo = [NSString stringWithContentsOfURL:[NSURL URLWithString:@""] encoding:NSUTF8StringEncoding error:nil];
    NSString *versionsInfo = [appStoreInfo substringFromIndex:[appStoreInfo rangeOfString:@"\"version\":"].location+10];
    versionsInfo = [[versionsInfo substringToIndex:[versionsInfo rangeOfString:@","].location]stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    if (versionsInfo != nil && [versionsInfo length] > 0) {
        [self localCompareAppStoreWithVersions:versionsInfo];
    }
}

- (void)localCompareAppStoreWithVersions:(NSString *)versions
{
    // 获取当前版本
    NSString *localVersionsInfo = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    CGFloat localVersions = [localVersionsInfo floatValue];
    CGFloat appStoreVersions = [versions floatValue];
    // 如果线上版本大于当前版本，则进入更新
    if (appStoreVersions > localVersions) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"发现新版本" message:@"是否前往更新" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *alert1 = [UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSString *urlStr = @"https://itunes.apple.com/cn/app/%E8%87%B3%E7%88%B1%E7%A9%BA%E9%97%B4/id1032586093?mt=8";
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:urlStr]];
        }];
        UIAlertAction *alert2 = [UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alertVC addAction:alert1];
        [alertVC addAction:alert2];
        //初始化UIWindows
        UIWindow *aW = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
        aW.rootViewController = [[UIViewController alloc]init];
        aW.windowLevel = UIWindowLevelAlert + 1;
        [aW makeKeyAndVisible];
        [aW.rootViewController presentViewController:alertVC animated:YES completion:nil];
    }
}











@end


