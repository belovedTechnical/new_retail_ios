//
//  StoreOrderModel.h
//  BelovedHotel
//
//  Created by BDSir on 2017/8/21.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreOrderModel : NSObject

/** 店铺总金额 */
@property(nonatomic, copy) NSString *totalPrice;
/** 子订单折后价总金额 */
@property(nonatomic, copy) NSString *zTotalPrice;
/** 子订单优惠总金额 */
@property(nonatomic, copy) NSString *yTotalPrice;
/** 店铺类型 */
@property(nonatomic, copy) NSString *storeType;
/** 店铺名称 */
@property(nonatomic, copy) NSString *storeName;
/** 店铺ID */
@property(nonatomic, copy) NSString *productStoreId;
/** 商品信息 */
@property(nonatomic, strong) NSArray *productList;

+ (instancetype)storeOrderWithDict:(NSDictionary *)dict;

@end
