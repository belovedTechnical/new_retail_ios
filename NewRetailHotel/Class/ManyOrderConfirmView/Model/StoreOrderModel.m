//
//  StoreOrderModel.m
//  BelovedHotel
//
//  Created by BDSir on 2017/8/21.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "StoreOrderModel.h"
#import "OrderCellModel.h"

@implementation StoreOrderModel

+ (instancetype)storeOrderWithDict:(NSDictionary *)dict
{
    StoreOrderModel *model = [[self alloc] init];
    [model setValuesForKeysWithDictionary:dict];
    
    NSMutableArray *tempArray = [NSMutableArray array];
    for (NSDictionary *cellDict in dict[@"productList"]) {
        OrderCellModel *cell = [OrderCellModel orderCellWithDict:cellDict];
        [tempArray addObject:cell];
    }
    model.productList = tempArray;
    
    return model;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}

@end
