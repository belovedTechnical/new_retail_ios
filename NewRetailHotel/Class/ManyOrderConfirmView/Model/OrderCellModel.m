//
//  OrderCellModel.m
//  BelovedHotel
//
//  Created by BDSir on 2017/8/21.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "OrderCellModel.h"

@implementation OrderCellModel

+ (instancetype)orderCellWithDict:(NSDictionary *)dict
{
    OrderCellModel *model = [[self alloc] init];
    [model setValuesForKeysWithDictionary:dict];
    
    return model;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}

@end
