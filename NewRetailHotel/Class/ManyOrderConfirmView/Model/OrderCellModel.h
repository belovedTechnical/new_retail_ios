//
//  OrderCellModel.h
//  BelovedHotel
//
//  Created by BDSir on 2017/8/21.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderCellModel : NSObject

/** 名称 */
@property(nonatomic, copy) NSString *productName;
/** 图片 */
@property(nonatomic, copy) NSString *productImg;
/** 数量 */
@property(nonatomic, copy) NSString *quantity;
/** 规格 */
@property(nonatomic, copy) NSString *featureName;
/** 商品ID */
@property(nonatomic, copy) NSString *productId;
/** 价格 */
@property(nonatomic, copy) NSString *price;

+ (instancetype)orderCellWithDict:(NSDictionary *)dict;

@end
