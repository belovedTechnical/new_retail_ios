//
//  VerifyOrderformModel.h
//  BelovedHotel
//
//  Created by BDSir on 2017/8/21.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VerifyOrderformModel : NSObject

/** 总价 */
@property(nonatomic, copy) NSString *totalPrice;
/** 店铺数组 */
@property(nonatomic, strong) NSArray *storeOrderList;

+ (instancetype)verifyOrderformModelWithDict:(NSDictionary *)dict;

@end
