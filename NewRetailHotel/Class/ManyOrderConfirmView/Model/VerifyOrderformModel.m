//
//  VerifyOrderformModel.m
//  BelovedHotel
//
//  Created by BDSir on 2017/8/21.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "VerifyOrderformModel.h"
#import "StoreOrderModel.h"

@implementation VerifyOrderformModel

+ (instancetype)verifyOrderformModelWithDict:(NSDictionary *)dict
{
    VerifyOrderformModel *model = [[self alloc] init];
    [model setValuesForKeysWithDictionary:dict];
    
    NSMutableArray *tempArray = [NSMutableArray array];
    for (NSDictionary *storeDict in dict[@"storeOrderList"]) {
        StoreOrderModel *storeItem = [StoreOrderModel storeOrderWithDict:storeDict];
        [tempArray addObject:storeItem];
    }
    model.storeOrderList = tempArray;
    
    return model;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}

@end
