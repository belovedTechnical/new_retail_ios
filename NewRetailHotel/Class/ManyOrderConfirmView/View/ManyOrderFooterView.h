//
//  ManyOrderFooterView.h
//  BelovedHotel
//
//  Created by BDSir on 2017/8/18.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManyOrderFooterView : UIView

/** 留言 */
@property(nonatomic, strong) UITextField *leaveText;
/** 名称输入框 */
@property(nonatomic, strong) UITextField *nameText;
/** 纳税人识别号 */
@property (nonatomic, strong) UITextField *identifierTF;
/** 发票选择按钮 */
@property(nonatomic, strong) UIButton *invoiceBtn;
/** 个人 */
@property(nonatomic, strong) UIButton *individualBtn;
/** 单位 */
@property(nonatomic, strong) UIButton *unitsBtn;

@end
