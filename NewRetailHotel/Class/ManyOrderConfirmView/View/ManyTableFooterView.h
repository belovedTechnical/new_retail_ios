//
//  ManyTableFooterView.h
//  BelovedHotel
//
//  Created by BDSir on 2017/8/22.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManyTableFooterView : UITableViewHeaderFooterView

/** 运费价格 */
@property(nonatomic, strong) UILabel *sumFreightLabel;
/** 店铺优惠价格 */
@property(nonatomic, strong) UILabel *sumDiscountLabel;
/** 小计价格 */
@property(nonatomic, strong) UILabel *sumSubtotalLabel;
/** 优惠券点击按钮 */
@property(nonatomic, strong) UIButton *couponBtn;

@property(nonatomic, strong) void(^tableFoorerViewTouchesBegan)(void);
@property(nonatomic, strong) void(^couponButtonClick)(void);

@end
