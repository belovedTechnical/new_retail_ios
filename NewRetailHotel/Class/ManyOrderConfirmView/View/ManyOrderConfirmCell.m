//
//  ManyOrderConfirmCell.m
//  BelovedHotel
//
//  Created by BDSir on 2017/8/18.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "ManyOrderConfirmCell.h"
#import "OrderCellModel.h"
#import "UIImageView+WebCache.h"

@interface ManyOrderConfirmCell()

@property(nonatomic, strong) UILabel *nameLabel;
@property(nonatomic, strong) UILabel *featureLabel;
@property(nonatomic, strong) UILabel *priceLabel;
@property(nonatomic, strong) UILabel *quantityLabel;
@property(nonatomic, strong) UIImageView *iconView;

@end
@implementation ManyOrderConfirmCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setupUI];
    }
    return  self;
}

- (void)setModel:(OrderCellModel *)model
{   _model = model;
    
    self.nameLabel.text = model.productName;
    self.priceLabel.text = [NSString stringWithFormat:@"¥%@",model.price];
    self.featureLabel.text = model.featureName;
    self.quantityLabel.text = [NSString stringWithFormat:@"x%@",model.quantity];
    [self.iconView sd_setImageWithURL:[NSURL URLWithString: model.productImg]];
}

- (void)setupUI
{
    // 图片
    UIImageView *iconView = [[UIImageView alloc] init];
    iconView.backgroundColor = [UIColor orangeColor];
    [self addSubview:iconView];
    self.iconView = iconView;
    
    // 名字
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.font = [UIFont systemFontOfSize:15];
    nameLabel.textColor = [UIColor colorWithHexString:@"#333333"];
    nameLabel.numberOfLines = 0;
    [self addSubview:nameLabel];
    self.nameLabel = nameLabel;
    
    // 规格
    UILabel *featureLabel = [[UILabel alloc] init];
    featureLabel.font = [UIFont systemFontOfSize:12];
    featureLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    [self addSubview:featureLabel];
    self.featureLabel = featureLabel;
    
    // 价格
    UILabel *priceLabel = [[UILabel alloc] init];
    priceLabel.font = [UIFont systemFontOfSize:16];
    priceLabel.textColor = [UIColor colorWithHexString:@"#b4282d"];
    [self addSubview:priceLabel];
    self.priceLabel = priceLabel;
    
    // 数量
    UILabel *quantityLabel = [[UILabel alloc] init];
    quantityLabel.font = [UIFont systemFontOfSize:14];
    quantityLabel.textColor = [UIColor colorWithHexString:@"#333333"];
    quantityLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:quantityLabel];
    self.quantityLabel = quantityLabel;
    
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.top.equalTo(self.mas_top).offset(12);
        make.bottom.equalTo(self.mas_bottom).offset(-12);
        make.width.equalTo(iconView.mas_height);
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(iconView.mas_top).offset(-5);
        make.left.equalTo(iconView.mas_right).offset(6);
        make.right.equalTo(self.mas_right).offset(-20);
        make.height.equalTo(@30);
    }];
    
    [featureLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(nameLabel.mas_bottom).offset(0);
        make.left.equalTo(nameLabel.mas_left).offset(0);
        make.right.equalTo(nameLabel.mas_right);
        make.height.equalTo(@20);
    }];
    
    [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(iconView.mas_bottom).offset(0);
        make.left.equalTo(nameLabel.mas_left).offset(0);
        make.height.equalTo(@20);
        make.width.equalTo(@80);
    }];
    
    [quantityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(iconView.mas_bottom).offset(0);
        make.right.equalTo(self.mas_right).offset(-20);
        make.height.equalTo(@23);
        make.width.equalTo(@60);
    }];
}


@end

