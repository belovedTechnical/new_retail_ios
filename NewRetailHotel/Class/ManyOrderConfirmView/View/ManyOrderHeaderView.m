//
//  ManyOrderHeaderView.m
//  BelovedHotel
//
//  Created by BDSir on 2017/8/18.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "ManyOrderHeaderView.h"
#import "AddressListModel.h"

@interface ManyOrderHeaderView()

@property(nonatomic, strong) UILabel *nameLabel;
@property(nonatomic, strong) UILabel *phoneLabel;
@property(nonatomic, strong) UILabel *addressLabel;
@property(nonatomic, strong) UIView *bgView;

@end

@implementation ManyOrderHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self NoAddress];
    }
    return self;
}

- (void)setModel:(AddressListModel *)model
{   _model = model;
    self.bgView.hidden = YES;
    [self existAddress];
    self.nameLabel.text = [NSString stringWithFormat:@"收货人:%@", model.attnName];
    self.phoneLabel.text = [NSString stringWithFormat:@"电话:%@", model.telePhone];
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@",model.provinceGeoNameLocal, model.cityGeoNameLocal, model.address1];
}

#pragma mark -有收货地址
- (void)existAddress
{
    // 送至我的地址
    //    UIButton *promptBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [promptBtn setImage:[UIImage imageNamed:@"icon_select_selected.png"] forState:UIControlStateNormal];
    //    [promptBtn setTitle:@"送货至我的地址" forState:UIControlStateNormal];
    //    [promptBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //    promptBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    //    promptBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
    //    promptBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //    [self addSubview:promptBtn];
    // 地址图标
    UIImageView *addressIcon = [[UIImageView alloc] init];
    addressIcon.frame = CGRectMake(5, self.center.y, 23, 23);
    addressIcon.image = [UIImage imageNamed:@"icon_position"];
    addressIcon.backgroundColor = [UIColor clearColor];
    [self addSubview:addressIcon];
    
    // 背景图片View
    UIImageView *bgImageView = [[UIImageView alloc] init];
    bgImageView.image = [UIImage imageNamed:@"bg_adress_shading"];
    bgImageView.userInteractionEnabled = YES;
    [self addSubview:bgImageView];
    _bgImageView = bgImageView;
    
    // 名字
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.font = [UIFont systemFontOfSize:15];
    nameLabel.textColor = [UIColor colorWithHexString:@"#333333"];
    nameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:nameLabel];
    self.nameLabel = nameLabel;
    
    // 电话
    UILabel *phoneLabel = [[UILabel alloc] init];
    phoneLabel.font = [UIFont systemFontOfSize:14];
    phoneLabel.textColor = [UIColor colorWithHexString:@"#333333"];
    [self addSubview:phoneLabel];
    self.phoneLabel = phoneLabel;
    
    // 地址
    UILabel *addressLabel = [[UILabel alloc] init];
    addressLabel.font = [UIFont systemFontOfSize:14];
    addressLabel.numberOfLines = 0;
    addressLabel.textAlignment = NSTextAlignmentLeft;
    addressLabel.textColor = [UIColor colorWithHexString:@"#444444"];
    [self addSubview:addressLabel];
    self.addressLabel = addressLabel;
    
    // 指引图标
    UIButton *directImage = [[UIButton alloc] init];
    [directImage setImage:[UIImage imageNamed:@"icon_rightarrow02.png"] forState:UIControlStateNormal];
    directImage.backgroundColor = [UIColor clearColor];
    [self addSubview:directImage];
    
    // 身份证号
    UITextField *idNumberF = [[UITextField alloc] init];
    idNumberF.font = [UIFont systemFontOfSize:13];
    idNumberF.backgroundColor = [UIColor whiteColor];
    idNumberF.clearButtonMode = UITextFieldViewModeAlways;
    idNumberF.secureTextEntry = YES;
    [self addSubview:idNumberF];
    self.idNumberF = idNumberF;
    
    // 保存
    UIButton *saveBtn = [[UIButton alloc] init];
    [saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    saveBtn.backgroundColor = [UIColor redColor];
    [saveBtn addTarget:self action:@selector(saveBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:saveBtn];
    _saveBtn = saveBtn;
    
    //    [promptBtn mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.top.equalTo(self.mas_top).offset(5);
    //        make.left.equalTo(self.mas_left).offset(10);
    //        make.width.equalTo(@(kScreen_Width - 20));
    //        make.height.equalTo(@23);
    //    }];
    
    CGFloat bgImageH = 0;
    if (self.frame.size.height > 80) {
        bgImageH = -60;
        idNumberF.placeholder = @"因涉及入境,请填写收货人身份证号";
        [saveBtn setTitle:@"保存" forState:UIControlStateNormal];
    }else {
        bgImageH = -2;
        idNumberF.placeholder = @"";
        [saveBtn setTitle:@"" forState:UIControlStateNormal];
    }
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.left.equalTo(self.mas_left).offset(0);
        make.width.equalTo(@(kScreen_Width));
        make.height.equalTo(@(3));
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(10);
        make.left.equalTo(self.mas_left).offset(35);
        make.height.equalTo(@30);
        make.width.equalTo(@120);
    }];
    
    [phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(nameLabel.mas_top).offset(0);
        make.left.equalTo(nameLabel.mas_right).offset(0);
        make.height.equalTo(nameLabel.mas_height);
        make.right.equalTo(self.mas_right).offset(-10);
    }];
    
    [addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(nameLabel.mas_bottom).offset(0);
        make.left.equalTo(self.mas_left).offset(35);
        make.right.equalTo(self.mas_right).offset(-30);
    }];
    
    [directImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(addressLabel.mas_centerY);
        make.left.equalTo(addressLabel.mas_right).offset(0);
        make.right.equalTo(self.mas_right).offset(-5);
        make.height.equalTo(addressLabel.mas_height);
    }];
    
    [idNumberF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_bottom).offset(5);
        make.bottom.equalTo(self.mas_bottom).offset(-5);
        make.left.equalTo(self.mas_left).offset(5);
        make.right.equalTo(self.mas_right).offset(-80);
    }];
    
    [saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(idNumberF.mas_top).offset(0);
        make.left.equalTo(idNumberF.mas_right).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.height.equalTo(idNumberF.mas_height);
    }];
}

#pragma mark -无收货地址
- (void)NoAddress
{
    UIView *bgView = [[UIView alloc] init];
    [self addSubview:bgView];
    self.bgView = bgView;
    
    UILabel *promptLabel = [[UILabel alloc] init];
    promptLabel.text = @"无地址信息，点击添加收货地址";
    promptLabel.textColor = [UIColor lightGrayColor];
    promptLabel.font = [UIFont systemFontOfSize:16];
    [bgView addSubview:promptLabel];
    
    // 指引图标
    UIImageView *directImage = [[UIImageView alloc] init];
    directImage.image = [UIImage imageNamed:@"icon_rightarrow03.png"];
    [bgView addSubview:directImage];
    
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
    }];
    
    [promptLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bgView.mas_centerY);
        make.left.equalTo(bgView.mas_left).offset(10);
        make.width.equalTo(@(kScreen_Width - 50));
        make.height.equalTo(@30);
    }];
    
    [directImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.mas_right);
        make.height.equalTo(@25);
        make.width.equalTo(@25);
    }];
}

- (void)saveBtnClick
{
    BOOL isLegitimacy = ([ManyOrderHeaderView verifyIDCardNumber:_idNumberF.text] == YES);
    if (self.saveBtnBlock) {
        self.saveBtnBlock(isLegitimacy, _saveBtn);
    }
}

/**
 *  身份证号全校验
 */
+ (BOOL)verifyIDCardNumber:(NSString *)IDCardNumber
{
    IDCardNumber = [IDCardNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([IDCardNumber length] != 18)
    {
        return NO;
    }
    NSString *mmdd = @"(((0[13578]|1[02])(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)(0[1-9]|[12][0-9]|30))|(02(0[1-9]|[1][0-9]|2[0-8])))";
    NSString *leapMmdd = @"0229";
    NSString *year = @"(19|20)[0-9]{2}";
    NSString *leapYear = @"(19|20)(0[48]|[2468][048]|[13579][26])";
    NSString *yearMmdd = [NSString stringWithFormat:@"%@%@", year, mmdd];
    NSString *leapyearMmdd = [NSString stringWithFormat:@"%@%@", leapYear, leapMmdd];
    NSString *yyyyMmdd = [NSString stringWithFormat:@"((%@)|(%@)|(%@))", yearMmdd, leapyearMmdd, @"20000229"];
    NSString *area = @"(1[1-5]|2[1-3]|3[1-7]|4[1-6]|5[0-4]|6[1-5]|82|[7-9]1)[0-9]{4}";
    NSString *regex = [NSString stringWithFormat:@"%@%@%@", area, yyyyMmdd  , @"[0-9]{3}[0-9Xx]"];
    
    NSPredicate *regexTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    if (![regexTest evaluateWithObject:IDCardNumber])
    {
        return NO;
    }
    int summary = ([IDCardNumber substringWithRange:NSMakeRange(0,1)].intValue + [IDCardNumber substringWithRange:NSMakeRange(10,1)].intValue) *7
    + ([IDCardNumber substringWithRange:NSMakeRange(1,1)].intValue + [IDCardNumber substringWithRange:NSMakeRange(11,1)].intValue) *9
    + ([IDCardNumber substringWithRange:NSMakeRange(2,1)].intValue + [IDCardNumber substringWithRange:NSMakeRange(12,1)].intValue) *10
    + ([IDCardNumber substringWithRange:NSMakeRange(3,1)].intValue + [IDCardNumber substringWithRange:NSMakeRange(13,1)].intValue) *5
    + ([IDCardNumber substringWithRange:NSMakeRange(4,1)].intValue + [IDCardNumber substringWithRange:NSMakeRange(14,1)].intValue) *8
    + ([IDCardNumber substringWithRange:NSMakeRange(5,1)].intValue + [IDCardNumber substringWithRange:NSMakeRange(15,1)].intValue) *4
    + ([IDCardNumber substringWithRange:NSMakeRange(6,1)].intValue + [IDCardNumber substringWithRange:NSMakeRange(16,1)].intValue) *2
    + [IDCardNumber substringWithRange:NSMakeRange(7,1)].intValue *1 + [IDCardNumber substringWithRange:NSMakeRange(8,1)].intValue *6
    + [IDCardNumber substringWithRange:NSMakeRange(9,1)].intValue *3;
    NSInteger remainder = summary % 11;
    NSString *checkBit = @"";
    NSString *checkString = @"10X98765432";
    checkBit = [checkString substringWithRange:NSMakeRange(remainder,1)];// 判断校验位
    return [checkBit isEqualToString:[[IDCardNumber substringWithRange:NSMakeRange(17,1)] uppercaseString]];
}

//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
//{
//    if (self.addressSelect) {
//        self.addressSelect();
//    }
//}

@end

