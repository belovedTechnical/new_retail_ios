//
//  ManyOrderConfirmCell.h
//  BelovedHotel
//
//  Created by BDSir on 2017/8/18.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OrderCellModel;
@interface ManyOrderConfirmCell : UITableViewCell

@property(nonatomic, strong) OrderCellModel *model;

@end
