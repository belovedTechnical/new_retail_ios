//
//  ManyOrderHeaderView.h
//  BelovedHotel
//
//  Created by BDSir on 2017/8/18.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>


@class AddressListModel;
@interface ManyOrderHeaderView : UIView

@property (nonatomic, strong) AddressListModel *model;
@property (nonatomic, assign) BOOL isGlobal;
@property (nonatomic, strong) UITextField *idNumberF;
@property (nonatomic, strong) UIButton *saveBtn;
@property (nonatomic, strong) UIImageView *bgImageView;

@property (nonatomic, copy) void (^addressSelect)(void);
@property (nonatomic, copy) void(^saveBtnBlock)(BOOL isLegitimacy, UIButton *sendBtn);

@end
