//
//  ManyTableHeaderTitleView.h
//  BelovedHotel
//
//  Created by BDSir on 2017/8/21.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManyTableHeaderTitleView : UITableViewHeaderFooterView

@property(nonatomic, strong) UILabel *nameLabel;

@end
