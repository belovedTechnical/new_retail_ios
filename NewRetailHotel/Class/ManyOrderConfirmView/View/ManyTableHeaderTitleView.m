//
//  ManyTableHeaderTitleView.m
//  BelovedHotel
//
//  Created by BDSir on 2017/8/21.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "ManyTableHeaderTitleView.h"

@implementation ManyTableHeaderTitleView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    // 店铺图片
    UIImageView *iconView = [[UIImageView alloc] init];
    iconView.image = [UIImage imageNamed:@"icon_shop.png"];
    [self addSubview:iconView];
    
    // 店铺名称
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.font = [UIFont systemFontOfSize:16];
    [self addSubview:nameLabel];
    self.nameLabel = nameLabel;
    
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left).offset(20);
        make.width.equalTo(@(25));
        make.height.equalTo(@(25));
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.left.equalTo(iconView.mas_right).offset(5);
        make.right.equalTo(self.mas_right).offset(0);
    }];
}

@end
