//
//  ManyOrderFooterView.m
//  BelovedHotel
//
//  Created by BDSir on 2017/8/18.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "ManyOrderFooterView.h"

@interface ManyOrderFooterView()

/** 发票View */
@property(nonatomic, strong) UIView *selecedInvoiceView;
/** 单位名称view */
@property(nonatomic, strong) UIView *riseView;
@property (nonatomic, strong) UIView *identifierView;
@property(nonatomic, strong) UIButton *selectBnt;

@end
@implementation ManyOrderFooterView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, 1)];
    [self addSubview:lineView];
    
    // 留言
    UIView *leaveView = [self foundContainerWithView:lineView];
    UILabel *leaveLabel = [self foundTitleLabelWithTop:leaveView];
    leaveLabel.text = @"留言";
    UITextField *leaveText = [[UITextField alloc] init];
    leaveText.placeholder = @"选填，30个字以内";
    leaveText.font = [UIFont systemFontOfSize:15];
    leaveText.clearButtonMode = UITextFieldViewModeWhileEditing;
    [leaveView addSubview:leaveText];
    self.leaveText = leaveText;
    
    // 发票
    UIView *invoiceView = [self foundContainerWithView:leaveView];
    UILabel *invoiceLabel = [self foundTitleLabelWithTop:invoiceView];
    invoiceLabel.text = @"发票";
    UIButton *invoiceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [invoiceBtn setImage:[UIImage imageNamed:@"switch_off.png"] forState:UIControlStateNormal];
    [invoiceBtn setImage:[UIImage imageNamed:@"switch_on.png"] forState:UIControlStateSelected];
    [invoiceBtn addTarget:self action:@selector(invoiceBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [invoiceView addSubview:invoiceBtn];
    self.invoiceBtn = invoiceBtn;
    
    // 发票选择View
    UIView *selecedInvoiceView = [self foundContainerWithView:invoiceView];
    selecedInvoiceView.hidden = YES;
    self.selecedInvoiceView = selecedInvoiceView;
    
    // 个人
    UIButton *individualBtn = [self foundInvoiceWithTop:selecedInvoiceView title:@" 个人" left:20];
    self.individualBtn = individualBtn;
    [individualBtn addTarget:self action:@selector(selectInvoiceClick:) forControlEvents:UIControlEventTouchUpInside];
    // 单位
    UIButton *unitsBtn = [self foundInvoiceWithTop:selecedInvoiceView title:@" 单位" left:130];
    self.unitsBtn = unitsBtn;
    [unitsBtn addTarget:self action:@selector(selectInvoiceClick:) forControlEvents:UIControlEventTouchUpInside];
    // 发票抬头
    UIView *riseView = [self foundContainerWithView:selecedInvoiceView];
    riseView.hidden = YES;
    self.riseView = riseView;
    UILabel *nameLabel = [self foundTitleLabelWithTop:riseView];
    nameLabel.text = @"发票抬头";
    UITextField *nameText = [[UITextField alloc] init];
    nameText.placeholder = @"请输入单位名称";
    nameText.font = [UIFont systemFontOfSize:15];
    nameText.clearButtonMode = UITextFieldViewModeWhileEditing;
    [riseView addSubview:nameText];
    self.nameText = nameText;
    // 纳税人识别号
    UIView *identifierView = [self foundContainerWithView:riseView];
    identifierView.hidden = YES;
    self.identifierView = identifierView;
    UILabel *identifierTitle = [self foundTitleLabelWithTop:identifierView];
    identifierTitle.text = @"识别号";
    UITextField *identifierTF = [[UITextField alloc] init];
    identifierTF.placeholder = @"请输入纳税人识别号";
    identifierTF.font = [UIFont systemFontOfSize:15];
    identifierTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    [identifierView addSubview:identifierTF];
    self.identifierTF = identifierTF;
    
    // 商品总额
    
    // 运费
    
    [leaveText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(leaveView.mas_top).offset(0);
        make.bottom.equalTo(leaveView.mas_bottom).offset(0);
        make.left.equalTo(leaveLabel.mas_right).offset(0);
        make.right.equalTo(leaveView.mas_right).offset(0);
    }];
    
    [invoiceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(invoiceView.mas_top).offset(0);
        make.bottom.equalTo(invoiceView.mas_bottom).offset(0);
        make.right.equalTo(invoiceView.mas_right).offset(-20);
        make.width.equalTo(@80);
    }];
    
    [nameText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(riseView.mas_top).offset(0);
        make.bottom.equalTo(riseView.mas_bottom).offset(0);
        make.left.equalTo(nameLabel.mas_right).offset(0);
        make.right.equalTo(riseView.mas_right).offset(0);
    }];
    
    [identifierTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(identifierView.mas_top).offset(0);
        make.bottom.equalTo(identifierView.mas_bottom).offset(0);
        make.left.equalTo(identifierTitle.mas_right).offset(0);
        make.right.equalTo(identifierView.mas_right).offset(0);
    }];
}

// 创建View
- (UIView *)foundContainerWithView:(UIView *)view
{
    UIView *containerView = [[UIView alloc] init];
    containerView.backgroundColor = [UIColor whiteColor];
    [self addSubview:containerView];
    
    [containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(view.mas_bottom).offset(5);
        make.left.equalTo(self.mas_left).offset(0);
        make.width.equalTo(@(kScreen_Width));
        make.height.equalTo(@50);
    }];
    
    return containerView;
}

// 创建标题Label
- (UILabel *)foundTitleLabelWithTop:(UIView *)topY
{
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfSize:15];
    titleLabel.textColor = [UIColor darkGrayColor];
    [topY addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topY.mas_top).offset(0);
        make.bottom.equalTo(topY.mas_bottom).offset(0);
        make.left.equalTo(topY.mas_left).offset(20);
        make.width.equalTo(@70);
    }];
    
    return titleLabel;
}

// 创建发票Butto
- (UIButton *)foundInvoiceWithTop:(UIView *)topY title:(NSString *)title left:(CGFloat)left
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"icon_select.png"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"icon_select_selected.png"] forState:UIControlStateSelected];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:15];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [topY addSubview:button];
    
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topY.mas_top).offset(0);
        make.bottom.equalTo(topY.mas_bottom).offset(0);
        make.left.equalTo(topY.mas_left).offset(left);
        make.width.equalTo(@80);
    }];
    
    return button;
}


#pragma mark -内部控制方法
// 选择发票
- (void)invoiceBtnClick:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if (sender.selected == YES) {
        self.selecedInvoiceView.hidden = NO;
        if (self.unitsBtn.selected == NO) {
            self.individualBtn.selected = YES;
        }
    }else {
        self.selecedInvoiceView.hidden = YES;
        self.riseView.hidden = YES;
        self.identifierView.hidden = YES;
    }
    
}

// 个人 单位
- (void)selectInvoiceClick:(UIButton *)sender
{
    self.individualBtn.selected = NO;
    _selectBnt.selected = NO;
    sender.selected = YES;
    _selectBnt = sender;
    if ([sender.titleLabel.text  isEqual: @" 单位"]) {
        self.riseView.hidden = NO;
        self.identifierView.hidden = NO;
    }else {
        self.riseView.hidden = YES;
        self.identifierView.hidden = YES;
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
}



























@end

