//
//  ManyTableFooterView.m
//  BelovedHotel
//
//  Created by BDSir on 2017/8/22.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "ManyTableFooterView.h"
#import "RightBaseButton.h"

@interface ManyTableFooterView()

@end
@implementation ManyTableFooterView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    UILabel *ToplineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, 2)];
    ToplineLabel.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:ToplineLabel];
    
    UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, kScreen_Width, 1)];
    lineLabel.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:lineLabel];
    
    // 优惠券
    UILabel *couponLabel = [self foundTitleLabelWithTop:ToplineLabel];
    couponLabel.text = @"优惠劵:";
    
    // 是否有可用优惠劵
    RightBaseButton *couponBtn = [RightBaseButton buttonWithType:UIButtonTypeCustom];
    [couponBtn setTitle:@"无可用" forState:UIControlStateNormal];
    [couponBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    couponBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    couponBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [couponBtn setImage:[UIImage imageNamed:@"icon_rightarrow02.png"] forState:UIControlStateNormal];
    [couponBtn addTarget:self action:@selector(couponBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:couponBtn];
    self.couponBtn = couponBtn;
    
    // 运费
    UILabel *freightLabel = [self foundTitleLabelWithTop:lineLabel];
    freightLabel.text = @"运费:";
    
    // 店铺优惠
    UILabel *DiscountLabel = [self foundTitleLabelWithTop:freightLabel];
    DiscountLabel.text = @"优惠:";
    
    // 小计
    UILabel *subtotalLabel = [self foundTitleLabelWithTop:DiscountLabel];
    subtotalLabel.text = @"小计:";
    
    // 运费价格
    UILabel *sumFreightLabel = [self foundPriceLabelWithTop:lineLabel];
    sumFreightLabel.text = @"+¥0.00";
    self.sumFreightLabel = sumFreightLabel;
    
    // 店铺优惠金额
    UILabel *sumDiscountLabel = [self foundPriceLabelWithTop:sumFreightLabel];
    sumDiscountLabel.text = @"-¥0.00";
    self.sumDiscountLabel = sumDiscountLabel;
    
    // 小计金额
    UILabel *sumSubtotalLabel = [self foundPriceLabelWithTop:sumDiscountLabel];
    sumSubtotalLabel.text = @"¥0.00";
    self.sumSubtotalLabel = sumSubtotalLabel;
    
    [couponBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(couponLabel.mas_right).offset(5);
        make.top.equalTo(couponLabel.mas_top).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.height.equalTo(couponLabel.mas_height);
    }];
}

// 创建标题Label
- (UILabel *)foundTitleLabelWithTop:(UILabel *)topY
{
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfSize:15];
    titleLabel.textColor = [UIColor colorWithHexString:@"#333333"];
    [self addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topY.mas_bottom).offset(5);
        make.left.equalTo(self.mas_left).offset(15);
        make.height.equalTo(@23);
        make.width.equalTo(@100);
    }];
    
    return titleLabel;
}

// 创建价格Label
- (UILabel *)foundPriceLabelWithTop:(UILabel *)topY
{
    UILabel *priceLabel = [[UILabel alloc] init];
    priceLabel.font = [UIFont systemFontOfSize:13];
    priceLabel.textColor = [UIColor colorWithHexString:@"#b4282d"];
    priceLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:priceLabel];
    
    [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topY.mas_bottom).offset(5);
        make.right.equalTo(self.mas_right).offset(-10);
        make.height.equalTo(@23);
        make.width.equalTo(@100);
    }];
    
    return priceLabel;
}

#pragma mark -内部控制方法
- (void)couponBtnClick
{
    if (self.couponButtonClick) {
        self.couponButtonClick();
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (self.tableFoorerViewTouchesBegan) {
        self.tableFoorerViewTouchesBegan();
    }
}







@end

