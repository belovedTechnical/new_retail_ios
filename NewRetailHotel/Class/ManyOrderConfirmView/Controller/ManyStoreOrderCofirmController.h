//
//  ManyStoreOrderCofirmController.h
//  BelovedHotel
//
//  Created by BDSir on 2017/8/18.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManyStoreOrderCofirmController : UIViewController

@property (nonatomic, copy) NSString *productId;
@property (nonatomic, copy) NSMutableArray *storeIDArray;

@end
