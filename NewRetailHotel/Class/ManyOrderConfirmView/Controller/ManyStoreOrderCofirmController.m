//
//  ManyStoreOrderCofirmController.m
//  BelovedHotel
//
//  Created by BDSir on 2017/8/18.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "ManyStoreOrderCofirmController.h"
#import "BDNetworkTools.h"
#import "ManyOrderHeaderView.h"
#import "ManyOrderFooterView.h"
#import "ManyOrderConfirmCell.h"
#import "VerifyOrderformModel.h"
#import "StoreOrderModel.h"
#import "ManyTableHeaderTitleView.h"
#import "ManyTableFooterView.h"
#import "AddressListModel.h"
#import "AddressListCell.h"
#import "PayOderViewController.h"
#import "AddressManageViewController.h"
#import "CouponViewController.h"
#import "CouponModel.h"

@interface ManyStoreOrderCofirmController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, OrderCofirmViewControllerDelegate>

@property (nonatomic, strong) NSString *apikey;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UIButton *referButton;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *adressArr;
@property (nonatomic, strong) VerifyOrderformModel *verifyOrderModel;
@property (nonatomic, strong) ManyOrderHeaderView *headerView;
@property (nonatomic, strong) ManyOrderFooterView *footerView;
@property (nonatomic, strong) AddressListModel *model;
@property (nonatomic, strong) NSDictionary *freightMoneyDict;
/** 优惠劵model数组 */
@property (nonatomic, strong) NSArray *couponModelArray;
/** 不可用优惠券数组 */
@property (nonatomic, strong) NSArray *notUsedCouponModelArray;
/** 优惠券ID */
@property (nonatomic, strong) NSString *userCouponId;
/** 优惠券名字 */
@property (nonatomic, strong) NSString *couponName;
/** 优惠券金额 */
@property (nonatomic, strong) NSString *couponAmount;
/** 商品总价 */
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) UIButton *saveBtn;

@end

static NSString * const manyCellID = @"manyCellID";
static NSString * const manyHeaderID = @"manyHeaderID";
static NSString * const manyFooterID = @"manyFooterID";
static NSString * const headerTitleView = @"headerTitleView";
static NSString * const tableFooterView = @"tableFooterView";
@implementation ManyStoreOrderCofirmController

- (void)viewWillAppear:(BOOL)animated
{   [super viewWillAppear:animated];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.tabBarController.tabBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"多店铺订单确认";
    self.tabBarController.tabBar.hidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self requestCheckShopInfoWithProductIdList:_productId];
    [self setupBottomView];
    [self requestAdressList];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationCouponID:) name:kbackCouponIDNoti object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noBackCouponIDNotiFunc) name:kNoBackCouponIDNoti object:nil];
}

#pragma mark -couponId通知方法
- (void)notificationCouponID:(NSNotification *)noti
{
    NSString *userCouponId = noti.userInfo[@"userCouponId"];
    NSString *couponAmount = noti.userInfo[@"amount"];
    NSString *couponName = noti.userInfo[@"name"];
    self.userCouponId = userCouponId;
    self.couponAmount = couponAmount;
    self.couponName = couponName;
    [self.tableView reloadData];
}

// 不使用优惠劵
- (void)noBackCouponIDNotiFunc
{
    _userCouponId = nil;
    _couponName = nil;
    [self.tableView reloadData];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -界面
- (void)setupMainUI
{
    BDWeakSelf();
    UITableView *tableView = [[UITableView alloc] init];
    tableView.dataSource = self;
    tableView.delegate = self;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    self.tableView.rowHeight = 100;
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, -50, 0);
    
    CGFloat headerH = 0;
    if ([_storeIDArray containsObject:URL_GlobalPurchaseID]) {
        headerH = 120;
    }else {
        headerH = 80;
    }
    ManyOrderHeaderView *headerView = [[ManyOrderHeaderView alloc] initWithFrame:CGRectMake(0, 0, 0, headerH)];
    headerView.model = _adressArr[0];
    _model = _adressArr[0];
    tableView.tableHeaderView = headerView;
    self.headerView = headerView;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addressViewClick)];
    [headerView.bgImageView addGestureRecognizer:tapGesture];
    
    __weak typeof(headerView) __weakHeaderView = headerView;
    headerView.saveBtnBlock = ^(BOOL isLegitimacy, UIButton *sender) {
        if (isLegitimacy == YES) {
            [self.view endEditing:YES];
            sender.backgroundColor = [UIColor darkGrayColor];
            sender.userInteractionEnabled = NO;
            [self.saveBtn setTitle:@"已认证" forState:UIControlStateNormal];
            __weakHeaderView.idNumberF.enabled = NO;
            _saveBtn = sender;
            [self requestNetAddressIDNumber];
        }else {
            [self showAlertWithTitle:@"提示" message:@"证件号码格式不正确,请检查后重新输入" AscertainHandler:nil];
        }
    };
    
    ManyOrderFooterView *footerView = [[ManyOrderFooterView alloc] initWithFrame:CGRectMake(0, 0, 0, 280)];
    footerView.leaveText.delegate = self;
    footerView.nameText.delegate = self;
    footerView.identifierTF.delegate = self;
    tableView.tableFooterView = footerView;
    self.footerView = footerView;
    
    [tableView registerClass:[ManyOrderConfirmCell class] forCellReuseIdentifier:manyCellID];
    [tableView registerClass:[ManyTableHeaderTitleView class] forHeaderFooterViewReuseIdentifier:headerTitleView];
    [tableView registerClass:[ManyTableFooterView class] forHeaderFooterViewReuseIdentifier:tableFooterView];
    
    headerView.addressSelect = ^{
        AddressManageViewController *addressVC = [[AddressManageViewController alloc]init];
        addressVC.defaultDelegate = weakSelf;
        addressVC.setDefault = @"Y";// 去设置默认地址
        [self.navigationController pushViewController:addressVC animated:YES];
    };
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(65);
        make.bottom.equalTo(self.view.mas_bottom).offset(-50);
        make.left.equalTo(self.view.mas_left).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
    }];
}

- (void)addressViewClick
{
    AddressManageViewController *addressVC = [[AddressManageViewController alloc]init];
    addressVC.defaultDelegate = self;
    addressVC.setDefault = @"Y";// 去设置默认地址
    [self.navigationController pushViewController:addressVC animated:YES];
}

#pragma mark -UITableViewDataSource && UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _verifyOrderModel.storeOrderList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    StoreOrderModel *store = _verifyOrderModel.storeOrderList[section];
    return store.productList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ManyOrderConfirmCell *cell = [tableView dequeueReusableCellWithIdentifier:manyCellID];
    if (cell == nil) {
        cell = [[ManyOrderConfirmCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:manyCellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    StoreOrderModel *storeModel = _verifyOrderModel.storeOrderList[indexPath.section];
    cell.model = storeModel.productList[indexPath.row];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ManyTableHeaderTitleView *titleView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:headerTitleView];
    StoreOrderModel *storeModel = _verifyOrderModel.storeOrderList[section];
    titleView.nameLabel.text = storeModel.storeName;

    return titleView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    ManyTableFooterView *footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:tableFooterView];
    StoreOrderModel *storeModel = _verifyOrderModel.storeOrderList[section];
    
    NSString *freightMoney = [_freightMoneyDict objectForKey:storeModel.productStoreId];
    NSString *totalPrice = [NSString stringWithFormat:@"¥%@", storeModel.totalPrice];
    NSString *yTotalPrice = [NSString stringWithFormat:@"-¥%@", storeModel.yTotalPrice];
    NSString *freightMoneys = [NSString stringWithFormat:@"+¥%@.00", freightMoney];
    
    footerView.couponBtn.userInteractionEnabled = NO;
    [footerView.couponBtn setTitle:@"无可用" forState:UIControlStateNormal];
    footerView.sumSubtotalLabel.text = [self stringContainsDecimalWithStr:totalPrice];
    footerView.sumDiscountLabel.text = storeModel.yTotalPrice == nil ? @"-¥0.00" : [self stringContainsDecimalWithStr:yTotalPrice];
    footerView.sumFreightLabel.text = freightMoney == nil ? @"+¥0.00" : [self stringContainsDecimalWithStr:freightMoneys];
    
    if (_couponModelArray.count > section) {
        CouponModel *couponItem = _couponModelArray[section];
        CGFloat couponPrice = [couponItem.amount floatValue];
        if (_couponModelArray.count > 0 && ([[self stringContainsDecimalWithStr:totalPrice] floatValue] < couponPrice)) {
            if (_couponName != nil) {
            [footerView.couponBtn setTitle:[NSString stringWithFormat:@"优惠券抵扣%.2f元", couponPrice] forState:UIControlStateNormal];
            CGFloat priceF = ([_price floatValue] - couponPrice) < 0.00 ? 0.00 : ([_price floatValue] - couponPrice);
            self.priceLabel.attributedText = [self setLabelString:[NSString stringWithFormat:@"%.2f", priceF]];
            }else {
                [footerView.couponBtn setTitle:[NSString stringWithFormat:@"%lu张优惠劵", (unsigned long)_couponModelArray.count] forState:UIControlStateNormal];
            [footerView.couponBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
            self.priceLabel.attributedText = [self setLabelString:[NSString stringWithFormat:@"%.2f", [_price floatValue]]];
            }
            footerView.couponBtn.userInteractionEnabled = YES;
        }
    }
    
    footerView.tableFoorerViewTouchesBegan = ^{
        [self.view endEditing:YES];
    };
    
    BDWeakSelf();
    #pragma mark -优惠券点击
    footerView.couponButtonClick = ^{
        CouponViewController *couponVC = [[CouponViewController alloc] init];
        couponVC.isMeController = NO;
        couponVC.couponModelArray = weakSelf.couponModelArray;
        couponVC.notUsedCouponModelArray = weakSelf.notUsedCouponModelArray;
        [self.navigationController pushViewController:couponVC animated:YES];
    };

    return footerView;
}

- (NSString *)stringContainsDecimalWithStr:(NSString *)str
{
    NSString *string = @"";
    if ([str containsString:@"."]) {
        string = str;
    }else {
        string = [NSString stringWithFormat:@"%@.00", str];
    }
    return string;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 160;
}

#pragma mark -OrderCofirmViewControllerDelegate
// 没有默认地址的情况下，设置默认地址后跳转到此界面 加载界面 显示地址
- (void)updateDefault{// 代理协议
    // 默认地址
    [self requestAdressList];
}
#pragma mark -解决header与footer停留问题
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat sectionHeaderHeight = 30;
    CGFloat sectionFooterHeight = 160;
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY >= 0 && offsetY <= sectionHeaderHeight)
    {
        scrollView.contentInset = UIEdgeInsetsMake(-offsetY, 0, -sectionFooterHeight, 0);
    }else if (offsetY >= sectionHeaderHeight && offsetY <= scrollView.contentSize.height - scrollView.frame.size.height - sectionFooterHeight)
    {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, -sectionFooterHeight, 0);
    }else if (offsetY >= scrollView.contentSize.height - scrollView.frame.size.height - sectionFooterHeight && offsetY <= scrollView.contentSize.height - scrollView.frame.size.height)
    {
        scrollView.contentInset = UIEdgeInsetsMake(-offsetY, 0, -(scrollView.contentSize.height - scrollView.frame.size.height - sectionFooterHeight), 0);
    }
}

#pragma mark -底部提交订单View
- (void)setupBottomView
{
    // 底部容器View
    UIView *bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomView];
    
    // 价格
    UILabel *priceLabel = [[UILabel alloc] init];
    priceLabel.textColor = [UIColor redColor];
    priceLabel.font = [UIFont systemFontOfSize:15];
    [bottomView addSubview:priceLabel];
    self.priceLabel = priceLabel;
    priceLabel.attributedText = [self setLabelString:@"0.00"];
    
    // 提交按钮
    UIButton *referButton = [[UIButton alloc] init];
    [referButton setTitle:@"支付订单" forState:UIControlStateNormal];
    [referButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [referButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    referButton.backgroundColor = [UIColor redColor];
    [referButton addTarget:self action:@selector(referButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:referButton];
    self.referButton = referButton;
 
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view.mas_bottom).offset(0);
        make.left.equalTo(self.view.mas_left).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.height.equalTo(@50);
    }];
    
    [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bottomView.mas_top).offset(0);
        make.bottom.equalTo(bottomView.mas_bottom).offset(0);
        make.left.equalTo(bottomView.mas_left).offset(20);
        make.width.equalTo(@160);
    }];
    
    [referButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bottomView.mas_top).offset(0);
        make.bottom.equalTo(bottomView.mas_bottom).offset(0);
        make.right.equalTo(bottomView.mas_right).offset(0);
        make.width.equalTo(@100);
    }];
}

#pragma mark -内部控制方法
#pragma mark -Label富文本属性
- (NSMutableAttributedString *)setLabelString:(NSString *)string
{
    NSString *text = [NSString stringWithFormat:@"合计:¥%@", string];
    NSMutableAttributedString *attrLabel = [[NSMutableAttributedString alloc] initWithString:text];
    NSRange range = [text rangeOfString:@"合计:"];
    [attrLabel addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
    [attrLabel addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:range];
    return attrLabel;
}

- (void)referButtonClick
{   self.referButton.userInteractionEnabled = NO;
    self.referButton.backgroundColor = [UIColor lightGrayColor];
    
    if ([_storeIDArray containsObject:URL_GlobalPurchaseID]) {
        if (_headerView.saveBtn.userInteractionEnabled == NO) {
            [self commitOrderNetworkRequest];
        }else {
//            [self alertShowWithMessage:@"收件人与与身份证号不一致" AscertainBlock:nil];
        }
    }else {
        [self commitOrderNetworkRequest];
    }
}

#pragma mark -网络请求
#pragma mark -多店铺产品价格计算
- (void)requestCheckShopInfoWithProductIdList:(NSString *)productIdList
{
    NSDictionary *paramDic = @{@"productIdList":productIdList};
    [[BDNetworkTools sharedInstance] postOrderManyPriceCountWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        NSDictionary *resDict = [responseObject objectForKey:@"order"];
        NSString *price = [resDict objectForKey:@"zTotalPrice"];
        self.price = price;
        CGFloat pricefloat = [price floatValue];
        self.priceLabel.attributedText = [self setLabelString:[NSString stringWithFormat:@"%.2f", pricefloat]];
        self.verifyOrderModel = [VerifyOrderformModel verifyOrderformModelWithDict:resDict];

        [self couponRequestNetwork];
        [self.tableView reloadData];
    }];
}

#pragma -请求地址列表判断是否有默认地址
- (void)requestAdressList{
    [self.adressArr removeAllObjects];
    [[BDNetworkTools sharedInstance] getAddressRedactWithBlock:^(NSDictionary *responseObject, NSError *error) {
        NSArray *aar = [responseObject objectForKey:@"postalAddressList"];
        for (NSDictionary *dic in aar) {
            AddressListModel *model = [[AddressListModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            self.model = model;
            // 只把默认地址加到数组里
            if ([model.isDefault isEqualToString:@"yes"] ) {
                [self.adressArr removeAllObjects];
                [self.adressArr addObject:model];
            }else {
                if (model != nil) {
                    [self.adressArr addObject:model];
                }
            }
        }
        [self setupMainUI];
        // 请求运费
        [self manyStoreFreightMoney];
    }];
}

#pragma mark -提交订单
- (void)commitOrderNetworkRequest
{
    // 发票信息
    static NSString *invoiceInfo = @"";
    if (_footerView.invoiceBtn.selected == YES) {
        _footerView.individualBtn.selected == YES ? invoiceInfo = @"个人" : (invoiceInfo = _footerView.nameText.text);
    }else {
        invoiceInfo = @"不需要发票";
    }
    NSString *taxpayerNum = _footerView.identifierTF.text;
    NSString *counponID = _userCouponId == nil ? @"" : _userCouponId;
    NSDictionary *paramDic = @{@"contactMechId": _model.contactMechId,
                               @"productIdList":_productId,
                               @"remark":_footerView.leaveText.text,
                               @"invoice": invoiceInfo,
                               @"userCouponId": counponID,
                               @"taxpayerNum":taxpayerNum
                               };
    [[BDNetworkTools sharedInstance] postOrderManyStoreMakeWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        if (error != nil) {
            self.referButton.userInteractionEnabled = YES;
            self.referButton.backgroundColor = [UIColor redColor];
            NSString *orderId = [responseObject objectForKey:@"orderId"];
            PayOderViewController *payVC = [[PayOderViewController alloc] init];
            payVC.orderId = orderId;
            payVC.isManyStore = YES;
            [self.navigationController pushViewController:payVC animated:YES];
        }else {
            self.referButton.userInteractionEnabled = YES;
            self.referButton.backgroundColor = [UIColor redColor];
            NSString *prompt = [NSString stringWithFormat:@"%@", error];
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示"
                                                                             message:prompt
                                                                      preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                           }];
            
            [alertVC addAction:action];
            [self presentViewController:alertVC animated:YES completion:nil];
        }
    }];
}

#pragma mark -多店铺获取总运费
- (void)manyStoreFreightMoney
{
    NSDictionary *paramDic = @{@"contactMechId":_model.contactMechId,
                               @"productIdList":_productId
                               };
    [[BDNetworkTools sharedInstance] postOrderManyPriceFreightWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        NSDictionary *freightList = [responseObject objectForKey:@"freightList"];
        //        NSString *totalFreight = [freightList objectForKey:@"totalFreight"];// 订单总运费
        NSDictionary *freightMoneyDict = [freightList objectForKey:@"storeFreightMap"];
        self.freightMoneyDict = freightMoneyDict;
        [self.tableView reloadData];
    }];
}

#pragma mark -多店铺优惠券接口
// 可以使用
- (void)couponRequestNetwork
{
    NSDictionary *paramDic = @{@"pageIndex":@"0",
                               @"pageSize":@"99",
                               @"productIdList":_productId,
                               @"amount":_price,
                               @"type":@"available"
                               };
    [[BDNetworkTools sharedInstance] postOrderManyCouponWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        NSMutableArray *tempArr = [NSMutableArray array];
        NSArray *dataArray = [responseObject objectForKey:@"data"];
        for (NSDictionary *dict in dataArray) {
            [tempArr addObject:[CouponModel couponModelWithDict:dict]];
        }
        self.couponModelArray = tempArr;
        [self notToBeUsedCouponRequestNetwork];
    }];
}

// 不可使用
- (void)notToBeUsedCouponRequestNetwork
{
    NSDictionary *paramDic = @{@"pageIndex":@"0",
                               @"pageSize":@"99",
                               @"productIdList":_productId,
                               @"amount":_price,
                               @"type":@"unavailable"
                               };
    [[BDNetworkTools sharedInstance] postOrderManyCouponWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        NSMutableArray *tempArr = [NSMutableArray array];
        NSArray *dataArray = [responseObject objectForKey:@"data"];
        for (NSDictionary *dict in dataArray) {
            [tempArr addObject:[CouponModel couponModelWithDict:dict]];
        }
        self.notUsedCouponModelArray = tempArr;
        [self.tableView reloadData];
    }];
}

- (void)requestNetAddressIDNumber
{
    NSDictionary *paramDic = @{@"contactMechId":_model.contactMechId,
                               @"oldAttnName":[NSString stringWithFormat:@"%@",_model.attnName],
                               @"toVerifyIdCard":_headerView.idNumberF.text
                               };
    [[BDNetworkTools sharedInstance] postIdentityCardCheckingWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {

        if (error == nil) {
            NSString *errorStr = (NSString *)error;
            if ([errorStr containsString:@"此收货人已做了身份证验证"]) {
                _saveBtn.userInteractionEnabled = NO;
                [self.saveBtn setTitle:@"已认证" forState:UIControlStateNormal];
                _headerView.idNumberF.enabled = NO;
            }else {
                _saveBtn.userInteractionEnabled = YES;
                _saveBtn.backgroundColor = [UIColor redColor];
                [self showAlertWithTitle:@"" message:errorStr AscertainHandler:^(UIAlertAction *action) {
                    
                }];
            }
        }
        _saveBtn.userInteractionEnabled = NO;
        [self.saveBtn setTitle:@"已认证" forState:UIControlStateNormal];
        _headerView.idNumberF.enabled = NO;
    }];
}

#pragma mark -懒加载
- (NSMutableArray *)adressArr
{
    if (!_adressArr) {
        _adressArr = [NSMutableArray array];
    }
    return _adressArr;
}

#pragma mark -UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.tableView.contentInset = UIEdgeInsetsMake(300, 0, 300, 0);
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}








@end
