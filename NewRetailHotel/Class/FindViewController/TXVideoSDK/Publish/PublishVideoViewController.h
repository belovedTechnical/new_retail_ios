//
//  PublishVideoViewController.h
//  TXVideoDemo
//
//  Created by BDSir on 2017/11/13.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TXLivePlayer.h"

/**
 *  短视频预览VC
 */
@class TXRecordResult;
@interface PublishVideoViewController : UIViewController

- (instancetype)initWithCoverImage:(UIImage *)coverImage
                         videoPath:(NSString*)videoPath
                        renderMode:(TX_Enum_Type_RenderMode)renderMode
                      isFromRecord:(BOOL)isFromRecord;
@end
