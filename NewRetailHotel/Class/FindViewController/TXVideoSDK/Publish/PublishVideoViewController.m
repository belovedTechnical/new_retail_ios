//
//  PublishVideoViewController.m
//  TXVideoDemo
//
//  Created by BDSir on 2017/11/13.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "PublishVideoViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>
#import "TXUGCPublish.h"
#import "SucceedPublishViewController.h"

#define BUTTON_PREVIEW_SIZE         65
#define BUTTON_CONTROL_SIZE         40
#define kScreenWith   [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height

@interface PublishVideoViewController ()<TXLivePlayListener,TXVideoPublishListener>
{
    UIView *                        _videoPreview;
    UIButton *                      _btnStartPreview;
    UILabel*                        _progressTipLabel;
    UISlider *                      _sdPreviewSlider;
    
    int                             _recordType;
    UIImage *                       _coverImage;
    BOOL                            _previewing;
    BOOL                            _startPlay;
    
    BOOL                            _navigationBarHidden;
    BOOL                            _statusBarHidden;
    
    NSString*                       _videoPath;
    TXLivePlayer*                   _livePlayer;
    TX_Enum_Type_RenderMode         _renderMode;
}
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) NSString *videoId;
@property (nonatomic, strong) UIButton *btnPublish;

@end

@implementation PublishVideoViewController

- (instancetype)initWithCoverImage:(UIImage *)coverImage
                         videoPath:(NSString*)videoPath
                        renderMode:(TX_Enum_Type_RenderMode)renderMode
                      isFromRecord:(BOOL)isFromRecord;
{
    if (self = [super init])
    {
        _coverImage = coverImage;
        _videoPath =  videoPath;
        _renderMode = renderMode;
        _previewing   = NO;
        _startPlay    = NO;
        
        _livePlayer = [[TXLivePlayer alloc] init];
        _livePlayer.delegate = self;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidEnterBackGround:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAudioSessionEvent:) name:AVAudioSessionInterruptionNotification object:nil];
    }
    return self;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self initPreviewUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _navigationBarHidden = self.navigationController.navigationBar.hidden;
    self.navigationController.navigationBar.hidden = YES;
    [UIApplication sharedApplication].statusBarHidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    
    if (_previewing)
    {
        [self startVideoPreview:NO];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBar.hidden = _navigationBarHidden;
    [UIApplication sharedApplication].statusBarHidden = NO;
    
    [self stopVideoPreview:YES];
}

-(void)viewDidUnload
{
    [super viewDidUnload];
}

-(void)dealloc{
    [_livePlayer removeVideoWidget];
    [_livePlayer stopPlay];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)onAppDidEnterBackGround:(UIApplication*)app
{
    [self stopVideoPreview:NO];
}

- (void)onAppWillEnterForeground:(UIApplication*)app
{
    if (_previewing)
    {
        [self startVideoPreview:NO];
    }
}

- (void)onAudioSessionEvent:(NSNotification *)notification
{
    NSDictionary *info = notification.userInfo;
    AVAudioSessionInterruptionType type = [info[AVAudioSessionInterruptionTypeKey] unsignedIntegerValue];
    if (type == AVAudioSessionInterruptionTypeBegan) {
        if (_previewing) {
            [self onBtnPreviewStartClicked];
        }
    }
}

-(void)startVideoPreview:(BOOL) startPlay
{
    if(startPlay == YES){
        [_livePlayer setupVideoWidget:CGRectZero containView:_videoPreview insertIndex:0];
        [_livePlayer startPlay:_videoPath type:PLAY_TYPE_LOCAL_VIDEO];
        [_livePlayer setRenderMode:_renderMode];
    }else{
        [_livePlayer resume];
    }
    
}

-(void)stopVideoPreview:(BOOL) stopPlay
{
    
    if(stopPlay == YES)
        [_livePlayer stopPlay];
    else
        [_livePlayer pause];
    
}

#pragma mark ---- Video Preview ----
-(void)initPreviewUI
{
    //[_livePlayer setRenderMode:RENDER_MODE_FILL_EDGE];
    self.title = @"上传视频";
    self.navigationItem.hidesBackButton = YES;
    
    CGFloat videoH = 180;
    CGRect videoFarme = CGRectMake(0, (kScreenHeight/2)-(videoH/2), kScreenWith, videoH);
    
    CGFloat titleTextH = (kScreenHeight/2) - (videoH/2) - 65;
    CGRect titleTextFarme = CGRectMake(0, videoFarme.origin.y - titleTextH, kScreenWith, titleTextH);
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWith, 65)];
    topView.backgroundColor = [UIColor redColor];
    [self.view addSubview:topView];
    
    CGFloat titleW = 100;
    CGFloat titleH = 50;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(topView.center.x - titleW/2, topView.center.y - titleH/2, titleW, titleH)];
    titleLabel.text = @"上传视频";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [topView addSubview:titleLabel];
    
    UITextView *textView = [[UITextView alloc] init];
    textView.frame = titleTextFarme;
    textView.font = [UIFont systemFontOfSize:16];
    textView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:textView];
    _textView = textView;

    UILabel *placeholderLabel = [[UILabel alloc] init];
    placeholderLabel.text = @"介绍下这段视频";
    placeholderLabel.font = [UIFont systemFontOfSize:15.f];
    placeholderLabel.textColor = [UIColor darkGrayColor];
    placeholderLabel.numberOfLines = 0;
    [placeholderLabel sizeToFit];
    [textView addSubview:placeholderLabel];
    [textView setValue:placeholderLabel forKey:@"_placeholderLabel"];
    
    UIImageView * coverImageView = [[UIImageView alloc] initWithFrame:videoFarme];
    coverImageView.backgroundColor = UIColor.blackColor;
    if(_renderMode == RENDER_MODE_FILL_EDGE){
        coverImageView.contentMode = UIViewContentModeScaleAspectFit;
    }else{
        coverImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    coverImageView.image = _coverImage;
    [self.view addSubview:coverImageView];
    
    _videoPreview = [[UIView alloc] initWithFrame:videoFarme];
    [self.view addSubview: _videoPreview];
    
    _btnStartPreview = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, BUTTON_PREVIEW_SIZE, BUTTON_PREVIEW_SIZE)];
    _btnStartPreview.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [_btnStartPreview setImage:[UIImage imageNamed:@"startpreview"] forState:UIControlStateNormal];
    [_btnStartPreview setImage:[UIImage imageNamed:@"startpreview_press"] forState:UIControlStateSelected];
    [_btnStartPreview addTarget:self action:@selector(onBtnPreviewStartClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnStartPreview];
    
    UIButton *btnPop = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, BUTTON_CONTROL_SIZE, BUTTON_CONTROL_SIZE)];
    btnPop.center = CGPointMake(30, 30);
    [btnPop setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [btnPop addTarget:self action:@selector(onBtnPopBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnPop];
    
    UIButton *btnDelete = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, BUTTON_CONTROL_SIZE, BUTTON_CONTROL_SIZE)];
    btnDelete.center = CGPointMake(self.view.frame.size.width / 4, self.view.frame.size.height - BUTTON_CONTROL_SIZE - 5);
    [btnDelete setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
    [btnDelete setImage:[UIImage imageNamed:@"delete_press"] forState:UIControlStateSelected];
    [btnDelete addTarget:self action:@selector(onBtnDeleteClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnDelete];
    
    UIButton *btnDownload = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, BUTTON_CONTROL_SIZE, BUTTON_CONTROL_SIZE)];
    btnDownload.center = CGPointMake(self.view.frame.size.width * 3 / 4, self.view.frame.size.height - BUTTON_CONTROL_SIZE - 5);
    [btnDownload setImage:[UIImage imageNamed:@"download"] forState:UIControlStateNormal];
    [btnDownload setImage:[UIImage imageNamed:@"download_press"] forState:UIControlStateSelected];
    [btnDownload addTarget:self action:@selector(onBtnDownloadClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnDownload];
    
    UIButton *btnPublish = [[UIButton alloc] init];
    btnPublish.frame = CGRectMake(kScreenWith/2-100, _videoPreview.frame.origin.y + _videoPreview.frame.size.height + 50, 200, 50);
    [btnPublish setTitle:@"上传" forState:UIControlStateNormal];
    [btnPublish setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnPublish setBackgroundColor:[UIColor redColor]];
    [btnPublish.layer setCornerRadius:20];
    [btnPublish setClipsToBounds:YES];
    [btnPublish addTarget:self action:@selector(btnPublishClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnPublish];
    _btnPublish = btnPublish;
    
}

-(void)onBtnPopBack
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
    //    [self.navigationController popViewControllerAnimated:YES];
}

-(void)onBtnPreviewStartClicked
{
    if (!_startPlay) {
        [self startVideoPreview:YES];
        _startPlay = YES;
    }
    _previewing = !_previewing;
    
    if (_previewing)
    {
        [self startVideoPreview:NO];
        [_btnStartPreview setImage:[UIImage imageNamed:@"pausepreview"] forState:UIControlStateNormal];
        [_btnStartPreview setImage:[UIImage imageNamed:@"pausepreview_press"] forState:UIControlStateSelected];
    }
    else
    {
        [self stopVideoPreview:NO];
        [_btnStartPreview setImage:[UIImage imageNamed:@"startpreview"] forState:UIControlStateNormal];
        [_btnStartPreview setImage:[UIImage imageNamed:@"startpreview_press"] forState:UIControlStateSelected];
    }
}

-(void)onBtnDownEditClicked
{

}

#pragma mark -保存视频
-(void)onBtnDownloadClicked
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library writeVideoAtPathToSavedPhotosAlbum:[NSURL fileURLWithPath:_videoPath] completionBlock:^(NSURL *assetURL, NSError *error) {
        if (error != nil) {
            BDLog(@"save video fail:%@", error);
        }
    }];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void)onBtnDeleteClicked
{
    [[NSFileManager defaultManager] removeItemAtPath:_videoPath error:nil];
    
    //[self.navigationController popViewControllerAnimated:YES];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - TXLivePlayListener
-(void) onPlayEvent:(int)EvtID withParam:(NSDictionary*)param
{
    NSDictionary* dict = param;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (EvtID == PLAY_EVT_PLAY_PROGRESS) {
            float progress = [dict[EVT_PLAY_PROGRESS] floatValue];
            [_sdPreviewSlider setValue:progress];
            
            float duration = [dict[EVT_PLAY_DURATION] floatValue];
            if (duration > 0 && _sdPreviewSlider.maximumValue != duration) {
                _sdPreviewSlider.minimumValue = 0;
                _sdPreviewSlider.maximumValue = duration;
            }
            NSString* progressTips = [NSString stringWithFormat:@"%02d:%02d/%02d:%02d", (int)progress / 60, (int)progress % 60, (int)duration / 60, (int)duration % 60];
            _progressTipLabel.text = progressTips;
            return ;
        } else if(EvtID == PLAY_EVT_PLAY_END) {
            [_sdPreviewSlider setValue:0];
            //           [self stopVideoPreview:YES];
            //           [self startVideoPreview:YES];
            //           [_livePlayer startPlay:_videoPath type:PLAY_TYPE_LOCAL_VIDEO];
            [_livePlayer resume];
            [_btnStartPreview setImage:[UIImage imageNamed:@"pausepreview"] forState:UIControlStateNormal];
            [_btnStartPreview setImage:[UIImage imageNamed:@"pausepreview_press"] forState:UIControlStateSelected];
            //[_livePlayer startPlay:_videoPath type:PLAY_TYPE_LOCAL_VIDEO];
            _progressTipLabel.text = @"00:00/00:00";
        }
    });
}

-(void) onNetStatus:(NSDictionary*) param
{
    return;
}

- (void)btnPublishClick
{
    NSString *apikey = [[NSUserDefaults standardUserDefaults] objectForKey:@"apikey"];
    if (apikey.length > 6) {
        _btnPublish.userInteractionEnabled = NO;
        TXPublishParam * param = [[TXPublishParam alloc] init];
        param.signature = @"dTyFMt9jBQ+BPZGfUntg14ru4M5zZWNyZXRJZD1BS0lEb1YwSlFNY21GWGRXRWpTOWY0SHNMaHNxY0JhbkFlS3ImY3VycmVudFRpbWVTdGFtcD0yMDE3MTExMTEyMDAmZXhwaXJlVGltZT0yMDI4MTExMTEyMDAmcmFuZG9tPTEyMzQ1NjIyJnByb2NlZHVyZT1RQ1ZCX1NpbXBsZVByb2Nlc3NGaWxlKCU3QjEwJTJDMjAlMkMzMCU3RCUyQzQwMDQwNiUyQzEwJTJDMTAp";                                // 需要填写第四步中计算的上传签名
        // 录制生成的视频文件路径 TXVideoRecordListener 的 onRecordComplete 回调中可以获取
        param.videoPath = _videoPath;
        // 录制生成的视频首帧预览图， TXVideoRecordListener 的 onRecordComplete 回调中可以获取，可以置为 nil
        param.coverImage = _coverImage;
        TXUGCPublish *_ugcPublish = [[TXUGCPublish alloc] init];
        // 文件发布默认是采用断点续传
        _ugcPublish.delegate = self;                                 // 设置 TXVideoPublishListener 回调
        [_ugcPublish publishVideo:param];
    }else {
        
    }
}

#pragma mark -TXVideoPublishListener
/**
 * 短视频发布进度
 */
-(void) onPublishProgress:(NSInteger)uploadBytes totalBytes: (NSInteger)totalBytes
{
    BDLog(@"uploadBytes--%ld", (long)uploadBytes);
    BDLog(@"totalBytes--%ld", (long)totalBytes);
}

/**
 * 短视频发布完成
 */
-(void) onPublishComplete:(TXPublishResult*)result
{
    BDLog(@"%d", result.retCode);
    BDLog(@"%@", result.descMsg);
    BDLog(@"%@", result.videoId);
    if (result.videoId.length > 6) {
        [self performSelector:@selector(requestNetPostUserVieoPulishWithVideoId:) withObject:result.videoId afterDelay:3.0f];
//        [self requestNetPostUserVieoPulishWithVideoId:result.videoId];
    }
}

/**
 * 短视频发布事件通知
 */
-(void) onPublishEvent:(NSDictionary *)evt
{
    BDLog(@"%@", evt);
}

#pragma mark -网络请求
- (void)requestNetPostUserVieoPulishWithVideoId:(NSString *)videoId
{
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

@end
