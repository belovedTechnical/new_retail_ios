//
//  SucceedPublishViewController.m
//  BelovedHotel
//
//  Created by BDSir on 2017/11/14.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "SucceedPublishViewController.h"

@implementation SucceedPublishViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"上传成功";
}





- (void)popWebGoBack
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}




@end
