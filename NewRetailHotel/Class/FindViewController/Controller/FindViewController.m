//
//  FindViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/7.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "FindViewController.h"
#import "VideoRecordViewController.h"

@interface FindViewController ()

@end

@implementation FindViewController
{
    VideoConfigure *_videoConfig;
}

- (CGRect)webViewFrame
{
    CGRect frame = CGRectMake(0, 0, kScreenW, kScreenH+44);
    return frame;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"视频";
    self.view.backgroundColor = randomColor;
    _videoConfig = [[VideoConfigure alloc] init];
    
//    UIButton *jumpBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.view.center.x, self.view.center.y, 50, 50)];
//    [jumpBtn setTitle:@"跳转" forState:UIControlStateNormal];
//    [jumpBtn addTarget:self action:@selector(webOpenShortVideo) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:jumpBtn];
}

- (NSURL *)webURL
{
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",HTML_URL_Base, urlWebFindVideo];
    NSLog(@"视频：%@", urlStr);
    NSURL *url = [NSURL URLWithString:urlStr];
    
    return url;
}

- (void)webOpenShortVideo
{
    _videoConfig.bps = 2400;
    _videoConfig.fps = 20;
    _videoConfig.gop = 3;
    _videoConfig.videoRatio = VIDEO_ASPECT_RATIO_9_16;
    _videoConfig.videoResolution = VIDEO_RESOLUTION_540_960;
    VideoRecordViewController *videoVC = [[VideoRecordViewController alloc] initWithConfigure:_videoConfig];
    self.tabBarController.tabBar.hidden = YES;
    [self.navigationController pushViewController:videoVC animated:YES];
}

@end
