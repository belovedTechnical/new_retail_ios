//
//  UIView+Frame.h
//  BDBaseMenuVC
//
//  Created by BDSir on 2017/6/14.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Frame)

@property CGFloat bd_x;
@property CGFloat bd_y;
@property CGFloat bd_centerX;
@property CGFloat bd_centerY;
@property CGFloat bd_height;
@property CGFloat bd_width;

@property (nonatomic) CGFloat bd_left;
@property (nonatomic) CGFloat bd_right;
@property (nonatomic) CGFloat bd_top;
@property (nonatomic) CGFloat bd_bottom;

@end
