//
//  UIViewController+Message.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/6.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Message)

/** 确定 */
- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message AscertainHandler:(void (^) (UIAlertAction *action))ascertainHandler;

/** 确定加取消 */
- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message AscertainHandler:(void (^) (UIAlertAction *action))ascertainHandler CancelHandler:(void(^)(UIAlertAction *action))cancelHandler;

@end
