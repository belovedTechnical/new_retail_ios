//
//  UIBarButtonItem+Item.h
//  BelovedHotel
//
//  Created by BDSir on 2017/6/19.
//  Copyright © 2017年 BeloverdSpace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (Item)

+ (instancetype)itemwithText:(NSString *)text target:(id)target action:(SEL)action;
// 快速创建UIBarButtonItem
+ (instancetype)itemWithImage:(UIImage *)image highImage:(UIImage *)highImage target:( id)target action:(SEL)action;

+ (instancetype)itemWithImage:(UIImage *)image selImage:(UIImage *)selImage target:( id)target action:(SEL)action;

@end
