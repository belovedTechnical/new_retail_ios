//
//  UIViewController+Message.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/6.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "UIViewController+Message.h"

@implementation UIViewController (Message)

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message AscertainHandler:(void (^) (UIAlertAction *action))ascertainHandler;
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:ascertainHandler];
    
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message AscertainHandler:(void (^) (UIAlertAction *action))ascertainHandler CancelHandler:(void(^)(UIAlertAction *action))cancelHandler;
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:ascertainHandler];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:cancelHandler];
    
    [alert addAction:action];
    [alert addAction:action1];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
