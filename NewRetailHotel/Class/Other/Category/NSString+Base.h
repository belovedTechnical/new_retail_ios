//
//  NSString+Password.h
//  03.数据加密
//
//  Created by BD on 13-12-10.
//  Copyright (c) 2013年 itcast. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Base)

/**
 *  32位MD5加密
 */
- (NSString *)baseMD5;

/**
 *  SHA1加密
 */
- (NSString *)baseSHA1;

/**  */
/** 字符串是否是 nil  长度为0  或者 @" " */
+ (BOOL)baseIsNilOrEmpty:(NSString *)string;
/** url 编码 */
+ (NSString*)baseURLEncodedString:(NSString *)string;


/** 验证手机号 符合 YES 不符合 NO */
+ (BOOL)validatePhone:(NSString *)phone;
/** 验证 邮箱 符合 YES 不符合 NO */
+ (BOOL)validateEmail:(NSString *)email;
/** 验证车牌号 符合 YES 不符合 NO */
+ (BOOL)validateCarNo:(NSString *)carNo;
/** 验证用户名 符合 YES 不符合 NO */
+ (BOOL)validateUserName:(NSString *)name;

/** 验证密码 符合 YES 不符合 NO */
+ (BOOL)validatePassword:(NSString *)passWord;

/** 验证昵称 符合 YES 不符合 NO */
+ (BOOL)validateNickname:(NSString *)nickname;

/** 验证身份证 符合 YES 不符合 NO */
+ (BOOL)validateIdentityCard: (NSString *)identityCard;

/** 验证身份证号码最后一位是否为X  可用于身份证号码输入的自动补全 符合 YES 不符合 NO */
- (BOOL)theLastIsX:(NSString *)IDNumber;

/** 验证金额 符合 YES 不符合 NO */
+ (BOOL)validateMoney:(NSString *)money;

/** 验证webUrl 符合 YES 不符合 NO */
+ (BOOL)validateWebUrl:(NSString*)url;

/** 银行卡号验证 符合 YES 不符合 NO */
+ (BOOL)validateBankCard:(NSString*)cardNo;

/** 验证IP  符合 YES 不符合 NO*/
+ (BOOL)isValidatIP:(NSString *)ipAddress;

/** 验证码  验证 */
+ (BOOL)validateCode:(NSString *)code;
@end
