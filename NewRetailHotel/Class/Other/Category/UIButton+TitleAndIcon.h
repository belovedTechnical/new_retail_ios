//
//  UIButton+TitleAndIcon.h
//  NewRetailHotel
//
//  Created by weilin on 2018/11/6.
//  Copyright © 2018 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (TitleAndIcon)

- (void)setupImageAndTitleEdge;

@end
