//
//  UIView+Frame.m
//  BDBaseMenuVC
//
//  Created by BDSir on 2017/6/14.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "UIView+Frame.h"

@implementation UIView (Frame)

- (void)setBd_centerX:(CGFloat)bd_centerX
{
    CGPoint center = self.center;
    center.x = bd_centerX;
    self.center = center;
}

- (CGFloat)bd_centerX
{
    return self.center.x;
}

- (void)setBd_centerY:(CGFloat)bd_centerY
{
    CGPoint center = self.center;
    center.y = bd_centerY;
    self.center = center;
}

- (CGFloat)bd_centerY
{
    return self.center.y;
}

- (void)setBd_x:(CGFloat)bd_x
{
    CGRect frame = self.frame;
    frame.origin.x = bd_x;
    self.frame = frame;
}

- (CGFloat)bd_x
{
    return self.frame.origin.x;
}

- (void)setBd_y:(CGFloat)bd_y
{
    CGRect frame = self.frame;
    frame.origin.y = bd_y;
    self.frame = frame;
}

- (CGFloat)bd_y
{
    return self.frame.origin.y;
}

- (void)setBd_width:(CGFloat)bd_width
{
    CGRect frame = self.frame;
    frame.size.width = bd_width;
    self.frame = frame;
}

- (CGFloat)bd_width
{
    return self.frame.size.width;
}

- (void)setBd_height:(CGFloat)bd_height
{
    CGRect frame = self.frame;
    frame.size.height = bd_height;
    self.frame = frame;
}

- (CGFloat)bd_height
{
    return self.frame.size.height;
}

- (CGFloat)bd_top {
    return self.frame.origin.y;
}
- (void)setBd_top:(CGFloat)bd_top {
    CGRect frame = self.frame;
    frame.origin.y = bd_top;
    self.frame = frame;
}

- (CGFloat)bd_bottom {
    return self.frame.origin.y + self.frame.size.height;
}
- (void)setBd_bottom:(CGFloat)bd_bottom {
    CGRect frame = self.frame;
    frame.origin.y = bd_bottom - frame.size.height;
    self.frame = frame;
}

- (CGFloat)bd_left {
    return self.frame.origin.x;
}

- (void)setBd_left:(CGFloat)bd_left {
    CGRect frame = self.frame;
    frame.origin.x = bd_left;
    self.frame = frame;
}

- (CGFloat)bd_right
{
    return self.frame.origin.x + self.frame.size.width;
}

- (void)setBd_right:(CGFloat)bd_right {
    CGRect frame = self.frame;
    frame.origin.x = bd_right - frame.size.width;
    self.frame = frame;
}


@end
