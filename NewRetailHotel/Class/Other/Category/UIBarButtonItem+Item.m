//
//  UIBarButtonItem+Item.m
//  BelovedHotel
//
//  Created by BDSir on 2017/6/19.
//  Copyright © 2017年 BeloverdSpace. All rights reserved.
//

#import "UIBarButtonItem+Item.h"

@implementation UIBarButtonItem (Item)

+ (instancetype)itemwithText:(NSString *)text target:(id)target action:(SEL)action
{
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:text forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [rightBtn sizeToFit];
    [rightBtn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    UIView *containView = [[UIView alloc] initWithFrame:rightBtn.bounds];
    [containView addSubview:rightBtn];
    return [[UIBarButtonItem alloc] initWithCustomView:containView];
}

+ (instancetype)itemWithImage:(UIImage *)image highImage:(UIImage *)highImage target:(nullable id)target action:(SEL)action
{
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:image forState:UIControlStateNormal];
    [leftButton setImage:highImage forState:UIControlStateHighlighted];
    [leftButton sizeToFit];
    [leftButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    UIView *containView = [[UIView alloc] initWithFrame:leftButton.bounds];
    [containView addSubview:leftButton];
    // bug:导航条上按钮点击范围扩大
    // 普通的话,设置一个按钮宽度,那么点击范围就是按钮范围,但是把按钮保证成UIBarButtonItem,点击范围扩大
    return [[UIBarButtonItem alloc] initWithCustomView:containView];
}

+ (instancetype)itemWithImage:(UIImage *)image selImage:(UIImage *)selImage target:(id)target action:(SEL)action
{
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:image forState:UIControlStateNormal];
    [leftButton setImage:selImage forState:UIControlStateSelected];
    [leftButton sizeToFit];
    [leftButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    UIView *containView = [[UIView alloc] initWithFrame:leftButton.bounds];
    [containView addSubview:leftButton];
    // bug:导航条上按钮点击范围扩大
    // 普通的话,设置一个按钮宽度,那么点击范围就是按钮范围,但是把按钮保证成UIBarButtonItem,点击范围扩大
    return [[UIBarButtonItem alloc] initWithCustomView:containView];
}

@end
