//
//  UIButton+TitleAndIcon.m
//  NewRetailHotel
//
//  Created by weilin on 2018/11/6.
//  Copyright © 2018 BDSir. All rights reserved.
//

#import "UIButton+TitleAndIcon.h"

@implementation UIButton (TitleAndIcon)

- (void)setupImageAndTitleEdge {
    
    CGRect imageRect = self.imageView.frame;
    CGRect titleRect = self.titleLabel.frame;
    
    CGFloat totalHeight = imageRect.size.height + titleRect.size.height;
    CGFloat selfHeight = self.frame.size.height;
    CGFloat selfWidth = self.frame.size.width;
    
    
    
    self.titleEdgeInsets = UIEdgeInsetsMake(
                                       ((selfHeight - totalHeight) / 2 + imageRect.size.height - titleRect.origin.y),
                                       (selfWidth / 2 - titleRect.origin.x - titleRect.size.width / 2),
                                       -((selfHeight - totalHeight) / 2 + imageRect.size.height - titleRect.origin.y),
                                       -(selfWidth / 2 - titleRect.origin.x - titleRect.size.width / 2)
                                            );
    self.imageEdgeInsets = UIEdgeInsetsMake(
                                       ((selfHeight - totalHeight) / 2 - imageRect.origin.y - 5),
                                       (selfWidth / 2 - imageRect.origin.x - imageRect.size.width / 2),
                                       -((selfHeight - totalHeight) / 2 - imageRect.origin.y - 5),
                                       -(selfWidth / 2 - imageRect.origin.x - imageRect.size.width / 2)
                                            );
}

@end
