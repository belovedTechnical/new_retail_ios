//
//  UIImage+Image.h
//  BelovedSpace
//
//  Created by Sam Feng on 2017/4/27.
//  Copyright © 2017年 beloved999. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Image)

+ (UIImage *)imageOriginalWhitImageName:(NSString *)imageName;

@end
