//
//  NSDate+BDCalendar.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/20.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (BDCalendar)

- (NSUInteger)numberOfDaysInCurrentMonth;           /** 计算这个月有多少天 */
- (NSUInteger)numberOfWeeksInCurrentMonth;          /** 获取这个月有多少周 */
- (NSUInteger)weeklyOrdinality;                     /** 计算这个月的第一天是礼拜几 */
- (NSDate *)firstDayOfCurrentMonth;                 /** 计算这个月最开始的一天 */
- (NSDate *)lastDayOfCurrentMonth;                  /** 计算这个月最后的一天 */
- (NSDate *)dayInThePreviousMonth;                  /** 上一个月 */
- (NSDate *)dayInTheFollowingMonth;                 /** 下一个月 */
- (NSDate *)dayInTheFollowingMonth:(int)month;      /** 获取当前日期之后的几个月 */
- (NSDate *)dayInTheFollowingDay:(int)day;          /** 获取当前日期之后的几天 */
- (NSDateComponents *)YMDComponents;                /** 获取年、月、日对象 */
- (NSDateComponents *)YMDWComponents;               /** 带时区的Components */
- (NSDate *)YMDCurrentlyDate;                       /** 带时区的当前日期 */
- (NSDate *)toLocalDate;
- (NSDate *)dateFromString:(NSString *)dateString;  /** NSString转NSDate */
- (NSString *)stringFromDate:(NSDate *)date;        /** NSDate转NSString */
- (NSInteger)getWeekIntValueWithDate;               /** 周日是“1”，周一是“2”... */
- (NSString *)compareIfTodayWithDate;               /** 判断日期是今天,明天,后天,周几 */
+ (NSString *)getWeekStringFromInteger:(NSInteger)week; /** 通过数字返回星期几 */
+ (NSInteger)getDayNumbertoDay:(NSDate *)today beforDay:(NSDate *)beforday;



@end
