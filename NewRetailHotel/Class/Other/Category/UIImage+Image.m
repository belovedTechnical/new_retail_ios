//
//  UIImage+Image.m
//  BelovedSpace
//
//  Created by Sam Feng on 2017/4/27.
//  Copyright © 2017年 beloved999. All rights reserved.
//

#import "UIImage+Image.h"

@implementation UIImage (Image)

+ (UIImage *)imageOriginalWhitImageName:(NSString *)imageName
{
    UIImage *image = [UIImage imageNamed:imageName];
    return [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}
@end
