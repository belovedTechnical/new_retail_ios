//
//  UIColor+Hex.h
//  BelovedHotel
//
//  Created by BDSir on 2017/6/16.
//  Copyright © 2017年 BeloverdSpace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)

+ (UIColor *)colorWithHexString:(NSString *)color;

//从十六进制字符串获取颜色，
//color:支持@“#123456”、 @“0X123456”、 @“123456”三种格式
+ (UIColor *)colorWithHexString:(NSString *)color alpha:(CGFloat)alpha;



//上面的代码在开头是两个宏定义，就是对[UIColor colorWithRed:green:blue:alpha]方法的简化，在UIColor(Hex)中声明两个方法-colorWithHexString和-colorWithHexString:alpha，这个很好理解。

@end
