//
//  RightBaseButton.m
//  BelovedHotel
//
//  Created by BDSir on 2017/6/19.
//  Copyright © 2017年 BeloverdSpace. All rights reserved.
//

#import "RightBaseButton.h"

@implementation RightBaseButton

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}

- (void)awakeFromNib
{   [super awakeFromNib];
    
}

- (void)layoutSubviews
{   [super layoutSubviews];
    if (self.contentHorizontalAlignment == UIControlContentHorizontalAlignmentRight) {
        self.imageView.bd_x = self.bd_width-self.imageView.bd_width;
        self.titleLabel.bd_x = self.bd_width-self.imageView.bd_width-self.titleLabel.bd_width;
    }else {
        self.titleLabel.bd_x = 0;
        self.imageView.bd_x = self.titleLabel.bd_width;
    }
}
@end
