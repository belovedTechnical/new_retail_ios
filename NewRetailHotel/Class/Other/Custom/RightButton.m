//
//  RightButton.m
//  BelovedHotel
//
//  Created by BDSir on 2017/6/17.
//  Copyright © 2017年 BeloverdSpace. All rights reserved.
//

#import "RightButton.h"

@implementation RightButton

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}

- (void)layoutSubviews
{   [super layoutSubviews];
    
    CGFloat titleW = self.bd_width - self.imageView.bd_width;
    CGFloat titleH = self.frame.size.height;
    self.titleLabel.frame = CGRectMake(20, 0, titleW, titleH);
    
    CGFloat imageX = titleW - 20;
    CGFloat imageY = self.bd_height / 2 - ((self.bd_height /2) /2);
    CGFloat imageW = self.frame.size.width - titleW;
    CGFloat imageH = imageW;
    self.imageView.frame = CGRectMake(imageX, imageY, imageW, imageH);
}

@end
