//
//  DateTextF.m
//  BelovedHotel
//
//  Created by 王保栋 on 2017/6/12.
//  Copyright © 2017年 BeloverdSpace. All rights reserved.
//

#import "DateTextF.h"

@implementation DateTextF

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setUP];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setUP];
    }
    return self;
}

- (void)setUP
{
    UIDatePicker *dateP = [[UIDatePicker alloc] init];
    dateP.datePickerMode = UIDatePickerModeDate;
    dateP.backgroundColor = [UIColor whiteColor];
    dateP.locale = [NSLocale localeWithLocaleIdentifier:@"zh"];
    
    self.inputView = dateP;
    [dateP addTarget:self action:@selector(dateChange:) forControlEvents:UIControlEventValueChanged];
}

- (void)dateChange:(UIDatePicker *)dateP
{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd";
    NSString *dateString = [fmt stringFromDate:dateP.date];
    
    self.text = dateString;
}

@end
