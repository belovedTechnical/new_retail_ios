//
//  BottomButton.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/6.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "BottomButton.h"

@implementation BottomButton

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.imageView.contentMode = UIViewContentModeCenter;
    
    CGFloat imageW = self.frame.size.width;
    CGFloat imageH = imageW-30;
    self.imageView.frame = CGRectMake(0, 0, imageW, imageH);
    
    CGFloat titleY = imageH - 10;
    CGFloat titleW = imageW;
    CGFloat titleH = self.frame.size.height - imageH;
    self.titleLabel.frame = CGRectMake(0, titleY, titleW, titleH);
}

@end
