//
//  UserInfoItem.h
//  BelovedHotel
//
//  Created by BDSir on 2017/7/6.
//  Copyright © 2017年 BeloverdSpace. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfoItem : NSObject

/** 头像 */
@property(nonatomic, copy) NSString *headImage;
/** 昵称 */
@property(nonatomic, copy) NSString *nickname;
/** 用户ID */
@property(nonatomic, copy) NSString *partyId;
/** 用户账号 */
@property(nonatomic, copy) NSString *userFullName;
/** 余额 */
@property(nonatomic, copy) NSString *availableBalance;
/** 积分 */
@property(nonatomic, copy) NSString *availablePoints;
/** 至爱币 */
@property(nonatomic, copy) NSString *availableVCs;
/** 优惠券 */
@property (nonatomic, copy) NSString *couponCount;

+ (instancetype)userInfoDict:(NSDictionary *)dict;
+ (instancetype)userInfoWaChatDict:(NSDictionary *)dict;


@end
