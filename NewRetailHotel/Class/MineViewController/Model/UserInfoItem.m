//
//  UserInfoItem.m
//  BelovedHotel
//
//  Created by BDSir on 2017/7/6.
//  Copyright © 2017年 BeloverdSpace. All rights reserved.
//

#import "UserInfoItem.h"

@implementation UserInfoItem

+ (instancetype)userInfoDict:(NSDictionary *)dict
{
    UserInfoItem *userItem = [[self alloc] init];
    userItem.headImage = dict[@"headImage"];
    userItem.nickname = dict[@"nickname"];
    userItem.partyId = dict[@"partyId"];
    userItem.userFullName = dict[@"userFullName"];
    userItem.availableBalance = dict[@"availableBalance"];
    userItem.availablePoints = dict[@"availablePoints"];
    userItem.availableVCs = dict[@"availableVCs"];
    userItem.couponCount = dict[@"couponCount"];
    return userItem;
}

+ (instancetype)userInfoWaChatDict:(NSDictionary *)dict
{
    UserInfoItem *userItem = [[self alloc] init];
    userItem.headImage = dict[@"headimgurl"];
    userItem.nickname = dict[@"nickname"];
    return userItem;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}

@end
