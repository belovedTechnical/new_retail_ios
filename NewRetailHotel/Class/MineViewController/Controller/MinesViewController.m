//
//  MinesViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/4.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "MinesViewController.h"
#import "ProductCollectionViewController.h"
#import "OnlineProductListViewController.h"
#import "StoreCollectionViewController.h"
#import "HotelOrderListViewController.h"
#import "ShopOrderFormViewController.h"
#import "ZhiaiBiListViewController.h"
#import "WebListingViewController.h"
#import "MyWelfareWebViewController.h"
#import "SettingUpViewController.h"
#import "MyBalanceViewController.h"
#import "RechargeViewController.h"
#import "FeedbackViewController.h"
#import "MyVideoViewController.h"
#import "CouponViewController.h"
#import "NoteLoginViewController.h"
#import "ScanViewController.h"

#import "BDNetworkTools.h"
#import "MeContentView.h"
#import "MeHeaderView.h"
#import "UserInfoItem.h"
#import "SaveTool.h"

@interface MinesViewController ()

@property (nonatomic, strong) MeHeaderView *headerView;
@property (nonatomic, strong) MeContentView *contentView;
@property (nonatomic, strong) UIScrollView *bgScrollView;
@property (nonatomic, strong) UIView *maskView;

@end

@implementation MinesViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    NSString *userApikey = [SaveTool objectForKey:apikey];
    if (userApikey.length > 8) {
        [self requestNetUserInfoAcount];
    }else {
        self.maskView.hidden = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f6f6f6"];
    self.title = @"我的";
    [self initUI];
    // 监听成功登陆
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(succeedLogin) name:succeedLoginNoti object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LogoutNotiFun) name:LogoutNoti object:nil];
}

// 移除监听
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initUI
{
    UIScrollView *bgScrollView = [[UIScrollView alloc] init];
    bgScrollView.frame = self.view.bounds;
    bgScrollView.contentSize = CGSizeMake(kScreenW, kScreenH);
    [self.view addSubview:bgScrollView];
    self.bgScrollView = bgScrollView;
    
    NSString *userApikey = [SaveTool objectForKey:apikey];
    if (userApikey.length < 8) { // 登录界面
        self.headerView = [MeHeaderView headerNotLoggedInView];
        self.headerView.frame = CGRectMake(0, 0, kScreenW, 100);
        [bgScrollView addSubview:self.headerView];
        [self.headerView.loginBtn addTarget:self action:@selector(loginBtnClicks) forControlEvents:UIControlEventTouchUpInside];
        [self.headerView.signBtn addTarget:self action:@selector(signInBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }else {
        self.headerView = [MeHeaderView headerLoggedInView];
        self.headerView.frame = CGRectMake(0, 0, kScreenW, 100);
        [bgScrollView addSubview:self.headerView];
        [self requestNetUserInfoAcount];
    }
    
    MeContentView *contentView = [MeContentView contentViewToLoad];
    contentView.frame = CGRectMake(0, 100, kScreenW, kScreenH-100);
    [bgScrollView addSubview:contentView];
    self.contentView = contentView;
    
    UIView *maskView = [[UIView alloc] init];
    maskView.frame = CGRectMake(0, 100, kScreenW, kScreenH-100);
    maskView.alpha = 0.1;
    if (userApikey == nil) {
        maskView.hidden = NO;
    }else {
        maskView.hidden = YES;
    }
    [bgScrollView addSubview:maskView];
    self.maskView = maskView;
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loginBtnClicks)];
    [maskView addGestureRecognizer:gesture];
    
    [contentView.hotleOrderBtn addTarget:self action:@selector(hotleOrderBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [contentView.onlineOrderBtn addTarget:self action:@selector(onlineOrderBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [contentView.integralBtn addTarget:self action:@selector(integralBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [contentView.couponBtn addTarget:self action:@selector(couponBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [contentView.balanceBtn addTarget:self action:@selector(balanceBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [contentView.rechargeBtn addTarget:self action:@selector(rechargeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [contentView.welfareBtn addTarget:self action:@selector(welfareBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [contentView.guestBtn addTarget:self action:@selector(guestBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [contentView.goodsBtn addTarget:self action:@selector(goodsBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [contentView.storeBtn addTarget:self action:@selector(storeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [contentView.suggestBtn addTarget:self action:@selector(suggestBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [contentView.browseBtn addTarget:self action:@selector(browseBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [contentView.settingUpBtn addTarget:self action:@selector(settingUpBtnClick) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark -内部控制方法
// 签到
- (void)signInBtnClick
{
    
}
// 登陆按钮点击
- (void)loginBtnClicks {
    NoteLoginViewController *loginVC = [[NoteLoginViewController alloc] init];
    [self.navigationController pushViewController:loginVC animated:YES];
}

// 登录成功通知监听方法
- (void)succeedLogin {
    self.maskView.hidden = YES;
    self.headerView = [MeHeaderView headerLoggedInView];
    self.headerView.frame = CGRectMake(0, 0, kScreenW, 100);
    [self.bgScrollView addSubview:self.headerView];
}

// 注销登录通知方法
- (void)LogoutNotiFun {
    self.maskView.hidden = NO;
    MeHeaderView *headerView = [MeHeaderView headerNotLoggedInView];
    headerView.frame = CGRectMake(0, 0, kScreenW, 100);
    [self.bgScrollView addSubview:headerView];
    [headerView.loginBtn addTarget:self action:@selector(loginBtnClicks)       forControlEvents:UIControlEventTouchUpInside];
    [self.contentView.integralBtn setTitle:@"0.00" forState:UIControlStateNormal];
    [self.contentView.couponBtn setTitle:@"0张" forState:UIControlStateNormal];
    [self.contentView.balanceBtn setTitle:@"0.00" forState:UIControlStateNormal];
}
/** 酒店订单 */
- (void)hotleOrderBtnClick {
    HotelOrderListViewController *hotelOrderVC = [[HotelOrderListViewController alloc] init];
    [self.navigationController pushViewController:hotelOrderVC animated:YES];
}
/** 电商订单 */
- (void)onlineOrderBtnClick {
    /*
    OnlineProductListViewController *onlineOrderVC = [[OnlineProductListViewController alloc] init];
    [self.navigationController pushViewController:onlineOrderVC animated:YES];
     */
    
    ShopOrderFormViewController *orderVC = [[ShopOrderFormViewController alloc]init];
    orderVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:orderVC animated:YES];
}
/** 积分 */
- (void)integralBtnClick {
    ZhiaiBiListViewController *zhiaiVC = [[ZhiaiBiListViewController alloc]init];
    zhiaiVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:zhiaiVC animated:YES];
}
/** 优惠券 */
- (void)couponBtnClick {
    CouponViewController *couponVC = [[CouponViewController alloc] init];
    couponVC.isMeController = YES;
    [self.navigationController pushViewController:couponVC animated:YES];
}
/** 账户余额 */
- (void)balanceBtnClick {
    MyBalanceViewController *myVC = [[MyBalanceViewController alloc]init];
    [self.navigationController pushViewController:myVC animated:YES];
}
/** 充值 */
- (void)rechargeBtnClick {
    RechargeViewController *rechargeVC = [[RechargeViewController alloc]init];
    rechargeVC.Balance = [NSString stringWithFormat:@"%@", _contentView.item.availableBalance];
    [self.navigationController pushViewController:rechargeVC animated:YES];
}
/** 星星公益 */
- (void)welfareBtnClick
{
//    MyVideoViewController *myVideoVC = [[MyVideoViewController alloc] init];
    MyWelfareWebViewController *myWelfareVC = [[MyWelfareWebViewController alloc] init];
    [self.navigationController pushViewController:myWelfareVC animated:YES];
}
/** 客房服务 */
- (void)guestBtnClick
{
    WebListingViewController *guestVC = [[WebListingViewController alloc] init];
    guestVC.listURL = [NSString stringWithFormat:@"%@users/roomServiceList.html",HTML_URL_Base];
    [self.navigationController pushViewController:guestVC animated:YES];
}
/** 商品收藏 */
- (void)goodsBtnClick {
    ProductCollectionViewController *productVC = [[ProductCollectionViewController alloc]init];
    [self.navigationController pushViewController:productVC animated:YES];
}
/** 店铺收藏 */
- (void)storeBtnClick {
    StoreCollectionViewController *productVC = [[StoreCollectionViewController alloc]init];
    [self.navigationController pushViewController:productVC animated:YES];
}
/** 客服反馈 */
- (void)suggestBtnClick
{
    FeedbackViewController *feedbackVC = [[FeedbackViewController alloc] init];
    [self.navigationController pushViewController:feedbackVC animated:YES];
}
/** 浏览记录 */
- (void)browseBtnClick {
    ScanViewController *scanVC = [[ScanViewController alloc]init];
    [self.navigationController pushViewController:scanVC animated:YES];
}
/** 账户设置 */
- (void)settingUpBtnClick {
    SettingUpViewController *settingUpVC = [[SettingUpViewController alloc] init];
    [self.navigationController pushViewController:settingUpVC animated:YES];
}

#pragma mark -网络请求
// 用户信息
- (void)requestNetUserInfoAcount {
    [[BDNetworkTools sharedInstance] getUserAcountWithBlock:^(NSDictionary *responseObject, NSError *error) {
        UserInfoItem *userItem = [UserInfoItem userInfoDict:responseObject];
        _headerView.item = userItem;
        _contentView.item = userItem;
    }];
}

@end
