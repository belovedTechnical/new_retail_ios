//
//  CouponCell.h
//  BelovedHotel
//
//  Created by BDSir on 2017/9/6.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CouponModel;
@interface CouponCell : UITableViewCell

/** 优惠券是否可用 */
@property(nonatomic, assign) BOOL isNoCoupon;
/** 立即使用Block */
@property(nonatomic, strong) void(^immediatelyUseBlock)(NSString *userCouponId, NSString *name, NSString *amount);

@property(nonatomic, strong) CouponModel *couponModel;

@end
