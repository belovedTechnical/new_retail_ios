//
//  CouponCell.m
//  BelovedHotel
//
//  Created by BDSir on 2017/9/6.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "CouponCell.h"
#import "CouponModel.h"

@interface CouponCell()
/** 背景图片 */
@property(nonatomic, strong) UIImageView *bgImageView;
/** 立即使用 */
@property(nonatomic, strong) UIButton *useBtn;
/** 标题 */
@property(nonatomic, strong) UILabel *titleLabel;
/** 金额 */
@property(nonatomic, strong) UILabel *moneyLabel;
/** 可使用范围说明 */
@property(nonatomic, strong) UILabel *ruleLabel;
/** 有效期 */
@property(nonatomic, strong) UILabel *availabilityLabel;
/** 时间 */
@property(nonatomic, strong) UILabel *dateLabel;
/** 优惠券ID */
@property(nonatomic, strong) NSString *couponID;
/** 状态图片 */
@property(nonatomic, strong) UIImageView *statusImageView;
/** 日期相差总和 */
@property (nonatomic, assign) NSInteger summationCmps;
/** 当前cellID */
@property(nonatomic, strong) NSString *currentlyCellID;

@end

static NSString *const couponCellID = @"couponCellID";
static NSString *const noCouponCellID = @"noCouponCellID";
@implementation CouponCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.currentlyCellID = reuseIdentifier;
        [self couponIsNoWithCellID:reuseIdentifier];
    }
    return self;
}

// 模型赋值
- (void)setCouponModel:(CouponModel *)couponModel
{
    _couponModel = couponModel;
    self.titleLabel.text = couponModel.name;
    CGFloat money = [couponModel.amount floatValue];
    self.moneyLabel.text = [NSString stringWithFormat:@"%.2f", money];
    self.ruleLabel.text = couponModel.guide;
    NSString *useValidTime = [couponModel.useValidTime substringToIndex:10];
    NSString *useDeadTime = [couponModel.useDeadTime substringToIndex:10];
    self.dateLabel.text = [NSString stringWithFormat:@"%@-%@", useValidTime, useDeadTime];
    
    if (_currentlyCellID == noCouponCellID) {
        // notuse:已过期 overdue:不满足使用 use:已使用
        NSComparisonResult relust = [self timeCompareWithDateTime:couponModel.useDeadTime];
        if (relust == NSOrderedAscending) {
            [self addStatusMarkIconWithImageName:@"notuse"];
            
        }else if (relust == NSOrderedDescending) {
            [self addStatusMarkIconWithImageName:@"use"];
        }
    }
}

#pragma mark -时间比较
- (NSComparisonResult)timeCompareWithDateTime:(NSString *)dateTime
{
    NSString *str = dateTime;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *date = [formatter dateFromString:str];
    NSDate *now = [NSDate date];
    NSComparisonResult relust = [date compare:now];
    return relust;
}

// 添加标签图片
- (void)addStatusMarkIconWithImageName:(NSString *)imageName
{
    // 优惠劵状态标签
    UIImageView *statusImageView = [[UIImageView alloc] init];
    statusImageView.image = [UIImage imageNamed:imageName];
    [self.bgImageView addSubview:statusImageView];
    
    [statusImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.bgImageView.mas_right).offset(-40);
    }];
}

- (void)couponIsNoWithCellID:(NSString *)cellID
{
    if (cellID == couponCellID) {
        [self setupUI:@"coupon"];
    }else {
        [self setupUI:@"noCoupon"];
    
        // 蒙版View
        UIView *envelopView = [[UIView alloc] init];
        envelopView.backgroundColor = [UIColor darkGrayColor];
        envelopView.alpha = 0.3;
        [self addSubview:envelopView];
        
        [envelopView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.centerY.equalTo(self.bgImageView);
            make.width.height.equalTo(self.bgImageView);
        }];
    }
}

- (void)setupUI:(NSString *)bgImage
{
    //背景图片View
    UIImageView *bgImageView = [[UIImageView alloc] init];
    bgImageView.image = [UIImage imageNamed:bgImage];
    bgImageView.userInteractionEnabled = YES;
    [self addSubview:bgImageView];
    self.bgImageView = bgImageView;
    
    // 优惠券标题
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"元宵节福利红包";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont systemFontOfSize:12];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [bgImageView addSubview:titleLabel];
    self.titleLabel = titleLabel;
    
    // 金额
    UILabel *moneyLabel = [[UILabel alloc] init];
    moneyLabel.text = @"¥100.00";
    moneyLabel.textColor = [UIColor whiteColor];
    moneyLabel.font = [UIFont systemFontOfSize:30];
    moneyLabel.textAlignment = NSTextAlignmentCenter;
    [bgImageView addSubview:moneyLabel];
    self.moneyLabel = moneyLabel;
    
    // 可使用规则
    UILabel *ruleLabel = [[UILabel alloc] init];
    ruleLabel.text = @"满500元可用 全平台通用";
    ruleLabel.textColor = [UIColor whiteColor];
    ruleLabel.font = [UIFont systemFontOfSize:12];
    ruleLabel.textAlignment = NSTextAlignmentCenter;
    [bgImageView addSubview:ruleLabel];
    self.ruleLabel = ruleLabel;
    
    // 有效期
    UILabel *availabilityLabel = [[UILabel alloc] init];
    availabilityLabel.text = @"有效期";
    availabilityLabel.textColor = [UIColor whiteColor];
    availabilityLabel.font = [UIFont systemFontOfSize:12];
    availabilityLabel.textAlignment = NSTextAlignmentCenter;
    [bgImageView addSubview:availabilityLabel];
    self.availabilityLabel = availabilityLabel;
    
    // 时间
    UILabel *dateLabel = [[UILabel alloc] init];
    dateLabel.text = @"2016.00.02-2017.00.22";
    dateLabel.textColor = [UIColor whiteColor];
    dateLabel.font = [UIFont systemFontOfSize:8];
    dateLabel.textAlignment = NSTextAlignmentCenter;
    [bgImageView addSubview:dateLabel];
    self.dateLabel = dateLabel;
    
    // 立即使用
    UIButton *useBtn = [[UIButton alloc] init];
    [useBtn setTitle:@"立即使用" forState:UIControlStateNormal];
    [useBtn.layer setMasksToBounds:YES];
    [useBtn.layer setCornerRadius:15];
    [useBtn.layer setBorderWidth:1];
    [useBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
    [useBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    useBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [useBtn addTarget:self action:@selector(immediatelyUseClick) forControlEvents:UIControlEventTouchUpInside];
    [bgImageView addSubview:useBtn];
    self.useBtn = useBtn;
    
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(10);
        make.bottom.equalTo(self.mas_bottom).offset(-10);
        make.left.equalTo(self.mas_left).offset(10);
        make.right.equalTo(self.mas_right).offset(-10);
    }];
    
    CGFloat width = (kScreenW - 20) / 2;
    CGFloat cellHeight = 120;
    [moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bgImageView.mas_centerY);
        make.left.equalTo(bgImageView.mas_left).offset(20);
        make.height.equalTo(@((cellHeight-20) / 3));
        make.width.equalTo(@(width));
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgImageView.mas_top).offset(10);
        make.bottom.equalTo(moneyLabel.mas_top).offset(0);
        make.left.equalTo(moneyLabel.mas_left);
        make.width.equalTo(@(width));
    }];
    
    [ruleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(moneyLabel.mas_bottom).offset(0);
        make.bottom.equalTo(bgImageView.mas_bottom).offset(-10);
        make.left.equalTo(moneyLabel.mas_left);
        make.width.equalTo(@(width));
    }];
    
    [availabilityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgImageView.mas_top).offset(20);
        make.right.equalTo(bgImageView.mas_right).offset(0);
        make.height.equalTo(@23);
        make.width.equalTo(@(width-20));
    }];
    
    [dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(availabilityLabel.mas_bottom).offset(-5);
        make.right.equalTo(bgImageView.mas_right).offset(0);
        make.height.equalTo(@23);
        make.width.equalTo(@(width-20));
    }];
    
    [useBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(dateLabel.mas_bottom).offset(0);
        make.bottom.equalTo(bgImageView.mas_bottom).offset(-10);
        make.right.equalTo(bgImageView.mas_right).offset(-20);
        make.width.equalTo(@(width/2));
    }];
}

#pragma mark -立即使用
- (void)immediatelyUseClick
{
    if (self.immediatelyUseBlock) {
        self.immediatelyUseBlock(_couponModel.userCouponId, _couponModel.name, _couponModel.amount);
    }
}

@end
