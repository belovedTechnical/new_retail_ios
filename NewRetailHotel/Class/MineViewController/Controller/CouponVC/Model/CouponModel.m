//
//  CouponModel.m
//  BelovedHotel
//
//  Created by BDSir on 2017/9/9.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "CouponModel.h"

@implementation CouponModel

+ (instancetype)couponModelWithDict:(NSDictionary *)dict
{
    CouponModel *couponModel = [[self alloc] init];
    [couponModel setValuesForKeysWithDictionary:dict];
    
    return couponModel;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        self.userCouponId = value;
    }
    
}

@end
