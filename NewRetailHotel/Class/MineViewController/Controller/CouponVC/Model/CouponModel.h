//
//  CouponModel.h
//  BelovedHotel
//
//  Created by BDSir on 2017/9/9.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CouponModel : NSObject

/** 优惠券名称 */
@property(nonatomic, strong) NSString *name;
/** 使用说明 */
@property(nonatomic, strong) NSString *guide;
/** 优惠券ID */
@property(nonatomic, strong) NSString *couponId;
/** 用户优惠券ID */
@property(nonatomic, strong) NSString *userCouponId;
/** 金额 */
@property(nonatomic, strong) NSString *amount;
/** 生效时间 */
@property(nonatomic, strong) NSString *useValidTime;
/** 结束时间 */
@property(nonatomic, strong) NSString *useDeadTime;

+ (instancetype)couponModelWithDict:(NSDictionary *)dict;

@end
