//
//  UseablenessCouponViewController.h
//  BelovedHotel
//
//  Created by BDSir on 2017/9/6.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UseablenessCouponViewController : UITableViewController

/** 是否从我的界面进入 */
@property(nonatomic, assign) BOOL isMeController;
/** 带有商品信息符合使用优惠券 */
@property(nonatomic, strong) NSArray *couponModelArray;

@end
