//
//  UseablenessCouponViewController.m
//  BelovedHotel
//
//  Created by BDSir on 2017/9/6.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "UseablenessCouponViewController.h"
#import "CouponCell.h"
#import "BDNetworkTools.h"
#import "CouponModel.h"
#import "MJRefresh.h"

@interface UseablenessCouponViewController ()

@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, assign) NSInteger pageSize;
/** 是否刷新 */
@property (nonatomic, assign) BOOL isReqfurbish;
@property(nonatomic, strong) NSMutableArray *modelArray;

@end

static NSString *const couponCellID = @"couponCellID";
@implementation UseablenessCouponViewController

#pragma mark -懒加载
- (NSMutableArray *)modelArray
{
    if (!_modelArray) {
        _modelArray = [NSMutableArray array];
    }
    return _modelArray;
}

- (NSArray *)couponModelArray
{
    if (_couponModelArray) {
        _couponModelArray = [NSArray array];
    }
    return _couponModelArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pageIndex = 0;
    self.pageSize = 20;
    self.isReqfurbish = YES;

    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.contentInset = UIEdgeInsetsMake(SafeAreaTopHeight + 35, 0, 49+40, 0);
    self.tableView.rowHeight = 120;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerClass:[CouponCell class] forCellReuseIdentifier:couponCellID];
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self requestNetUseablenessDataWithPageIndex:_pageIndex pageSize:_pageSize];
    [self addRefires];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_isMeController == YES) {
        return _modelArray.count;
    }else {
        return _couponModelArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CouponCell *cell = [tableView dequeueReusableCellWithIdentifier:couponCellID];
    if (cell == nil) {
        cell = [[CouponCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:couponCellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (_isMeController == YES) {
        
        cell.couponModel = _modelArray[indexPath.row];
    }else {
        cell.couponModel = _couponModelArray[indexPath.row];
    }
    // 立即使用
    cell.immediatelyUseBlock = ^(NSString *userCouponId, NSString *name, NSString *amount) {
        if (_isMeController == YES) {
            self.navigationController.tabBarController.selectedIndex = 0;
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else {
            NSDictionary *dict = @{@"userCouponId":userCouponId, @"name":name, @"amount":amount};
            [[NSNotificationCenter defaultCenter] postNotificationName:kbackCouponIDNoti object:nil userInfo:dict];
            [self.navigationController popViewControllerAnimated:YES];
        }
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CouponCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    // 立即使用
    if (_isMeController == YES) {
        self.navigationController.tabBarController.selectedIndex = 0;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else {
        NSDictionary *dict = @{@"userCouponId":cell.couponModel.userCouponId, @"name":cell.couponModel.name, @"amount":cell.couponModel.amount};
        [[NSNotificationCenter defaultCenter] postNotificationName:kbackCouponIDNoti object:nil userInfo:dict];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark -网络请求
- (void)requestNetUseablenessDataWithPageIndex:(NSInteger)pageIndex pageSize:(NSInteger)pageSize
{
    NSDictionary *paramDic = @{@"type":@"available", @"pageIndex":@(pageIndex), @"pageSize":@(pageSize)};
    [[BDNetworkTools sharedInstance] getCouponMeDataWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        if (_isReqfurbish == YES) {
            [self.modelArray removeAllObjects];
        }
        NSMutableArray *tempArr = [NSMutableArray array];
        NSArray *dataArray = [responseObject objectForKey:@"data"];
        for (NSDictionary *dict in dataArray) {
            [tempArr addObject:[CouponModel couponModelWithDict:dict]];
        }
        self.modelArray = tempArr;
        [self.tableView reloadData];
        
        [self.tableView.mj_header endRefreshing];
        if (dataArray.count < 20) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }else {
            [self.tableView.mj_footer endRefreshing];
        }
    }];
}

#pragma mark -添加上拉加载下拉更新
- (void)addRefires
{
    BDWeakSelf();
    // 下拉刷新
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.isReqfurbish = YES;
        [weakSelf requestNetUseablenessDataWithPageIndex:weakSelf.pageIndex pageSize:weakSelf.pageSize];
    }];
    
    // 上拉加载
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.isReqfurbish = NO;
        weakSelf.pageIndex++;
        [weakSelf requestNetUseablenessDataWithPageIndex:weakSelf.pageIndex pageSize:weakSelf.pageSize];
    }];
}






@end
