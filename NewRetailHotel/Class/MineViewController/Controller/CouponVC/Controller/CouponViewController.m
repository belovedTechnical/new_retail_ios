//
//  CouponViewController.m
//  BelovedHotel
//
//  Created by BDSir on 2017/9/6.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "CouponViewController.h"
#import "UseablenessCouponViewController.h"
#import "NoCouponViewController.h"

@interface CouponViewController ()

@property(nonatomic, strong) UIButton *noUseBtn;
@property(nonatomic, strong) UIView *bottomView;

@end

@implementation CouponViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的优惠劵";
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.tabBarController.tabBar.hidden = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_return"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    
    [self setupAllChildViewConroller];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bottomeBtnIsHide:) name:@"couponSelectNoti" object:nil];
}

// 移除监听通知
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -布局界面
- (void)setupAllChildViewConroller
{
    UseablenessCouponViewController *useablenessVC = [[UseablenessCouponViewController alloc] init];
    useablenessVC.title = @"可用优惠劵";
    useablenessVC.isMeController = _isMeController;
    useablenessVC.couponModelArray = _couponModelArray;
    [self addChildViewController:useablenessVC];
    
    NoCouponViewController *noCouponVC = [[NoCouponViewController alloc] init];
    noCouponVC.title = @"不可用优惠券";
    noCouponVC.isMeController = _isMeController;
    noCouponVC.notUsedCouponModelArray = _notUsedCouponModelArray;
    [self addChildViewController:noCouponVC];
    
    // 中心分割线
    UIView *centerLine = [[UIView alloc] initWithFrame:CGRectMake(kScreen_Width/2, 66, 2, 32)];
    centerLine.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:centerLine];
    // 底部按钮
    [self setupBottomNoUseBtn];
}

#pragma mark -底部不使用按钮
- (void)setupBottomNoUseBtn
{
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, kScreen_Height - 60, kScreen_Width, 60)];
    bottomView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:bottomView];
    self.bottomView = bottomView;
    
    CGFloat space = 40;
    CGFloat noUseHeight = 40;
    UIButton *noUseBtn = [[UIButton alloc] initWithFrame:CGRectMake(space, kHeightCoefficient- noUseHeight-10, kScreen_Width - space*2, noUseHeight)];
    [noUseBtn setTitle:@"不使用优惠券" forState:UIControlStateNormal];
    [noUseBtn.layer setBorderColor:[UIColor redColor].CGColor];
    [noUseBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [noUseBtn.layer setBorderWidth:1];
    [noUseBtn addTarget:self action:@selector(noUseBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:noUseBtn];
    self.noUseBtn = noUseBtn;
}

#pragma mark -内部控方法
- (void)noUseBtnClick
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kNoBackCouponIDNoti object:nil userInfo:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)bottomeBtnIsHide:(NSNotification *)noti
{
    NSString *btntitle = noti.userInfo[@"btnTitle"];
    
    if ([btntitle isEqualToString:@"可用优惠劵"]) {
        self.noUseBtn.hidden = NO;
        self.bottomView.hidden = NO;
    }
    if ([btntitle isEqualToString:@"不可用优惠券"]) {
        self.noUseBtn.hidden = YES;
        self.bottomView.hidden = YES;
    }
}

#pragma mark -返回按钮
- (void)backAction{
    
    [self.navigationController popViewControllerAnimated:YES];
}
















@end
