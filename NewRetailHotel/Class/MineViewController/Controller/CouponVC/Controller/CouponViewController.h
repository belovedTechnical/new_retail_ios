//
//  CouponViewController.h
//  BelovedHotel
//
//  Created by BDSir on 2017/9/6.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "BDBaseMenuViewController.h"

@interface CouponViewController : BDBaseMenuViewController

@property(nonatomic, assign) BOOL isMeController;
/** 带有商品信息符合使用优惠券 */
@property(nonatomic, strong) NSArray *couponModelArray;
/** 带有商品信息不可使用优惠劵 */
@property(nonatomic, strong) NSArray *notUsedCouponModelArray;

@end
