//
//  NoCouponViewController.m
//  BelovedHotel
//
//  Created by BDSir on 2017/9/6.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "NoCouponViewController.h"
#import "CouponCell.h"
#import "BDNetworkTools.h"
#import "CouponModel.h"

#import "MJRefresh.h"

@interface NoCouponViewController ()

@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, assign) NSInteger pageSize;
/** 是否刷新 */
@property (nonatomic, assign) BOOL isReqfurbish;
@property(nonatomic, strong) CouponModel *couponModel;
@property(nonatomic, strong) NSMutableArray *modelArray;

@end

static NSString *const noCouponCellID = @"noCouponCellID";
@implementation NoCouponViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pageIndex = 0;
    self.pageSize = 20;
    self.isReqfurbish = YES;
    
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.contentInset = UIEdgeInsetsMake(SafeAreaTopHeight + 35, 0, 49, 0);
    self.tableView.rowHeight = 120;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerClass:[CouponCell class] forCellReuseIdentifier:noCouponCellID];
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self requestNetUseablenessDataWithPageIndex:_pageIndex pageSize:_pageSize];
    [self addRefires];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_isMeController == YES) {
        
        return _modelArray.count;
    }else {
        return _notUsedCouponModelArray.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CouponCell *cell = [tableView dequeueReusableCellWithIdentifier:noCouponCellID];
    if (cell == nil) {
        cell = [[CouponCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noCouponCellID];
        cell.isNoCoupon = NO;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (_isMeController == YES) {
        cell.couponModel = _modelArray[indexPath.row];
    }else {
        cell.couponModel = _notUsedCouponModelArray[indexPath.row];
    }
    
    return cell;
}

#pragma mark -网络请求
- (void)requestNetUseablenessDataWithPageIndex:(NSInteger)pageIndex pageSize:(NSInteger)pageSize
{
    NSDictionary *paramDic = @{@"type":@"unavailable", @"pageIndex":@(pageIndex), @"pageSize":@(pageSize)};
    [[BDNetworkTools sharedInstance] getCouponMeDataWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        if (_isReqfurbish == YES) {
            [self.modelArray removeAllObjects];
        }
        
        NSMutableArray *tempArr = [NSMutableArray array];
        NSArray *dataArray = [responseObject objectForKey:@"data"];
        for (NSDictionary *dict in dataArray) {
            [tempArr addObject:[CouponModel couponModelWithDict:dict]];
        }
        self.modelArray = tempArr;
        [self.tableView reloadData];
        
        [self.tableView.mj_header endRefreshing];
        if (dataArray.count < 20) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }else {
            [self.tableView.mj_footer endRefreshing];
        }
    }];
}

#pragma mark -添加上拉加载下拉更新
- (void)addRefires
{
    BDWeakSelf();
    // 下拉刷新
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.isReqfurbish = YES;
        [weakSelf requestNetUseablenessDataWithPageIndex:weakSelf.pageIndex pageSize:weakSelf.pageSize];
    }];
    
    // 上拉加载
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.isReqfurbish = NO;
        weakSelf.pageIndex++;
        [weakSelf requestNetUseablenessDataWithPageIndex:weakSelf.pageIndex pageSize:weakSelf.pageSize];
    }];
}

#pragma mark -懒加载
- (NSMutableArray *)modelArray
{
    if (!_modelArray) {
        _modelArray = [NSMutableArray array];
    }
    return _modelArray;
}





@end
