//
//  AccoutManageViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/8.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "AccoutManageViewController.h"
#import "UpdateNameViewController.h"
#import "SafetyAccoutViewController.h"
#import "AddressManageViewController.h"

@interface AccoutManageViewController ()

@property (nonatomic, strong) NSArray *oneTableViewArray;
@property (nonatomic, strong) NSArray *twoTableViewArray;

@end

@implementation AccoutManageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"账户管理";
    [self setupTableFooterView];
    self.tableView.rowHeight = 50;
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (section == 0) {
        return 2;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    NSArray *oneSection = @[@"个人信息",@"地址管理"];
    NSArray *twoSection = @[@"账户安全",];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    cell.textLabel.textColor = [UIColor colorWithHexString:@"#333333"];
    if (indexPath.section == 0) {
        cell.textLabel.text = oneSection[indexPath.row];
    }else if (indexPath.section == 1) {
        cell.textLabel.text = twoSection[indexPath.row];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            UpdateNameViewController *updateVC = [[UpdateNameViewController alloc] init];
            [self.navigationController pushViewController:updateVC animated:YES];
        }else
        {
            AddressManageViewController *addressVC = [[AddressManageViewController alloc]init];
            [self.navigationController pushViewController:addressVC animated:YES];
        }
    }else if (indexPath.section == 1) {
        SafetyAccoutViewController *safetyVC = [[SafetyAccoutViewController alloc] init];
        [self.navigationController pushViewController:safetyVC animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (void)setupTableFooterView
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 50)];
    UIButton *btnLogout = [[UIButton alloc] init];
    [btnLogout setTitle:@"注销登录" forState:UIControlStateNormal];
    [btnLogout setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
    [btnLogout addTarget:self action:@selector(signOutBtnClick) forControlEvents:UIControlEventTouchUpInside];
    btnLogout.titleLabel.textAlignment = NSTextAlignmentCenter;
    btnLogout.titleLabel.font = [UIFont systemFontOfSize:15];
    btnLogout.backgroundColor = [UIColor whiteColor];
    btnLogout.frame = footerView.bounds;
    [footerView addSubview:btnLogout];
    self.tableView.tableFooterView = footerView;
}

#pragma mark -内部控制方法
- (void)signOutBtnClick
{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:@"确定退出？" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alert1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self signOutNetwork];
    }];
    UIAlertAction *alert2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alertVC addAction:alert1];
    [alertVC addAction:alert2];
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark -网络请求
- (void)signOutNetwork
{
    [[BDNetworkTools sharedInstance] getLoginSigOutWithBlock:^(NSDictionary *responseObject, NSError *error) {
        if ([[responseObject objectForKey:@"errmsg"] isEqualToString:@"ok"]) {
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            [defaults removeObjectForKey:apikey];
            [self.navigationController popToRootViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:LogoutNoti object:nil];
        }
    }];
}

@end
