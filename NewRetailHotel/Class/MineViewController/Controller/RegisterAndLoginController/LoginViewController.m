//
//  LoginViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/4.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "LoginViewController.h"
#import "RetrievePasswordViewController.h"
#import "NoteLoginViewController.h"
#import "RegisterViewController.h"

#import "PasswordLoginView.h"
#import "SaveTool.h"

@interface LoginViewController ()

@property (nonatomic, strong) PasswordLoginView *passwordView;

@end

@implementation LoginViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //self.navigationController.navigationBarHidden = NO;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self initUI];
}

- (void)initUI
{
    self.title = @"密码登录";
    _passwordView = [[PasswordLoginView alloc] initWithFrame:CGRectMake(0, kNavBarHeight, kScreenW, kScreenH)];
    [self.view addSubview:_passwordView];
    [_passwordView.noteBtn addTarget:self action:@selector(noteBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [_passwordView.loginBtn addTarget:self action:@selector(loginBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [_passwordView.retrieveBtn addTarget:self action:@selector(retrieveBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [_passwordView.registerBtn addTarget:self action:@selector(registerBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
}

#pragma mark -按钮点击监听
// 登录按钮点击
- (void)loginBtnClick
{
    [self.view endEditing:YES];
    _passwordView.superview.frame = _passwordView.superview.bounds;
    [self requestNetPasswordLogin];
}

// 短信登录
- (void)noteBtnClick
{
    NoteLoginViewController *loginVC = [[NoteLoginViewController alloc] init];
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.navigationController.childViewControllers];
    [array removeLastObject];
    [array addObject:loginVC];
    [self.navigationController setViewControllers:array animated:true];
}

// 忘记密码
- (void)retrieveBtnClick
{
    RetrievePasswordViewController *retrieveVC = [[RetrievePasswordViewController alloc] init];
    [self.navigationController pushViewController:retrieveVC animated:YES];
}

// 注册
- (void)registerBtnClick
{
    RegisterViewController *registerVC = [[RegisterViewController alloc] init];
    [self.navigationController pushViewController:registerVC animated:YES];
}

- (void)backClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -网络请求
- (void)requestNetPasswordLogin
{
    NSDictionary *parameters = @{@"username":_passwordView.phoneF.text , @"password":_passwordView.passwordF.text};
    [[BDNetworkTools sharedInstance] postPasswordLoginWihtParameters:parameters Block:^(NSDictionary *responseObject, NSError *error) {
        if ([[responseObject objectForKey:@"errmsg"] isEqualToString:@"ok"]) {
            [self.navigationController popViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:succeedLoginNoti object:nil];
        }else {
            [self showAlertWithTitle:@"提示" message:[NSString stringWithFormat:@"%@", [responseObject objectForKey:@"errmsg"]] AscertainHandler:nil];
        }
    }];
}

@end
