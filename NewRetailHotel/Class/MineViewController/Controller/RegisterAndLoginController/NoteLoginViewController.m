//
//  NoteLoginViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/4.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "NoteLoginViewController.h"
#import "NoteLoginView.h"
#import "RetrievePasswordViewController.h"
#import "RegisterViewController.h"
#import "LoginViewController.h"

@interface NoteLoginViewController ()

@property (nonatomic, strong) NoteLoginView *noteLoginView;

@end

@implementation NoteLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.view.backgroundColor = [UIColor whiteColor];
    // self.navigationController.navigationBar.hidden = YES;
    [self initUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // self.navigationController.navigationBarHidden = NO;
    
}

- (void)initUI
{
    
    self.title = @"手机验证码登录";
    _noteLoginView = [[NoteLoginView alloc] initWithFrame:CGRectMake(0, kNavBarHeight, kScreenW, kScreenH)];
    [_noteLoginView.checkingBtn addTarget:self action:@selector(requestNetSendChecking) forControlEvents:UIControlEventTouchUpInside];
    [_noteLoginView.loginBtn addTarget:self action:@selector(requestNetLogin) forControlEvents:UIControlEventTouchUpInside];
    
    
    [_noteLoginView.passwordBtn addTarget:self action:@selector(passwordBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [_noteLoginView.retrieveBtn addTarget:self action:@selector(retrieveBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [_noteLoginView.registerBtn addTarget:self action:@selector(registerBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_noteLoginView];
    
}

#pragma mark -网络请求
// 获取验证码
- (void)requestNetSendChecking
{
    [[BDNetworkTools sharedInstance] postLoginSendCodeWihtProduct:_noteLoginView.phoneF.text Block:^(NSDictionary *responseObject, NSError *error) {
        if ([[responseObject objectForKey:@"errmsg"] isEqualToString:@"ok"]) {
            [BDShowHUD showMBPShowTime:1 stateLabel:@"验证码发送成功"];
        }else {
            [self showAlertWithTitle:@"提示" message:[NSString stringWithFormat:@"%@", [responseObject objectForKey:@"errmsg"]] AscertainHandler:nil];
        }
    }];
}

// 立即登录
- (void)requestNetLogin
{
    [self.view endEditing:YES];
    _noteLoginView.superview.frame = _noteLoginView.superview.bounds;
    NSDictionary *parameters = @{@"username":_noteLoginView.phoneF.text , @"verifyCode":_noteLoginView.checkingF.text};
    [[BDNetworkTools sharedInstance] postLoginSignInWihtParameters:parameters Block:^(NSDictionary *responseObject, NSError *error) {
        if ([[responseObject objectForKey:@"errmsg"] isEqualToString:@"ok"]) {
            [self.navigationController popViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:succeedLoginNoti object:nil];
        }else {
            [self showAlertWithTitle:@"提示" message:[NSString stringWithFormat:@"%@", [responseObject objectForKey:@"errmsg"]] AscertainHandler:nil];            
        }
    }];
}

// 密码登录
- (void)passwordBtnClick
{
    LoginViewController *loginVC = [[LoginViewController alloc] init];
    // NSMutableArray *array = [NSMutableArray arrayWithArray:self.navigationController.childViewControllers];
    // [array removeLastObject];
    // [array addObject:loginVC];
    // [self.navigationController setViewControllers:array animated:true];
    [self.navigationController pushViewController:loginVC animated:true];

}

// 忘记密码
- (void)retrieveBtnClick
{
    RetrievePasswordViewController *retrieveVC = [[RetrievePasswordViewController alloc] init];
    [self.navigationController pushViewController:retrieveVC animated:YES];
}

// 注册
- (void)registerBtnClick
{
    RegisterViewController *registerVC = [[RegisterViewController alloc] init];
    [self.navigationController pushViewController:registerVC animated:YES];
}

- (void)backClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
