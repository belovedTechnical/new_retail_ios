//
//  RetrievePasswordViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/4.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "RetrievePasswordViewController.h"
#import "RetrievePasswordView.h"

@interface RetrievePasswordViewController ()

@property (nonatomic, strong) RetrievePasswordView *retrievePasswordView;

@end

@implementation RetrievePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self initUI];
}

- (void)initUI
{
    self.title = @"忘记密码";
    _retrievePasswordView = [[RetrievePasswordView alloc] initWithFrame:CGRectMake(0, kNavBarHeight, kScreenW, kScreenH)];
    [_retrievePasswordView.checkingBtn addTarget:self action:@selector(requestNetSendChecking) forControlEvents:UIControlEventTouchUpInside];
    [_retrievePasswordView.loginBtn addTarget:self action:@selector(requestNetResetPassword) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_retrievePasswordView];
    
    UIButton *backBtn = [[UIButton alloc] init];
    backBtn.frame = CGRectMake(0, StatusBarHeight, 44, 44);
    [backBtn setImage:[UIImage imageNamed:@"icon_return"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
}

#pragma mark -网络请求
- (void)requestNetSendChecking
{
    [[BDNetworkTools sharedInstance] postLoginSendCodeWihtProduct:_retrievePasswordView.phoneF.text Block:^(NSDictionary *responseObject, NSError *error) {

    }];
}

- (void)requestNetResetPassword
{
    [self.view endEditing:YES];
    _retrievePasswordView.superview.frame = _retrievePasswordView.superview.bounds;
    NSDictionary *parameters = @{@"username":_retrievePasswordView.phoneF.text,
                                 @"verifyCode":_retrievePasswordView.checkingF.text,
                                 @"newPassword":_retrievePasswordView.verifyF.text,
                                 @"afterAutoLogin": @"true"
                                 };
    [[BDNetworkTools sharedInstance] postResetPasswordWihtParameters:parameters Block:^(NSDictionary *responseObject, NSError *error) {
        if ([[responseObject objectForKey:@"errmsg"] isEqualToString:@"ok"]) {
            [self.navigationController popToRootViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:succeedLoginNoti object:nil];
        } else {
            [self showAlertWithTitle:@"提示" message:[NSString stringWithFormat:@"%@", [responseObject objectForKey:@"errmsg"]] AscertainHandler:nil];
        }
    }];
}

- (void)backClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
