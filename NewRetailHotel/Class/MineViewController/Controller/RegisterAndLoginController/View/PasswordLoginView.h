//
//  PasswordLoginView.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/4.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PasswordLoginView : UIView

@property (nonatomic, strong) UITextField *phoneF;
@property (nonatomic, strong) UITextField *passwordF;
@property (nonatomic, strong) UIButton *loginBtn;
@property (nonatomic, strong) UIButton *noteBtn;
@property (nonatomic, strong) UIButton *retrieveBtn;
@property (nonatomic, strong) UIButton *registerBtn;

@end
