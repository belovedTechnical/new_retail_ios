//
//  RegisterView.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/4.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterView : UIView

@property (nonatomic, strong) UITextField *phoneF;
@property (nonatomic, strong) UITextField *checkingF;
@property (nonatomic, strong) UITextField *setPasswordF;
@property (nonatomic, strong) UIButton *checkingBtn;
@property (nonatomic, strong) UIButton *loginBtn;

@end
