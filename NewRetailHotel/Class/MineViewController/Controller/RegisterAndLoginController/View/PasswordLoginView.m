//
//  PasswordLoginView.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/4.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "PasswordLoginView.h"
#import "BDMobileNumberTools.h"
#import "UIButton+TitleAndIcon.h"

@interface PasswordLoginView()<UITextFieldDelegate>

@end

@implementation PasswordLoginView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    
    UILabel *titleLb = [[UILabel alloc] init];
    titleLb.frame = CGRectMake(0, 59, kScreenW, 25);
    titleLb.text = @"登录";
    titleLb.font = [UIFont systemFontOfSize:25];
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.textColor = [UIColor colorWithHexString:@"#444444"];
    [self addSubview:titleLb];
    
    
    CGFloat phoneX = 25;
    CGFloat phoneY = titleLb.bd_y + titleLb.bd_height + 53;
    CGFloat phoneH = 60;
    CGFloat titleLblW = 60;
    CGFloat phoneW = kScreenW - phoneX * 2 - titleLblW;
    
    UILabel *phoneLbl = [[UILabel alloc] initWithFrame:CGRectMake(phoneX, phoneY, titleLblW, phoneH)];
    phoneLbl.font = [UIFont systemFontOfSize:17];
    phoneLbl.text = @"手机号";
    phoneLbl.textColor = UIColor.grayColor;
    [self addSubview:phoneLbl];
    
    UITextField *phoneF = [[UITextField alloc] initWithFrame:CGRectMake(phoneLbl.bd_right, phoneY, phoneW, phoneH)];
//    [phoneF addTarget:self action:@selector(fieldChange) forControlEvents:UIControlEventEditingChanged];
    [phoneF addTarget:self action:@selector(phoneTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    phoneF.textColor = [UIColor colorWithHexString:@"#333333"];
    phoneF.clearButtonMode = UITextFieldViewModeAlways;
    phoneF.keyboardType = UIKeyboardTypeNumberPad;
    phoneF.font = [UIFont systemFontOfSize:17];
    phoneF.placeholder = @"请输入11位手机号";
    phoneF.delegate = self;
    UIView *onLine = [[UIView alloc] initWithFrame:CGRectMake(0, phoneH -0.5, phoneW, 0.5)];
    onLine.backgroundColor = [UIColor colorWithHexString:@"#e1e1e1"];
    [phoneF addSubview:onLine];
    [self addSubview:phoneF];
    self.phoneF = phoneF;
    
    
    UILabel *pswdLbl = [[UILabel alloc] initWithFrame:CGRectMake(phoneX, phoneY + phoneH, titleLblW, phoneH)];
    pswdLbl.font = [UIFont systemFontOfSize:17];
    pswdLbl.text = @"密  码";
    pswdLbl.textColor = UIColor.grayColor;
    [self addSubview:pswdLbl];
    
    UITextField *passwordF = [[UITextField alloc] initWithFrame:CGRectMake(pswdLbl.bd_right, phoneY + phoneH, phoneW, phoneH)];
    [passwordF addTarget:self action:@selector(fieldChange) forControlEvents:UIControlEventEditingChanged];
    passwordF.textColor = [UIColor colorWithHexString:@"#333333"];
    passwordF.keyboardType = UIKeyboardTypeASCIICapable;
    passwordF.font = [UIFont systemFontOfSize:17];
    passwordF.placeholder = @"请输入密码";
    passwordF.secureTextEntry = YES;
    passwordF.delegate = self;
    UIView *pasLine = [[UIView alloc] initWithFrame:CGRectMake(0, passwordF.frame.size.height - 0.5, phoneW, 0.5)];
    pasLine.backgroundColor = [UIColor colorWithHexString:@"#e1e1e1"];
    [passwordF addSubview:pasLine];
    [self addSubview:passwordF];
    self.passwordF = passwordF;
    
    CGFloat lookW = passwordF.bd_height;
    UIButton *lookBtn = [[UIButton alloc] initWithFrame:CGRectMake(passwordF.frame.size.width - lookW, (phoneH - lookW)/2, lookW, lookW)];
    [lookBtn addTarget:self action:@selector(lookBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [lookBtn setImage:[UIImage imageNamed:@"icon_password_hide"] forState:UIControlStateNormal];
    [lookBtn setImage:[UIImage imageNamed:@"icon_password_show"] forState:UIControlStateSelected];
    [self.passwordF addSubview:lookBtn];

    UIButton *loginBtn = [[UIButton alloc] initWithFrame:CGRectMake(phoneX, passwordF.frame.origin.y + phoneH + 35, kScreenW - 2 * phoneX, 44)];
    [loginBtn setTitleColor:[UIColor colorWithHexString:@"#ffffff"] forState:UIControlStateNormal];
    [loginBtn setTitle:@"登录" forState:UIControlStateNormal];
    loginBtn.titleLabel.font = [UIFont systemFontOfSize:17];
    loginBtn.backgroundColor = [UIColor colorWithHexString:@"#a6926b"];
    loginBtn.layer.cornerRadius = 4.0;
    loginBtn.layer.masksToBounds = YES;
    loginBtn.userInteractionEnabled = NO;
    [loginBtn setAlpha:0.4];
    [self addSubview:loginBtn];
    self.loginBtn = loginBtn;
    
    
    // 分割线部分
    CGFloat seperatorX = phoneX;
    CGFloat seperatorY = kScreenH - 90.0 - NavigationHeight;
    CGFloat seperatorW = (kScreenW - 2 * phoneX - 60.0) / 2.0;
    UILabel *leftSeperatorLbl = [[UILabel alloc] initWithFrame:CGRectMake(seperatorX, seperatorY, seperatorW, 1.0)];
    leftSeperatorLbl.backgroundColor = [UIColor colorWithHexString:@"#e1e1e1"];
    [self addSubview:leftSeperatorLbl];
    
    UILabel *centerSeperatorLbl = [[UILabel alloc] initWithFrame:CGRectMake(leftSeperatorLbl.bd_right, seperatorY - 20, 60, 40)];
    centerSeperatorLbl.text = @"更多";
    centerSeperatorLbl.textAlignment = NSTextAlignmentCenter;
    centerSeperatorLbl.textColor = [UIColor colorWithHexString:@"#e1e1e1"];
    centerSeperatorLbl.font = [UIFont systemFontOfSize:10.0];
    [self addSubview:centerSeperatorLbl];
    
    UILabel *rightSeperatorLbl = [[UILabel alloc] initWithFrame:CGRectMake(centerSeperatorLbl.bd_right, seperatorY, seperatorW, 1.0)];
    rightSeperatorLbl.backgroundColor = [UIColor colorWithHexString:@"#e1e1e1"];
    [self addSubview:rightSeperatorLbl];
    
    // 底部按钮
    UIStackView *stackView = [[UIStackView alloc] initWithFrame:CGRectMake(0, kScreenH - 66.0 - NavigationHeight, kScreenW, 66)];
    stackView.axis = UILayoutConstraintAxisHorizontal;
    stackView.distribution = UIStackViewDistributionFillEqually;
    stackView.alignment = UIStackViewAlignmentCenter;
    [self addSubview:stackView];
    
    UIButton *noteBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kScreenW / 3.0, 60)];
    [noteBtn setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
    noteBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [noteBtn setTitle:@"短信登录" forState:UIControlStateNormal];
    [noteBtn setImage:[UIImage imageNamed:@"icon_message"] forState:UIControlStateNormal];
    noteBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [noteBtn setupImageAndTitleEdge];
    [stackView addArrangedSubview:noteBtn];
    self.noteBtn = noteBtn;
    
    UIButton *retrieveBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kScreenW / 3.0, 60)];
    [retrieveBtn setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
    [retrieveBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    [retrieveBtn setTitle:@"忘记密码" forState:UIControlStateNormal];
    [retrieveBtn setImage:[UIImage imageNamed:@"icon_question"] forState:UIControlStateNormal];
    retrieveBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [retrieveBtn setupImageAndTitleEdge];
    [stackView addArrangedSubview:retrieveBtn];
    self.retrieveBtn = retrieveBtn;
    
    UIButton *registerBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kScreenW / 3.0, 60)];
    [registerBtn setTitleColor:[UIColor colorWithHexString:@"#a6926b"] forState:UIControlStateNormal];
    [registerBtn setTitle:@"注册" forState:UIControlStateNormal];
    [registerBtn setImage:[UIImage imageNamed:@"icon_register"] forState:UIControlStateNormal];
    registerBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [registerBtn setupImageAndTitleEdge];
    [stackView addArrangedSubview:registerBtn];
    self.registerBtn = registerBtn;

    
}

- (void)fieldChange
{
    self.loginBtn.userInteractionEnabled = (self.phoneF.text.length > 10) && (self.passwordF.text.length > 5);
    if (self.loginBtn.userInteractionEnabled == YES) {
        self.loginBtn.alpha = 1.0;
    }else {
        self.loginBtn.alpha = 0.4;
    }
}

- (void)phoneTextFieldDidChange:(UITextField *)textField
{
    [BDMobileNumberTools textFieldDidChange:textField selfField:self.phoneF];
}

// 密码是否明文
- (void)lookBtnClick:(UIButton *)send
{
    [self hideKeyboard];
    send.selected = !send.selected;
    if (send.selected == YES) {
        self.passwordF.secureTextEntry = NO;
    }else {
        self.passwordF.secureTextEntry = YES;
    }
}

#pragma mark -UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // 判断键盘是否遮挡
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self hideKeyboard];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // 收键盘
    [self hideKeyboard];
    return YES;
}

//系统弹出键盘时发出通知的回调方法
-(void)keyboardWillShow:(NSNotification *)not
{
    //转换成CGRect
    CGRect keyboardRect=[not.userInfo[@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
    //判断输入框是否被遮挡
    NSInteger distance= 36.5 + self.loginBtn.frame.origin.y + self.loginBtn.frame.size.height-keyboardRect.origin.y + 5;
    if(distance>0)
    {
        //把界面上移
        [UIView animateWithDuration:0.25 animations:^
         {
             self.superview.frame=CGRectMake(0, -distance, self.superview.frame.size.width, self.superview.frame.size.height);
         }];
    }
}

// 收键盘
- (void)hideKeyboard{
    [self endEditing:YES];
    // 界面下移
    self.superview.frame = self.superview.bounds;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self hideKeyboard];
}


@end
