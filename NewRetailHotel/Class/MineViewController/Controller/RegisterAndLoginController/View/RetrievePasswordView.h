//
//  RetrievePasswordView.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/4.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RetrievePasswordView : UIView

@property (nonatomic, strong) UITextField *phoneF;
@property (nonatomic, strong) UITextField *verifyF;
@property (nonatomic, strong) UITextField *checkingF;
@property (nonatomic, strong) UITextField *passNewF;
@property (nonatomic, strong) UIButton *checkingBtn;
@property (nonatomic, strong) UIButton *loginBtn;

@end
