//
//  RegisterViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/4.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "RegisterViewController.h"
#import "RegisterView.h"

@interface RegisterViewController ()

@property (nonatomic, strong) RegisterView *registerView;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self initUI];
}

- (void)initUI
{
    self.title = @"注册至爱";
    _registerView = [[RegisterView alloc] init];
    _registerView.frame = CGRectMake(0, kNavBarHeight, kScreenW, kScreenH);
    [_registerView.checkingBtn addTarget:self action:@selector(requestNetSendChecking) forControlEvents:UIControlEventTouchUpInside];
    [_registerView.loginBtn addTarget:self action:@selector(requestNetUserRegister) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_registerView];
    
    UIButton *backBtn = [[UIButton alloc] init];
    backBtn.frame = CGRectMake(0, StatusBarHeight, 44, 44);
    [backBtn setImage:[UIImage imageNamed:@"icon_return"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    UIBarButtonItem *rightBtn = [UIBarButtonItem itemwithText:@"登录" target:self action:@selector(backClick)];
    self.navigationItem.rightBarButtonItem = rightBtn;
    
}

#pragma mark -网络请求
- (void)requestNetSendChecking
{
    [[BDNetworkTools sharedInstance] postLoginSendCodeWihtProduct:_registerView.phoneF.text Block:^(NSDictionary *responseObject, NSError *error) {
        if ([[responseObject objectForKey:@"errmsg"] isEqualToString:@"ok"]) {
            [BDShowHUD showMBPShowTime:1 stateLabel:@"验证码发送成功"];
        }else {
            [self showAlertWithTitle:@"提示" message:[NSString stringWithFormat:@"%@", [responseObject objectForKey:@"errmsg"]] AscertainHandler:nil];
        }
    }];
}

- (void)requestNetUserRegister
{
    [self.view endEditing:YES];
    _registerView.superview.frame = _registerView.superview.bounds;
    NSDictionary *parameters = @{@"username":_registerView.phoneF.text,
                                 @"verifyCode":_registerView.checkingF.text,
                                 @"password":_registerView.setPasswordF.text
                                 };
    [[BDNetworkTools sharedInstance] postUserRegisterWihtParameters:parameters Block:^(NSDictionary *responseObject, NSError *error) {
        if ([[responseObject objectForKey:@"errmsg"] isEqualToString:@"ok"]) {
            [self.navigationController popToRootViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:succeedLoginNoti object:nil];
        }else {
            [self showAlertWithTitle:@"提示" message:[NSString stringWithFormat:@"%@", [responseObject objectForKey:@"errmsg"]] AscertainHandler:nil];
        }
    }];
}

- (void)backClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
