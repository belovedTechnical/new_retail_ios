//
//  AccountHeaderView.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/4.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountHeaderView : UIView
- (instancetype)initWithFrame:(CGRect)frame;

@property (weak, nonatomic) IBOutlet UIButton *EditeBtn;


@property (copy,nonatomic)void(^saveBtn)(NSInteger index);

@end
