//
//  AddressListCell.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/7.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressListModel.h"
@interface AddressListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLab;

@property (weak, nonatomic) IBOutlet UILabel *phoneLab;

@property (weak, nonatomic) IBOutlet UILabel *addressLab;

- (void)setAddressListCell:(AddressListModel *)model;
@end
