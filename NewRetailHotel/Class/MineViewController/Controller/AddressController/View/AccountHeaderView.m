//
//  AccountHeaderView.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/4.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "AccountHeaderView.h"

@implementation AccountHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"AccountHeaderView" owner:self options:nil];
        self = viewArray[0];
        self.frame = frame;
    }
    return self;
}

- (IBAction)saveBtn:(UIButton *)sender {
    
    self.saveBtn(sender.tag);
    
}



@end
