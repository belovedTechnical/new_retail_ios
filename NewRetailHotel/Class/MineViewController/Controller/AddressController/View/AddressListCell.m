//
//  AddressListCell.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/7.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "AddressListCell.h"

@implementation AddressListCell

- (void)awakeFromNib {
    [super awakeFromNib];

}
- (void)setAddressListCell:(AddressListModel *)model{
    
    self.nameLab.text = [NSString stringWithFormat:@"%@",model.attnName];
    self.phoneLab.text = [NSString stringWithFormat:@"%@",model.telePhone];
    NSString *addressStr = [NSString stringWithFormat:@"%@%@%@",model.provinceGeoNameLocal,model.cityGeoNameLocal,model.address1];
    self.addressLab.text = addressStr;
    
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
