//
//  AddressListBtnCell.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/7.
//  Copyright © 2016年 至爱. All rights reserved.
//
#define kDefault_tag  201
#define kEdite_tag  202
#define kDelete_tag  203

#import <UIKit/UIKit.h>

@interface AddressListBtnCell : UITableViewCell


@property (copy,nonatomic)void(^DefaultEditeDeleteBtn)(NSInteger index);


@property (weak, nonatomic) IBOutlet UIButton *defaultAddressBtn;
@property (weak, nonatomic) IBOutlet UIButton *editeBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@end
