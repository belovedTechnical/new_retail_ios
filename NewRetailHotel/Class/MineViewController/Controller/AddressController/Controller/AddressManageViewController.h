//
//  AddressManageViewController.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/7.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderCofirmViewController.h"
@protocol  OrderCofirmViewControllerDelegate <NSObject>

- (void)updateDefault;

@end
@interface AddressManageViewController : UIViewController
// 设置协议
@property (nonatomic, assign) id <OrderCofirmViewControllerDelegate> defaultDelegate;
@property(nonatomic,copy)NSString *setDefault;

@end
