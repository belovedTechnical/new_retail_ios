//
//  NewAddressViewController.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/7.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol  AddressManageViewControllerDelegate <NSObject>

- (void)updateDataWithName:(NSString *)name withPhone:(NSString *)phone withAddress:(NSString *)address;


@end

@interface NewAddressViewController : UIViewController
// 设置协议
@property (nonatomic, assign) id <AddressManageViewControllerDelegate> mydelegate;



@end
