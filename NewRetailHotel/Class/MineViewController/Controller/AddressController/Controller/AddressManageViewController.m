//
//  AddressManageViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/7.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "AddressManageViewController.h"
#import "NewAddressViewController.h"
#import "AddressListCell.h"
#import "AddressListBtnCell.h"
#import "EditeAddressViewController.h"
#import "AccountHeaderView.h"
#import "AddressListModel.h"

@interface AddressManageViewController ()<UITableViewDataSource,UITableViewDelegate,AddressManageViewControllerDelegate,AddressManageViewControllerUpdataDelegate,AddressManageViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lab;

@property (weak, nonatomic) IBOutlet UIButton *addressBtn;

@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *phone;
@property(nonatomic,copy)NSString *address;


@property (nonatomic,strong)NSMutableArray *dataArr;
@property (nonatomic,strong)AddressListCell *cell;


@end

@implementation AddressManageViewController
- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        self.dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"地址管理";
    self.tableView.tableFooterView = [[UIView alloc]init];
    [self loadCantactList];
}


- (void)loadSubviews{
    // 根据数据源数组的个数 判断显隐
    // 0个 隐藏
    // 无收货地址时，tableView是隐藏的
    if (self.dataArr.count == 0) {
        self.tableView.hidden = YES;
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:@"AddressListCell" bundle:nil] forCellReuseIdentifier:@"AddressListCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"AddressListBtnCell" bundle:nil] forCellReuseIdentifier:@"AddressListBtnCell"];
    
    self.tableView.estimatedRowHeight = 600;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    AccountHeaderView *header = [[AccountHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenW, 100)];
    header.backgroundColor = [UIColor clearColor];
    [header.EditeBtn setTitle:@"新建地址" forState:(UIControlStateNormal)];
    
    header.saveBtn = ^(NSInteger index){
        
        // 跳转到新建界面
        NewAddressViewController *newVC = [[NewAddressViewController alloc]init];
        newVC.mydelegate = self;
        [self.navigationController pushViewController:newVC animated:YES];
    };
    self.tableView.tableFooterView = header;
}

#pragma mark -新建地址 delegate
- (void)updateDataWithName:(NSString *)name withPhone:(NSString *)phone withAddress:(NSString *)address{
    // 有收货地址时
        self.imgView.hidden = YES;
        self.lab.hidden = YES;
        self.addressBtn.hidden   = YES;
        self.tableView.hidden = NO;
        [self loadCantactList];
}

#pragma mark - 编辑界面Updatedelegate
- (void)updateData{
    
    // 重新请求数剧
    [self loadCantactList];
}

// 新建地址
- (IBAction)newAddressBtn:(UIButton *)sender {
    
    NewAddressViewController *newVC = [[NewAddressViewController alloc]init];
    //指定代理对象
    newVC.mydelegate = self;
    [self.navigationController pushViewController:newVC animated:YES];
}

#pragma mark - UITableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return -1;
    }else{
    return 40;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        self.cell = [tableView dequeueReusableCellWithIdentifier:@"AddressListCell" forIndexPath:indexPath];
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        AddressListModel *model = self.dataArr[indexPath.section];
        [_cell setAddressListCell:model];
        return _cell;
    }else {
 
        AddressListBtnCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddressListBtnCell" forIndexPath:indexPath];
           cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        for (int i = 0; i < self.dataArr.count; i++) {
            AddressListModel *model1 = self.dataArr[i];
            if ([model1.isDefault isEqualToString:@"yes"]) {
                AddressListModel *model0 = self.dataArr[0];
                self.dataArr[0] = model1;
                self.dataArr[i] = model0;
            }
        }

        AddressListModel *model = self.dataArr[indexPath.section];
        if ([model.isDefault isEqualToString:@"yes"]) {
            [cell.defaultAddressBtn setImage:[UIImage imageNamed:@"icon_checkbox_selected"] forState:(UIControlStateNormal)];
        }else{
            [cell.defaultAddressBtn setImage:[UIImage imageNamed:@"icon_checkbox"] forState:(UIControlStateNormal)];
        }

        cell.DefaultEditeDeleteBtn = ^(NSInteger index){
       
            switch (index) {
                case kDefault_tag:
//                    NSLog(@"******model.isDefault----%@----%ld",model.isDefault,(long)indexPath.section);
                    
                    [self loadSetDefaultWithId:model.contactMechId withDefault:@"yes" AtIndexPath:indexPath];
                    break;
                case kEdite_tag:
                {
                    EditeAddressViewController *editeVC = [[EditeAddressViewController alloc]init];
                    // 传值
                    editeVC.UpdataDelegate = self;
                    editeVC.editeModel = model;
                    [self.navigationController pushViewController:editeVC animated:YES];
                }
                      break;
                case kDelete_tag:{

                    [self loadDeleteAddressWithId:model.contactMechId];
                }
                    break;
                default:
                    break;
            }
        };
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath{
    [self.dataArr exchangeObjectAtIndex:fromIndexPath.row withObjectAtIndex:toIndexPath.row];
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return YES;
}

//  解决cell分割线左边短20px的问题
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark -网络请求
#pragma mark -查询收货地址
- (void)loadCantactList{
    [self.dataArr removeAllObjects];
    [[BDNetworkTools sharedInstance] getAddressRedactWithBlock:^(NSDictionary *responseObject, NSError *error) {
        NSArray *aar = [responseObject objectForKey:@"postalAddressList"];
        for (NSDictionary *dic in aar) {
            AddressListModel *model = [AddressListModel addressListModelWithDict:dic];
            [self.dataArr addObject:model];
        }
        // 判断 排序
        for (int i = 0; i < self.dataArr.count; i++) {
            AddressListModel *model1 = self.dataArr[i];
            if ([model1.isDefault isEqualToString:@"yes"]) {
                AddressListModel *model0 = self.dataArr[0];
                self.dataArr[0] = model1;
                self.dataArr[i] = model0;
            }
        }
        // 无收货地址时，tableView是隐藏的
        if (self.dataArr.count == 0) {
            self.tableView.hidden = YES;
        }
        [self loadSubviews];
        [self.tableView reloadData];
    }];
}

#pragma mark - 设置默认地址
- (void)loadSetDefaultWithId:(NSString *)mechId withDefault:(NSString *)isDefault AtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = @{@"contactMechId":mechId,@"isDefault":isDefault};
    [[BDNetworkTools sharedInstance] postAddressAcquiesceWithParameter:dic Block:^(NSDictionary *responseObject, NSError *error) {
        //  重新请求一次
        [self loadCantactList];
        // 通知确认订单页面 加载默认地址
        if ([self.setDefault isEqualToString:@"Y"]) {
                if ([self.defaultDelegate respondsToSelector:@selector(updateDefault)]) {
                    [self.defaultDelegate updateDefault];
                }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

#pragma mark - 删除地址
- (void)loadDeleteAddressWithId:(NSString *)mechId{
    NSDictionary *dic = @{@"contactMechId":mechId};
    [[BDNetworkTools sharedInstance] postAddressDeleteWithParameter:dic Block:^(NSDictionary *responseObject, NSError *error) {
        // 重新请求数据
        [self loadCantactList];
    }];
}



@end
