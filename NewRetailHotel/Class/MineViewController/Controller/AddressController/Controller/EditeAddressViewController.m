//
//  EditeAddressViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/7.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "EditeAddressViewController.h"

#import "UIView+RGSize.h"
#import "AddressManageViewController.h"
#import "ProvinceModel.h"
#import "CityModel.h"
#import "TownsModel.h"
#import "BDMobileNumberTools.h"

@interface EditeAddressViewController ()<UITextFieldDelegate,UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UITextField *detailAddField;
//////////
@property (strong, nonatomic) IBOutlet UIButton *locationBtn;

@property (strong, nonatomic) IBOutlet UIPickerView *myPicker;

@property (strong, nonatomic) UIView *maskView;

@property (strong, nonatomic) IBOutlet UIView *pickerBgView;
//data
@property (strong, nonatomic) NSArray *provinceArray;
@property (strong, nonatomic) NSArray *cityArray;
@property (strong, nonatomic) NSArray *townArray;
@property (strong, nonatomic) NSArray *selectedArray;

/////
@property (strong, nonatomic) NSMutableArray *pArr;// 存储省的model对象
@property (strong, nonatomic) NSMutableArray *cArr;
@property (strong, nonatomic) NSMutableArray *tArr;
@property (strong, nonatomic) NSMutableArray *provinArr;// 存储省的名字

// 最后获取到的对象 省市县
@property (strong, nonatomic)ProvinceModel *pModel;
@property (strong, nonatomic)CityModel *cModel;
@property (strong, nonatomic)TownsModel *tModel;

/** 用户选择省 */
@property (copy, nonatomic)NSString *provinceInput;
/** 用户选择市 */
@property (copy, nonatomic)NSString *cityInput;
/** 用户选择区 */
@property (copy, nonatomic)NSString *districtInput;

@end

@implementation EditeAddressViewController

#pragma mark -懒加载
- (NSMutableArray *)pArr{
    if (!_pArr) {
        self.pArr = [NSMutableArray array];
    }
    return _pArr;
}

- (NSMutableArray *)cArr{
    if (!_cArr) {
        self.cArr = [NSMutableArray array];
    }
    return _cArr;
}

- (NSMutableArray *)tArr{
    if (!_tArr) {
        self.tArr = [NSMutableArray array];
    }
    return _tArr;
}

- (NSMutableArray *)provinArr{
    if (!_provinArr) {
        self.provinArr = [NSMutableArray array];
    }
    return _provinArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"编辑收货地址";
    
    [self setField:self.nameField];
    [self setField:self.phoneField];
    [self setField:self.detailAddField];
    
    // 传值 上个界面传过来的 model对象
    self.nameField.text = [NSString stringWithFormat:@"%@",self.editeModel.attnName];
    self.phoneField.text =[NSString stringWithFormat:@"%@",self.editeModel.telePhone];

    self.addressLab.text = [NSString stringWithFormat:@"%@%@%@",self.editeModel.provinceGeoNameLocal,self.editeModel.cityGeoNameLocal,self.editeModel.countyGeoNameLocal];
    self.detailAddField.text = [NSString stringWithFormat:@"%@",self.editeModel.address1];

//    [self.phoneField addTarget:self action:@selector(textChang) forControlEvents:UIControlEventEditingChanged];
    [self.phoneField addTarget:self action:@selector(phoneTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    self.phoneField.keyboardType = UIKeyboardTypePhonePad;

    [self getPickerData];
    [self initView];
}

#pragma mark - private method
// 获取位置
- (IBAction)showMyPicker:(id)sender {
    
    [self.view endEditing:YES];
    [self.view addSubview:self.maskView];
    [self.view addSubview:self.pickerBgView];
    self.maskView.alpha = 0;
    self.pickerBgView.top = self.view.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.maskView.alpha = 0.3;
        self.pickerBgView.bottom = self.view.height;
    }];
}

#pragma mark -保存
- (IBAction)saveBtn:(UIButton *)sender {
    
    [self loadUpdateAddress];
}

//- (void)textChang
//{
//    if (self.phoneField.text.length > 11) {
//        self.phoneField.text = [self.phoneField.text substringToIndex:11];
//    }
//}

- (void)phoneTextFieldDidChange:(UITextField *)textField
{
    [BDMobileNumberTools textFieldDidChange:textField selfField:self.phoneField];
}

- (void)setField:(UITextField *)textField{
    UIButton *button = [textField valueForKey:@"_clearButton"];
    
    [button setImage:[UIImage imageNamed:@"icon_clear"] forState:UIControlStateNormal];
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    [textField setValue:[UIColor colorWithHexString:@"#777777"] forKeyPath:@"_placeholderLabel.textColor"];
    [textField setValue:[UIFont boldSystemFontOfSize:13] forKeyPath:@"_placeholderLabel.font"];
    textField.borderStyle = UITextBorderStyleNone;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

///*********
#pragma mark - init view
- (void)initView {
    
    self.maskView = [[UIView alloc] initWithFrame:kScreen_Frame];
    self.maskView.backgroundColor = [UIColor blackColor];
    self.maskView.alpha = 0;
    [self.maskView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideMyPicker)]];
    
    self.pickerBgView.width = kScreen_Width;
    
}
#pragma mark - 获取data文件信息
- (void)getPickerData {
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"china_pct" ofType:@"data"];
    NSString *str = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    id json = [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments | NSJSONReadingMutableLeaves | NSJSONReadingMutableContainers error:nil];
    NSArray *arr = [NSArray arrayWithArray:json];
    // 省
    for (NSDictionary *dic in arr) {
        ProvinceModel *model = [[ProvinceModel alloc]init];
        [model setValuesForKeysWithDictionary:dic];
        [self.pArr addObject:model];
        [self.provinArr addObject:model.geoNameLocal];
    }
    // 已选择 省 角标
    NSInteger pIndex = 0;
    if ([_provinArr containsObject:self.editeModel.provinceGeoNameLocal]) {
        pIndex = [_provinArr indexOfObject:self.editeModel.provinceGeoNameLocal];
    }
    self.provinceArray = self.provinArr;
    ProvinceModel *model = self.pArr[pIndex];
    // 设置默认选中的省
    self.selectedArray = [self.provinArr objectAtIndex:pIndex];
    
    self.pModel = model;
    // 默认选中第一个省的第一个市
    NSMutableArray *cityA = [NSMutableArray array];
    for (NSDictionary *cDic in model.cities) {
        CityModel *model = [[CityModel alloc]init];
        [model setValuesForKeysWithDictionary:cDic];
        [self.cArr addObject:model];
        [cityA addObject:model.geoNameLocal];// 默认第一省的城市的数组
    }
    
    self.cityArray = cityA;
    // 已选择 市 角标
    NSInteger cIndex = 0;
    if ([cityA containsObject:self.editeModel.cityGeoNameLocal]) {
        cIndex = [cityA indexOfObject:self.editeModel.cityGeoNameLocal];
    }
    NSMutableArray *townOneArr =[NSMutableArray array];
    CityModel *model1 = self.cArr[cIndex];
    // 市code
    self.cModel = model1;
    // 县
    for (NSDictionary *dic  in model1.towns) {
        TownsModel *model = [[TownsModel alloc]init];
        [model setValuesForKeysWithDictionary:dic];
        
        [self.tArr addObject:model];
        
        [townOneArr addObject:model.geoNameLocal];
    }
    // 已选择 县 角标
    NSInteger tIndex = 0;
    if ([townOneArr containsObject:self.editeModel.countyGeoNameLocal]) {
        tIndex = [townOneArr indexOfObject:self.editeModel.countyGeoNameLocal];
    }
    // 县code
    TownsModel *tmodel = self.tArr[tIndex];
    self.tModel = tmodel;
    self.townArray = townOneArr;
    
    [_myPicker selectRow:pIndex inComponent:0 animated:NO];
    [_myPicker selectRow:cIndex inComponent:1 animated:NO];
    [_myPicker selectRow:tIndex inComponent:2 animated:NO];
}

#pragma mark - UIPicker Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return self.provinceArray.count;
    } else if (component == 1) {
        return self.cityArray.count;
    } else {
        return self.townArray.count;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        return [self.provinceArray objectAtIndex:row];
    } else if (component == 1) {
        return [self.cityArray objectAtIndex:row];
    } else {
        return [self.townArray objectAtIndex:row];
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (component == 0) {
        return kScreenW /3;
    } else if (component == 1) {
        return kScreenW /3;
    } else {
        return kScreenW /3;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 0) {
        // 省
        self.selectedArray = [self.provinceArray objectAtIndex:row];
        
        // 市
        [self.cArr removeAllObjects];
        NSMutableArray *cityOneArr =[NSMutableArray array];
        ProvinceModel *model = self.pArr[row];
        // 省的code
        self.pModel = model;
        
        for (NSDictionary *cityDic in model.cities) {
            CityModel *model = [[CityModel alloc]init];
            [model setValuesForKeysWithDictionary:cityDic];
            
            [self.cArr addObject:model];
            [cityOneArr addObject:model.geoNameLocal];
        }
        self.cityArray = cityOneArr;
        // 市的code
        CityModel *cmodel = self.cArr[0];
        self.cModel = cmodel;
        
        if (self.cArr.count > 0) {
            // 县
            NSMutableArray *townOneArr =[NSMutableArray array];
            CityModel *model1 = self.cArr[0];
            for (NSDictionary *dic  in model1.towns) {
                TownsModel *model = [[TownsModel alloc]init];
                [model setValuesForKeysWithDictionary:dic];
                
                [self.tArr addObject:model];
                
                [townOneArr addObject:model.geoNameLocal];
            }
            self.townArray = townOneArr;
            
            // 县code
            TownsModel *tModel = self.cArr[0];
            self.tModel = tModel;
            
        }else{
            self.tArr = nil;
        }
        
    }
    [pickerView selectedRowInComponent:1];
    [pickerView reloadComponent:1];
    [pickerView selectedRowInComponent:2];
    
    if (component == 1) {
        
        if (self.cityArray.count > 0) {
            CityModel *model = [self.cArr objectAtIndex:row];
            // 市的code
            self.cModel = model;
            
            NSMutableArray *townArr =[NSMutableArray array];
            [self.tArr removeAllObjects];
            for (NSDictionary *dic in model.towns) {
                TownsModel *model = [[TownsModel alloc]init];
                [model setValuesForKeysWithDictionary:dic];
                
                [self.tArr addObject:model];
                
                [townArr addObject:model.geoNameLocal];
            }
            
            self.townArray =townArr;
            
            // 县code
            TownsModel *toModel = self.cArr[row];
            self.tModel = toModel;
        } else {
            self.townArray = nil;
        }
        
        [pickerView selectRow:1 inComponent:2 animated:YES];
    }
    [pickerView reloadComponent:2];
    
    if (component == 2) {// 滑动第三个
        if (self.townArray.count > 0) {
            if (component == 1) {
                CityModel *model = [self.cArr objectAtIndex:0];
                // 市的code
                self.cModel = model;
                
                NSMutableArray *townArr =[NSMutableArray array];
                [self.tArr removeAllObjects];
                for (NSDictionary *dic in model.towns) {
                    TownsModel *model = [[TownsModel alloc]init];
                    [model setValuesForKeysWithDictionary:dic];
                    
                    [self.tArr addObject:model];
                    
                    [townArr addObject:model.geoNameLocal];
                }
                
                self.townArray =townArr;
                // 县code
                TownsModel *toModel = self.cArr[row];
                self.tModel = toModel;
            }
        }
    }
    [pickerView reloadComponent:2];
}

- (void)hideMyPicker {
    [UIView animateWithDuration:0.3 animations:^{
        self.maskView.alpha = 0;
        self.pickerBgView.top = self.view.height;
    } completion:^(BOOL finished) {
        [self.maskView removeFromSuperview];
        [self.pickerBgView removeFromSuperview];
    }];
}

#pragma mark - xib click
- (IBAction)cancel:(id)sender {
    [self hideMyPicker];
}

- (IBAction)ensure:(id)sender {
    
    NSString *str1 = [self.provinceArray objectAtIndex:[self.myPicker selectedRowInComponent:0]];
    NSString *str2 = [self.cityArray objectAtIndex:[self.myPicker selectedRowInComponent:1]];
    self.provinceInput = str1;
    self.cityInput = str2;
    if (self.townArray.count == 0) {
        NSString *str3 = @"";
        self.addressLab.text = [NSString stringWithFormat:@"%@ %@ %@",str1,str2,str3];
    }else {
        NSString *str3 = [self.townArray objectAtIndex:[self.myPicker selectedRowInComponent:2]];
        self.addressLab.text = [NSString stringWithFormat:@"%@ %@ %@",str1,str2,str3];
        self.districtInput = str3;
    }
    [self hideMyPicker];
}

#pragma mark -网络请求
#pragma mark -修改收货地址
- (void)loadUpdateAddress{
    
    NSString *detailStr = _detailAddField.text;
    NSRange city = [_detailAddField.text rangeOfString:@"市"];
    NSRange area = [_detailAddField.text rangeOfString:@"区"];
    NSRange county = [_detailAddField.text rangeOfString:@"县"];
    if ([_detailAddField.text containsString:@"县"]) {
        detailStr = [_detailAddField.text substringFromIndex:county.location+1];
    }
    if ([_detailAddField.text containsString:@"区"]) {
        detailStr = [_detailAddField.text substringFromIndex:area.location+1];
    }
    if ([_detailAddField.text containsString:@"市"]) {
        detailStr = [_detailAddField.text substringFromIndex:city.location+1];
    }
    NSString *address1 = @"";
    // 由于地址data文件数据与后台数据有问题。暂时解决方法为区(县)不传，区拼接到详细地址前
    if (_districtInput.length > 0) {
        address1 = [NSString stringWithFormat:@"%@%@",_districtInput,detailStr];
    }else {
        address1 = [NSString stringWithFormat:@"%@",detailStr];
    }
    NSDictionary *dic = @{@"contactMechId":self.editeModel.contactMechId,
                          @"telePhone":self.phoneField.text,
                          @"attnName":self.nameField.text,
                          @"address1":address1,
                          @"provinceCode":self.pModel.geoCodeNumeric,
                          @"cityCode":self.cModel.geoCodeNumeric,
                          @"countyCode": _tModel.geoCodeNumeric,// 空县地址
                          };
    [[BDNetworkTools sharedInstance] postAddressAmendWithParameter:dic Block:^(NSDictionary *responseObject, NSError *error) {
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[AddressManageViewController class]]) {
                //  通知地址管理界面
                [self.UpdataDelegate updateData];
                [self.navigationController popToViewController:controller animated:YES];
            }
        }
    }];
}

@end
