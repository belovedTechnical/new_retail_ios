//
//  EditeAddressViewController.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/7.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressListModel.h"
@protocol  AddressManageViewControllerUpdataDelegate <NSObject>

- (void)updateData;


@end@interface EditeAddressViewController : UIViewController

// 设置协议
@property (nonatomic, assign) id <AddressManageViewControllerUpdataDelegate> UpdataDelegate;
// copy要开辟空间
@property (nonatomic,strong)AddressListModel *editeModel;

@end
