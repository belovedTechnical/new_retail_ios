//
//  NewAddressViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/7.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "NewAddressViewController.h"
//#import "LocationPickerVC.h"
#import "UIView+RGSize.h"
#import "AddressManageViewController.h"
#import "ProvinceModel.h"
#import "CityModel.h"
#import "TownsModel.h"
#import "BDMobileNumberTools.h"

@interface NewAddressViewController ()<UITextFieldDelegate,UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UITextField *detailAddField;
//////////
@property (strong, nonatomic) IBOutlet UIButton *locationBtn;

@property (strong, nonatomic) IBOutlet UIPickerView *myPicker;

@property (strong, nonatomic) UIView *maskView;

@property (strong, nonatomic) IBOutlet UIView *pickerBgView;

/** 网络请求省、市、区 */
@property (nonatomic, strong) NSArray *provinceNetArray;
@property (nonatomic, strong) NSArray *cityNetArray;
@property (nonatomic, strong) NSArray *townNetArray;

@property (strong, nonatomic) NSArray *provinceArray;
@property (strong, nonatomic) NSArray *cityArray;
@property (strong, nonatomic) NSArray *townArray;
@property (strong, nonatomic) NSArray *selectedArray;

/** 用户选择省 */
@property (copy, nonatomic)NSString *provinceInput;
/** 用户选择市 */
@property (copy, nonatomic)NSString *cityInput;
/** 用户选择区 */
@property (copy, nonatomic)NSString *districtInput;
@property (copy, nonatomic)NSString *apikey;
//
@property (strong, nonatomic) NSMutableArray *pArr;// 存储省的model对象
@property (strong, nonatomic) NSMutableArray *cArr;
@property (strong, nonatomic) NSMutableArray *tArr;
@property (strong, nonatomic) NSMutableArray *provinArr;// 存储省的名字

// 最后获取到的对象 省市县
@property (strong, nonatomic)ProvinceModel *pModel;
@property (strong, nonatomic)CityModel *cModel;
@property (strong, nonatomic)TownsModel *tModel;
@end

@implementation NewAddressViewController
- (NSMutableArray *)pArr{
    if (!_pArr) {
        self.pArr = [NSMutableArray array];
    }
    return _pArr;
}
- (NSMutableArray *)cArr{
    if (!_cArr) {
        self.cArr = [NSMutableArray array];
    }
    return _cArr;
}
- (NSMutableArray *)tArr{
    if (!_tArr) {
        self.tArr = [NSMutableArray array];
    }
    return _tArr;
}
- (NSMutableArray *)provinArr{
    if (!_provinArr) {
        self.provinArr = [NSMutableArray array];
    }
    return _provinArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   self.title = @"新建收货地址";
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
   self.apikey = [user objectForKey:@"apikey"];
    
    [self setField:self.nameField];
    [self setField:self.phoneField];
    [self setField:self.detailAddField];
    
    // 网络地址请求
//    [self requestNetCityProvinceTown];
    [self getPickerData];
    [self initView];
    
//    [self.phoneField addTarget:self action:@selector(textChang) forControlEvents:UIControlEventEditingChanged];
    [self.phoneField addTarget:self action:@selector(phoneTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    self.phoneField.keyboardType = UIKeyboardTypePhonePad;

}

#pragma mark -保存
- (IBAction)saveBtn:(UIButton *)sender {
    
    if (self.phoneField.text.length <= 0 ||self.detailAddField.text.length <= 0 ||self.nameField.text.length <= 0||self.pModel.geoCodeNumeric.length <= 0) {
       
        
    }else{
         [self loadNewAddress];
    }

}

//- (void)textChang
//{
//    if (self.phoneField.text.length > 11) {
//        self.phoneField.text = [self.phoneField.text substringToIndex:11];
//    }
//}

- (void)phoneTextFieldDidChange:(UITextField *)textField
{
    [BDMobileNumberTools textFieldDidChange:textField selfField:self.phoneField];
}

- (void)setField:(UITextField *)textField{
    UIButton *button = [textField valueForKey:@"_clearButton"];
    
    [button setImage:[UIImage imageNamed:@"icon_clear"] forState:UIControlStateNormal];
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [textField setValue:[UIColor colorWithHexString:@"#777777"] forKeyPath:@"_placeholderLabel.textColor"];
    [textField setValue:[UIFont boldSystemFontOfSize:13] forKeyPath:@"_placeholderLabel.font"];
    
    textField.borderStyle = UITextBorderStyleNone;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}



///*********
#pragma mark - init view
- (void)initView {

    self.maskView = [[UIView alloc] initWithFrame:kScreen_Frame];
    self.maskView.backgroundColor = [UIColor blackColor];
    self.maskView.alpha = 0;
    [self.maskView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideMyPicker)]];
    
    self.pickerBgView.width = kScreen_Width;
}
#pragma mark - get data
- (void)getPickerData {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"china_pct" ofType:@"data"];
    NSString *str = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    id json = [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments | NSJSONReadingMutableLeaves | NSJSONReadingMutableContainers error:nil];
    NSArray *arr = [NSArray arrayWithArray:json];
    
    // 省
    for (NSDictionary *dic in arr) {
        ProvinceModel *model = [[ProvinceModel alloc]init];
        [model setValuesForKeysWithDictionary:dic];
        [self.pArr addObject:model];
        [self.provinArr addObject:model.geoNameLocal];
    }
    self.provinceArray = self.provinArr;
    ProvinceModel *model = self.pArr[0];
    // 设置默认选中的省
    self.selectedArray = [self.provinArr objectAtIndex:0];
    
    self.pModel = model;
    // 默认选中第一个省的第一个市
    NSMutableArray *cityA = [NSMutableArray array];
    for (NSDictionary *cDic in model.cities) {
        CityModel *model = [[CityModel alloc]init];
        [model setValuesForKeysWithDictionary:cDic];
        [self.cArr addObject:model];
        [cityA addObject:model.geoNameLocal];// 默认第一省的城市的数组
    }
    
    self.cityArray = cityA;
    
    
    NSMutableArray *townOneArr =[NSMutableArray array];
    CityModel *model1 = self.cArr[0];
    // 市code
    self.cModel = model1;
    // 县
    for (NSDictionary *dic  in model1.towns) {
        TownsModel *model = [[TownsModel alloc]init];
        [model setValuesForKeysWithDictionary:dic];
        
        [self.tArr addObject:model];
        
        [townOneArr addObject:model.geoNameLocal];
    }
    // 县code
    TownsModel *tmodel = self.cArr[0];
    self.tModel = tmodel;
    
    
    self.townArray = townOneArr;
    
  
}

#pragma mark - UIPicker Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return self.provinceArray.count;
    } else if (component == 1) {
        return self.cityArray.count;
    } else {
        return self.townArray.count;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        return [self.provinceArray objectAtIndex:row];
    } else if (component == 1) {
        return [self.cityArray objectAtIndex:row];
    } else {
        return [self.townArray objectAtIndex:row];
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (component == 0) {
        return 110;
    } else if (component == 1) {
        return 100;
    } else {
        return 110;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 0) {
        // 省
        self.selectedArray = [self.provinceArray objectAtIndex:row];
        
        // 市
        [self.cArr removeAllObjects];
        NSMutableArray *cityOneArr =[NSMutableArray array];
        ProvinceModel *model = self.pArr[row];
        // 省的code
        self.pModel = model;
        
        for (NSDictionary *cityDic in model.cities) {
            CityModel *model = [[CityModel alloc]init];
            [model setValuesForKeysWithDictionary:cityDic];
            
            [self.cArr addObject:model];
            [cityOneArr addObject:model.geoNameLocal];
        }
        self.cityArray = cityOneArr;
        // 市的code
        CityModel *cmodel = self.cArr[0];
        self.cModel = cmodel;
        
        if (self.cArr.count > 0) {
            [self.tArr removeAllObjects];
            // 县
            NSMutableArray *townOneArr =[NSMutableArray array];
            CityModel *model1 = self.cArr[0];
            for (NSDictionary *dic  in model1.towns) {
                TownsModel *model = [[TownsModel alloc]init];
                [model setValuesForKeysWithDictionary:dic];
                
                [self.tArr addObject:model];
                
                [townOneArr addObject:model.geoNameLocal];
            }
            self.townArray = townOneArr;
            
            // 县code
            TownsModel *tModel = self.tArr[0];
            self.tModel = tModel;
            
  
        }else{
            self.tArr = nil;
        }
        
    }
    [pickerView selectedRowInComponent:1];
    [pickerView reloadComponent:1];
    [pickerView selectedRowInComponent:2];
    
    if (component == 1) {
        
        if (self.cityArray.count > 0) {
            CityModel *model = [self.cArr objectAtIndex:row];
            // 市的code
            self.cModel = model;
            
            NSMutableArray *townArr =[NSMutableArray array];
            [self.tArr removeAllObjects];
            for (NSDictionary *dic in model.towns) {
                TownsModel *model = [[TownsModel alloc]init];
                [model setValuesForKeysWithDictionary:dic];
                
                [self.tArr addObject:model];
                
                [townArr addObject:model.geoNameLocal];
            }
            
            self.townArray =townArr;
            
            // 县code
            TownsModel *toModel = self.cArr[row];
            self.tModel = toModel;
        } else {
            self.townArray = nil;
        }

        [pickerView selectRow:1 inComponent:2 animated:YES];
    }
      [pickerView reloadComponent:2];
    
  if (component == 2) {// 滑动第三个
      if (self.townArray.count > 0) {

          if (component == 1) {
              CityModel *model = [self.cArr objectAtIndex:0];
              // 市的code
              self.cModel = model;
              
              NSMutableArray *townArr =[NSMutableArray array];
              [self.tArr removeAllObjects];
              for (NSDictionary *dic in model.towns) {
                  TownsModel *model = [[TownsModel alloc]init];
                  [model setValuesForKeysWithDictionary:dic];
                  
                  [self.tArr addObject:model];
                  
                  [townArr addObject:model.geoNameLocal];
              }
              
              self.townArray =townArr;
              // 县code
              TownsModel *toModel = self.cArr[row];
              self.tModel = toModel;

          }
          
        }
    }
    [pickerView reloadComponent:2];
}

#pragma mark - private method
// 获取位置
- (IBAction)showMyPicker:(id)sender {
    
    [self.view endEditing:YES];
    [self.view addSubview:self.maskView];
    [self.view addSubview:self.pickerBgView];
    self.maskView.alpha = 0;
    self.pickerBgView.top = self.view.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.maskView.alpha = 0.3;
        self.pickerBgView.bottom = self.view.height;
    }];
    
}

- (void)hideMyPicker {
    [UIView animateWithDuration:0.3 animations:^{
        self.maskView.alpha = 0;
        self.pickerBgView.top = self.view.height;
    } completion:^(BOOL finished) {
        [self.maskView removeFromSuperview];
        [self.pickerBgView removeFromSuperview];
    }];
}

#pragma mark - xib click
- (IBAction)cancel:(id)sender {
    [self hideMyPicker];
}

- (IBAction)ensure:(id)sender {

    self.provinceInput = [self.provinceArray objectAtIndex:[self.myPicker selectedRowInComponent:0]];
    self.cityInput = [self.cityArray objectAtIndex:[self.myPicker selectedRowInComponent:1]];
    self.districtInput = [self.townArray objectAtIndex:[self.myPicker selectedRowInComponent:2]];
    self.addressLab.text = [NSString stringWithFormat:@"%@%@%@",_provinceInput,_cityInput,_districtInput];
    [self hideMyPicker];
}

#pragma mark -网络请求
// 新建收货地址
- (void)loadNewAddress{
    NSString *address1 = @"";
    // 由于地址data文件数据与后台数据有问题。暂时解决方法为区(县)不传，区拼接到详细地址前
    if ([_districtInput isEqualToString:@"(null)"]) {
        address1 = [NSString stringWithFormat:@"%@",self.detailAddField.text];
    }else {
        address1 = [NSString stringWithFormat:@"%@%@",_districtInput,self.detailAddField.text];
    }
    NSDictionary *dic = @{
                          @"attnName": self.nameField.text,
                          @"telePhone": self.phoneField.text,
                          @"provinceCode": self.pModel.geoCodeNumeric,
                          @"cityCode": self.cModel.geoCodeNumeric,
                          @"countyCode": _tModel.geoCodeNumeric,// 空县地址
                          @"address1": address1
                          };
    [[BDNetworkTools sharedInstance] postAddressNewWithParameter:dic Block:^(NSDictionary *responseObject, NSError *error) {
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[AddressManageViewController class]]) {
                //  通知地址管理界面 重新请求数据
                NSString *addressStr = [NSString stringWithFormat:@"%@%@",self.addressLab.text,self.detailAddField.text];
                [self.mydelegate updateDataWithName:self.nameField.text withPhone:self.phoneField.text withAddress:addressStr];
                [self.navigationController popToViewController:controller animated:YES];
            }
        }
    }];
}

@end
