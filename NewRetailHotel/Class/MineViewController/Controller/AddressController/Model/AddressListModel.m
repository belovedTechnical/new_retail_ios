//
//  AddressListModel.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/16.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "AddressListModel.h"

@implementation AddressListModel

+ (instancetype)addressListModelWithDict:(NSDictionary *)dict
{
    AddressListModel *addressModel = [[self alloc] init];
    [addressModel setValuesForKeysWithDictionary:dict];
    
    return addressModel;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}

@end
