//
//  AddressListModel.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/16.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressListModel : NSObject

@property (nonatomic,copy)NSString *address1;
@property (nonatomic,copy)NSString *attnName;
@property (nonatomic,copy)NSString *cityGeoNameLocal;
@property (nonatomic,copy)NSString *contactMechId;
@property (nonatomic,copy)NSString *countyGeoNameLocal;
@property (nonatomic,copy)NSString *provinceGeoNameLocal;
@property (nonatomic,copy)NSString *telePhone;
@property (nonatomic,copy)NSString *isDefault;

+ (instancetype)addressListModelWithDict:(NSDictionary *)dict;

@end
