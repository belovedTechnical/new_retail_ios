//
//  SettingUpViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/5.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "SettingUpViewController.h"
#import "AccoutManageViewController.h"

#import "SettingUpView.h"
#import "BDFileManager.h"

@interface SettingUpViewController ()

@property(nonatomic, assign) NSInteger totalSize;
@property (nonatomic, strong) SettingUpView *settingUpView;

@end

#define BDCachePath [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject]
@implementation SettingUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f6f6f6"];
    [self initUI];
}

- (void)initUI
{
    SettingUpView *settingView = [[SettingUpView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, kScreenW, kScreenH)];
    [self.view addSubview:settingView];
    self.settingUpView = settingView;
    
    // 获取缓存
    [BDFileManager getDirectorySizeOfDirectoryPath:BDCachePath completion:^(NSInteger totalSize) {
        _totalSize = totalSize;
        [settingView.cacheD setTitle:[self totalSizeStr] forState:UIControlStateNormal];
    }];
    
    [settingView.accountTBtn addTarget:self action:@selector(accountTBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [settingView.spreadTBtn addTarget:self action:@selector(spreadTBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [settingView.pushTBtn addTarget:self action:@selector(pushTBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [settingView.cacheTBtn addTarget:self action:@selector(cacheTBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [settingView.editionTBtn addTarget:self action:@selector(editionTBtnClick) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark -监听点击
- (void)accountTBtnClick
{
    AccoutManageViewController *accoutVC = [[AccoutManageViewController alloc] init];
    [self.navigationController pushViewController:accoutVC animated:YES];
}
- (void)spreadTBtnClick
{
    
}
- (void)pushTBtnClick
{
    
}
- (void)cacheTBtnClick
{
    [BDFileManager removeDirectoryPath:BDCachePath];
    [self.settingUpView.cacheD setTitle:@"0M" forState:UIControlStateNormal];
}
- (void)editionTBtnClick
{
    
}

// 获取尺寸字符串
- (NSString *)totalSizeStr
{
    NSString *totalSzieStr = @"缓存";
    CGFloat totalSizeF = 0;
    if (_totalSize > 1000 * 1000) { // 1MB = 1000KB = 1000 * 1000B
        totalSizeF = _totalSize / 1000.0 / 1000.0;
        totalSzieStr = [NSString stringWithFormat:@"%@(%.1fMB)",totalSzieStr,totalSizeF];
    } else if (_totalSize > 1000) { // KB
        totalSizeF = _totalSize / 1000.0 ;
        totalSzieStr = [NSString stringWithFormat:@"%@(%.1fKB)",totalSzieStr,totalSizeF];
    } else if (_totalSize > 0) { // B
        totalSzieStr = [NSString stringWithFormat:@"%@(%ldB)",totalSzieStr,_totalSize];
    }
    totalSzieStr =  [totalSzieStr stringByReplacingOccurrencesOfString:@".0" withString:@""];
    
    return totalSzieStr;
}







@end
