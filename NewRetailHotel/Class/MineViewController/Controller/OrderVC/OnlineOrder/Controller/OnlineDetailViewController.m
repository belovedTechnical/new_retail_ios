//
//  OnlineDetailViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/7.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "OnlineDetailViewController.h"
#import "ListTableViewCell.h"
#import "OnlineDetailHeaderView.h"
#import "OnlineProListModel.h"
#import "OnlineSendCell.h"
#import "OnlineFooterView.h"
#import "PayOderViewController.h"
#import "OnlineDetailTimeCell.h" //setDetailTimeWith
#import "OrderAddressCell.h"// 地址的cell setAddressCellWith
#import "ExpressInformationCell.h"
#import "ExpressInformationViewController.h"
#import "WebShopDetailsViewController.h"

@interface OnlineDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic,strong)OnlineFooterView *onlineFooterView;
@property (nonatomic,strong)NSMutableArray *listArr;
@property(nonatomic,strong)NSDictionary *dic;

@end

@implementation OnlineDetailViewController
- (NSMutableArray *)listArr{
    if (!_listArr) {
        self.listArr = [NSMutableArray array];
    }
    return _listArr;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"订单详情";
    [self loadData];
    //区尾
    UIView *footerView = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, kScreenW,200))];
    self.tableview.tableFooterView = footerView;
    self.onlineFooterView = [[OnlineFooterView alloc]initWithFrame:CGRectMake(0, 0, footerView.frame.size.width, 200)];
    self.onlineFooterView.orderLable = self.orderLable;
    [footerView addSubview:self.onlineFooterView];
    
    [self.tableview registerNib:[UINib nibWithNibName:@"OnlineSendCell" bundle:nil] forCellReuseIdentifier:@"OnlineSendCell"];
    [self.tableview registerNib:[UINib nibWithNibName:@"OrderAddressCell" bundle:nil] forCellReuseIdentifier:@"OrderAddressCell"];
    [self.tableview registerNib:[UINib nibWithNibName:@"ListTableViewCell" bundle:nil] forCellReuseIdentifier:@"ListTableViewCell"];
    [self.tableview registerNib:[UINib nibWithNibName:@"OnlineDetailTimeCell" bundle:nil] forCellReuseIdentifier:@"OnlineDetailTimeCell"];
    [self.tableview registerNib:[UINib nibWithNibName:@"ExpressInformationCell" bundle:nil] forCellReuseIdentifier:@"ExpressInformationCell"];
    self.tableview.estimatedRowHeight = 500;
    self.tableview.rowHeight = UITableViewAutomaticDimension;
    
}

- (void)loadFooterViewWith:(OnlineFooterView *)view{
    
    __weak typeof(self) weakSelf = self;
    view.clickBtn = ^(){
        
        UIAlertController *vc = [UIAlertController alertControllerWithTitle:@"确认收货吗？" message:nil preferredStyle:    UIAlertControllerStyleAlert];
        UIAlertAction *act = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"confirmReceiptClick" object:weakSelf.orderId];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
        UIAlertAction *act2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [vc addAction:act];
        [vc addAction:act2];
        [weakSelf presentViewController:vc animated:YES completion:nil];
    };
    
    view.payBtnClick = ^(){
        PayOderViewController *payVC = [[PayOderViewController alloc]init];
        payVC.orderId = weakSelf.orderId;
        
        [weakSelf.navigationController pushViewController:payVC animated:YES];
    };
    
    view.cancelBtnClick = ^(){
        UIAlertController *vc = [UIAlertController alertControllerWithTitle:@"确定要取消订单吗？" message:nil preferredStyle:    UIAlertControllerStyleAlert];
        UIAlertAction *act = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"cancelBtnClick" object:weakSelf.orderId];
            [self.navigationController popViewControllerAnimated:YES];
        }];
        UIAlertAction *act2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [vc addAction:act];
        [vc addAction:act2];
        [weakSelf presentViewController:vc animated:YES completion:nil];
        
    };
   // pointsLab;// 积分抵扣  balanceLab;// 余额抵扣  accountPrice 余额抵扣  productPrice  actualPrice
    view.totalLab.text = [NSString stringWithFormat:@"¥%@",[self.dic objectForKey:@"grandTotal"]];
    
    if (![[self.dic allKeys] containsObject:@"pointsPirce"]) {
        view.pointsLab.text = @"-¥0.00";
    }else{
        view.pointsLab.text = [NSString stringWithFormat:@"-¥%@",[self.dic objectForKey:@"pointsPirce"]];
    }
    
    if (![[self.dic allKeys] containsObject:@"accountPrice"]) {
        view.balanceLab.text = @"-¥0.00";
    }else{
        view.balanceLab.text = [NSString stringWithFormat:@"-¥%@",[self.dic objectForKey:@"accountPrice"]];
    }
    
    if (![[self.dic allKeys] containsObject:@"grandTotal"]) {
        view.totalPriceLab.text = @"¥0.00";
    }else{
        view.totalPriceLab.text = [NSString stringWithFormat:@"¥%@",[self.dic objectForKey:@"grandTotal"]];;
    }
    
    
    if (![[self.dic allKeys] containsObject:@"totalFreight"]) {
        view.moneyLab.text = @"+¥0.00";
    }else{
        view.moneyLab.text = [NSString stringWithFormat:@"+¥%@",[self.dic objectForKey:@"totalFreight"]];// 运费
    }
    
    view.couponDLab.text = [NSString stringWithFormat:@"-¥%@", [self.dic objectForKey:@"couponAmount"]];
}

- (void)displayDisponse
{
    if ([@"待收货" containsString:self.orderLable]) {
        self.onlineFooterView.ConfirmReceiptBtn.hidden = NO;
    }else {
        self.onlineFooterView.ConfirmReceiptBtn.hidden = YES;
    }
    if ([@"待付款" containsString:self.orderLable]) {
        self.onlineFooterView.payAndCancelView.hidden = NO;
    }else {
        self.onlineFooterView.payAndCancelView.hidden = YES;
    }
}

#pragma mark -<UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
        
        return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (section == 0) {
        if ([@"待收货" containsString:self.orderLable]) {
                return 3;
        }else {
               return 2;
        }
     
    }else if (section == 1){
        return self.listArr.count ;
    }

        return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return 130;
        }else{
            return 100;
        }
    }else if(indexPath.section == 1){
        return 86;
    }
    return 65;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            
            OnlineDetailTimeCell *cell = [self.tableview dequeueReusableCellWithIdentifier:@"OnlineDetailTimeCell" forIndexPath:indexPath];
            cell.orderLable = self.orderLable;
            // 发货 订单详细信息
            [cell setDetailTimeWith:self.dic];
            
            return cell;
            
        }else if (indexPath.row == 1) {
            OrderAddressCell *addressCell = [self.tableview dequeueReusableCellWithIdentifier:@"OrderAddressCell" forIndexPath:indexPath];
            addressCell.bgImageConstraints.constant = 10;
            addressCell.IDNumberF.placeholder = @"";
            [addressCell.saveBtn setTitle:@"" forState:UIControlStateNormal];
            [addressCell setAddressCellWith:self.dic];
            
            return addressCell;
            
        }else {// 物流详情
            ExpressInformationCell *expressCell = [self.tableview dequeueReusableCellWithIdentifier:@"ExpressInformationCell" forIndexPath:indexPath];
            
            [expressCell setExpressDetails:self.dic];
            return expressCell;
        }
        
    }else if (indexPath.section == 1) {
        ListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ListTableViewCell" forIndexPath:indexPath];
        if (self.listArr.count > 0) {
//            OnlineProListModel *model = self.listArr[self.indexTag];
            OnlineProListModel *model = self.listArr[indexPath.row];
            [cell setListCell:model];
        }
        return cell;
        
    }else{
        OnlineSendCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OnlineSendCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row ==0) {
            
//            cell.sendLab.text = @"留言备注";
            if (![[self.dic allKeys] containsObject:@"remarks"]) {
                cell.matterLabel.text = @"";
            }else{
                cell.matterLabel.text = [NSString stringWithFormat:@"%@",[self.dic objectForKey:@"remarks"]];
            }
            
//            cell.sendLab.text = @"发票信息";
            if ([[self.dic allKeys] containsObject:@"invoice"]) {
                NSString *str = [NSString stringWithFormat:@"%@",[self.dic objectForKey:@"invoice"]];
                if (str.length >0) {
                    cell.contenLab.text =str;
                }else{
                    cell.contenLab.text =@"不需要发票";
                }
                
            }else{
                cell.contenLab.text =@"不需要发票";
            }
//
//        }else{
//            cell.sendLab.text = @"备注";
//            if (![[self.dic allKeys] containsObject:@"remarks"]) {
//                cell.contenLab.text = @"";
//            }else{
//                cell.contenLab.text = [NSString stringWithFormat:@"%@",[self.dic objectForKey:@"remarks"]];
//            }
        }
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        if (indexPath.row == 2) {
            
            NSString *OrderNumber = [NSString stringWithFormat:@"%@", [self.dic objectForKey:@"expressNumber"]];
            NSString *expressCompany = [NSString stringWithFormat:@"%@", [self.dic objectForKey:@"expressCompany"]];
            ExpressInformationViewController *expressVC = [[ExpressInformationViewController alloc] init];
            expressVC.expressNumber = OrderNumber;
            expressVC.expressCompany = expressCompany;
            expressVC.orderId = self.orderId;
            [self.navigationController pushViewController:expressVC animated:YES];
        }
    }
    
    if (indexPath.section == 1) {
        // 104468_102354
        OnlineProListModel *model = self.listArr[indexPath.row];
        NSString *IDString = model.productId;
        NSString *urlID = [IDString substringToIndex:6];
        NSString *urlStr = [NSString stringWithFormat:@"%@c-shopDetiles/%@", HTML_URL_Base, urlID];
        
        WebShopDetailsViewController *shopDetaVC = [[WebShopDetailsViewController alloc] init];
        shopDetaVC.shopDetailsURL = urlStr;
        [self.navigationController pushViewController:shopDetaVC animated:YES];
    }
}

#pragma mark -网络请求
- (void)loadData
{
    NSDictionary *dic1 = @{@"orderId":self.orderId};
    [[BDNetworkTools sharedInstance] getOnlineListDetailWithParameter:dic1 Block:^(NSDictionary *responseObject, NSError *error) {
        self.dic = [NSDictionary dictionaryWithDictionary: responseObject];
        NSArray *arr = [self.dic objectForKey:@"productList"];
        for (NSDictionary *dic in arr) {
            OnlineProListModel *model = [[OnlineProListModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [self.listArr addObject:model];
        }
        [self.tableview reloadData];
        [self loadFooterViewWith:self.onlineFooterView];
        [self displayDisponse];
    }];
}


@end
