//
//  ExpressInformationViewController.m
//  BelovedHotel
//
//  Created by Sam Feng on 2017/5/10.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "ExpressInformationViewController.h"
#import <WebKit/WebKit.h>
#import "ExpressItem.h"
#import "ExpressDetailsCell.h"
#import "ExpressTopView.h"
#import "ExpressCorpModel.h"

@interface ExpressInformationViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) ExpressTopView *expressTopView;
@property(nonatomic, strong) ExpressItem *expressItem;
@property(nonatomic, strong) NSArray *expressArray;
@property (nonatomic, strong) NSArray *groupArray;
@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) UIView *topView;
@property (nonatomic, assign) NSInteger age;
@end

static NSString *const expressID = @"expressID";
@implementation ExpressInformationViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"物流详情";
    self.view.backgroundColor = [UIColor colorWithHexString:@"f6f6f6" alpha:1];
    self.age = 0;
    [self requestNetworkData];
    [self setupUI];
}

- (void)setupUI
{
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, kScreenW, 70)];
    [self.view addSubview:topView];
    self.topView = topView;
    
    ExpressTopView *nameNumberView = [[ExpressTopView alloc] initWithFrame:topView.bounds];
    [topView addSubview:nameNumberView];
    self.expressTopView = nameNumberView;
    nameNumberView.corpExpressDetailsClick = ^(NSInteger age) {
        self.age = age;
        [self.tableView reloadData];
    };
    [self setupUITabelView:topView];
    if (@available(iOS 11.0, *)) {
        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    //    UILabel *numberLabel = [[UILabel alloc] init];
    //    numberLabel.text = @"快递单号:";
    //    numberLabel.textColor = [UIColor colorWithHexString:@"#666666" alpha:1];
    //    [topView addSubview:numberLabel];
    //
    //    UILabel *companyLabel = [[UILabel alloc] init];
    //    companyLabel.text = @"物流公司:";
    //    companyLabel.textColor = [UIColor colorWithHexString:@"#666666" alpha:1];
    //    [topView addSubview:companyLabel];
    //
    //    UILabel *expressNumbern = [[UILabel alloc] init];
    //    expressNumbern.text = self.expressNumber;
    //    expressNumbern.textColor = [UIColor colorWithHexString:@"#666666" alpha:1];
    //    [topView addSubview:expressNumbern];
    //
    //    UILabel *expressCompany = [[UILabel alloc] init];
    //    expressCompany.text = self.expressCompany;
    //    expressCompany.textColor = [UIColor colorWithHexString:@"#666666" alpha:1];
    //    [topView addSubview:expressCompany];
    
//    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.view.mas_top).offset(SafeAreaTopHeight);
//        make.left.equalTo(self.view.mas_left).offset(0);
//        make.right.equalTo(self.view.mas_right).offset(0);
//        make.height.equalTo(@70);
//    }];
    
    //    [numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.top.equalTo(topView.mas_top).offset(10);
    //        make.left.equalTo(topView.mas_left).offset(15);
    //        make.height.equalTo(@20);
    //        make.width.equalTo(@80);
    //    }];
    //
    //    [companyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.width.height.left.equalTo(numberLabel);
    //        make.top.equalTo(numberLabel.mas_bottom).offset(11);
    //    }];
    //
    //    [expressNumbern mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.height.top.equalTo(numberLabel);
    //        make.left.equalTo(numberLabel.mas_right).offset(5);
    //        make.right.equalTo(topView.mas_right).offset (0);
    //    }];
    //
    //    [expressCompany mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.height.top.equalTo(companyLabel);
    //        make.left.equalTo(companyLabel.mas_right).offset(5);
    //        make.right.equalTo(topView.mas_right).offset (0);
    //    }];
    
}

// 有物流信息View
- (void)setupUITabelView:(UIView *)topView
{
    UITableView *tabelView = [[UITableView alloc] init];
    tabelView.dataSource = self;
    tabelView.delegate = self;
    tabelView.separatorStyle = UITableViewCellEditingStyleNone;
    [self.view addSubview:tabelView];
    self.tableView = tabelView;
    
    // 注册cell
    [tabelView registerNib:[UINib nibWithNibName:@"ExpressDetailsCell" bundle:nil] forCellReuseIdentifier:expressID];
    [tabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom).offset(20);
        make.bottom.equalTo(self.view.mas_bottom).offset(0);
        make.left.equalTo(self.view.mas_left).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
    }];
}

// 无物流信息View
- (void)setupUIBackgroundView:(UIView *)topView
{
    UIView *backgroundView = [[UIView alloc] init];
    [self.view addSubview:backgroundView];
    
    UIButton *strollBtn = [[UIButton alloc] init];
    [strollBtn setTitle:@"商城逛逛" forState:UIControlStateNormal];
    strollBtn.backgroundColor = [UIColor redColor];
    strollBtn.layer.cornerRadius = 6.0;
    strollBtn.layer.masksToBounds = YES;
    [strollBtn addTarget:self action:@selector(goToStrollClick) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:strollBtn];
    
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"expressBackgroud"]];
    [backgroundView addSubview:backgroundImage];
    
    [backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom).offset(20);
        make.bottom.equalTo(self.view.mas_bottom).offset(0);
        make.left.equalTo(self.view.mas_left).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
    }];
    
    [strollBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(backgroundView.mas_bottom).offset(-50);
        make.centerX.equalTo(backgroundView);
        make.height.equalTo(@50);
        make.width.equalTo(@160);
    }];
    
    [backgroundImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backgroundView.mas_top).offset(0);
        make.bottom.equalTo(strollBtn.mas_top).offset(0);
        make.left.equalTo(backgroundView.mas_left).offset(0);
        make.right.equalTo(backgroundView.mas_right).offset(0);
    }];
    
}

#pragma mark -UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.expressArray.count > 0) {
        ExpressItem *exModel = self.expressArray[_age];
        return exModel.data.count;
    }else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ExpressDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:expressID];
    ExpressItem *exModel = self.expressArray[_age];
    ExpressItem *dataModel = exModel.data[indexPath.row];
    cell.item = dataModel;
    
    if (indexPath.row == 0) {
        cell.topLineLabel.hidden = YES;
        cell.amongLabel.backgroundColor = [UIColor redColor];
        cell.routeLabel.textColor = [UIColor redColor];
    }else {
        cell.topLineLabel.hidden = NO;
        cell.amongLabel.backgroundColor = [UIColor lightGrayColor];
        cell.routeLabel.textColor = [UIColor darkGrayColor];
    }
    
    if (indexPath.row == exModel.data.count-1) {
        cell.bottomLineLabel.hidden = YES;
    }else {
        cell.bottomLineLabel.hidden = NO;
    }
    
    return cell;
}

#pragma mark -UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

#pragma mark -内部控制方法
// 去逛逛按钮点击
- (void)goToStrollClick
{
    self.tabBarController.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark -网络请求
- (void)requestNetworkData
{
    [SVProgressHUD showWithStatus:@"物流信息加载中"];
    NSDictionary   *paramDic = @{@"orderId" : self.orderId};
    [[BDNetworkTools sharedInstance] postKuaiDiDelailsWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        [SVProgressHUD dismiss];
        if (error != nil) {
            [self setupUIBackgroundView:_topView];
        }
        NSArray *expressLiset = [responseObject objectForKey:@"expressList"];
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *dict in expressLiset) {
            [tempArray addObject:[ExpressItem expressIemWithDict:dict]];
        }
        self.expressArray = tempArray;
        self.expressTopView.dataArray = tempArray;
        [self.tableView reloadData];
        if (self.expressArray.count == 0) {
            [self setupUIBackgroundView:_topView];
        }
    }];
}



@end
