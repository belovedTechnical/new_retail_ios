//
//  OnlineDetailViewController.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/7.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  OnlineProductListViewControllerDelegate<NSObject>

-(void)pustListVC;
- (void)refreshComfirmListVC;

@end

@interface OnlineDetailViewController : UIViewController

@property(nonatomic,copy) NSString *orderId;
@property(nonatomic,copy) NSString *orderLable;
/// 点击的行号
//@property (assign, nonatomic) NSUInteger indexTag;


// 设置协议
@property (nonatomic, assign) id <OnlineProductListViewControllerDelegate> popDelegate;

@end
