//
//  OnlineProductListViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/6.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "OnlineProductListViewController.h"
#import "MuneHeaderView.h"
#import "ListTableViewCell.h"
#import "OnlineOrderHeaderView.h"
#import "OnlineOrderFooterView.h"

#import "CustomCell.h"
#import "BDNetworkTools.h"
#import "MJRefresh.h"
#import "OnlineModel.h"
#import "OnlineProListModel.h"
#import "OnlineDetailViewController.h"
#import "PayOderViewController.h"
//#import "CommentViewController.h"



@interface OnlineProductListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *coverView;
@property (weak, nonatomic) IBOutlet UIView *showView;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic,strong)NSMutableArray *dataArr;

@property (nonatomic, strong) NSMutableDictionary *paramDic;
// YES:上拉  NO:下拉
@property (nonatomic, assign) BOOL isUp;
//@property (nonatomic,assign)NSInteger tag;
@property (nonatomic,strong)NSMutableArray *pDataArr;

@property (nonatomic,copy) NSString *statusStr;
@property (weak, nonatomic) IBOutlet UILabel *showViewLab;
@property (weak, nonatomic) IBOutlet UIButton *cancleBtn;

@property (nonatomic,strong) OnlineModel *model;
@property (nonatomic,strong) OnlineOrderHeaderView *onlineHeader;

@property(nonatomic,strong) OnlineOrderFooterView *footerView;
@property(nonatomic,strong) MuneHeaderView *muneView;

@property(nonatomic, assign) NSInteger pageSize;
@property(nonatomic, assign) NSInteger pageIndex;
@property(nonatomic, assign) NSInteger pageIndex1;
@property(nonatomic, assign) NSUInteger dingDanLeiXing;

@property(nonatomic, assign) NSUInteger pageIndexAll;
@property(nonatomic, assign) NSUInteger pageIndexDaiFu;
@property(nonatomic, assign) NSUInteger pageIndexDaiFa;
@property(nonatomic, assign) NSUInteger pageIndexDaiShou;
@property(nonatomic, assign) NSUInteger pageIndexFinish;
@property(nonatomic, assign) BOOL dingDanLeiXingMiddle;

@property (nonatomic, copy) NSString *apikey;
@property (nonatomic, strong) NSArray *storeIdArray;

@end

@implementation OnlineProductListViewController

- (NSArray *)storeIdArray
{
    if (!_storeIdArray) {
        _storeIdArray = [NSArray array];
    }
    return _storeIdArray;
}

- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        self.dataArr = [NSMutableArray array];
    }
    return _dataArr;
    
}
- (NSMutableArray *)pDataArr{
    if (!_pDataArr) {
        self.pDataArr = [NSMutableArray array];
    }
    return _pDataArr;
}

- (void)viewDidDisappear:(BOOL)animated
{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.pageSize = 20;
    self.dingDanLeiXing = DingDanLeiXingAll;
    [self resetPageIndex];
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    self.apikey = [user objectForKey:@"apikey"];
    
    self.title = @"电商订单";
    self.showView.hidden = YES;
    self.coverView.hidden = YES;
    self.tableview.tableFooterView = [[UIView alloc]init];
    
    [self requestCommonwealStoreIDListWithApikey:_apikey];
    [self.tableview registerNib:[UINib nibWithNibName:@"ListTableViewCell" bundle:nil] forCellReuseIdentifier:@"ListTableViewCell"];
    
    //获取订单分类数目
    [self setOnlineHeaderViewNum];
    CGFloat muneY = 0;
    if (Device_iPhoneX) {
        muneY = 24;
    }
    self.muneView = [[MuneHeaderView alloc]initWithFrame:CGRectMake(0, muneY, self.headerView.frame.size.width, 40)];
    [self.headerView addSubview:self.muneView];
    self.statusStr = @"";
    self.isUp = YES; // 设置默认是向上

    if (@available(iOS 11.0, *)) {
        _tableview.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        _tableview.contentInset = UIEdgeInsetsMake(64+muneY, 0, 49, 0);//导航栏如果使用系统原生半透明的，top设置为64
        _tableview.scrollIndicatorInsets = _tableview.contentInset;
    }else {
        self.automaticallyAdjustsScrollViewInsets = YES;
    }
    // 全部
    [self loadDataWithStatus:@"" pageIndex:self.pageIndexAll];
    [self addRefresh];
    
    __weak typeof(self) weakSelf = self;
    self.muneView.buttonClick = ^(NSUInteger index){
        weakSelf.dingDanLeiXingMiddle = YES;
        switch (index) {
            case DingDanLeiXingAll:
                [weakSelf loadDataWithStatus:@"" pageIndex:weakSelf.pageIndexAll];
                weakSelf.dingDanLeiXing = DingDanLeiXingAll;
                break;
            case DingDanLeiXingDaiFu:// 待付款 OrderPlaced
                [weakSelf loadDataWithStatus:@"OrderPlaced" pageIndex:weakSelf.pageIndexDaiFu];
                weakSelf.dingDanLeiXing = DingDanLeiXingDaiFu;
                break;
            case DingDanLeiXingDaiFa://待发货
                weakSelf.dingDanLeiXing = DingDanLeiXingDaiFa;
                [weakSelf loadDataWithStatus:@"OrderApproved" pageIndex:weakSelf.pageIndexDaiFa];
                break;
            case DingDanLeiXingDaiShou://待收货
                weakSelf.dingDanLeiXing = DingDanLeiXingDaiShou;
                [weakSelf loadDataWithStatus:@"OrderApproved" pageIndex:weakSelf.pageIndexDaiShou];
                break;
            case DingDanLeiXingFinish:// 已完成
                 weakSelf.dingDanLeiXing = DingDanLeiXingFinish;
                 [weakSelf loadDataWithStatus:@"OrderCompleted" pageIndex:weakSelf.pageIndexFinish];
                break;
            default:
                break;
        }
    };
    
    
    // 订单详情界面 -> 确认收货按钮点击监听
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(sureBtnClick:) name:@"confirmReceiptClick" object:nil];
    // 取消按钮
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(cancelBtnClick:) name:@"cancelBtnClick" object:nil];
}
- (void)resetPageIndex
{
    self.pageIndexAll = 0;
    self.pageIndexDaiFa = 0;
    self.pageIndexDaiFu = 0;
    self.pageIndexDaiShou = 0;
    self.pageIndexFinish = 0;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - 小红标
- (void)setOnlineHeaderViewNum
{
    // 全部
    NSDictionary *paramDic =  @{@"orderType":@""};
    [[BDNetworkTools sharedInstance] getOnlineListWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        [self.muneView.allBtn setTitle:@"全部" forState:(UIControlStateNormal)];
    }];
    
    // 待付款
    NSDictionary *paramDic1 =  @{@"orderType":@"OrderPlaced",@"pageIndex":@"0",@"pageSize":@"50"};
    [[BDNetworkTools sharedInstance] getOnlineListWithParameter:paramDic1 Block:^(NSDictionary *responseObject, NSError *error) {
        NSUInteger num = [[NSString stringWithFormat:@"%@",[responseObject objectForKey:@"pageCount"]]integerValue];
        [self.muneView.unPayBtn setTitle:[NSString stringWithFormat:@"待付款(%lu)",(unsigned long)num] forState:(UIControlStateNormal)];

    }];
    
    // 待发货sendStatus = 0;
    NSDictionary *paramDic2 =  @{@"orderType":@"OrderApproved",@"pageIndex":@"0",@"pageSize":@"150"};
    [[BDNetworkTools sharedInstance] getOnlineListWithParameter:paramDic2 Block:^(NSDictionary *responseObject, NSError *error) {
        NSMutableArray *arrCo = [NSMutableArray array];
        NSArray *arr = [responseObject objectForKey:@"orderList"];
        for (NSDictionary *dic in arr) {
            OnlineModel *model = [[OnlineModel alloc]init];// 店铺对象
            [model setValuesForKeysWithDictionary:dic];
            NSString *str = [NSString stringWithFormat:@"%@",model.sendStatus];
            if ([str isEqualToString:@"0"]) {
                [arrCo addObject:model];
            }
        }
        [self.muneView.unRecevGoodsBtn setTitle:[NSString stringWithFormat:@"待发货(%lu)",arrCo.count] forState:(UIControlStateNormal)];
    }];
    
    // 待收货
    // sendStatus = 1;
    NSDictionary *paramDic4 =  @{@"orderType":@"OrderApproved",@"pageIndex":@"0",@"pageSize":@"150"};
    [[BDNetworkTools sharedInstance] getOnlineListWithParameter:paramDic4 Block:^(NSDictionary *responseObject, NSError *error) {
        NSMutableArray *arrCo = [NSMutableArray array];
        NSArray *arr = [responseObject objectForKey:@"orderList"];
        for (NSDictionary *dic in arr) {
            OnlineModel *model = [[OnlineModel alloc]init]; // 店铺对象
            [model setValuesForKeysWithDictionary:dic];
            NSString *str = [NSString stringWithFormat:@"%@",model.sendStatus];
            if ([str isEqualToString:@"1"]) {
                [arrCo addObject:model];
            }
        }
        [self.muneView.finishBtn setTitle:[NSString stringWithFormat:@"待收货(%lu)",arrCo.count] forState:(UIControlStateNormal)];
    }];
    
    // 已完成
    NSDictionary *paramDic3 =  @{@"orderType":@"OrderCompleted",@"pageIndex":@"0",@"pageSize":@"300"};// 全部
    [[BDNetworkTools sharedInstance] getOnlineListWithParameter:paramDic3 Block:^(NSDictionary *responseObject, NSError *error) {
        NSUInteger num = [[NSString stringWithFormat:@"%@",[responseObject objectForKey:@"pageCount"]]integerValue];
        [self.muneView.cancleBtn setTitle:[NSString stringWithFormat:@"已完成(%lu)",num] forState:(UIControlStateNormal)];
    }];
    
    
}

#pragma mark - 代理协议
- (void)updateData
{
//    self.tag = kCancleBtn_Tag;
    self.dingDanLeiXing = DingDanLeiXingFinish;
    [self loadDataWithStatus:@"OrderCompleted" pageIndex:self.pageIndexFinish];
    
}
#pragma mark - Tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.dataArr.count;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    OnlineModel *model = self.dataArr[section];
    
    return model.productList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 46;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    self.onlineHeader = [[OnlineOrderHeaderView alloc]initWithFrame:CGRectMake(0, 10, kScreenW, 36)];
    // 赋值
   __weak  OnlineModel *model = self.dataArr[section];
//    self.model = model;
    
    NSString *storeNameStr = [NSString stringWithFormat:@"%@",model.storeName];
    BOOL isCommonwealID = [_storeIdArray containsObject:model.productStoreId];
    if (isCommonwealID == YES) {
        
        _onlineHeader.storeNameLAb.attributedText = [self setLabelString:storeNameStr];
//        _onlineHeader.storeNameLAb.text = [NSString stringWithFormat:@"%@(爱心订单)", storeNameStr];
    }else {
        _onlineHeader.storeNameLAb.text = storeNameStr;
    }
    NSString *str =  [NSString stringWithFormat:@"%@",model.orderStatus];
    NSString *strPay =  [NSString stringWithFormat:@"%@",model.payStatus];// 分为待付款 和已付款
    NSString *sendStr =  [NSString stringWithFormat:@"%@",model.sendStatus];//
    
    [self loadHeaderViewWithStr:str withPay:strPay withSend:sendStr];
    
    return self.onlineHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 36;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    self.footerView = [[OnlineOrderFooterView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 36)];
    
    OnlineModel *model = self.dataArr[section];
    
    _footerView.numLab.text = [NSString stringWithFormat:@"共计%@件,合计",model.orderTotalNum];
    _footerView.priceLab.text = [NSString stringWithFormat:@"¥%.2f",[model.orderTotalPrice floatValue]];
    
    NSString *str =  [NSString stringWithFormat:@"%@",model.orderStatus];
    NSString *strPay =  [NSString stringWithFormat:@"%@",model.payStatus];// 分为待付款 和已付款
    NSString *sendStr =  [NSString stringWithFormat:@"%@",model.sendStatus];//发货状态 0:待发 1:代收
    
    [self loadFooterViewWithStr:str withPay:strPay withSend:sendStr with:model];

    __weak typeof(self) weakSelf = self;
    __weak OnlineOrderFooterView *footer = _footerView;
    
    // 删除按钮点击
    _footerView.deleteBtnClick = ^(){
        
        __weak  OnlineModel *model = weakSelf.dataArr[section];
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:@"确定要删除吗？" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
            [weakSelf loadDeleteBtnClickOrderWith:model.orderId];
        }];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertVC addAction:action];
        [alertVC addAction:action1];
        
        [weakSelf presentViewController:alertVC animated:YES completion:nil];
    };
    
    _footerView.cancelBtnAndPayBtn = ^(NSInteger index){
        
        switch (index) {
            case kCancleBtn_tag:
            {
                if([footer.cancelbtn.titleLabel.text isEqualToString:@"取消"]){
                    
                    __weak  OnlineModel *model = weakSelf.dataArr[section];
                    weakSelf.model = model;
                    weakSelf.showView.hidden = NO;
                }else{
                    
                }
            }
                break;
            case kPayMoneyBtn_tag:
            {
                if([footer.payBtn.titleLabel.text isEqualToString:@"评价"]){
                
//                    __weak  OnlineModel *model = weakSelf.dataArr[section];
//                    CommentViewController *commentVC = [[CommentViewController alloc]init];
//                    commentVC.commentDelegate = weakSelf;
//                    commentVC.orderId = model.orderId;
//                    commentVC.productImg = model.productImg;
//                    commentVC.productList = model.productList;
//                    commentVC.type = @"MALL";
//                    [weakSelf.navigationController pushViewController:commentVC animated:YES];
                }else  if([footer.payBtn.titleLabel.text isEqualToString:@"确认收货"]){
    
                    __weak  OnlineModel *model = weakSelf.dataArr[section];
                    weakSelf.model = model;
                    weakSelf.statusStr = @"OrderPlaced";
                    weakSelf.showView.hidden = NO;
                    weakSelf.showViewLab.text = @"确认收货吗？";
                    [weakSelf.cancleBtn setTitle:@"取消" forState:(UIControlStateNormal)];
                    [weakSelf.sureBtn setTitle:@"确认" forState:(UIControlStateNormal)];
                   
                }else {
                    
                    {
                        PayOderViewController *payVC = [[PayOderViewController alloc]init];
                        payVC.orderId = model.orderId;
                        [weakSelf.navigationController pushViewController:payVC animated:YES];
                    }
                }
            }
                break;
                
            default:
                break;
        }
        
    };
    
    return self.footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 81;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ListTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    OnlineModel *model = self.dataArr[indexPath.section];
    [cell setListCell:model.productList[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OnlineModel *model = self.dataArr[indexPath.section];

    OnlineDetailViewController *onlineVC = [[OnlineDetailViewController alloc] init];
    onlineVC.orderLable = [self displayDisponse:model];
    onlineVC.popDelegate = self;
    onlineVC.orderId = model.orderId;
//    onlineVC.indexTag = indexPath.row;
    [self.navigationController pushViewController:onlineVC animated:YES];
}

- (NSString *)displayDisponse:(OnlineModel *)model
{
    NSString *orderStatus = model.orderStatus;
    NSString *sendStatus = [model.sendStatus description];
    NSString *backString = @"";
    if ([@"OrderApproved" isEqualToString:orderStatus] && [@"1" isEqualToString:sendStatus]) {
        backString = @"待收货";
    }else if ([@"OrderApproved" isEqualToString:orderStatus] && [@"0" isEqualToString:sendStatus]) {
        backString = @"待发货";
    }else if ([@"OrderPlaced" isEqualToString:orderStatus]) {
        backString = @"待付款";
    }else if ([@"OrderCancelled" isEqualToString:orderStatus]) {
        backString = @"已取消";
    }else if ([@"OrderCompleted" isEqualToString:orderStatus]) {
        backString = @"已完成";
    }
    return backString;
}
- (void)loadHeaderViewWithStr:(NSString *)str withPay:(NSString *)payStr withSend:(NSString *)sendStr
{
    if ([str isEqualToString:@"OrderPlaced"]) {// 下单
//        _footerView.cancelbtn.hidden = NO;
//        _footerView.payBtn.hidden = NO;
        // 待付款 和 已付款
        if ([payStr isEqualToString:@"0"]) {// 待付款
            _onlineHeader.payStausLab.text = @"待支付";
            _onlineHeader.payStausLab.textColor = [UIColor colorWithHexString:@"#ff6600"];
        }else{//已付款
            _onlineHeader.payStausLab.text = @"已付款";
            _onlineHeader.payStausLab.textColor = [UIColor colorWithHexString:@"#ff6600"];
        }
    }else if([str isEqualToString:@"OrderApproved"]){// 确认//       sendStatus
        if ([sendStr isEqualToString:@"0"]) {// 待发货
            
            _onlineHeader.payStausLab.text = @"待发货";
            _onlineHeader.payStausLab.textColor = [UIColor colorWithHexString:@"#E22425"];
            
        }else{
            
            _onlineHeader.payStausLab.text = @"待收货";
            _onlineHeader.payStausLab.textColor = [UIColor colorWithHexString:@"#E22425"];
        }
    }else if([str isEqualToString:@"OrderCompleted"]){//完成
        _onlineHeader.payStausLab.text = @"已完成";
        _onlineHeader.payStausLab.textColor = [UIColor colorWithHexString:@"#E22425"];

    }else if([str isEqualToString:@"OrderCancelled"]){//取消
        _onlineHeader.payStausLab.text = @"已取消";
        _onlineHeader.payStausLab.textColor = [UIColor colorWithHexString:@"#E22425"];
    }
}

- (void)loadFooterViewWithStr:(NSString *)str withPay:(NSString *)payStr withSend:(NSString *)sendStr with:(OnlineModel *)model{
    if ([str isEqualToString:@"OrderPlaced"]) {// 下单
         _footerView.cancelbtn.hidden = NO;
         _footerView.payBtn.hidden = NO;
        // 待付款 和 已付款
        if ([payStr isEqualToString:@"0"]) {// 待付款
            self.footerView.cancelbtn.hidden = NO;
            self.footerView.payBtn.hidden = NO;
            [_footerView.cancelbtn setTitle:@"取消" forState:(UIControlStateNormal)];
            [_footerView.payBtn setTitle:@"支付" forState:(UIControlStateNormal)];
        }else{//已付款
            self.footerView.cancelbtn.hidden = YES;
            self.footerView.payBtn.hidden = YES;
//            _onlineHeader.payStausLab.text = @"已付款";
//            _onlineHeader.payStausLab.textColor = [UIColor colorWithHexString:@"#ff6600"];
        }
    }else if([str isEqualToString:@"OrderApproved"]){// 确认//       sendStatus
        if ([sendStr isEqualToString:@"0"]) {// 待发货
            _footerView.cancelbtn.hidden = YES;
            _footerView.payBtn.hidden = YES;

        }else{// 待收货
            _footerView.cancelbtn.hidden = YES;
            _footerView.payBtn.hidden = NO;
            
            [_footerView.payBtn setTitle:@"确认收货" forState:(UIControlStateNormal)];
        }
    }else if([str isEqualToString:@"OrderCompleted"]){//完成
      
        _footerView.cancelbtn.hidden = YES;
        _footerView.payBtn.hidden = NO;
        [_footerView.payBtn setTitle:@"评价" forState:(UIControlStateNormal)];

        NSString *commetStatus = [NSString stringWithFormat:@"%@",model.commentStatus];
        if ([commetStatus isEqualToString:@"1"]) {
            _footerView.payBtn.hidden = YES;
        }else{
            _footerView.payBtn.hidden = NO;
        }
        
        
    }else if([str isEqualToString:@"OrderCancelled"]){//取消OrderCancelled

        self.footerView.cancelbtn.hidden = YES;
        _footerView.payBtn.hidden = YES;
        _footerView.deleteBtn.hidden = NO;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

// 点错了
- (IBAction)cilckedWrongBtn:(UIButton *)sender {
    
    self.showView.hidden = YES;
}

// 取消订单 / 确认收货
- (IBAction)cancleOrderBtn:(UIButton *)sender {
    
    self.showView.hidden = YES;
    if ([self.statusStr isEqualToString:@"OrderPlaced"]){//
        // 点击确认收货
          [self loadCompleteOrderWith:self.model.orderId];
    
    }else{// 待付款的
        // 取消订单
           [self loadCancelOrderWith:self.model.orderId]; 
    }
}

#pragma mark -Label富文本属性
- (NSMutableAttributedString *)setLabelString:(NSString *)string
{
    NSString *text = [NSString stringWithFormat:@"%@(爱心订单)", string];
    NSMutableAttributedString *attrLabel = [[NSMutableAttributedString alloc] initWithString:text];
    NSRange range = [text rangeOfString:@"(爱心订单)"];
    [attrLabel addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
    [attrLabel addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:range];
    return attrLabel;
}

#pragma mark - -------------网络请求发送--------------------------------------
#pragma mark - 点击删除
- (void)loadDeleteBtnClickOrderWith:(NSString *)orderId
{
    NSDictionary *parameter = @{@"orderId":orderId};
    [[BDNetworkTools sharedInstance] deleteOrderWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        // 全部
        [self loadDataWithStatus:@"" pageIndex:self.pageIndexAll];
        [self.tableview.mj_header beginRefreshing];
    }];
}

#pragma mark - 点击取消
- (void)loadCancelOrderWith:(NSString *)orderId{
    NSDictionary *parameter = @{@"orderId":orderId};
    [[BDNetworkTools sharedInstance] putOnlineOrderCancelWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        // 刷新界面  判断点击的是全部 还是 待收货
        //        if (self.tag == kAllBtn_Tag) {
        if (self.dingDanLeiXing == DingDanLeiXingAll) {
            [self loadDataWithStatus:@"" pageIndex:self.pageIndexAll];
        }else{
            [self loadDataWithStatus:@"OrderPlaced" pageIndex:self.pageIndexDaiShou];
        }
        [self.tableview.mj_header beginRefreshing];
        [self setOnlineHeaderViewNum];
    }];
}
#pragma mark - 确认收货
- (void)loadCompleteOrderWith:(NSString *)orderId{
    NSDictionary *parameter = @{@"orderId":orderId};
    [[BDNetworkTools sharedInstance] putOnlineOrderVerifyWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        // 刷新界面  判断点击的是全部 还是 待付款
        if (self.dingDanLeiXing == DingDanLeiXingAll) {
            [self loadDataWithStatus:@"" pageIndex:self.pageIndexAll];
        }else{
            [self loadDataWithStatus:@"OrderApproved" pageIndex:self.pageIndexDaiFu];
        }
        [self.tableview.mj_header beginRefreshing];
        [self setOnlineHeaderViewNum];
    }];
}

- (void)loadDataWithStatus:(NSString *)orderType pageIndex:(NSUInteger)pageIndex
{
    NSDictionary *paramDic =  @{@"orderType":orderType,@"pageIndex":[NSString stringWithFormat:@"%ld", (unsigned long)pageIndex],@"pageSize":[NSString stringWithFormat:@"%ld", (long)self.pageSize]};
    self.paramDic = [NSMutableDictionary dictionaryWithDictionary:paramDic];
    [self requestDataList];
}

#pragma mark -获取全民公益店铺ID
- (void)requestCommonwealStoreIDListWithApikey:(NSString *)apikey
{

}

#pragma mark - 请求数据
- (void)requestDataList
{
    [[BDNetworkTools sharedInstance] getOnlineListWithParameter:self.paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        if (self.isUp || self.dingDanLeiXingMiddle) {// _isUp = YES是上啦
            //下拉时 清空之前的数据
            [self.dataArr removeAllObjects];
        }
        
        NSArray *arr = [responseObject objectForKey:@"orderList"];
        for (NSDictionary *dic in arr) {
            OnlineModel *model = [[OnlineModel alloc]init];// 店铺对象
            [model setValuesForKeysWithDictionary:dic];
            if (self.dingDanLeiXing == DingDanLeiXingDaiFa || self.dingDanLeiXing == DingDanLeiXingDaiShou) {//待发货  待收货
                NSString *str = [NSString stringWithFormat:@"%@",model.sendStatus];
                if (self.dingDanLeiXing == DingDanLeiXingDaiFa && [str isEqualToString:@"0"]) {//sendStatus = 0;//待发货
                    [self.dataArr addObject:model]; //self.dataArr店铺数组
                }
                if(self.dingDanLeiXing == DingDanLeiXingDaiShou && [str isEqualToString:@"1"]){
                    [self.dataArr addObject:model];
                }
            }else{
                [self.dataArr addObject:model]; //self.dataArr店铺数组
            }
        }
        [self.tableview reloadData];
        [self.tableview.mj_header endRefreshing];
        if (arr.count < 20) {
            [self.tableview.mj_footer endRefreshingWithNoMoreData];
        }else {
            [self.tableview.mj_footer endRefreshing];
        }
    }];
}
// 添加上拉加载和下拉刷新
- (void)addRefresh
{
    __weak typeof(self) weakSelf = self;
    // 添加下啦刷新
    self.tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self resetPageIndex];
        self.isUp = YES;
        self.dingDanLeiXingMiddle = YES;
        switch (self.dingDanLeiXing) {
            case DingDanLeiXingAll:
                [weakSelf loadDataWithStatus:@"" pageIndex:self.pageIndexAll];
                break;
            case DingDanLeiXingDaiFu:// 待付款 OrderPlaced
                [weakSelf loadDataWithStatus:@"OrderPlaced" pageIndex:self.pageIndexDaiFu];
                break;
            case DingDanLeiXingDaiFa://待发货
                [weakSelf loadDataWithStatus:@"OrderApproved" pageIndex:self.pageIndexDaiFa];
                break;
            case DingDanLeiXingDaiShou://待收货
                [weakSelf loadDataWithStatus:@"OrderApproved" pageIndex:self.pageIndexDaiShou];
                break;
            case DingDanLeiXingFinish:// 已完成
                [weakSelf loadDataWithStatus:@"OrderCompleted" pageIndex:self.pageIndexFinish];
                break;
            default:
                break;
        }
        
    }];
    // 添加上拉加载
    self.tableview.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.isUp = NO;
        self.dingDanLeiXingMiddle = NO;
        switch (self.dingDanLeiXing) {
            case DingDanLeiXingAll:
                self.pageIndexAll++;
                [weakSelf loadDataWithStatus:@"" pageIndex:self.pageIndexAll];
                break;
            case DingDanLeiXingDaiFu:// 待付款 OrderPlaced
                self.pageIndexDaiFu++;
                [weakSelf loadDataWithStatus:@"OrderPlaced" pageIndex:self.pageIndexDaiFu];
                break;
            case DingDanLeiXingDaiFa://待发货
                self.pageIndexDaiFa++;
                [weakSelf loadDataWithStatus:@"OrderApproved" pageIndex:self.pageIndexDaiFa];
                break;
            case DingDanLeiXingDaiShou://待收货
                self.pageIndexDaiShou++;
                [weakSelf loadDataWithStatus:@"OrderApproved" pageIndex:self.pageIndexDaiShou];
                break;
            case DingDanLeiXingFinish:// 已完成
                self.pageIndexFinish++;
                [weakSelf loadDataWithStatus:@"OrderCompleted" pageIndex:self.pageIndexFinish];
                break;
            default:
                break;
        }
    }];
}

// 监听订单详情界面确定收货通知按钮点击
-(void)sureBtnClick:(NSNotification *)ordID {
   
    [self loadCompleteOrderWith:ordID.object];
}

- (void)cancelBtnClick:(NSNotification *)noti
{
    // 取消订单
    [self loadCancelOrderWith:noti.object];
}

@end
