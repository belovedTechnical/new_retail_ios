//
//  OrderCofirmViewController.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/8.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressManageViewController.h"

@interface OrderCofirmViewController : UIViewController

@property(nonatomic,copy)NSString *idStr;
@property(nonatomic,copy)NSString *isCart;
@property(nonatomic,copy)NSString *productStoreId;// 店铺id
@property(nonatomic,assign)NSInteger quantity;

@end
