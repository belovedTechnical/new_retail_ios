//
//  ExpressInformationViewController.h
//  BelovedHotel
//
//  Created by Sam Feng on 2017/5/10.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpressInformationViewController : UIViewController

@property (strong, nonatomic) NSString *expressNumber;
@property (strong, nonatomic) NSString *expressCompany;
@property (nonatomic, strong) NSString *orderId;
@end
