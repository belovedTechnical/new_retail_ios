//
//  OrderCofirmViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/8.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "OrderCofirmViewController.h"
#import "AddressManageViewController.h"
#import "SendTicketViewController.h"
#import "PayOderViewController.h"
#import "CouponViewController.h"

#import "OrderConfirmProductView.h"
#import "OrderConfirmHeaderview.h"
#import "OrderConfirmHotelName.h"
#import "OrderAddressCell.h"
#import "InstantJudgeCell.h"
#import "NoneAddressCell.h"
#import "ListBuyCell.h"

#import "OrderConfirmListModel.h"
#import "AddressListModel.h"
#import "CouponModel.h"


@interface OrderCofirmViewController()<
                                        UITableViewDataSource,
                                        UITableViewDelegate,
                                        UITextFieldDelegate,
                                        OrderConfirmVCDelegate,
                                        OrderCofirmViewControllerDelegate
                                        >
// 去支付底部View
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewConstraints;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic, strong) OrderConfirmHeaderview *orderheaderView;
@property (nonatomic, strong) OrderConfirmHotelName *hotelView;
@property (nonatomic, strong) InstantJudgeCell *instantJudgeCell;
@property (nonatomic, assign) BOOL isDelegate;// 是否跳转设置 发票
@property (nonatomic, assign) BOOL noneAddress;
@property (nonatomic, copy) NSString *apikey;
@property (nonatomic, strong) UIButton *saveBtn;

// 商品总价
@property (weak, nonatomic) IBOutlet UILabel *taotalPriceLab;
/** 是否打折商品价格 */
@property(nonatomic, strong) NSString *price;
/** 发票抬头 个人/单位 */
@property (nonatomic, copy) NSString *NameStr;
@property (nonatomic, strong) NSDictionary *dic;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableArray *adressArr;// 地址
@property (strong, nonatomic) OrderConfirmProductView *productView;
@property (nonatomic, strong) OrderAddressCell *addressCell;
@property (nonatomic, assign) BOOL isVCPrice;
/** 总运费 */
@property (nonatomic, strong) NSString *totalFreight;
/** 优惠券ID */
@property (nonatomic, strong) NSString *userCouponId;
/** 优惠券名字 */
@property (nonatomic, strong) NSString *couponName;
/** 优惠券金额 */
@property (nonatomic, strong) NSString *couponAmount;
/** 优惠劵model数组 */
@property (nonatomic, strong) NSArray *couponModelArray;
/** 不可用优惠券数组 */
@property (nonatomic, strong) NSArray *notUsedCouponModelArray;
@property (nonatomic, strong) NSArray *hotelAllIDArray;
@property (nonatomic, strong) NSString *addressName;
@property (nonatomic, strong) NSString *taxpayerNumber;
@property (nonatomic, assign) BOOL isMail;
@property (nonatomic, assign) BOOL isHotelOrder;

@end

@implementation OrderCofirmViewController
- (NSArray *)hotelAllIDArray
{
    if (_hotelAllIDArray) {
        _hotelAllIDArray = [NSArray array];
    }
    return _hotelAllIDArray;
}
- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        self.dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (NSMutableArray *)adressArr{
    if (!_adressArr) {
        self.adressArr = [NSMutableArray array];
    }
    return _adressArr;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBar.hidden = NO;
    
    self.bottomViewConstraints.constant = SafeAreaBottomHeight;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"确认订单";

    //[self requestNetworkHotelAllIDList];
    
    [self initUI];
    // 默认地址
    [self loadCantactList];
}

- (void)initUI
{
    self.isDelegate = NO;
    // 从购物车跳转   用户自己立即购买
    [self loadDataFromCart];
    
    [self.tableview registerNib:[UINib nibWithNibName:@"NoneAddressCell" bundle:nil] forCellReuseIdentifier:@"NoneAddressCell"];
    [self.tableview registerNib:[UINib nibWithNibName:@"OrderAddressCell" bundle:nil] forCellReuseIdentifier:@"OrderAddressCell"];
    [self.tableview registerNib:[UINib nibWithNibName:@"ListBuyCell" bundle:nil] forCellReuseIdentifier:@"ListBuyCell"];
    self.tableview.estimatedRowHeight = 600;
    self.tableview.rowHeight = UITableViewAutomaticDimension;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationCouponID:) name:kbackCouponIDNoti object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noBackCouponIDNotiFunc) name:kNoBackCouponIDNoti object:nil];
}
#pragma mark -couponId通知方法
- (void)notificationCouponID:(NSNotification *)noti
{
    NSString *userCouponId = noti.userInfo[@"userCouponId"];
    NSString *couponAmount = noti.userInfo[@"amount"];
    NSString *couponName = noti.userInfo[@"name"];
    self.userCouponId = userCouponId;
    self.couponAmount = couponAmount;
    self.couponName = couponName;
    [self loadSubviews];
}

// 不使用优惠劵
- (void)noBackCouponIDNotiFunc
{
    _userCouponId = nil;
    self.couponName = nil;
    _productView.totalPriceLab.text = [NSString stringWithFormat:@"¥%.2f",[_price floatValue]];
    self.taotalPriceLab.text = [NSString stringWithFormat:@"¥%.2f",[_price floatValue]];
    [_productView.couponBtn setTitle:[NSString stringWithFormat:@"%lu张优惠劵", (unsigned long)_couponModelArray.count] forState:UIControlStateNormal];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setSendHeader:(NSString *)personOrCompany number:(NSString *)number
{
    self.NameStr = personOrCompany;
    self.taxpayerNumber = number;
    self.isDelegate = YES;
    [self loadSubviews];
}
// 没有默认地址的情况下，设置默认地址后跳转到此界面 加载界面 显示地址
- (void)updateDefault{// 代理协议
    // 默认地址
    [self loadCantactList];
}

- (void)loadSubviews{

    
    
    if (_isHotelOrder == NO) {//非酒店订单，不需要选择控件
        self.tableview.tableHeaderView = [UIView new];
    } else {
        UIView *headerView = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, kScreenW, 85))];
        headerView.backgroundColor = [UIColor whiteColor];
        self.tableview.tableHeaderView = headerView;
        self.orderheaderView = [[OrderConfirmHeaderview alloc]initWithFrame:CGRectMake(0, 0, kScreenW, 85)];
        [headerView addSubview:self.orderheaderView];
    }
    
    [self.orderheaderView.instantBtn addTarget:self action:@selector(instantBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.orderheaderView.mailBtn addTarget:self action:@selector(mailBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *footerView = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, kScreenW, 255))];
    self.tableview.tableFooterView = footerView;
    _productView = [[OrderConfirmProductView alloc]initWithFrame:CGRectMake(0, 0, kScreenW, 255)];
    _productView.textField.delegate = self;
   
    [self loadProductViewWith:_productView];
   
    if (!self.isDelegate) {//不选择发票
        _productView.isSendLab.text =@"不需要发票";
    }else{// 选择发票从后面跳转
        _productView.isSendLab.text = self.NameStr;
    }
    if (_couponModelArray.count == 0) {
        _productView.couponBtn.userInteractionEnabled = NO;
    }else {
        [_productView.couponBtn setTitle:[NSString stringWithFormat:@"%lu张优惠劵", (unsigned long)_couponModelArray.count] forState:UIControlStateNormal];
        _productView.couponBtn.userInteractionEnabled = YES;
    }
    if (self.couponName != nil) {
        [self.productView.couponBtn setTitle:[NSString stringWithFormat:@"优惠抵扣%.2f元", [self.couponAmount floatValue]] forState:UIControlStateNormal];
        [self.productView.couponBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        CGFloat totalPriceF = ([_price floatValue] - [self.couponAmount floatValue]) < 0.00 ? 0.00 : [_price floatValue] - [self.couponAmount floatValue];
        _productView.totalPriceLab.text = [NSString stringWithFormat:@"¥%.2f", totalPriceF];
        self.taotalPriceLab.text = [NSString stringWithFormat:@"¥%.2f",totalPriceF];
    }else {
        _productView.totalPriceLab.text = [NSString stringWithFormat:@"¥%@",_price];
    }
    _productView.textField.borderStyle = UITextBorderStyleNone;
    NSString *freightStr = [NSString stringWithFormat:@"¥%@",_totalFreight];
    if (freightStr != nil) {
        CGFloat freightF = [freightStr floatValue];
        _productView.yunFeiLab.text = [NSString stringWithFormat:@"¥%.2f", freightF];
    }else {
        _productView.yunFeiLab.text = @"¥0.00";
    }
    [footerView addSubview:_productView];
   
    #pragma mark -优惠券点击
    // 优惠劵点击
    BDWeakSelf();
    _productView.couponBtnBlock = ^{
        CouponViewController *couponVC = [[CouponViewController alloc] init];
        couponVC.isMeController = NO;
        couponVC.couponModelArray = weakSelf.couponModelArray;
        couponVC.notUsedCouponModelArray = weakSelf.notUsedCouponModelArray;
        [weakSelf.navigationController pushViewController:couponVC animated:YES];
    };
}

- (void)instantBtnClick
{
    self.isMail = NO;
    [self.tableview reloadData];
}
- (void)mailBtnClick
{
    self.isMail = YES;
    [self.tableview reloadData];
}

#pragma mark - UITableview
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }
    
    return self.dataArr.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
           return 30;
    }
    return -1;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        OrderConfirmHotelName *view = [[OrderConfirmHotelName alloc]initWithFrame:CGRectMake(0, 0, kScreenW, 30)];
        
        return view;
    }    
    return [[UIView alloc]init];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (_isMail == NO) {
            return 210;
        }
        return 100;
    }
    
    return 86;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section ==  0) {
            if (_isMail == NO) {
                InstantJudgeCell *cell = [[InstantJudgeCell alloc] init];
                self.instantJudgeCell = cell;
                return cell;
            }
        if (self.adressArr.count >= 1) {
            // 有默认地址 返回的cell
            OrderAddressCell *addressCell = [tableView dequeueReusableCellWithIdentifier:@"OrderAddressCell" forIndexPath:indexPath];
            addressCell.selectionStyle = UITableViewCellSelectionStyleNone;
            _addressCell = addressCell;
            AddressListModel *model = self.adressArr[0];
            NSString *addressName = [NSString stringWithFormat:@"%@", model.attnName];
            [addressCell setAddressCellDefaultWith:model];
            if ([_productStoreId containsString:URL_GlobalPurchaseID]) {
                addressCell.bgImageConstraints.constant = 60;
                if ([_addressName isEqualToString:addressName]) {
                    _saveBtn.userInteractionEnabled = NO;
                }else {
                    _saveBtn.userInteractionEnabled = YES;
                    _saveBtn.backgroundColor = [UIColor redColor];
                    addressCell.IDNumberF.text = @"";
                    addressCell.IDNumberF.enabled = YES;
                    _addressName = addressName;
                }
            }else {
                addressCell.bgImageConstraints.constant = 10;
                addressCell.IDNumberF.placeholder = @"";
            }
            __weak typeof(addressCell) weakAddressCell = addressCell;
            addressCell.saveBtnBlock = ^(BOOL isLegitimacy, UIButton *sender) {
                if (isLegitimacy == YES) {
                    _saveBtn = sender;
                    [self.view endEditing:YES];
                    sender.backgroundColor = [UIColor darkGrayColor];
                    [sender setTitle:@"已认证" forState:UIControlStateNormal];
                    sender.userInteractionEnabled = NO;
                    weakAddressCell.IDNumberF.enabled = NO;
                    [self requestNetAddressIDNumber:sender];
                }else {
                    
                }
            };
            return addressCell;
        }else{
            
            NoneAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NoneAddressCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
       
    }else{
        ListBuyCell *cell2 = [tableView dequeueReusableCellWithIdentifier:@"ListBuyCell" forIndexPath:indexPath];
        cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        OrderConfirmListModel *model = self.dataArr[indexPath.row];
        [cell2 setCellWithModel:model isVCPrice:NO];
        return cell2;
    }
    
    return [[UITableViewCell alloc]init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addressViewClick)];
        [_addressCell.addressView addGestureRecognizer:tapGesture];
        
        if (self.adressArr.count >= 1) {
            [self addressViewClick];
        } else {
            [self addressViewClick];
        }
        
    }
    
}

- (void)addressViewClick
{
    AddressManageViewController *addressVC = [[AddressManageViewController alloc]init];
    addressVC.defaultDelegate = self;
    addressVC.setDefault = @"Y";// 去设置默认地址
    [self.navigationController pushViewController:addressVC animated:YES];
}

- (void)loadProductViewWith:(OrderConfirmProductView *)productView{
    
      __weak typeof(self) weakSelf = self;
      productView.sendBtn = ^(NSInteger index){ // 发票
        SendTicketViewController *sendVC = [[SendTicketViewController alloc]init];
        sendVC.popDelegate = self;
        [weakSelf.navigationController pushViewController:sendVC animated:YES];
    };
}

- (void)showMessage:(NSString *)string{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"" message:string preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertVC addAction:action1];
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark - 去支付
- (IBAction)goToPayBtn:(id)sender {
    
    if (self.adressArr.count > 0) {
    
    AddressListModel *model = self.adressArr[0];
    NSDictionary *dict = [NSDictionary dictionary];
    // remark 备注留言
    // 数量
    NSString *quantity = [NSString stringWithFormat:@"%ld",(long)self.quantity];
    // 留言
    NSString *remark = self.productView.textField.text;
    // 优惠券
    NSString *counponID = _userCouponId == nil ? @"" : _userCouponId;
    // 发票抬头
    NSString *invoice = _productView.isSendLab.text == nil? @"":_productView.isSendLab.text;
    // 纳税人识别号
    NSString *taxpayerNum = self.taxpayerNumber == nil?@"":self.taxpayerNumber;
    // 是否立即品鉴  YES邮寄回家
    NSString *isEatSoon = _isMail == YES ? @"N":@"Y";
    // 品鉴人姓名
    NSString *contactName = _instantJudgeCell.personDudgeTF.text;
    // 品鉴人电话
    NSString *contactPhone = _instantJudgeCell.phoneNuberTF.text;
    // 品鉴人房间号
    NSString *roomNum = _instantJudgeCell.roomNumberTF.text;
//    BDLog(@"idStr=%@productStoreId=%@isCart=%@", self.idStr, self.productStoreId,self.isCart);
    // _isCart == NO 为酒店店铺
     if ([self.isCart isEqualToString:@"Y"]) {
         if (_isMail == NO) {
             dict = @{
                      @"productIdList":self.idStr,
                      @"productStoreId":self.productStoreId,
                      @"isCart":self.isCart,
                      @"remark": remark,
                      @"userCouponId":counponID,
                      @"taxpayerNum":taxpayerNum,
                      @"invoice":invoice,
                      @"isEatSoon":isEatSoon,
                      @"contactName":contactName,
                      @"contactPhone":contactPhone,
                      @"roomNum":roomNum,
                      };
         }
           dict = @{
                    @"contactMechId":model.contactMechId,
                    @"productIdList":self.idStr,
                    @"productStoreId":self.productStoreId,
                    @"isCart":self.isCart,
                    @"remark": remark,
                    @"userCouponId":counponID,
                    @"taxpayerNum":taxpayerNum,
                    @"invoice":invoice,
                    @"isEatSoon":isEatSoon,
                    @"contactName":contactName,
                    @"contactPhone":contactPhone,
                    @"roomNum":roomNum,
                    };
     }else{ //contactMechId
         if (_isMail == NO) {
             dict = @{
                      @"productId":self.idStr,
                      @"isCart":self.isCart,
                      @"productStoreId":self.productStoreId,
                      @"quantity":quantity,
                      @"remark": remark,
                      @"userCouponId":counponID,
                      @"taxpayerNum":taxpayerNum,
                      @"invoice":invoice
                      };
         }
          dict = @{
                   @"contactMechId":model.contactMechId,
                   @"productId":self.idStr,
                   @"isCart":self.isCart,
                   @"productStoreId":self.productStoreId,
                   @"quantity":quantity,
                   @"remark": remark,
                   @"userCouponId":counponID,
                   @"taxpayerNum":taxpayerNum,
                   @"invoice":invoice
                  };
     }
    if ([_productStoreId containsString:URL_GlobalPurchaseID]) {
        if (_addressCell.saveBtn.userInteractionEnabled == NO) {
            [self loadDetailWith:dict];
        }else {
            [self showAlertWithTitle:@"提示" message:@"收件人与与身份证号不一致" AscertainHandler:^(UIAlertAction *action) {
                
            }];
        }
    }else {
        [self loadDetailWith:dict];
    }
    } else {
        [self showMessage:@"无地址信息，请先添加收货地址吧！"];
    }
}

- (void)loadDetailWith:(NSDictionary *)dict{
    [[BDNetworkTools sharedInstance] postOrderMakeWithParameter:dict Block:^(NSDictionary *responseObject, NSError *error) {
        self.dic = [NSDictionary dictionaryWithDictionary:responseObject];
        PayOderViewController *payVC = [[PayOderViewController alloc]init];
        payVC.orderId = [NSString stringWithFormat:@"%@",[self.dic objectForKey:@"orderId"]];
        payVC.isManyStore = NO;
        [self.navigationController pushViewController:payVC animated:YES];
        
        [self.tableview reloadData];
    }];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    self.tableview.contentInset = UIEdgeInsetsMake(-230, 0, 0, 0);
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.tableview.contentInset = UIEdgeInsetsMake(65, 0, 0, 0);
}

#pragma mark -网络请求
#pragma -请求地址列表判断是否有默认地址
- (void)loadCantactList{
    [self.adressArr removeAllObjects];
    [[BDNetworkTools sharedInstance] getAddressRedactWithBlock:^(NSDictionary *responseObject, NSError *error) {
        NSArray *aar = [responseObject objectForKey:@"postalAddressList"];
        for (NSDictionary *dic in aar) {
            AddressListModel *model = [[AddressListModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            // 只把默认地址加到数组里
            if ([model.isDefault isEqualToString:@"yes"] ) {
                [self.adressArr removeAllObjects];
                [self.adressArr addObject:model];
            }else {
                if (model != nil) {
                    [self.adressArr addObject:model];
                }
            }
        }
        if (_isMail == YES) {
            // 请求运费
            [self singleStoreFreightMoney];
        }
        [BDShowHUD dismissSVP];
        [self.tableview reloadData];
    }];
}

#pragma mark -单店铺价格计算产品确定
- (void)loadDataFromCart{
    [self.dataArr removeAllObjects];
    NSDictionary *dic = [NSDictionary dictionary];
    if ([self.isCart isEqualToString:@"Y"]) {// 购物车界面
        dic = @{@"productIdList":self.idStr,
                @"productStoreId":self.productStoreId,
                @"isCart":self.isCart
                };
    }else{
        //h5 用户自己立即购买
        self.isCart = @"N";
        dic = @{@"productId":self.idStr,
                @"isCart":self.isCart,
                @"productStoreId":self.productStoreId,
                @"quantity":[NSString stringWithFormat:@"%ld",(long)self.quantity]
                };
    }
    [[BDNetworkTools sharedInstance] postOrderAlonePriceCountWithParameter:dic Block:^(NSDictionary *responseObject, NSError *error) {
        self.dic = [NSDictionary dictionaryWithDictionary:responseObject];
        NSString *price = @"";
        if ([self.dic objectForKey:@"zTotalPrice"] == nil) {
            price = [NSString stringWithFormat:@"%@",[self.dic objectForKey:@"totalPrice"]];
        }else {
            price = [NSString stringWithFormat:@"%@",[self.dic objectForKey:@"zTotalPrice"]];
        }
        
        NSString *storeType = [self.dic objectForKey:@"storeType"];
        if ([storeType isEqualToString:@"MALL_STORE"] || [storeType isEqualToString:@"MALL"]) {
            _isHotelOrder = false;
            _isMail = true;
        } else {
            _isHotelOrder = true;
            _isMail = false;
        }
        
        self.taotalPriceLab.text = [NSString stringWithFormat:@"¥%@", price];
        self.price = price;
        NSArray *reArr = [self.dic objectForKey:@"productList"];
        for (NSDictionary *dic in reArr) {
            OrderConfirmListModel *model = [[OrderConfirmListModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [self.dataArr addObject:model];
        }
        [self couponRequestNetwork];
        [self loadSubviews];
        [self.tableview reloadData];
    }];
}

#pragma mark -获取总运费
- (void)singleStoreFreightMoney 
{
    if (self.adressArr.count > 0) {
        AddressListModel *model = self.adressArr[0];
        NSDictionary *paramDic = @{@"contactMechId":model.contactMechId,
                                   @"productIdList":_idStr,
                                   @"productStoreId":_productStoreId,
                                   @"isCart":_isCart
                                   };
        [[BDNetworkTools sharedInstance] postOrderAlonePriceFreightWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
            NSString *totalFreight = [responseObject objectForKey:@"totalFreight"];
            self.totalFreight = totalFreight;
        }];
    }
}

#pragma mark -优惠券接口
// 可以使用
- (void)couponRequestNetwork
{
    NSDictionary *paramDic = @{@"isCart":self.isCart,
                               @"pageIndex":@"0",
                               @"pageSize":@"99",
                               @"productStoreId":_productStoreId,
                               @"productIdList":_idStr ,
                               @"amount":_price,
                               @"quantity":[NSString stringWithFormat:@"%ld",(long)self.quantity],
                               @"type":@"available"
                               };
    [[BDNetworkTools sharedInstance] postOrderAloneCouponWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        NSMutableArray *tempArr = [NSMutableArray array];
        NSArray *dataArray = [responseObject objectForKey:@"data"];
        for (NSDictionary *dict in dataArray) {
            [tempArr addObject:[CouponModel couponModelWithDict:dict]];
        }
        self.couponModelArray = tempArr;
        [self notToBeUsedCouponRequestNetwork];
    }];
}

// 不可使用
- (void)notToBeUsedCouponRequestNetwork
{
    NSDictionary *paramDic = @{@"isCart":self.isCart,
                               @"pageIndex":@"0",
                               @"pageSize":@"99",
                               @"productStoreId":_productStoreId,
                               @"productIdList":_idStr ,
                               @"amount":_price,
                               @"quantity":[NSString stringWithFormat:@"%ld",(long)self.quantity],
                               @"type":@"unavailable"
                               };
    [[BDNetworkTools sharedInstance] postOrderAloneCouponWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        NSMutableArray *tempArr = [NSMutableArray array];
        NSArray *dataArray = [responseObject objectForKey:@"data"];
        for (NSDictionary *dict in dataArray) {
            [tempArr addObject:[CouponModel couponModelWithDict:dict]];
        }
        self.notUsedCouponModelArray = tempArr;
        [self loadSubviews];
    }];
}

- (void)requestNetAddressIDNumber:(UIButton *)sender
{
    if (self.adressArr.count > 0) {
        AddressListModel *model = self.adressArr[0];
        NSDictionary *paramDic = @{@"contactMechId":model.contactMechId,
                                   @"oldAttnName":[NSString stringWithFormat:@"%@",model.attnName],
                                   @"toVerifyIdCard":_addressCell.IDNumberF.text
                                   };
        [[BDNetworkTools sharedInstance] postIdentityCardCheckingWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
            sender.userInteractionEnabled = NO;
            if (error != nil) {
                // 错误提示
                NSString *errorStr = (NSString *)error;
                if ([errorStr containsString:@"此收货人已做了身份证验证"]) {
                    sender.userInteractionEnabled = NO;
                    [sender setTitle:@"已认证" forState:UIControlStateNormal];
                    _addressCell.IDNumberF.enabled = NO;
                }else {
                    sender.userInteractionEnabled = YES;
                    sender.backgroundColor = [UIColor redColor];
                    [self showAlertWithTitle:@"" message:errorStr AscertainHandler:^(UIAlertAction *action) {
                        
                    }];
            }
            }
        }];
    } else {
        [self showMessage:@"无地址信息，请先添加收货地址吧！"];
    }
}

- (void)requestNetworkHotelAllIDList
{
    [[BDNetworkTools sharedInstance] getHotelAllIDListWithParameter:nil Block:^(NSDictionary *responseObject, NSError *error) {
        self.hotelAllIDArray = [responseObject objectForKey:@"hsIdList"];
        if ([self.hotelAllIDArray containsObject:_productStoreId]) {
            _isMail = NO;
            self.isHotelOrder = YES;
        }else {
            _isMail = YES;
            self.isHotelOrder = NO;
        }

    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

@end
