//
//  OnlineProductListViewController.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/6.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, DingDanLeiXing)
{
    DingDanLeiXingAll,
    DingDanLeiXingDaiFu,
    DingDanLeiXingDaiFa,
    DingDanLeiXingDaiShou,
    DingDanLeiXingFinish,
};


@interface OnlineProductListViewController : UIViewController


@end
