//
//  SendTicketViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/8.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "SendTicketViewController.h"
#import "SendHeaderView.h"
#import "CompanyNameView.h"
@interface SendTicketViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *isSendImg;
@property (weak, nonatomic) IBOutlet UIImageView *noSendImg;

@property(nonatomic,strong)SendHeaderView *senderView;

// 个人
@property (strong, nonatomic) UIButton *individualBtn;
// 单位
@property (strong, nonatomic) UIButton *unitsBtn;
// 单位名称背景view
@property (strong, nonatomic) UIView *textView;

@property(nonatomic,assign)NSInteger isPerson;

@property(nonatomic,strong)UITextField *name;
@property (nonatomic, strong) UITextField *taxpayerNumber;
@end

@implementation SendTicketViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"发票信息";
    
    self.senderView = [[SendHeaderView alloc]initWithFrame:CGRectMake(0, 170, kScreenW, 85)];
    
    __weak typeof(self) weakSelf = self;
    self.senderView.personAndConConfimBtn = ^(NSInteger index){
        switch (index) {
            case kPerson_tag: // 个人
                
                weakSelf.isPerson = YES;
                [weakSelf loadPersonBtn];
                weakSelf.unitsBtn.hidden = YES;
                weakSelf.textView.hidden = YES;
                break;
            case kCompany_tag: // 单位
                
                weakSelf.isPerson = NO;
                [weakSelf loadCompanyBtn];
                weakSelf.individualBtn.hidden = YES;
                break;
            default:
                break;
        }
    };
    
}

- (void)loadPersonBtn{
    
    _individualBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    _individualBtn.frame = CGRectMake(20, self.senderView.frame.origin.y + 85 +25, kScreenW-40, 49);
    _individualBtn.backgroundColor = [UIColor colorWithHexString:@"#E22425"];
    [_individualBtn setTitle:@"确认发票信息" forState:(UIControlStateNormal)];
    _individualBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [_individualBtn addTarget:self action:@selector(hadlePersonBtn:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:_individualBtn];
}

// 选择个人 点击按钮
- (void)hadlePersonBtn:(UIButton *)sender{
    if ([self.popDelegate respondsToSelector:@selector(setSendHeader:number:)]) {
        [self.popDelegate setSendHeader:@"个人" number:self.taxpayerNumber.text];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadCompanyBtn{
    // 白色背景
    _textView = [[UIView alloc]initWithFrame:CGRectMake(0,  self.senderView.frame.origin.y + 85 +10, kScreenW, 100)];
    _textView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_textView];
    CGFloat comLabH = 50;
    //单位名称
    UILabel *comLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, comLabH)];
    comLab.text = @"单位名称";
    comLab.textColor = [UIColor colorWithHexString:@"#777777"];
    comLab.font = [UIFont systemFontOfSize:13];
    comLab.textAlignment = NSTextAlignmentCenter;
    [_textView addSubview:comLab];
    
    self.name =[[UITextField alloc]initWithFrame:CGRectMake(70, 0, kScreen_Width-90, comLabH)];
    _name.placeholder = @"请填写单位名称";
    _name.font = [UIFont systemFontOfSize:13];
    _name.borderStyle = UITextBorderStyleNone;
    _name.delegate = self;
    [_textView addSubview:_name];
    
    //纳税人识别号
    UILabel *taxpayerT = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, 60, comLabH)];
    taxpayerT.text = @"纳税人号";
    taxpayerT.textColor = [UIColor colorWithHexString:@"#777777"];
    taxpayerT.font = [UIFont systemFontOfSize:13];
    taxpayerT.textAlignment = NSTextAlignmentCenter;
    [_textView addSubview:taxpayerT];
    
    self.taxpayerNumber =[[UITextField alloc]initWithFrame:CGRectMake(70, 50, kScreen_Width-90, comLabH)];
    self.taxpayerNumber.placeholder = @"请纳税人识别号";
    self.taxpayerNumber.font = [UIFont systemFontOfSize:13];
    self.taxpayerNumber.borderStyle = UITextBorderStyleNone;
    self.taxpayerNumber.delegate = self;
    [_textView addSubview:self.taxpayerNumber];
    
    _unitsBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    _unitsBtn.frame = CGRectMake(20, _textView.bd_bottom+25, kScreenW-40, 49);
    _unitsBtn.backgroundColor = [UIColor colorWithHexString:@"#E22425"];
    [_unitsBtn setTitle:@"确认发票信息" forState:(UIControlStateNormal)];
    _unitsBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [_unitsBtn addTarget:self action:@selector(hadleCompanyBtn:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:_unitsBtn];
}

// 选择单位 点击按钮
- (void)hadleCompanyBtn:(UIButton *)sender{
    
    if ([self.popDelegate respondsToSelector:@selector(setSendHeader:number:)]) {
        [self.popDelegate setSendHeader:self.name.text number:self.taxpayerNumber.text];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

// 不需要发票
- (IBAction)noSendBtn:(UIButton *)sender {
    
    self.isSendImg.image = [UIImage imageNamed:@"icon_select_selected"];
    self.noSendImg.image = [UIImage imageNamed:@"icon_select"];
    self.senderView.hidden = YES;
    _individualBtn.hidden = YES;
    _unitsBtn.hidden = YES;
    _textView.hidden = YES;
    if ([self.popDelegate respondsToSelector:@selector(setSendHeader:number:)]) {
        [self.popDelegate setSendHeader:@"不需要发票" number:self.taxpayerNumber.text];
    }
}

// 需要发票
- (IBAction)sendBtn:(UIButton *)sender {
    
    self.senderView.hidden = NO;
    self.noSendImg.image = [UIImage imageNamed:@"icon_select_selected"];
    self.isSendImg.image = [UIImage imageNamed:@"icon_select"];
    [self.view addSubview:self.senderView];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //设置动画的名字
    [UIView beginAnimations:@"Animation" context:nil];
    //设置动画的间隔时间
    [UIView setAnimationDuration:0.20];
    //??使用当前正在运行的状态开始下一段动画
    [UIView setAnimationBeginsFromCurrentState: YES];
    //设置视图移动的位移
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - 100, self.view.frame.size.width, self.view.frame.size.height);
    //设置动画结束
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //设置动画的名字
    [UIView beginAnimations:@"Animation" context:nil];
    //设置动画的间隔时间
    [UIView setAnimationDuration:0.20];
    //??使用当前正在运行的状态开始下一段动画
    [UIView setAnimationBeginsFromCurrentState: YES];
    //设置视图移动的位移
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +100, self.view.frame.size.width, self.view.frame.size.height);
    //设置动画结束
    [UIView commitAnimations];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
@end

