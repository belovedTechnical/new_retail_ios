//
//  SendTicketViewController.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/8.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  OrderConfirmVCDelegate<NSObject>

- (void)setSendHeader:(NSString *)personOrCompany number:(NSString *)number;

@end
@interface SendTicketViewController : UIViewController
// 设置协议
@property (nonatomic, assign) id <OrderConfirmVCDelegate> popDelegate;
@end



