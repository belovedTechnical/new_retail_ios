//
//  ExpressCorpModel.h
//  BelovedHotel
//
//  Created by BDSir on 2018/3/14.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExpressCorpModel : NSObject
@property(nonatomic, strong) NSString *com;
@property(nonatomic, strong) NSString *nu;

+ (instancetype)expressCorpModelWithDict:(NSDictionary *)dict;

@end
