//
//  OnlineModel.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/6.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OnlineModel : NSObject
/** 商品图片 */
@property (nonatomic, copy) NSString *productImg;
/** 店铺名字 */
@property (nonatomic, copy) NSString *storeName;
/** 商品id */
@property (nonatomic, copy) NSString *orderId;
/** 订单状态 */
@property (nonatomic, copy) NSString *orderStatus;
/** 支付状态 */
@property (nonatomic, copy) NSString *payStatus;
@property (nonatomic, copy) NSString *commentStatus;
/** 发货状态 */
@property (nonatomic, copy) NSString *sendStatus;
/** 总数量 */
@property (nonatomic, copy) NSString *orderTotalNum;
/** 总价格 */
@property (nonatomic, copy) NSString *orderTotalPrice;
/** 店铺ID */
@property (nonatomic, copy) NSString *productStoreId;
@property (nonatomic, copy) NSString *featureName;
/** 商家电话 */
@property (nonatomic, copy) NSString *contactNumber;
@property (nonatomic, copy) NSArray *productList;

- (void)setProductList:(NSArray *)productList;

@end
