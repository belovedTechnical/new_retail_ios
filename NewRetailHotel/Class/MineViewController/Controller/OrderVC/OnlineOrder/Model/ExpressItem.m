//
//  ExpressItem.m
//  BelovedHotel
//
//  Created by BDSir on 2017/7/21.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "ExpressItem.h"

@implementation ExpressItem

+ (instancetype)expressIemWithDict:(NSDictionary *)dict
{
    ExpressItem *item = [[self alloc] init];
    [item setValuesForKeysWithDictionary:dict];
    [item setValuesForKeysWithDictionary:item.expressInfo];
    NSMutableArray *tempArray = [NSMutableArray array];
    for (NSDictionary *dateDict in item.data) {
        ExpressItem *dataModel = [[ExpressItem alloc] init];
        [dataModel setValuesForKeysWithDictionary:dateDict];
        [tempArray addObject:dataModel];
    }
    item.data = tempArray;
    return item;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}

@end
