//
//  OnlineProListModel.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/7.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OnlineProListModel : NSObject
/**
 productId:产品ID,productImg:产品图片、productPrice:产品单价、quantity:数量、productName:名称、featureName: 商品颜色等详情
 afterSidStr:商品状态
 */
@property (nonatomic, copy) NSString *featureName;
@property (nonatomic, copy) NSString *productId;
@property (nonatomic, copy) NSString *productImg;
@property (nonatomic, copy) NSString *productPrice;
@property (nonatomic, copy) NSString *quantity;
@property (nonatomic, copy) NSString *productName;
@property (nonatomic, copy) NSString *asoId;
@property (nonatomic, copy) NSString *afterSidStr;
@property (nonatomic, copy) NSString *contactNumber;
@end
