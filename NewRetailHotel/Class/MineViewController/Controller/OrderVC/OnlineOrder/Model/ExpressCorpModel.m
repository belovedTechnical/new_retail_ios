//
//  ExpressCorpModel.m
//  BelovedHotel
//
//  Created by BDSir on 2018/3/14.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "ExpressCorpModel.h"

@implementation ExpressCorpModel

+ (instancetype)expressCorpModelWithDict:(NSDictionary *)dict
{
    ExpressCorpModel *model = [[self alloc] init];
    model.nu  = dict[@"nu"];
    model.com = dict[@"com"];
    return model;
}



@end
