//
//  ExpressItem.h
//  BelovedHotel
//
//  Created by BDSir on 2017/7/21.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExpressItem : NSObject

@property (nonatomic, strong) NSString *orderExId;
@property (nonatomic, strong) NSString *expressNumber;
@property (nonatomic, strong) NSString *expressName;
@property (nonatomic, strong) NSString *expressCode;

@property (nonatomic, strong) NSDictionary *expressInfo;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *ftime;
@property (nonatomic, strong) NSString *context;
//@property(nonatomic, strong) NSString *time;
//@property(nonatomic, strong) NSString *context;

+ (instancetype)expressIemWithDict:(NSDictionary *)dict;

@end
