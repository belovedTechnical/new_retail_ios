//
//  OnlineModel.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/6.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "OnlineModel.h"
#import "OnlineProListModel.h"
@implementation OnlineModel
// 重写setter方法
- (void)setProductList:(NSArray *)productList{
    NSMutableArray *temArr = [NSMutableArray array];
    for (NSDictionary *dic in productList) {
        OnlineProListModel *model = [[OnlineProListModel alloc]init];
        [model setValuesForKeysWithDictionary:dic];
        [temArr addObject:model];
    }
    
   _productList = [NSArray arrayWithArray:temArr];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}

@end
