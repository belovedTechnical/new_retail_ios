//
//  OrderConfirmListModel.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/16.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderConfirmListModel : NSObject
@property(nonatomic,copy)NSString *price;
@property(nonatomic,copy)NSString *productId;
@property(nonatomic,copy)NSString *productImg;
@property(nonatomic,copy)NSString *productName;
@property(nonatomic,assign)NSInteger quantity;

@end
