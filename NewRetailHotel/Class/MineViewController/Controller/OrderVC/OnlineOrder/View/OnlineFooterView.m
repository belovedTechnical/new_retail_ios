//
//  OnlineFooterView.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/7.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "OnlineFooterView.h"

@implementation OnlineFooterView

- (void)awakeFromNib
{
    self.ConfirmReceiptBtn.hidden = YES;
    self.ConfirmReceiptBtn.layer.borderColor = [UIColor colorWithHexString:@"#E22425"].CGColor;
    self.ConfirmReceiptBtn.layer.borderWidth = 0.5;
    self.paymentBtn.layer.borderColor = [UIColor colorWithHexString:@"#E22425"].CGColor;
    self.paymentBtn.layer.borderWidth = 0.5;
    self.cancelBtn.layer.borderColor = [UIColor colorWithHexString:@"#E22425"].CGColor;
    self.cancelBtn.layer.borderWidth = 0.5;
    [super awakeFromNib];

}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"OnlineFooterView" owner:self options:nil];
        self = viewArray[0];
        self.frame = frame;
    }
    return self;
}

// 确认收货按钮点击loadCompleteOrderWith:(NSString *)orderId
- (IBAction)confirmReceiptClick:(id)sender {
    
    if (self.clickBtn) {
        self.clickBtn();
    }

    
}
// 取消订单按钮
- (IBAction)cancelOrderFormClick:(id)sender {
    
    if (self.cancelBtnClick) {
        self.cancelBtnClick();
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"confirmReceiptClick" object:nil];
}
// 支付订单按钮
- (IBAction)payOrderFormClick:(id)sender {
    
    if (self.payBtnClick) {
        self.payBtnClick();
    }
}


@end
