//
//  ExpressGroupNumbersCell.m
//  BelovedHotel
//
//  Created by BDSir on 2018/3/14.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "ExpressGroupNumbersCell.h"
#import "ExpressItem.h"

@interface ExpressGroupNumbersCell()
@property (nonatomic, strong) UILabel *lbName;
@property (nonatomic, strong) UILabel *lbNumeber;

@end
@implementation ExpressGroupNumbersCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:@"groupNumberID"]) {
        [self initUI];
    }
    return self;
}

- (void)setCorpModel:(ExpressItem *)corpModel {
    _corpModel = corpModel;
    self.lbName.text = corpModel.expressName;
    self.lbNumeber.text = corpModel.expressNumber;
}

- (void)initUI
{
    CGFloat space = 20;
    UILabel *lbName = [[UILabel alloc] init];
    lbName.frame = CGRectMake(space, 0, 80, self.frame.size.height);
    lbName.text = _corpModel.expressName;
    [self addSubview:lbName];
    self.lbName = lbName;
    
    CGFloat nameW = self.lbName.frame.size.width;
    CGFloat numeberX = space + nameW + 10;
    UILabel *lbNumeber = [[UILabel alloc] init];
    lbNumeber.frame = CGRectMake(numeberX, 0, 150, self.frame.size.height);
    lbNumeber.text = _corpModel.expressNumber;
    [self addSubview:lbNumeber];
    self.lbNumeber = lbNumeber;
    
    UIButton *btnDetails = [[UIButton alloc] init];
    btnDetails.frame = CGRectMake(self.frame.size.width - 70, 5, 60, self.frame.size.height-15);
    [btnDetails setTitle:@"详情" forState:UIControlStateNormal];
    [btnDetails setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnDetails.titleLabel.font = [UIFont systemFontOfSize:15];
    [btnDetails.layer setBorderWidth:0.5];
    [btnDetails addTarget:self action:@selector(expressDetailsClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnDetails];
    self.btnDetails = btnDetails;
}

- (void)expressDetailsClick:(UIButton *)sender {
    if (self.btnDetailsClick) {
        self.btnDetailsClick();
    }
}

@end
