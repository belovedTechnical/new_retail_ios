//
//  CompanyNameView.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/8.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyNameView : UIView
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UITextField *nameTFiel;


- (instancetype)initWithFrame:(CGRect)frame;
@property (copy,nonatomic)void(^confimBtnCilcked)(NSInteger index);
@end
