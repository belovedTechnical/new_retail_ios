//
//  OnlineDetailTimeCell.h
//  BelovedHotel
//
//  Created by 至爱 on 17/2/21.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnlineDetailTimeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *orderNum; // 订单号
@property (weak, nonatomic) IBOutlet UILabel *payStatus; // 发货状态
@property (weak, nonatomic) IBOutlet UILabel *orderTimeLab; // 下单时间
@property (weak, nonatomic) IBOutlet UILabel *shenYuTimeLab; // 剩余时间
@property (weak, nonatomic) IBOutlet UIView *shengYuView;

@property (strong, nonatomic) NSString *orderLable;



- (void)setDetailTimeWith:(NSDictionary *)dic;
// 地址的cell
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *numLab;

@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UILabel *companyName;

@property (weak, nonatomic) IBOutlet UILabel *kuaiDiNum;
@end
