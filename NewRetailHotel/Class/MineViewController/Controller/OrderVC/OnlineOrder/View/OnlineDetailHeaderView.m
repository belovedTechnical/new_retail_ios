//
//  OnlineDetailHeaderView.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/7.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "OnlineDetailHeaderView.h"

@implementation OnlineDetailHeaderView
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"OnlineDetailHeaderView" owner:self options:nil];
        self = viewArray[0];
        self.frame = frame;
    }
    return self;
}

@end
