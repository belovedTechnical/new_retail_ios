//
//  ListTableViewCell.h
//  CustomHeaderView
//
//  Created by 至爱 on 16/12/5.
//  Copyright © 2016年 张宏. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnlineProListModel.h"

@interface ListTableViewCell : UITableViewCell

- (void)setListCell:(OnlineProListModel *)model;

@end
