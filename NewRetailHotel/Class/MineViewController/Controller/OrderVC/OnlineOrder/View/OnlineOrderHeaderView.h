//
//  OnlineOrderHeaderView.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/28.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnlineOrderHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *storeNameLAb;
@property (weak, nonatomic) IBOutlet UILabel *payStausLab;




- (instancetype)initWithFrame:(CGRect)frame;
@end
