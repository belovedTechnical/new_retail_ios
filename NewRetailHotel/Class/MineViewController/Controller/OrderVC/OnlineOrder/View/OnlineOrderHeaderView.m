//
//  OnlineOrderHeaderView.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/28.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "OnlineOrderHeaderView.h"

@implementation OnlineOrderHeaderView

//重写
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"OnlineOrderHeaderView" owner:self options:nil];
        self = viewArray[0];
        self.frame = frame;
        
    }
    return self;
}
@end
