//
//  OrderConfirmProductView.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/8.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "OrderConfirmProductView.h"

@implementation OrderConfirmProductView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"OrderConfirmProductView" owner:self options:nil];
        self = viewArray[0];
        self.frame = frame;
        
        
    }
    return self;
}

- (IBAction)sendbtn:(UIButton *)sender {
    self.sendBtn(sender.tag);
}

- (IBAction)couponBtnClick:(id)sender {
    if (self.couponBtnBlock) {
        self.couponBtnBlock();
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
}


@end
