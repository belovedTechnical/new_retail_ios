//
//  SendHeaderView.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/8.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "SendHeaderView.h"

@implementation SendHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"SendHeaderView" owner:self options:nil];
        self = viewArray[0];
        self.frame = frame;
        
        self.personBtn.tag = kPerson_tag;
        self.companyBtn.tag = kCompany_tag;
        
        
        
    }
    return self;
}





- (IBAction)clickedBtn:(UIButton *)sender {
    if (sender.tag == kPerson_tag) {
        self.personImg.image = [UIImage imageNamed:@"icon_select_selected"];
         self.companyImg.image = [UIImage imageNamed:@"icon_select"];
    }else if (sender.tag == kCompany_tag){
        self.companyImg.image = [UIImage imageNamed:@"icon_select_selected"];
        self.personImg.image = [UIImage imageNamed:@"icon_select"];
    }
    
    
    
    self.personAndConConfimBtn(sender.tag);
    
}





@end
