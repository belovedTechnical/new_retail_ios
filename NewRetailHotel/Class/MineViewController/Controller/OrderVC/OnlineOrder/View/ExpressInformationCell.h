//
//  ExpressInformationCell.h
//  BelovedHotel
//
//  Created by Sam Feng on 2017/5/10.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpressInformationCell : UITableViewCell

/// 快递名称
@property (weak, nonatomic) IBOutlet UILabel *expressNameLabel;
/// 快递单号
@property (weak, nonatomic) IBOutlet UILabel *expressNumberLabel;
/// 查看物流详情
@property (weak, nonatomic) IBOutlet UIButton *detailsBtn;

- (void)setExpressDetails:(NSDictionary *)dic;

@end
