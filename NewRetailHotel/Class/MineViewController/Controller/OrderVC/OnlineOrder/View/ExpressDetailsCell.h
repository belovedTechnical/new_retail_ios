//
//  ExpressDetailsCell.h
//  BelovedHotel
//
//  Created by BDSir on 2017/7/21.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ExpressItem;
@interface ExpressDetailsCell : UITableViewCell

/** 上半部分连线 */
@property (weak, nonatomic) IBOutlet UILabel *topLineLabel;
/** 下半部分连线 */
@property (weak, nonatomic) IBOutlet UILabel *bottomLineLabel;
/** 中心圆 */
@property (weak, nonatomic) IBOutlet UILabel *amongLabel;
/** 物流信息 */
@property (weak, nonatomic) IBOutlet UILabel *routeLabel;
/** 物流时间 */
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property(nonatomic, strong) ExpressItem *item;

@end
