//
//  ListBuyCell.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/15.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "ListBuyCell.h"
#import "UIImageView+WebCache.h"
@implementation ListBuyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setCellWithModel:(OrderConfirmListModel *)model isVCPrice:(BOOL)isVCPrice{
    
    NSString *price = [NSString stringWithFormat:@"%@",model.price];
    if (isVCPrice == YES) {
        self.zhiaibiLabel.hidden = NO;
        if ([price containsString:@"."]) {
            self.priceLab.text = price;
        }else {
            self.priceLab.text = [NSString stringWithFormat:@"%@.00",model.price];
        }
    }else {
        if ([price containsString:@"."]) {
            self.priceLab.text = [NSString stringWithFormat:@"¥%@", price];
        }else {
            self.priceLab.text = [NSString stringWithFormat:@"¥%@.00",model.price];
        }
    }

    self.nameLab.text = [NSString stringWithFormat:@"%@",model.productName];
    self.numLab.text = [NSString stringWithFormat:@"x %ld",(long)model.quantity];
    [self.img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.productImg]]];
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
