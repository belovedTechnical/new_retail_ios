//
//  CustomCell.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/6.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *coverView;

@end
