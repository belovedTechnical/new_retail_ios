//
//  OnlineOrderFooterView.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/28.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "OnlineOrderFooterView.h"

@implementation OnlineOrderFooterView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"OnlineOrderFooterView" owner:self options:nil];
        self = viewArray[0];
        self.frame = frame;
        self.cancelbtn.tag = kCancleBtn_tag;
        self.payBtn.tag = kPayMoneyBtn_tag;
        
        self.cancelbtn.layer.borderColor = [UIColor colorWithHexString:@"#777777"].CGColor;
        self.cancelbtn.layer.borderWidth = 0.5;
        
        self.payBtn.layer.borderColor = [UIColor colorWithHexString:@"#E22425"].CGColor;
        self.payBtn.layer.borderWidth = 0.5;

        self.deleteBtn.layer.borderColor = [UIColor colorWithHexString:@"#777777"].CGColor;
        self.deleteBtn.layer.borderWidth = 0.5;
        
    }
    return self;
}

// 支付  取消
- (IBAction)cancelAndPayBtn:(UIButton *)sender {
    
    self.cancelBtnAndPayBtn(sender.tag);
    
    
}

// 删除按钮点击
- (IBAction)deleteBtnClick:(id)sender {
    
    self.deleteBtnClick();
}

@end
