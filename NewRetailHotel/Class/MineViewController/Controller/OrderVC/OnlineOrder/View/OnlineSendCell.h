//
//  OnlineSendCell.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/7.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnlineSendCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *sendLab;
@property (weak, nonatomic) IBOutlet UILabel *contenLab;

/// 留言内容备注
@property (weak, nonatomic) IBOutlet UILabel *matterLabel;

@end
