//
//  OnlineFooterView.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/7.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnlineFooterView : UIView

/** 商品总额 */
@property (weak, nonatomic) IBOutlet UILabel *totalLab;
/** 运费 */
@property (weak, nonatomic) IBOutlet UILabel *moneyLab;
/** 积分抵扣 */
@property (weak, nonatomic) IBOutlet UILabel *pointsLab;
/** 余额抵扣 */
@property (weak, nonatomic) IBOutlet UILabel *balanceLab;
/** 优惠券抵扣 */
@property (weak, nonatomic) IBOutlet UILabel *couponDLab;
/** 合计总价 */
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLab;
/** 确认收货 */
@property (weak, nonatomic) IBOutlet UIButton *ConfirmReceiptBtn;
/** 取消按钮 */
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
/** 支付按钮 */
@property (weak, nonatomic) IBOutlet UIButton *paymentBtn;
/** 取消和支付view */
@property (weak, nonatomic) IBOutlet UIView *payAndCancelView;

@property (strong, nonatomic) NSString *orderLable;

@property (copy, nonatomic) void(^clickBtn)(void) ;
@property (copy, nonatomic) void(^payBtnClick)(void);
@property (copy, nonatomic) void(^cancelBtnClick)(void);

- (instancetype)initWithFrame:(CGRect)frame;





@end
