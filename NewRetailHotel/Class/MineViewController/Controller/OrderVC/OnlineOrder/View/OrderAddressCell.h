//
//  OrderAddressCell.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/15.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressListModel.h"

@interface OrderAddressCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UITextField *IDNumberF;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bgImageConstraints;
@property (weak, nonatomic) IBOutlet UIView *addressView;

@property (nonatomic, copy) void(^saveBtnBlock)(BOOL isLegitimacy, UIButton *sendBtn);

- (void)setAddressCellDefaultWith:(AddressListModel *)model;
- (void)setAddressCellWith:(NSDictionary *)dic;

@end
