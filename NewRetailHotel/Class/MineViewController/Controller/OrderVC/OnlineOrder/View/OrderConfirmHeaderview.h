//
//  OrderConfirmHeaderview.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/8.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderConfirmHeaderview : UIView

@property (weak, nonatomic) IBOutlet UIButton *instantBtn;
@property (weak, nonatomic) IBOutlet UIButton *mailBtn;

@end
