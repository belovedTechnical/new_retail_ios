//
//  ListBuyCell.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/15.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderConfirmListModel.h"
@interface ListBuyCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (weak, nonatomic) IBOutlet UILabel *numLab;
@property (weak, nonatomic) IBOutlet UILabel *zhiaibiLabel;

- (void)setCellWithModel:(OrderConfirmListModel *)model isVCPrice:(BOOL)isVCPrice;

@end
