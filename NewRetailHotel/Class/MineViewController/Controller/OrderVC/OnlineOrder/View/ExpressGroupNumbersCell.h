//
//  ExpressGroupNumbersCell.h
//  BelovedHotel
//
//  Created by BDSir on 2018/3/14.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ExpressItem;
@interface ExpressGroupNumbersCell : UITableViewCell
@property (nonatomic, strong) ExpressItem *corpModel;
@property (nonatomic, strong) UIButton *btnDetails;

@property (nonatomic, copy) void(^btnDetailsClick)(void);

@end
