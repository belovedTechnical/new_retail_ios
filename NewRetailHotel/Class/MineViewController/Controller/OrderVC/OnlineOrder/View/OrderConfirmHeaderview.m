//
//  OrderConfirmHeaderview.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/8.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "OrderConfirmHeaderview.h"

@interface OrderConfirmHeaderview()

@property (nonatomic, strong) UIButton *selectBtn;
@end

@implementation OrderConfirmHeaderview

- (void)awakeFromNib {
    [super awakeFromNib];
    self.instantBtn.selected = YES;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"OrderConfirmHeaderview" owner:self options:nil];
        self = viewArray[0];
        self.frame = frame;
    }
    return self;
}
- (IBAction)instantClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected == YES) {
        self.mailBtn.selected = NO;
    }
}

- (IBAction)mailBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected == YES) {
        self.instantBtn.selected = NO;
    }
}

@end
