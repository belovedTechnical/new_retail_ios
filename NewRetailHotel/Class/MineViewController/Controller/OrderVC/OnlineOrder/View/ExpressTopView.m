//
//  ExpressTopView.m
//  BelovedHotel
//
//  Created by BDSir on 2018/3/14.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "ExpressTopView.h"
#import "ExpressGroupNumbersCell.h"

@interface ExpressTopView()<UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;

@end

static NSString *const groupNumberID = @"groupNumberID";
@implementation ExpressTopView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}

- (void)initUI {
    _tableView = [[UITableView alloc] initWithFrame:self.bounds];
    _tableView.dataSource = self;
    _tableView.rowHeight = 35;
    _tableView.scrollEnabled = NO;
//    _tableView.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0);
    _tableView.tableFooterView = [[UIView alloc] init];
    [self addSubview:_tableView];
    [_tableView registerClass:[ExpressGroupNumbersCell class] forCellReuseIdentifier:groupNumberID];
}

#pragma mark -UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ExpressGroupNumbersCell *cell = [tableView dequeueReusableCellWithIdentifier:groupNumberID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.corpModel = _dataArray[indexPath.row];
    if (_dataArray.count == 1) {
        cell.btnDetails.hidden = YES;
    }else {
        cell.btnDetails.hidden = NO;
    }
    cell.btnDetailsClick = ^{
        if (self.corpExpressDetailsClick) {
            self.corpExpressDetailsClick(indexPath.row);
        }
    };
    return cell;
}

- (void)setDataArray:(NSArray *)dataArray {
    _dataArray = dataArray;
    [self.tableView reloadData];
}

@end
