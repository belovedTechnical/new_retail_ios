//
//  ExpressTopView.h
//  BelovedHotel
//
//  Created by BDSir on 2018/3/14.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpressTopView : UIView
@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, copy) void(^corpExpressDetailsClick)(NSInteger age);

@end
