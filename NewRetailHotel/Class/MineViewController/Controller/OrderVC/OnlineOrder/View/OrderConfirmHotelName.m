//
//  OrderConfirmHotelName.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/8.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "OrderConfirmHotelName.h"

@implementation OrderConfirmHotelName

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"OrderConfirmHotelName" owner:self options:nil];
        self = viewArray[0];
        self.frame = frame;
   
    }
    return self;
}

@end
