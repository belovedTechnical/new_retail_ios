//
//  SendHeaderView.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/8.
//  Copyright © 2016年 至爱. All rights reserved.
//
#define kPerson_tag 1010
#define kCompany_tag 1011


#import <UIKit/UIKit.h>

@interface SendHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *personImg;
@property (weak, nonatomic) IBOutlet UIImageView *companyImg;

@property (weak, nonatomic) IBOutlet UIButton *personBtn;
@property (weak, nonatomic) IBOutlet UIButton *companyBtn;



- (instancetype)initWithFrame:(CGRect)frame;

@property (copy,nonatomic)void(^personAndConConfimBtn)(NSInteger index);









@end
