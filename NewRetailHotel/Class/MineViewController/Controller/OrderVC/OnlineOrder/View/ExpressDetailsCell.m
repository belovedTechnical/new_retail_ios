//
//  ExpressDetailsCell.m
//  BelovedHotel
//
//  Created by BDSir on 2017/7/21.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "ExpressDetailsCell.h"
#import "ExpressItem.h"

@interface ExpressDetailsCell()

@end

@implementation ExpressDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.amongLabel.layer.cornerRadius = _amongLabel.bounds.size.width / 2;
    self.amongLabel.layer.masksToBounds = YES;
    
}

- (void)setItem:(ExpressItem *)item
{
    _item = item;
    
    self.routeLabel.text = item.context;
    self.timeLabel.text = item.time;
}


@end
