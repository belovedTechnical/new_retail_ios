//
//  OnlineDetailTimeCell.m
//  BelovedHotel
//
//  Created by 至爱 on 17/2/21.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "OnlineDetailTimeCell.h"
#import "MuneHeaderView.h"

@interface OnlineDetailTimeCell (){
    dispatch_source_t _timer;
    
}

@property(nonatomic,strong)UILabel *dayLabel;
@property(nonatomic,strong)UILabel *hourLabel;
@property(nonatomic,strong)UILabel *minuteLabel;
@property(nonatomic,strong)UILabel *secondLabel;

@property(nonatomic,assign)long timeCha;// 时间差
@property (weak, nonatomic) IBOutlet UILabel *dayLableleft;

@end

@implementation OnlineDetailTimeCell

- (void)awakeFromNib {
    [super awakeFromNib];

}
/**
 currentSystemDate:1493720627409,
 productStoreName:至爱自营,
 orderDeliveryDate:1493685101629,
 orderStatus:OrderApproved,
 errmsg:ok,
 productList:
 [
 产品价格:        {productPrice:98.00,
 订单数量:        quantity:1,
 productId:103250_102838,
 商家名字:        productStoreName:至爱自营,
 商品名字:        productName:至爱 魔镜VR眼镜 HY-6 黑色,
 productStoreId:MALL,
 商品图片:        productImg:http://image.beloved999.com/6EBB925907AD40329812B644BD9EFD56,
 商品参数:        featureName:规格：一副
 ],
 placedDate:1493618584484,
 下单时间:        orderCompleteDate:2017-05-01T06:04:09+0000,
 快递单号:        expressNumber:70051929004245,
 sendStatus:1,
 收货姓名:        attnName:王保栋,
 商品总额:        grandTotal:98.00,
 订单号:        orderNo:P20170501020304451173037,
 收货电话:        telePhone:18695812063,
 commentStatus:0,
 快递名称:        errcode:0,
 快递名称:        expressCompany:百世汇通,
 payStatus:1,
 actualPrice:98,
 备注信息:        remarks:dhhjjkkjjj,
 productStoreType:MALL,
 productStoreId:MALL,
 orderId:106187,
 收货地址:        address:
 }
 
 
 */


- (void)setDetailTimeWith:(NSDictionary *)dic{
    
    self.orderNum.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"orderNo"]];
 
    // 下单时间
    NSString *str1 = [NSString stringWithFormat:@"%@",[dic objectForKey:@"placedDate"]];
    
    // 发货时间
    NSString *str2 = [NSString stringWithFormat:@"%@",[dic objectForKey:@"orderDeliveryDate"]];
    
    NSString *str111 = [str1 substringToIndex:(str1.length - 3)];
    NSTimeInterval time = [str111 doubleValue];
    
    NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    // 显示下单时间
    self.orderTimeLab.text = [dateFormatter stringFromDate: detaildate];
    
    //    状态
    NSString *str =  [NSString stringWithFormat:@"%@",[dic objectForKey:@"orderStatus"]];
    
    // 倒计时
    NSString *currentSystemDate =[NSString stringWithFormat:@"%@", [dic objectForKey:@"currentSystemDate"]];
    long ChaTime = [currentSystemDate longLongValue] - [str1 longLongValue];
    long ChaTime2 = [currentSystemDate longLongValue] - [str2 longLongValue];
    
    /*
        OrderApproved
     
     */
    
    // 发收货状态OrderApproved 待发货  OrderCancelled 已取消 OrderPlaced 待付款
    if ([str isEqualToString:@"OrderPlaced"]) {
        self.payStatus.text = @"待付款";
        
        self.shengYuView.hidden = NO;
        // 30min -
        self.timeCha = 24 * 60 * 60 * 1000 - ChaTime;
    
    
// 待发货  待收货
    }else if([str isEqualToString:@"OrderApproved"]){
//      代发货待收货状态  是从上个界面取值下来的。不是从后台获取的
        self.payStatus.text = self.orderLable;
        
        if([self.payStatus.text  isEqual: @"待发货"]){
        
            
        } else {
            self.shengYuView.hidden = NO;
             self.dayLableleft.text = @"后自动确认收货";
            // 15天
            self.timeCha = 15 *24 *60 *60* 1000 - ChaTime2;

        }
        //完成
    }else if([str isEqualToString:@"OrderCompleted"]){
        self.payStatus.text = @"已完成";
         self.shengYuView.hidden = YES;
//        取消
    }else if([str isEqualToString:@"OrderCancelled"]){
        self.payStatus.text = @"已取消";
         self.shengYuView.hidden = YES;
    }
 
    if (ChaTime !=0) {
         [self shengYuTime:self.timeCha / 1000];
    }

}



- (void)shengYuTime:(long )time{
    
    
    if (_timer==nil) {
        __block long timeout = time; //倒计时时间
        
        if (timeout!=0) {
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
            dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
            dispatch_source_set_event_handler(_timer, ^{
                if(timeout<=0){ //倒计时结束，关闭
                    dispatch_source_cancel(_timer);
                    _timer = nil;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.dayLabel.text = @"";
                        self.hourLabel.text = @"00";
                        self.minuteLabel.text = @"00";
                        self.secondLabel.text = @"00";
                    });
                }else{
                    int days = (int)(timeout/(3600*24));
                    if (days==0) {
                        self.dayLabel.text = @"";
                    }
                    int hours = (int)((timeout-days*24*3600)/3600);
                    int minute = (int)(timeout-days*24*3600-hours*3600)/60;
                    int second = (int)timeout-days*24*3600-hours*3600-minute*60;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (days==0) {
                            self.dayLabel.text = @"0天";
                        }else{
                            self.dayLabel.text = [NSString stringWithFormat:@"%d天",days];
                        }
                        if (hours<10) {
                            self.hourLabel.text = [NSString stringWithFormat:@"0%d",hours];
                        }else{
                            self.hourLabel.text = [NSString stringWithFormat:@"%d",hours];
                        }
                        if (minute<10) {
                            self.minuteLabel.text = [NSString stringWithFormat:@"0%d",minute];
                        }else{
                            self.minuteLabel.text = [NSString stringWithFormat:@"%d",minute];
                        }
                        if (second<10) {
                            self.secondLabel.text = [NSString stringWithFormat:@"0%d",second];
                        }else{
                            self.secondLabel.text = [NSString stringWithFormat:@"%d",second];
                        }
                        
                        if (days == 0) {
                            
                            self.shenYuTimeLab.text = [NSString stringWithFormat:@"%d:%d:%d", hours, minute, second];
                            
                        }else
                        {
                            self.shenYuTimeLab.text = [NSString stringWithFormat:@"%d天%d:%d:%d",days, hours, minute, second];
                            
                        }
                        
                        
                    });
                    timeout--;
                }
            });
            dispatch_resume(_timer);
        }
    }
 
}


@end
