//
//  ExpressInformationCell.m
//  BelovedHotel
//
//  Created by Sam Feng on 2017/5/10.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "ExpressInformationCell.h"

@implementation ExpressInformationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setExpressDetails:(NSDictionary *)dic
{
    self.expressNameLabel.text = [NSString stringWithFormat:@"%@", [dic objectForKey:@"expressCompany"]];
    self.expressNumberLabel.text = [NSString stringWithFormat:@"%@", [dic objectForKey:@"expressNumber"]];
}

// 快递详情查看
- (IBAction)expressDetailsBtnClick:(id)sender {
    

}


@end
