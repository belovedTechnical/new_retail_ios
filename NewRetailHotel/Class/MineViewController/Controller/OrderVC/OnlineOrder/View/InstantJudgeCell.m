//
//  InstantJudgeCell.m
//  BelovedHotel
//
//  Created by BDSir on 2018/1/11.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "InstantJudgeCell.h"
#import "BDMobileNumberTools.h"

@interface InstantJudgeCell()<UITextFieldDelegate>

@end

@implementation InstantJudgeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    CGFloat titleH = 50;
    [self setupTitleLabel:@"品鉴人" Y:0];
    [self setupTitleLabel:@"手机号" Y:titleH];
    [self setupTitleLabel:@"地址" Y:titleH*2];
    [self setupTitleLabel:@"房间号" Y:titleH*3];
    
    UITextField *personDudgeTF = [self setupTextField:0];
    personDudgeTF.placeholder = @"请输入品鉴人";
    personDudgeTF.delegate = self;
    self.personDudgeTF = personDudgeTF;
    UITextField *phoneNuberTF = [self setupTextField:titleH];
    [phoneNuberTF addTarget:self action:@selector(phoneTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    phoneNuberTF.delegate = self;
    phoneNuberTF.placeholder = @"请输入手机号";
    self.phoneNuberTF = phoneNuberTF;
    UITextField *addressTF = [self setupTextField:titleH*2];
    addressTF.delegate = self;
    addressTF.placeholder = @"深圳至爱空间1号店";
    self.addressTF = addressTF;
    UITextField *roomNumberTF = [self setupTextField:titleH*3];
    roomNumberTF.placeholder = @"请填写房间号、详细地址";
    roomNumberTF.delegate = self;
    self.roomNumberTF = roomNumberTF;
    
    [self setupLineViewY:titleH];
    [self setupLineViewY:titleH*2];
    [self setupLineViewY:titleH*3];
}

- (void)setupTitleLabel:(NSString *)title Y:(CGFloat)y
{
    UILabel *lb = [[UILabel alloc] init];
    lb.frame = CGRectMake(0, y, 80, 50);
    lb.textColor = [UIColor colorWithHexString:@"#333333"];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.font = [UIFont systemFontOfSize:15];
    lb.text = title;
    [self addSubview:lb];
}

- (UITextField *)setupTextField:(CGFloat)y
{
    UITextField *tf = [[UITextField alloc] init];
    tf.frame = CGRectMake(80, y, kScreen_Width-80, 50);
    tf.textColor = [UIColor colorWithHexString:@"#333333"];
    tf.font = [UIFont systemFontOfSize:14];
    [self addSubview:tf];
    return tf;
}

- (void)setupLineViewY:(CGFloat)y
{
    UIView *line = [[UIView alloc] init];
    line.frame = CGRectMake(15, y, kScreen_Width- 15, 0.5);
    line.backgroundColor = [UIColor colorWithHexString:@"#e1e1e1"];
    [self addSubview:line];
}

- (void)phoneTextFieldDidChange:(UITextField *)textField
{
    [BDMobileNumberTools textFieldDidChange:textField selfField:self.phoneNuberTF];
}


#pragma mark -UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // 判断键盘是否遮挡
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self hideKeyboard];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // 收键盘
    [self hideKeyboard];
    return YES;
}


// 收键盘
- (void)hideKeyboard{
    [self endEditing:YES];
}

@end
