//
//  OrderConfirmProductView.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/8.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RightBaseButton.h"

@interface OrderConfirmProductView : UIView

@property (weak, nonatomic) IBOutlet UILabel *isSendLab;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLab;
@property (weak, nonatomic) IBOutlet UILabel *yunFeiLab;
@property (weak, nonatomic) IBOutlet RightBaseButton *couponBtn;

- (instancetype)initWithFrame:(CGRect)frame;

@property (copy, nonatomic) void(^sendBtn)(NSInteger index);
@property (copy, nonatomic) void(^couponBtnBlock)(void);


@end
