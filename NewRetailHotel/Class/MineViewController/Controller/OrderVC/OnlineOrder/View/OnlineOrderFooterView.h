//
//  OnlineOrderFooterView.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/28.
//  Copyright © 2016年 至爱. All rights reserved.
//
#define kCancleBtn_tag 456
#define kPayMoneyBtn_tag 457

#import <UIKit/UIKit.h>

@interface OnlineOrderFooterView : UIView
@property (weak, nonatomic) IBOutlet UILabel *numLab;// 数量
@property (weak, nonatomic) IBOutlet UILabel *priceLab;// 价格
@property (weak, nonatomic) IBOutlet UILabel *priceLastLab;// 小数点2位
@property (weak, nonatomic) IBOutlet UIButton *cancelbtn;   // 取消
@property (weak, nonatomic) IBOutlet UIButton *payBtn;      // 支付、评价、确认收货
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;   // 删除按钮

@property(nonatomic,copy)void(^cancelBtnAndPayBtn)(NSInteger index);
@property(nonatomic,copy) void(^deleteBtnClick)(void);

- (instancetype)initWithFrame:(CGRect)frame;
@end
