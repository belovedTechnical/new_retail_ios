//
//  MuneHeaderView.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/6.
//  Copyright © 2016年 至爱. All rights reserved.
//
#define kAllBtn_Tag 0
#define kUnPayBtn_Tag 1 // 待付款按钮
#define kUnReceiveBtn_Tag 2 // 待收货
#define kFinishBtn_tag 3 // 已完成
#define kCancleBtn_Tag 4 // 已取消



#import <UIKit/UIKit.h>

@interface MuneHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIButton *allBtn;
@property (weak, nonatomic) IBOutlet UIButton *unPayBtn;// 待付款按钮
@property (weak, nonatomic) IBOutlet UIButton *unRecevGoodsBtn;// 待发货
@property (weak, nonatomic) IBOutlet UIButton *finishBtn;// 待收货
@property (weak, nonatomic) IBOutlet UIButton *cancleBtn;// 已完成


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftConstraint;

@property (weak, nonatomic) IBOutlet UIView *lineView;





@property (copy,nonatomic)void(^buttonClick)(NSUInteger index);

- (instancetype)initWithFrame:(CGRect)frame;
@end
