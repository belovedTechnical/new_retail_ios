//
//  InstantJudgeCell.h
//  BelovedHotel
//
//  Created by BDSir on 2018/1/11.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InstantJudgeCell : UITableViewCell

@property (nonatomic, strong) UITextField *personDudgeTF;
@property (nonatomic, strong) UITextField *phoneNuberTF;
@property (nonatomic, strong) UITextField *addressTF;
@property (nonatomic, strong) UITextField *roomNumberTF;

@end
