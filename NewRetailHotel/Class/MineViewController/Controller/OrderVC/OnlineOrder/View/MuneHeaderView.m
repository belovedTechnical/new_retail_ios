//
//  MuneHeaderView.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/6.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "MuneHeaderView.h"

@interface MuneHeaderView ()
@end
@implementation MuneHeaderView
//重写
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"MuneHeaderView" owner:self options:nil];
        self = viewArray[0];
        self.frame = frame;
        
        self.allBtn.tag = kAllBtn_Tag;
        self.unPayBtn.tag = kUnPayBtn_Tag;
        self.unRecevGoodsBtn.tag = kUnReceiveBtn_Tag;
        self.finishBtn.tag = kFinishBtn_tag;
        self.cancleBtn.tag = kCancleBtn_Tag;
    }
    return self;
}
- (IBAction)clickedbtn:(UIButton *)sender {
    // 全部
    if (sender.tag == 0) {
        
        // 默认线的初始位置
        self.leftConstraint.constant = 0;
        // 移动下方的线
        [UIView animateWithDuration:0.15 animations:^{
            self.lineView.frame = CGRectMake(0,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height);
        }];
        
        
        [self.allBtn setTitleColor:[UIColor colorWithHexString:@"#E22425"] forState:(UIControlStateNormal)];
        
        [self.unPayBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.unRecevGoodsBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.finishBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.cancleBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        
    }else if (sender.tag == 1){// 待付款
        
        self.leftConstraint.constant = self.unPayBtn.frame.size.width;
        // 移动线的位置
        [UIView animateWithDuration:0.15 animations:^{
            
            self.lineView.frame = CGRectMake(self.unPayBtn.frame.origin.x,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height);
            
        }];
        [self.allBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.unPayBtn setTitleColor:[UIColor colorWithHexString:@"#E22425"] forState:(UIControlStateNormal)];
        [self.unRecevGoodsBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.finishBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
         [self.cancleBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
    }else if (sender.tag == 2 ){//  待收货
        self.leftConstraint.constant = self.unRecevGoodsBtn.frame.size.width * 2;
        // 移动线的位置
        [UIView animateWithDuration:0.15 animations:^{
            self.lineView.frame = CGRectMake(self.unRecevGoodsBtn.frame.origin.x,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height);
            
        }];
        
        [self.allBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.unPayBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.unRecevGoodsBtn setTitleColor:[UIColor colorWithHexString:@"#E22425"] forState:(UIControlStateNormal)];
        [self.finishBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
         [self.cancleBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
    }else if (sender.tag == 3 ){// 已完成
        
        self.leftConstraint.constant = self.finishBtn.frame.size.width * 3;
        // 移动线的位置
        [UIView animateWithDuration:0.15 animations:^{
            self.lineView.frame = CGRectMake(self.finishBtn.frame.origin.x,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height);
            
        }];
        
        [self.allBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.unPayBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.unRecevGoodsBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.finishBtn setTitleColor:[UIColor colorWithHexString:@"#E22425"] forState:(UIControlStateNormal)];
         [self.cancleBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        
    }else{
        self.leftConstraint.constant = self.finishBtn.frame.size.width *4;
        // 移动线的位置
        [UIView animateWithDuration:0.15 animations:^{
            self.lineView.frame = CGRectMake(self.finishBtn.frame.origin.x,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height);
            
        }];
        
        [self.allBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.unPayBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.unRecevGoodsBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.finishBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.cancleBtn setTitleColor:[UIColor colorWithHexString:@"#E22425"] forState:(UIControlStateNormal)];
  
    }
    
    // 传值
    self.buttonClick(sender.tag);
}

@end
