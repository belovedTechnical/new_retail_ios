//
//  ListTableViewCell.m
//  CustomHeaderView
//
//  Created by 至爱 on 16/12/5.
//  Copyright © 2016年 张宏. All rights reserved.
//

#import "ListTableViewCell.h"
#import "UIImageView+WebCache.h"

@interface ListTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (weak, nonatomic) IBOutlet UILabel *numLab;
@property (weak, nonatomic) IBOutlet UILabel *tagLab;

@end
@implementation ListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setListCell:(OnlineProListModel *)model{
    // 展示图片
    NSString *imgURLStr = [NSString stringWithFormat:@"%@", model.productImg];
    [self.img sd_setImageWithURL:[NSURL URLWithString:imgURLStr]];
    self.nameLab.text = [NSString stringWithFormat:@"%@",model.productName];
    self.priceLab.text = [NSString stringWithFormat:@"¥%@",model.productPrice];
    self.numLab.text = [NSString stringWithFormat:@"x%@",model.quantity];
    self.nameLab.text = [NSString stringWithFormat:@"%@",model.productName];
    //?
    self.tagLab.text  = [NSString stringWithFormat:@"%@",model.featureName];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
