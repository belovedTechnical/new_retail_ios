//
//  HoteOrderListHeaderView.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/2.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kAllBtn_Tag 0 // 全部
#define kUnconfirmBtn_Tag 1 // 未确认按钮
#define kConfirmBtn_Tag 2 // 已确认按钮
#define kFinishBtn_Tag 3 // 已完成按钮
#define kCancledBtn_tag 4 // 已取消

@interface HoteOrderListHeaderView : UIView

@property (weak, nonatomic) IBOutlet UIButton *unconfirmBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIButton *finishBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelbtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftConstraint;
@property (weak, nonatomic) IBOutlet UIButton *allBtn;

@property (weak, nonatomic) IBOutlet UIView *lineView;


@property (copy,nonatomic)void(^buttonClick)(NSUInteger index);

- (instancetype)initWithFrame:(CGRect)frame;







@end
