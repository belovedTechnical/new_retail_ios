//
//  UnfirmedCell.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/10.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "UnfirmedCell.h"


@interface UnfirmedCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (weak, nonatomic) IBOutlet UILabel *dateLab;
@property (weak, nonatomic) IBOutlet UILabel *dayLab;
@property (weak, nonatomic) IBOutlet UILabel *roomNumLab;
@property (weak, nonatomic) IBOutlet UILabel *roomTypeLba;
@property (weak, nonatomic) IBOutlet UIButton *cancelbtn;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;

@end
@implementation UnfirmedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.cancelbtn.tag = kUncofirmCancelBtn_tag;
    self.payBtn.tag = kPayBtn_tag;
    
    self.cancelbtn.layer.borderColor = [UIColor colorWithHexString:@"#777777"].CGColor;
    self.cancelbtn.layer.borderWidth = 0.5;
    
    self.payBtn.layer.borderColor = [UIColor colorWithHexString:@"#E22425"].CGColor;
    self.payBtn.layer.borderWidth = 0.5;
   
}
- (void)setUmfirmCell:(HotelOrderListModel *)model{
    
    
    
    self.nameLab.text = [NSString stringWithFormat:@"%@",model.organizationName];
    
    self.priceLab.text = [NSString stringWithFormat:@"¥%@",model.totalPrices];
    self.dateLab.text = [NSString stringWithFormat:@"%@",model.checkInDate];
    
    if ([[NSString stringWithFormat:@"%@",model.payStatus] isEqualToString:@"1"]) {// 已经支付
        self.cancelbtn.hidden = YES;
        
        self.payBtn.layer.borderColor = [UIColor colorWithHexString:@"#777777"].CGColor;
        self.payBtn.layer.borderWidth = 0.5;
        [self.payBtn setTitle:@"取消" forState:(UIControlStateNormal)];
        
//        [self.payBtn setTintColor:[UIColor colorWithHexString:@"#777777"]];
        
        [self.payBtn setTitleColor:[UIColor colorWithHexString:@"#777777"] forState:(UIControlStateNormal)];
        
        
    }else{
        self.cancelbtn.hidden = NO;
        self.payBtn.hidden = NO;
        
    }
    


}
- (IBAction)cancelAndPayBtn:(UIButton *)sender {
    
    self.cancelAndPayBtn(sender.tag);
    
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
