//
//  ConfirmedCell.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/10.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "ConfirmedCell.h"
@interface ConfirmedCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (weak, nonatomic) IBOutlet UILabel *dateLab;
@property (weak, nonatomic) IBOutlet UILabel *dayLab;
@property (weak, nonatomic) IBOutlet UILabel *roomNumLab;
@property (weak, nonatomic) IBOutlet UILabel *roomTypeLba;
@property (weak, nonatomic) IBOutlet UIButton *cancelbtn;


@end


@implementation ConfirmedCell

- (void)awakeFromNib {
   [super awakeFromNib];
    self.cancelbtn.tag = kConfirmCancelBtn_tag;
 
    
    self.cancelbtn.layer.borderColor = [UIColor colorWithHexString:@"#777777"].CGColor;
    self.cancelbtn.layer.borderWidth = 0.5;
    
}
- (IBAction)cancelBtn:(UIButton *)sender {
    
    self.cancelBtnClicked(sender.tag);
    
    
}
- (void)setUmfirmCell:(HotelOrderListModel *)model{
    self.nameLab.text = [NSString stringWithFormat:@"%@",model.organizationName];
    
    self.priceLab.text = [NSString stringWithFormat:@"¥%@",model.totalPrices];
    self.dateLab.text = [NSString stringWithFormat:@"%@",model.checkInDate];
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
