//
//  CancelCell.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/10.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "CancelCell.h"
@interface CancelCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (weak, nonatomic) IBOutlet UILabel *dateLab;
@property (weak, nonatomic) IBOutlet UILabel *orderStatus;



@end
@implementation CancelCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}
- (void)setUmfirmCell:(HotelOrderListModel *)model{
    self.nameLab.text = [NSString stringWithFormat:@"%@",model.organizationName];
    
    NSString *priceStr = [NSString stringWithFormat:@"¥%@",model.totalPrices];
    NSString *str = @".00";
    if ([priceStr containsString:@"."]) {
        self.priceLab.text = priceStr;
    }else {
        self.priceLab.text = [priceStr stringByAppendingString:str];
    }
//    self.priceLab.text = [NSString stringWithFormat:@"¥%@",model.totalPrices];
    self.dateLab.text = [NSString stringWithFormat:@"%@",model.checkInDate];

    NSString *status = [NSString stringWithFormat:@"%@",model.orderStatus];
    if ([status isEqualToString:@"OrderPlaced"]) {
        self.orderStatus.text = @"待确认";
    }else if ([status isEqualToString:@"OrderCancelled"]){
        self.orderStatus.text = @"已取消";
    }else if ([status isEqualToString:@"OrderApproved"]){
        self.orderStatus.text = @"已确认";
    }else{
        self.orderStatus.text = @"已完成";
    }
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
