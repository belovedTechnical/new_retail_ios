//
//  FinishedCell.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/10.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "FinishedCell.h"
@interface FinishedCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (weak, nonatomic) IBOutlet UILabel *dateLab;
@property (weak, nonatomic) IBOutlet UILabel *dayLab;
@property (weak, nonatomic) IBOutlet UILabel *roomNumLab;
@property (weak, nonatomic) IBOutlet UILabel *roomTypeLba;
@property (weak, nonatomic) IBOutlet UIButton *comBtn;



@end
@implementation FinishedCell

- (void)awakeFromNib {
    self.comBtn.tag = kCommentBtn_tag;
    
    self.comBtn.layer.borderColor = [UIColor colorWithHexString:@"#E22425"].CGColor;
    self.comBtn.layer.borderWidth = 0.5;
 [super awakeFromNib];
    
    
    
    
}
- (void)setUmfirmCell:(HotelOrderListModel *)model{
    self.nameLab.text = [NSString stringWithFormat:@"%@",model.organizationName];
    
    self.priceLab.text = [NSString stringWithFormat:@"¥%@",model.totalPrices];
    self.dateLab.text = [NSString stringWithFormat:@"%@",model.checkInDate];
    
}
- (IBAction)commentbtn:(UIButton *)sender {
    self.commentBtn(sender.tag);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
