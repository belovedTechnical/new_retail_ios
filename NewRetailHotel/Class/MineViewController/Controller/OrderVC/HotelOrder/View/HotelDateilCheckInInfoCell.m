//
//  HotelDateilCheckInInfoCell.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/10.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "HotelDateilCheckInInfoCell.h"

@interface HotelDateilCheckInInfoCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *roomNumLab;// 房间数
@property (weak, nonatomic) IBOutlet UILabel *roomLab;// 房间号

@end

@implementation HotelDateilCheckInInfoCell

- (void)awakeFromNib {
    // Initialization code
     [super awakeFromNib];
}
- (void)setCellWith:(NSDictionary *)dic{
    
    NSString *nameStr = [NSString stringWithFormat:@"%@",[dic objectForKey:@"checkInPerson"]];
    if ([nameStr containsString:@"null"]) {
        self.nameLab.text = @"";
    }else {
        self.nameLab.text = nameStr;
    }
    
    NSArray *arr = [dic objectForKey:@"costDetail"];
    NSDictionary *dict = [arr lastObject];
    
      self.roomNumLab.text = [NSString stringWithFormat:@"%@",[dict  objectForKey:@"quantity"]];
    
    // 房间号
      self.roomLab.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"roomNumber"]];
    
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
