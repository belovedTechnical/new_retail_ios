//
//  CostDetailCell.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/21.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "CostDetailCell.h"

@implementation CostDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setCellWith:(CostListModel *)model{
            self.dateLab.text =  [[NSString stringWithFormat:@"%@",model.LiveDate  ]substringToIndex:10];;
            self.priceLab.text = [NSString stringWithFormat:@"¥%@x%@",model.price,model.quantity];
      
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
