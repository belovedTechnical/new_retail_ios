//
//  UnfirmedCell.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/10.
//  Copyright © 2016年 至爱. All rights reserved.
//
#define kUncofirmCancelBtn_tag 555
#define kPayBtn_tag 556

#import <UIKit/UIKit.h>
#import "HotelOrderListModel.h"
@interface UnfirmedCell : UITableViewCell


@property(nonatomic,copy)void(^cancelAndPayBtn)(NSInteger index);

- (void)setUmfirmCell:(HotelOrderListModel *)model;
@end
