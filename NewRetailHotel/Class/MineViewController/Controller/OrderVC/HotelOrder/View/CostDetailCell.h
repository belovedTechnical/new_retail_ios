//
//  CostDetailCell.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/21.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CostListModel.h"
@interface CostDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;

- (void)setCellWith:(CostListModel *)model;
@end
