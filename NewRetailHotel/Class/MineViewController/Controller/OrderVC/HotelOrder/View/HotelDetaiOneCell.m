//
//  HotelDetaiOneCell.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/10.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "HotelDetaiOneCell.h"

/*
                 {orderStatus:OrderCancelled,
    入住人名字             linkmanName:测试,
                 errmsg:ok,
    酒店名字             hotelName:深圳至爱空间商务酒店（1号店）,
                 payType:PrepaidPayment,
    酒店电话             hotelTelePhone:075529636151,
    入住人电话             linkmanPhone:18695812063,
                 placedDate:2017-04-26T07:08:03+0000,
                 latitude:22.584232,
                 thruDate:2017-04-26T16:00:00+0000,
                 roomNumber:1,
                 roomTypeName:豪华大床房（含单早）,
                 orderTotalPrice:218.00,
                 fromDate:2017-04-25T16:00:00+0000,
                 checkInPerson:null,costDetail:[
                 {
                 price:218.00,
                 quantity:1,
                 LiveDate:2017-04-25T16:00:00+0000}
                 ],
                 orderNo:H20170426030803950588798,
                 longitude:113.89283,
                 errcode:0,
                 payStatus:0,
                 orderPolicy:请于入住中午12:00后办理入住，如提前到店，视酒店空房情况安排。,
                 orderId:105843
                 }
 */

@interface HotelDetaiOneCell ()
@property (weak, nonatomic) IBOutlet UILabel *orderStatusLab;

@property (weak, nonatomic) IBOutlet UILabel *orderTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *orderNumLab;
@property (weak, nonatomic) IBOutlet UILabel *nameLba;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *daysLab;
@property (weak, nonatomic) IBOutlet UILabel *typeLab;

@end
@implementation HotelDetaiOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.daysLab.layer.borderColor = [UIColor colorWithHexString:@"#E22425"].CGColor;
    self.daysLab.layer.borderWidth = 0.5;
    self.daysLab.layer.cornerRadius = 4;
    self.daysLab.layer.masksToBounds = YES;

}
- (void)setCellWith:(NSDictionary *)dic{

    // 订单状态
    NSString *str= [NSString stringWithFormat:@"%@",[dic objectForKey:@"orderStatus"]];
    if ([str isEqualToString:@"OrderPlaced"]) {
        self.orderStatusLab.text = @"未确认";
    }else if ([str isEqualToString:@"OrderApproved"]) {
        self.orderStatusLab.text = @"已确认";
    }else if ([str isEqualToString:@"OrderCompleted"]) {
        self.orderStatusLab.text = @"已完成";
    }else {
        self.orderStatusLab.text = @"已取消";
    }
    
    // 显示下单时间
    NSString *utcDate = [NSString stringWithFormat:@"%@",[dic objectForKey:@"placedDate"]];
    //    self.orderTimeLab.text = [[NSString stringWithFormat:@"%@",[dic objectForKey:@"placedDate"]] substringToIndex:19];
    self.orderTimeLab.text = [self getLocalDateFormateUTCDate:utcDate];
    self.orderNumLab.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"orderNo"]];
    self.nameLba.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"hotelName"]];
    self.startTimeLab.text = [[NSString stringWithFormat:@"%@",[dic objectForKey:@"fromDate"]] substringWithRange:NSMakeRange(5, 5)];
    self.endTimeLab.text = [[NSString stringWithFormat:@"%@",[dic objectForKey:@"thruDate"]] substringWithRange:NSMakeRange(5, 5)];
     NSInteger str11 = [self.startTimeLab.text integerValue]- [ self.endTimeLab.text integerValue];
    self.daysLab.text = [NSString stringWithFormat:@"%ld晚",(long)str11 + 1];
    self.typeLab.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"roomTypeName"]];
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (NSDate *)getNowDateFromatAnDate:(NSDate *)anyDate
{
    //设置源日期时区
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];//或GMT
    //设置转换后的目标日期时区
    NSTimeZone* destinationTimeZone = [NSTimeZone localTimeZone];
    //得到源日期与世界标准时间的偏移量
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:anyDate];
    //目标日期与本地时区的偏移量
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:anyDate];
    //得到时间偏移量的差值
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    //转为现在时间
    NSDate* destinationDateNow = [[NSDate alloc] initWithTimeInterval:interval sinceDate:anyDate];
    return destinationDateNow;
}

//将UTC日期字符串转为本地时间字符串
//输入的UTC日期格式2013-08-03T04:53:51+0000
-(NSString *)getLocalDateFormateUTCDate:(NSString *)utcDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //输入格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSTimeZone *localTimeZone = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:localTimeZone];
    
    NSDate *dateFormatted = [dateFormatter dateFromString:utcDate];
    //输出格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:dateFormatted];
    return dateString;
}

@end
