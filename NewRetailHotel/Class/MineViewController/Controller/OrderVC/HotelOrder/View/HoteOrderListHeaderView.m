//
//  HoteOrderListHeaderView.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/2.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "HoteOrderListHeaderView.h"

@implementation HoteOrderListHeaderView
//重写
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"HoteOrderListHeaderView" owner:self options:nil];
        self = viewArray[0];
        self.frame = frame;
        self.allBtn.tag = kAllBtn_Tag;
        self.unconfirmBtn.tag = kUnconfirmBtn_Tag;
        self.confirmBtn.tag = kConfirmBtn_Tag;
        self.finishBtn.tag = kFinishBtn_Tag;
        self.cancelbtn.tag = kCancledBtn_tag;
        
    }
    return self;
}

- (IBAction)clickedbtn:(UIButton *)sender {
    
    // 全部
    if (sender.tag == 0) {
        
        // 默认线的初始位置
        self.leftConstraint.constant = 0;
        // 移动下方的线
        [UIView animateWithDuration:0.15 animations:^{
            self.lineView.frame = CGRectMake(0,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height);
        }];
        
        [self.allBtn setTitleColor:[UIColor colorWithHexString:@"#E22425"] forState:(UIControlStateNormal)];
        
        [self.unconfirmBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.confirmBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.finishBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.cancelbtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
    
    }else if (sender.tag == 1) {
        // 待确认
        // 移动下方的线
        [UIView animateWithDuration:0.15 animations:^{
            self.lineView.frame = CGRectMake(self.unconfirmBtn.frame.origin.x,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height);
        }];
        
        [self.unconfirmBtn setTitleColor:[UIColor colorWithHexString:@"#E22425"] forState:(UIControlStateNormal)];
      
        [self.allBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.confirmBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.finishBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
         [self.cancelbtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        
    }else if (sender.tag == 2){// 已确认
        
//        self.leftConstraint.constant = self.confirmBtn.frame.size.width;
        // 移动线的位置
        [UIView animateWithDuration:0.15 animations:^{
            
            self.lineView.frame = CGRectMake(self.confirmBtn.frame.origin.x,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height);
            
        }];
        
        [self.confirmBtn setTitleColor:[UIColor colorWithHexString:@"#E22425"] forState:(UIControlStateNormal)];
        
        [self.unconfirmBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.unconfirmBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.finishBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.cancelbtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
    }else if (sender.tag == 3 ){// 已完成
//        self.leftConstraint.constant = self.confirmBtn.frame.size.width * 2;
        // 移动线的位置
        [UIView animateWithDuration:0.15 animations:^{
            self.lineView.frame = CGRectMake(self.finishBtn.frame.origin.x,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height);
            
        }];
        
        [self.finishBtn setTitleColor:[UIColor colorWithHexString:@"#E22425"] forState:(UIControlStateNormal)];
        
        [self.unconfirmBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.unconfirmBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.confirmBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.cancelbtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
    }else{
        
//        self.leftConstraint.constant = self.confirmBtn.frame.size.width * 3;
        // 移动线的位置
        [UIView animateWithDuration:0.15 animations:^{
            self.lineView.frame = CGRectMake(self.cancelbtn.frame.origin.x,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height);
            
        }];

        [self.cancelbtn setTitleColor:[UIColor colorWithHexString:@"#E22425"] forState:(UIControlStateNormal)];
        
        [self.unconfirmBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.unconfirmBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.confirmBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.finishBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
    }
    
    // 传值
    self.buttonClick(sender.tag);
}

@end
