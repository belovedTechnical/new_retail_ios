//
//  FinishedCell.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/10.
//  Copyright © 2016年 至爱. All rights reserved.
//
#define kCommentBtn_tag 558
#import <UIKit/UIKit.h>
#import "HotelOrderListModel.h"
@interface FinishedCell : UITableViewCell
@property(nonatomic,copy)void(^commentBtn)(NSInteger index);
- (void)setUmfirmCell:(HotelOrderListModel *)model;
@end
