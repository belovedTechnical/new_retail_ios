//
//  HotelDetaiOneCell.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/10.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelDetaiOneCell : UITableViewCell

- (void)setCellWith:(NSDictionary *)dic;
@end
