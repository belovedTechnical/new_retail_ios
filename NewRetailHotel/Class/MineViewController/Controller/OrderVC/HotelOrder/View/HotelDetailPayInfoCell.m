//
//  HotelDetailPayInfoCell.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/10.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "HotelDetailPayInfoCell.h"

@interface HotelDetailPayInfoCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *phoneLba;

@property (weak, nonatomic) IBOutlet UILabel *receiptNameLab;
@property (weak, nonatomic) IBOutlet UILabel *policyLab;

@property (weak, nonatomic) IBOutlet UILabel *totalLab;
@property (weak, nonatomic) IBOutlet UILabel *zhiaiBiLab;

@property (weak, nonatomic) IBOutlet UILabel *balanceLab;

@property (weak, nonatomic) IBOutlet UILabel *payLab;

@property (weak, nonatomic) IBOutlet UIView *zhiaibiView;
@property (weak, nonatomic) IBOutlet UIView *balanceView;

@property (weak, nonatomic) IBOutlet UIView *payView;

@end
@implementation HotelDetailPayInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setCellWith:(NSDictionary *)dic{
    
    NSString *nameStr = [NSString stringWithFormat:@"%@",[dic objectForKey:@"linkmanName"]];
    if ([nameStr containsString:@"null"]) {
        self.nameLab.text = @"";
    }else {
        self.nameLab.text = nameStr;
    }

    NSString *phoneStr = [NSString stringWithFormat:@"%@",[dic objectForKey:@"linkmanPhone"]];
    if ([phoneStr containsString:@"null"]) {
        self.phoneLba.text = @"";
    }else {
        self.phoneLba.text = phoneStr;
    }
    
    if(![[dic allKeys] containsObject:@"receipt"]){
        
        self.receiptNameLab.text = @"";
    }else{
        self.receiptNameLab.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"receipt"]];
    }
    
    if(![[dic allKeys] containsObject:@"orderPolicy"]){
        
          self.policyLab.text = @"";
    }else{
          self.policyLab.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"orderPolicy"]];
    }

    // 订单总价
     self.totalLab.text = [NSString stringWithFormat:@"¥%@",[dic objectForKey:@"orderTotalPrice"]];
    // 至爱币
    if(![[dic allKeys] containsObject:@"pointsPirce"]){
//        self.zhiaibiView.hidden = YES;
         self.zhiaiBiLab.text = @"¥0.00";
    }else{
       self.zhiaiBiLab.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"pointsPirce"]];
    }
    // 我的余额
    if(![[dic allKeys] containsObject:@"accountPrice"]){
//        self.balanceView.hidden = YES;
        
        self.balanceLab.text  = @"¥0.00";
    }else{
        self.balanceLab.text = [NSString stringWithFormat:@"%@个",[dic objectForKey:@"accountPrice"]];
    }
    // 实际支付
    if(![[dic allKeys] containsObject:@"orderTotalPrice"]){

//        self.payView.hidden = YES;
        
        self.payLab.text = @"¥0.00";
    }else{
        self.payLab.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"orderTotalPrice"]];
    }
  
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
