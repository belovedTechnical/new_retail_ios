//
//  HotelDetailPayWaysCell.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/18.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelDetailPayWaysCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;

@end
