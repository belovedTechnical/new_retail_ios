//
//  HotelOrderListModel.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/18.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelOrderListModel : NSObject

@property (nonatomic,copy)NSString *checkInDate;
@property (nonatomic,copy)NSString *orderId;
@property (nonatomic,copy)NSString *orderStatus;
@property (nonatomic,copy)NSString *organizationName;
@property (nonatomic,copy)NSString *payStatus;
@property (nonatomic,copy)NSString *payType;
@property (nonatomic,copy)NSString *totalPrices;


@end
