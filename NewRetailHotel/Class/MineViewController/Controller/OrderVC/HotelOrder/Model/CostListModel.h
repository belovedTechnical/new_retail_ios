//
//  CostListModel.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/21.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CostListModel : NSObject
@property (nonatomic,copy)NSString *LiveDate;
@property (nonatomic,copy)NSString *price;
@property (nonatomic,copy)NSString *quantity;
@end
