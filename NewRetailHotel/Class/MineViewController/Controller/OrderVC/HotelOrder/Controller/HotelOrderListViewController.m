//
//  HotelOrderListViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/2.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "HotelOrderListViewController.h"
#import "UnfirmedCell.h"
#import "ConfirmedCell.h"
#import "FinishedCell.h"
#import "CancelCell.h"
#import "BDNetworkTools.h"
#import "PayOderViewController.h"
#import "HoteOrderListHeaderView.h"
#import "MJRefresh.h"
#import "HotelOrderDetailViewController.h"
#import "CommentViewController.h"
#import "OnlineOrderFooterView.h"

@interface HotelOrderListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *coverView;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property(nonatomic,strong) HoteOrderListHeaderView *headerView;
@property(nonatomic,strong) OnlineOrderFooterView *footerView;
@property (weak, nonatomic) IBOutlet UIView *header;

@property (nonatomic,strong) NSMutableArray *dataArr;
@property (strong,nonatomic) NSString *apikey;


@property (nonatomic, strong) NSMutableDictionary *paramDic;
// YES:上拉  NO:下拉
@property (nonatomic,assign) BOOL isUp;
@property (nonatomic,assign) NSInteger tag;
@property (nonatomic,strong) HotelOrderListModel *model;

@property(nonatomic, assign) NSInteger pageSize;
@property(nonatomic, assign) NSInteger pageIndex;
@property(nonatomic, assign) NSUInteger hotelDingDanLeiXing;

@property(nonatomic, assign) NSUInteger pageIndexHotelAll;
@property(nonatomic, assign) NSUInteger pageIndexDaiQueRen;
@property(nonatomic, assign) NSUInteger pageIndexYiQueRen;
@property(nonatomic, assign) NSUInteger pageIndexYiWanCheng;
@property(nonatomic, assign) NSUInteger pageIndexYiQuXiao;
@property(nonatomic, assign) BOOL hotelDingDanLeiXingMiddle;

@end

@implementation HotelOrderListViewController
- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        self.dataArr = [NSMutableArray array];
    }
    return _dataArr;
    
}

// 点错了
- (IBAction)cilckedWrongBtn:(UIButton *)sender {
    
     self.coverView.hidden = YES;
}

// 取消订单
- (IBAction)cancleOrderBtn:(UIButton *)sender {
    self.coverView.hidden = YES;
    
    [self loadCancelOrderWith:self.model.orderId];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"酒店订单";
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    self.apikey = [user objectForKey:@"apikey"];
    
    self.pageSize = 20;
    self.hotelDingDanLeiXing = DingDanLeiXingHotelAll;
    [self hotelResetPageIndex];
    
    self.coverView.hidden = YES;
    
    [self setHeaderViewNum];// 如果不显示多少个订单 只需要不调用这一句就可以了
    self.tableview.tableFooterView = [[UIView alloc]init];
    CGFloat muneY = 0;
    if (Device_iPhoneX) {
        muneY = 24;
    }
    self.headerView = [[HoteOrderListHeaderView alloc]initWithFrame:CGRectMake(0, muneY, self.header.frame.size.width, 38)];
    [self.header addSubview:self.headerView];
    if (@available(iOS 11.0, *)) {
        _tableview.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        _tableview.contentInset = UIEdgeInsetsMake(muneY, 0, 49, 0);//导航栏如果使用系统原生半透明的，top设置为64
        _tableview.scrollIndicatorInsets = _tableview.contentInset;
    }else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    // 注册cell
    [self.tableview registerNib:[UINib nibWithNibName:@"UnfirmedCell" bundle:nil] forCellReuseIdentifier:@"UnfirmedCell"];
    [self.tableview registerNib:[UINib nibWithNibName:@"ConfirmedCell" bundle:nil] forCellReuseIdentifier:@"ConfirmedCell"];
    [self.tableview registerNib:[UINib nibWithNibName:@"FinishedCell" bundle:nil] forCellReuseIdentifier:@"FinishedCell"];
    [self.tableview registerNib:[UINib nibWithNibName:@"CancelCell" bundle:nil] forCellReuseIdentifier:@"CancelCell"];
     _isUp = YES; // 设置默认是向上
 
    // 全部
    [self loadDataWithStatus:@"" pageIndex:self.pageIndexHotelAll];
    [self hotelAddRefresh];
    
    __weak typeof(self) weakSelf = self;
    self.headerView.buttonClick =  ^(NSUInteger index){
        weakSelf.hotelDingDanLeiXingMiddle = YES;
        switch (index) {
            case DingDanLeiXingHotelAll:
                [weakSelf loadDataWithStatus:@"" pageIndex:weakSelf.pageIndexHotelAll];
                weakSelf.hotelDingDanLeiXing = DingDanLeiXingHotelAll;
                break;
            case DingDanLeiXingDaiQueRen: //OrderPlaced
                [weakSelf loadDataWithStatus:@"OrderPlaced" pageIndex:weakSelf.pageIndexDaiQueRen];
                weakSelf.hotelDingDanLeiXing = DingDanLeiXingDaiQueRen;
                break;
            case DingDanLeiXingYiQueRen: //OrderApproved
                [weakSelf loadDataWithStatus:@"OrderApproved" pageIndex:weakSelf.pageIndexYiQueRen];
                weakSelf.hotelDingDanLeiXing = DingDanLeiXingYiQueRen;
                break;
            case DingDanLeiXingYiWanCheng://OrderCompleted
                [weakSelf loadDataWithStatus:@"OrderCompleted" pageIndex:weakSelf.pageIndexYiWanCheng];
                weakSelf.hotelDingDanLeiXing = DingDanLeiXingYiWanCheng;
                break;
            case DingDanLeiXingYiQuXiao: //OrderCancelled
                [weakSelf loadDataWithStatus:@"OrderCancelled" pageIndex:weakSelf.pageIndexYiQuXiao];
                weakSelf.hotelDingDanLeiXing = DingDanLeiXingYiQuXiao;
                break;
            default:
                break;
        }
    };
}

#pragma mark - 小红标
- (void)setHeaderViewNum{
    
    // 全部
    NSDictionary *paramDic =  @{@"orderStatus":@""};// 全部
    [[BDNetworkTools sharedInstance] getHotelListWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        
    }];

    // 待确认
    NSDictionary *paramDic1 =  @{@"orderStatus":@"OrderPlaced",@"pageIndex":@"0",@"pageSize":@"200"};
    [[BDNetworkTools sharedInstance] getHotelListWithParameter:paramDic1 Block:^(NSDictionary *responseObject, NSError *error) {
        NSUInteger num = [[NSString stringWithFormat:@"%@",[responseObject objectForKey:@"pageCount"]]integerValue];
        [self.headerView.unconfirmBtn setTitle:[NSString stringWithFormat:@"待确认 (%lu)",(unsigned long)num] forState:(UIControlStateNormal)];
    }];
    
    // 已确认
    NSDictionary *paramDic2 =  @{@"orderStatus":@"OrderApproved",@"pageIndex":@"0",@"pageSize":@"200"};
    [[BDNetworkTools sharedInstance] getHotelListWithParameter:paramDic2 Block:^(NSDictionary *responseObject, NSError *error) {
        NSUInteger num = [[NSString stringWithFormat:@"%@",[responseObject objectForKey:@"pageCount"]]integerValue];
        [self.headerView.confirmBtn setTitle:[NSString stringWithFormat:@"已确认(%lu)",(unsigned long)num] forState:(UIControlStateNormal)];
    }];
    
    // 已完成
    NSDictionary *paramDic3 =  @{@"orderStatus":@"OrderCompleted",@"pageIndex":@"0",@"pageSize":@"200"};
    [[BDNetworkTools sharedInstance] getHotelListWithParameter:paramDic3 Block:^(NSDictionary *responseObject, NSError *error) {
        NSUInteger num = [[NSString stringWithFormat:@"%@",[responseObject objectForKey:@"pageCount"]]integerValue];
        [self.headerView.finishBtn setTitle:[NSString stringWithFormat:@"已完成(%lu)",(unsigned long)num] forState:(UIControlStateNormal)];
    }];
    
    // 已取消
    NSDictionary *paramDic4 =  @{@"orderStatus":@"OrderCancelled",@"pageIndex":@"0",@"pageSize":@"200"};
    [[BDNetworkTools sharedInstance] getHotelListWithParameter:paramDic4 Block:^(NSDictionary *responseObject, NSError *error) {
        NSUInteger num = [[NSString stringWithFormat:@"%@",[responseObject objectForKey:@"pageCount"]]integerValue];
        [self.headerView.cancelbtn setTitle:[NSString stringWithFormat:@"已取消(%lu)",(unsigned long)num] forState:(UIControlStateNormal)];
    }];
}


#pragma mark - 代理协议
- (void)updateData{
    
//    self.tag = kCancledBtn_tag;
    self.hotelDingDanLeiXing = DingDanLeiXingYiQuXiao;
   [self loadDataWithStatus:@"OrderCancelled" pageIndex:self.pageIndexYiQuXiao];
    
}

- (void)loadDataWithStatus:(NSString *)orderStatus pageIndex:(NSUInteger)pageIndex {
    
    NSDictionary *paramDic =  @{@"orderStatus":orderStatus,@"pageIndex":[NSString stringWithFormat:@"%lu", (unsigned long)pageIndex],@"pageSize":[NSString stringWithFormat:@"%lu", (long)self.pageSize]};;
    
    self.paramDic = [NSMutableDictionary dictionaryWithDictionary:paramDic];
    
    [self requestDataList];
}

/**
 订单状态【OrderPlaced下单、OrderApproved确认、OrderCompleted完成、OrderCancelled取消】
 */
#pragma mark -  -------------------  网络请求----------------------------------
#pragma mark -数据加载
- (void)requestDataList{
    [[BDNetworkTools sharedInstance] getHotelListWithParameter:self.paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        if (self.isUp || self.hotelDingDanLeiXingMiddle) {
            // 清空之前的数据
            [self.dataArr removeAllObjects];
        }
        NSArray *arr = [responseObject objectForKey:@"orderList"];
        for (NSDictionary *dic in arr) {
            HotelOrderListModel *model = [[HotelOrderListModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [self.dataArr addObject:model];
            //            if ([model.orderStatus isEqualToString:@"OrderPlaced"]) {// 待确认
            //
            //                [self.Arr1 addObject:model];
            //
            //            }else if([model.orderStatus isEqualToString:@"OrderApproved"]){// 确认
            //                [self.Arr2 addObject:model];
            //            }else if([model.orderStatus isEqualToString:@"OrderCompleted"]){// 完成
            //                [self.Arr3 addObject:model];
            //            }else if([model.orderStatus isEqualToString:@"OrderCancelled"]){
            //
            //                [self.Arr4 addObject:model];
            //            }
            
        }
        [self.tableview reloadData];
        [self.tableview.mj_header endRefreshing];
        if (arr.count < 15) {
            [self.tableview.mj_footer endRefreshingWithNoMoreData];
        }else
        {
            [self.tableview.mj_footer endRefreshing];
        }
    }];
}

#pragma mark - 点击删除
- (void)loadHotelDeleteBtnClickOrderWith:(NSString *)orderId
{
    NSDictionary *parameter = @{@"orderId":orderId};
    [[BDNetworkTools sharedInstance] deleteOrderWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        // 全部
        [self loadDataWithStatus:@"" pageIndex:self.pageIndexHotelAll];
        [self.tableview.mj_header beginRefreshing];
        [self setHeaderViewNum];
    }];
}
// 点击取消
- (void)loadCancelOrderWith:(NSString *)orderId{
    NSDictionary *parameter = @{@"orderId":orderId};
    [[BDNetworkTools sharedInstance] putHotelOrderCancelWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        // 重新请求数据
        [self loadDataWithStatus:@"OrderPlaced" pageIndex:self.pageIndexDaiQueRen];
        [self setHeaderViewNum];
    }];
}

#pragma mark - MJRefresh.h 第三方上拉加载和下拉刷新
/**
 订单状态【OrderPlaced下单、OrderApproved确认、OrderCompleted完成、OrderCancelled取消】
 */
// 添加上拉加载和下拉刷新
- (void)hotelAddRefresh
{
    __weak typeof(self) weakSelf = self;
    // 添加下啦刷新
    self.tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self hotelResetPageIndex];
        self.isUp = YES;
        self.hotelDingDanLeiXingMiddle = YES;
        switch (self.hotelDingDanLeiXing) {
            case DingDanLeiXingHotelAll:    // 全部
                
                [weakSelf loadDataWithStatus:@"" pageIndex:self.pageIndexHotelAll];
                 break;
            case DingDanLeiXingDaiQueRen:// 待确认
                [weakSelf loadDataWithStatus:@"OrderPlaced" pageIndex:self.pageIndexDaiQueRen];
                break;
            case DingDanLeiXingYiQueRen://已确认
                [weakSelf loadDataWithStatus:@"OrderApproved" pageIndex:self.pageIndexYiQueRen];
                break;
            case DingDanLeiXingYiWanCheng://待完成
                [weakSelf loadDataWithStatus:@"OrderCompleted" pageIndex:self.pageIndexYiWanCheng];
                break;
            case DingDanLeiXingYiQuXiao:// 已取消
                [weakSelf loadDataWithStatus:@"OrderCancelled" pageIndex:self.pageIndexYiQuXiao];
                break;
            default:
                break;
        }
        
    }];
    // 添加上拉加载
    self.tableview.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.isUp = NO;
        self.hotelDingDanLeiXingMiddle = NO;
        switch (self.hotelDingDanLeiXing) {
            case DingDanLeiXingHotelAll:
                self.pageIndexHotelAll++;
                [weakSelf loadDataWithStatus:@"" pageIndex:self.pageIndexHotelAll];
                break;
            case DingDanLeiXingDaiQueRen:// 待确认
                self.pageIndexDaiQueRen++;
                [weakSelf loadDataWithStatus:@"OrderPlaced" pageIndex:self.pageIndexDaiQueRen];
                break;
            case DingDanLeiXingYiQueRen://已确认
                self.pageIndexYiQueRen++;
                [weakSelf loadDataWithStatus:@"OrderApproved" pageIndex:self.pageIndexYiQueRen];
                break;
            case DingDanLeiXingYiWanCheng:// 已完成
                self.pageIndexYiWanCheng++;
                [weakSelf loadDataWithStatus:@"OrderApproved" pageIndex:self.pageIndexYiWanCheng];
                break;
            case DingDanLeiXingYiQuXiao:// 已取消
                self.pageIndexYiQuXiao++;
                [weakSelf loadDataWithStatus:@"OrderCompleted" pageIndex:self.pageIndexYiQuXiao];
                break;
            default:
                break;
        }
    }];
}

- (void)hotelResetPageIndex
{
    self.pageIndexHotelAll = 0;
    self.pageIndexDaiQueRen = 0;
    self.pageIndexYiQueRen = 0;
    self.pageIndexYiWanCheng = 0;
    self.pageIndexYiQuXiao = 0;
}

#pragma mark tableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataArr.count;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
     return 132;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 36;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    self.footerView = [[OnlineOrderFooterView alloc]initWithFrame:CGRectMake(0, 0, kScreenW, 36)];
    HotelOrderListModel *model = self.dataArr[section];
    _footerView.numLab.hidden = YES;
    _footerView.priceLab.hidden = YES;
     __weak HotelOrderListViewController *hotelVC = self;
    
    NSString *str =  [NSString stringWithFormat:@"%@",model.orderStatus];
    NSString *payStr =  [NSString stringWithFormat:@"%@",model.payStatus];// 分为待付款 和已付款
//    NSString *sendStr =  [NSString stringWithFormat:@"%@",model.sendStatus];//
    
    [self loadFooterViewWithStr:str withPay:payStr withSend:@"" with:model];
    
    
    __weak typeof(self) weakSelf = self;
    __weak OnlineOrderFooterView *footer = _footerView;
    // 删除按钮点击
    _footerView.deleteBtnClick = ^(){

        __weak  HotelOrderListModel *model = weakSelf.dataArr[section];
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:@"确定要删除吗？" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [weakSelf loadHotelDeleteBtnClickOrderWith:model.orderId];
        }];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertVC addAction:action];
        [alertVC addAction:action1];
        
        [weakSelf presentViewController:alertVC animated:YES completion:nil];
    };

    
    _footerView.cancelBtnAndPayBtn = ^(NSInteger index){
        
        switch (index) {
            case kCancleBtn_tag:
            {
                if([footer.cancelbtn.titleLabel.text isEqualToString:@"取消"]){
                    
                    weakSelf.coverView.hidden = NO;
                    
                    __block  HotelOrderListModel *model = weakSelf.dataArr[section];
                    weakSelf.model = model;
                    
                }
            }
                break;
            case kPayMoneyBtn_tag:
            {
                if([footer.payBtn.titleLabel.text isEqualToString:@"评价"]){
                    {
                        __block  HotelOrderListModel *model = hotelVC.dataArr[section];
                        CommentViewController *commentVC = [[CommentViewController alloc]init];
                        commentVC.orderId = model.orderId;
                        commentVC.isHotelOrder = YES;
                        [hotelVC.navigationController pushViewController:commentVC animated:YES];
                    }
                    
                }else if([footer.payBtn.titleLabel.text isEqualToString:@"支付"]){
                    {
                        __block  HotelOrderListModel *model = weakSelf.dataArr[section];
                        weakSelf.model = model;
                    
                            PayOderViewController *payVC = [[PayOderViewController alloc]init];
                            payVC.orderId = model.orderId;
                            
                            [weakSelf.navigationController pushViewController:payVC animated:YES];
                    }
                    
                }else{// 已支付订单的取消
                    weakSelf.coverView.hidden = NO;
                    
                    __block  HotelOrderListModel *model = weakSelf.dataArr[section];
                    weakSelf.model = model;
                    
                }
                
            }
                break;
                
            default:
                break;
        }
        
    };
    
    return _footerView;
}
- (void)loadFooterViewWithStr:(NSString *)str withPay:(NSString *)payStr withSend:(NSString *)sendStr with:(HotelOrderListModel *)model{
   
    if ([str isEqualToString:@"OrderPlaced"]) {// 待确认
        if ([payStr isEqualToString:@"0"]) {// 待付款
            _footerView.cancelbtn.hidden = NO;
            _footerView.payBtn.hidden = NO;
            
        }else{//已付款
            _footerView.cancelbtn.hidden = YES;
            _footerView.payBtn.hidden = NO;
            [_footerView.payBtn setTitle:@"取消" forState:(UIControlStateNormal)];
            _footerView.payBtn.layer.borderColor = [UIColor colorWithHexString:@"#777777"].CGColor;
            _footerView.payBtn.layer.borderWidth = 0.5;
            [_footerView.payBtn setTitleColor:[UIColor colorWithHexString:@"#777777"] forState:(UIControlStateNormal)];
            
        }
    }else if([str isEqualToString:@"OrderApproved"]){// 确认  sendStatus
//        if ([sendStr isEqualToString:@"0"]) {// 待发货
//            _footerView.cancelbtn.hidden = YES;
//            _footerView.payBtn.hidden = YES;
//            
//            
//            
//        }else{// 待收货
//            _footerView.cancelbtn.hidden = YES;
//            _footerView.payBtn.hidden = NO;
//            
//            
//           
//        }
    }else if([str isEqualToString:@"OrderCompleted"]){//完成
        
        _footerView.cancelbtn.hidden = YES;
        _footerView.payBtn.hidden = NO;
        [_footerView.payBtn setTitle:@"评价" forState:(UIControlStateNormal)];
//        
//        NSString *commetStatus = [NSString stringWithFormat:@"%@",model.commentStatus];
//        if ([commetStatus isEqualToString:@"1"]) {
//            _footerView.payBtn.hidden = YES;
//        }else{
//            _footerView.payBtn.hidden = NO;
//        }
        
        
    }else if([str isEqualToString:@"OrderCancelled"]){//取消
        
        _footerView.cancelbtn.hidden = YES;
        _footerView.payBtn.hidden = YES;
        _footerView.deleteBtn.hidden = NO;
        // 实在不行 取消   加载取消的cell
        
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
     __block  HotelOrderListModel *model = self.dataArr[indexPath.section];
    
        CancelCell *cell4 = [self.tableview dequeueReusableCellWithIdentifier:@"CancelCell" forIndexPath:indexPath];
        cell4.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell4 setUmfirmCell:model];
        return cell4;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    // 跳转到cell 详情界面
    HotelOrderDetailViewController *hotelDetailVC = [[HotelOrderDetailViewController alloc]init];
    HotelOrderListModel *model = self.dataArr[indexPath.section];
    hotelDetailVC.orderId = model.orderId;
    hotelDetailVC.payStatus = model.payStatus;
    hotelDetailVC.orderStatus = model.orderStatus;
    [self.navigationController pushViewController:hotelDetailVC animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}




@end
