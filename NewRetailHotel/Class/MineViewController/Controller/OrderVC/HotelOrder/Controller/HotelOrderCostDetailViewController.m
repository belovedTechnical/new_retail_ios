//
//  HotelOrderCostDetailViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/21.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "HotelOrderCostDetailViewController.h"
#import "CostDetailCell.h"
#import "CostListModel.h"

@interface HotelOrderCostDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic,strong)NSMutableArray *dataArr;

@end

@implementation HotelOrderCostDetailViewController
- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        self.dataArr = [NSMutableArray array];
    }
    return _dataArr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"费用明细";
    _tableview.tableFooterView = [[UIView alloc]init];
    
    for (NSDictionary *dic in self.costArr) {
        CostListModel *model = [[CostListModel alloc]init];
        [model setValuesForKeysWithDictionary:dic];
        [self.dataArr addObject:model];
    }

    

    [self.tableview registerNib:[UINib nibWithNibName:@"CostDetailCell" bundle:nil] forCellReuseIdentifier:@"CostDetailCell"];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.costArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 49;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CostDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CostDetailCell"];
 
    CostListModel *model = self.dataArr[indexPath.row];
    
    [cell setCellWith:model];

    
    return cell;
}
//  解决cell分割线左边短20px的问题
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
