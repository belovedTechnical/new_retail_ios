//
//  HotelOrderDetailViewController.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/10.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelOrderDetailViewController : UIViewController
@property (nonatomic,copy)NSString *orderId;
@property (nonatomic,copy)NSString *orderStatus;

@property (nonatomic,copy)NSString *payStatus;
@end
