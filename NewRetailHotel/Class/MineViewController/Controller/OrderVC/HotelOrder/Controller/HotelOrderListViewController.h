//
//  HotelOrderListViewController.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/2.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, HotelDingDanLeiXing)
{
    DingDanLeiXingHotelAll,
    DingDanLeiXingDaiQueRen,
    DingDanLeiXingYiQueRen,
    DingDanLeiXingYiWanCheng,
    DingDanLeiXingYiQuXiao,
};

@interface HotelOrderListViewController : UIViewController

@end
