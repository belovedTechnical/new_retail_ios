//
//  HotelOrderDetailViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/10.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "HotelOrderDetailViewController.h"
#import "HotelDetaiOneCell.h"
#import "SetCell.h"
#import "HotelDateilCheckInInfoCell.h"
#import "HotelDetailRemarksCell.h"
#import "HotelDetailPayInfoCell.h"
#import "HotelDetailPayWaysCell.h"
#import "HotelOrderCostDetailViewController.h"
#import "PayOderViewController.h"
#import "HotelOrderListViewController.h"

#import "BDNetworkTools.h"

@interface HotelOrderDetailViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
//@property (strong, nonatomic)UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *totalLab;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLba;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;
@property (weak, nonatomic) IBOutlet UIButton *detailBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewConstraints;
@property (nonatomic,strong)NSDictionary *dic;
// 明细
@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UILabel *dateLab;
@property (weak, nonatomic) IBOutlet UILabel *priceAndNumLAb;

@property (weak, nonatomic) IBOutlet UILabel *detailprice;

@property (weak, nonatomic) IBOutlet UIView *bottomView;

@end

@implementation HotelOrderDetailViewController
- (NSDictionary *)dic{
    if (!_dic) {
        self.dic = [NSDictionary dictionary];
    }
    return _dic;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.bottomViewConstraints.constant = SafeAreaBottomHeight;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"订单详情";
    
    self.detailView.hidden = YES;

    
    NSString *orderS = [NSString stringWithFormat:@"%@",self.orderStatus];
    NSString *payStr = [NSString stringWithFormat:@"%@",self.payStatus];
    if ([orderS isEqualToString:@"OrderPlaced"]) {// 下单
        
 #pragma mark - 列表界面悬浮 区尾
        // 待付款 和 已付款
        if ([payStr isEqualToString:@"0"]) {// 待付款
            _payBtn.hidden = NO;
            _cancelBtn.hidden = NO;
 
            
        }else{//已付款
            _cancelBtn.hidden = YES;
            _payBtn.hidden = NO;
            
            [self.payBtn setTitle:@"取消" forState:(UIControlStateNormal)];
            [self.payBtn setTitleColor:[UIColor colorWithHexString:@"#E22425"] forState:(UIControlStateNormal)];
            self.payBtn.backgroundColor = [UIColor colorWithHexString:@"#E22425" alpha:0.3];
            
        }
    }else if([orderS isEqualToString:@"OrderApproved"]){// 确认  sendStatus
       
    }else if([orderS isEqualToString:@"OrderCompleted"]){//完成
        
    }else if([orderS isEqualToString:@"OrderCancelled"]){//取消
        self.payBtn.hidden = YES;
        self.cancelBtn.hidden = YES;
        
    }
    [self loadDetail];
    
    self.tableView.tableFooterView = [[UIView alloc]init];
    self.tableView.estimatedRowHeight = 1200;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [self.tableView setScrollEnabled:YES];
    // 注册cell
    [_tableView registerNib:[UINib nibWithNibName:@"HotelDetaiOneCell" bundle:nil] forCellReuseIdentifier:@"HotelDetaiOneCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"SetCell" bundle:nil] forCellReuseIdentifier:@"SetCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"HotelDetailRemarksCell" bundle:nil] forCellReuseIdentifier:@"HotelDetailRemarksCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"HotelDetailPayInfoCell" bundle:nil] forCellReuseIdentifier:@"HotelDetailPayInfoCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"HotelDetailPayWaysCell" bundle:nil] forCellReuseIdentifier:@"HotelDetailPayWaysCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"HotelDateilCheckInInfoCell" bundle:nil] forCellReuseIdentifier:@"HotelDateilCheckInInfoCell"];
}

// 酒店详情数据请求
- (void)loadDetail {
    NSDictionary *paramDic = @{
                               @"orderId":self.orderId
                               };
    [[BDNetworkTools sharedInstance] getHotelOrderDetailsWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        self.dic = [NSDictionary dictionaryWithDictionary:responseObject];
        self.totalPriceLba.text =[NSString stringWithFormat:@"¥%@",[self.dic objectForKey:@"orderTotalPrice"]];
        NSArray *costArr =  [self.dic objectForKey:@"costDetail"];
        NSDictionary *dict = costArr[0];
        self.dateLab.text =  [[NSString stringWithFormat:@"%@",[dict objectForKey:@"LiveDate"] ]substringToIndex:10];
        NSString *price = [NSString stringWithFormat:@"%@",[dict objectForKey:@"price"]];
        NSString *num = [NSString stringWithFormat:@"%@",[dict objectForKey:@"quantity"]];
        self.priceAndNumLAb.text = [NSString stringWithFormat:@"¥%@x%@",price ,num];
        CGFloat total = [price floatValue] * [num integerValue];
        self.detailprice.text = [NSString stringWithFormat:@"¥%.2f",total];
        [self.tableView reloadData];
    }];
}
- (IBAction)detailBtn:(UIButton *)sender {
    //费用明细："LiveDate":日期、"price":价格、"quantity":房间数量
    self.detailView.hidden = NO;
}
- (IBAction)detailViewCancle:(UIButton *)sender {
    self.detailView.hidden = YES;

}
// 取消按钮
- (IBAction)cancelBtn:(UIButton *)sender {
    
     [self loadCancelOrderWith:self.orderId];
    
}
/// 去支付按钮
- (IBAction)payBtn:(UIButton *)sender {
    
    if ([[NSString stringWithFormat:@"%@",self.payStatus]  isEqualToString:@"1"]) {
        
        [self loadCancelOrderWith:self.orderId];

    }else{
        PayOderViewController *PayVC = [[PayOderViewController alloc]init];
        PayVC.orderId =[NSString stringWithFormat:@"%@",[self.dic objectForKey:@"orderId"]] ;
        
        [self.navigationController pushViewController:PayVC animated:YES];
  
    }
    
    
}
- (void)loadCancelOrderWith:(NSString *)orderId{
    NSDictionary *parameter = @{
                                @"orderId":orderId
                                };
    [[BDNetworkTools sharedInstance] putHotelOrderCancelWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        // 跳转都列表界面 刷新数据
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 6;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 233;
    }else if (indexPath.row == 1) {
        return 44;
    }else if (indexPath.row == 2) {
        return 44;
    }else if (indexPath.row == 3) {
        return 183;
    }else if (indexPath.row == 4) {
        return 44;
    }else if (indexPath.row == 5){
        return 460;
    }else{
       
      return 440;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        HotelDetaiOneCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotelDetaiOneCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (self.dic.count>0){
            
          [cell setCellWith:self.dic];
        }
        return cell;
    }else if(indexPath.row == 1 || indexPath.row == 2){
        SetCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SetCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 1) {
            cell.nameLab.text =@"酒店地址";
        }else{
            cell.nameLab.text = @"酒店电话";
        }
        return cell;
    }else if (indexPath.row == 3){
        HotelDateilCheckInInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotelDateilCheckInInfoCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        if (self.dic.count>0){
            [cell setCellWith:self.dic];
        }
        return cell;
    }else if (indexPath.row == 4){
        HotelDetailRemarksCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotelDetailRemarksCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
         if (self.dic.count>0){
             if (![[self.dic allKeys] containsObject:@"remarks"]) {
                 cell.remarksLab.text = @"";
             }else{
             cell.remarksLab.text = [NSString stringWithFormat:@"%@",[self.dic objectForKey:@"remarks"]];
             }
         }

        return cell;
    }else{
    HotelDetailPayInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotelDetailPayInfoCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
        if (self.dic.count>0){
            [cell setCellWith:self.dic];
        }
    return cell;
    }

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 1) {//酒店地址 导航
    
//#pragma mark - 导航
//        MapNavigation *mapNavigation = [[MapNavigation alloc] init];
//        mapNavigation.Latitude = [[self.dic objectForKey:@"latitude"] floatValue];
//        mapNavigation.lon = [[self.dic objectForKey:@"longitude"] floatValue];
//        [self.navigationController pushViewController:mapNavigation animated:YES];
    }else if (indexPath.row == 2){//酒店电话
        
        [self callZhiaiPhone];
    }
}
//  解决cell分割线左边短20px的问题
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (void)callZhiaiPhone{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"" message:@"400-811-6519" preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        // 点击确定，调用系统拨打电话
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://400-811-6519"]];
 
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleDefault) handler:nil];
    
    
    [alertVC addAction:action1];
    
    [alertVC addAction:action2];
    [self presentViewController:alertVC animated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
