//
//  RechargeXib.h
//  BelovedHotel
//
//  Created by BDSir on 2017/6/16.
//  Copyright © 2017年 BeloverdSpace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RechargeXib : UIView

/** 自定义金额 */
@property (weak, nonatomic) IBOutlet UITextField *rechargeText;
/** 余额 */
@property (weak, nonatomic) IBOutlet UILabel *remainingSumLabel;
/** 充值 */
@property (weak, nonatomic) IBOutlet UIButton *rechargeBtn;
@property(nonatomic, strong) UIButton *selectedBtn;

+ (instancetype)loadRechargeXib;

@end
