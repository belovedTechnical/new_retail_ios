//
//  CustomCollectionCell.m
//  BelovedHotel
//
//  Created by 至爱 on 17/3/7.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "CustomCollectionCell.h"

#import "UIImageView+WebCache.h"

@implementation CustomCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setProductCell:(ProductCollectModel *)model{
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.productImg]]];
    self.nameLab.text = [NSString stringWithFormat:@"%@",model.productName];
    
    self.priceLab.text = [NSString stringWithFormat:@"¥%@",model.productPrice];

    
    
}

- (void)setScanCell:(ScanModel *)model{
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.img]]];
    self.nameLab.text = [NSString stringWithFormat:@"%@",model.name];
    
    self.priceLab.text = [NSString stringWithFormat:@"¥%@",model.price];
    
    
    
}
- (IBAction)deleteBtn:(UIButton *)sender {
    self.deletaBtnClick(sender.tag);
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
