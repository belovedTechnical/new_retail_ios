//
//  ShoppingHeaderView.m
//  BelovedBao
//
//  Created by 至爱 on 16/9/9.
//  Copyright © 2016年 zhiai. All rights reserved.
//

#import "ShoppingHeaderView.h"
#import "UIColor+Hex.h"
@implementation ShoppingHeaderView

//重写
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"ShoppingHeaderView" owner:self options:nil];
        self = viewArray[0];
        self.frame = frame;
        self.outboxBtn.tag = kOutboxBtn_tag;
        self.resignedBtn.tag = kResignedBtn_tag;// 已发货
        
        
    }
    return self;
}

// 酒店商品浏览
- (IBAction)shoppingStatusBtn:(UIButton *)sender {
    
    if (sender.tag == kOutboxBtn_tag) {
        
        // 酒店
        [self.outboxBtn setTitleColor:[UIColor colorWithHexString:@"#E22425"] forState:(UIControlStateNormal)];
        [self.resignedBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        
        // 默认线的初始位置
        self.leftConstraint.constant = 0;
        // 移动下方的线
        [UIView animateWithDuration:0.15 animations:^{
            self.lineView.frame = CGRectMake(0,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height);
        }];
    }else if(sender.tag == kResignedBtn_tag){// 商品
        self.leftConstraint.constant = self.lineView.frame.size.width;

        [self.outboxBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.resignedBtn setTitleColor:[UIColor colorWithHexString:@"#E22425"] forState:(UIControlStateNormal)];
        
        // 移动线的位置
        [UIView animateWithDuration:0.15 animations:^{
            self.lineView.frame = CGRectMake(self.resignedBtn.frame.origin.x,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height);
            
        }];
        
    }
   
    // 传值
    self.shoppingClickedBtn(sender.tag);
    
}


@end
