//
//  ShoppingHeaderView.h
//  BelovedBao
//
//  Created by 至爱 on 16/9/9.
//  Copyright © 2016年 zhiai. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kOutboxBtn_tag 501
#define kResignedBtn_tag 502

@interface ShoppingHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIButton *outboxBtn;// 酒店浏览
@property (weak, nonatomic) IBOutlet UIButton *resignedBtn;//商品浏览
@property (weak, nonatomic) IBOutlet UIView *lineView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftConstraint;// 左侧距离

@property (copy,nonatomic)void(^shoppingClickedBtn)(NSInteger index);

- (instancetype)initWithFrame:(CGRect)frame;

@end
