//
//  StoreCollectionCell.m
//  BelovedHotel
//
//  Created by 至爱 on 17/3/7.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "StoreCollectionCell.h"
#import "UIImageView+WebCache.h"
@implementation StoreCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (IBAction)deleteBtttn:(UIButton *)sender {
    self.deleteStoreBtn(sender.tag);
}
- (void)setStoreCell:(StoreCollectModel *)model{
//    
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.storeLogo]]];
    self.nameLab.text = [NSString stringWithFormat:@"%@",model.storeName];
    
    
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
