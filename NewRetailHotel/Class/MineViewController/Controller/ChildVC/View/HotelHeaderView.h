//
//  HotelHeaderView.h
//  BelovedBao
//
//  Created by 至爱 on 16/9/8.
//  Copyright © 2016年 zhiai. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kUnbolted_Tag 201
#define kBolted_Tag 202
#define kLeaved_Tag 203 



@interface HotelHeaderView : UIView

@property (weak, nonatomic) IBOutlet UIButton *unboltedBtn;
@property (weak, nonatomic) IBOutlet UIButton *boltedBtn;

@property (weak, nonatomic) IBOutlet UIButton *leavedBtn;

@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineLeftFrame;// 左侧距离


@property (copy,nonatomic)void(^clickIncomeBtn)(NSInteger index);

- (instancetype)initWithFrame:(CGRect)frame;



@end
