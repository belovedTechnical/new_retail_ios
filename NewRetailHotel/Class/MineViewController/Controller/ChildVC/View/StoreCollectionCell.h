//
//  StoreCollectionCell.h
//  BelovedHotel
//
//  Created by 至爱 on 17/3/7.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoreCollectModel.h"
@interface StoreCollectionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;

@property (copy,nonatomic)void(^deleteStoreBtn)();

- (void)setStoreCell:(StoreCollectModel *)model;
@end
