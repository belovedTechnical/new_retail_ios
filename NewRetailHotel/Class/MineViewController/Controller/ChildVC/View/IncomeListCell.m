//
//  IncomeListCell.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/8.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "IncomeListCell.h"

@interface IncomeListCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLAb;

@property (weak, nonatomic) IBOutlet UILabel *dateLab;

@property (weak, nonatomic) IBOutlet UILabel *numLab;
@end
@implementation IncomeListCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}
- (void)setZhiAiBiListCell:(ZhiAiBiListModel *)model{
    
   
    self.dateLab.text = [NSString stringWithFormat:@"%@",model.entryDate];
    NSString *money = [NSString stringWithFormat:@"%@",model.amount];
    if ([money containsString:@"."]) {
        self.numLab.text = money;
    }else {
        self.numLab.text = [NSString stringWithFormat:@"%@.00", money];
    }
    
    NSString *str = [NSString stringWithFormat:@"%@",model.reason];
    if ([str isEqualToString:@"FatrInvite"]) {
        self.nameLAb.text = @"邀请获赠";
        
    }else if ([str isEqualToString:@"FatrPointsByOrderDeposit"]){
        self.nameLAb.text = @"订单获赠";
    }else if ([str isEqualToString:@"FatrPointsDisbursement"]){
        self.nameLAb.text = @"订单抵扣";
    }else{
        self.nameLAb.text = @"其他";
    }

    
    
    
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
