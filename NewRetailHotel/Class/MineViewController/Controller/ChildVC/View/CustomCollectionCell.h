//
//  CustomCollectionCell.h
//  BelovedHotel
//
//  Created by 至爱 on 17/3/7.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductCollectModel.h"
#import "ScanModel.h"
@interface CustomCollectionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (copy,nonatomic)void(^deletaBtnClick)();
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

- (void)setProductCell:(ProductCollectModel *)model;

- (void)setScanCell:(ScanModel *)model;
@end
