//
//  FeedbackTableViewCell.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/17.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FeebackModel;
@interface FeedbackTableViewCell : UITableViewCell

@property (nonatomic, strong) FeebackModel *feedbackModel;

@end
