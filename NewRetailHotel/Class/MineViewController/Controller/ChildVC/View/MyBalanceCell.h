//
//  MyBalanceCell.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/8.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyBalanceModel.h"
@interface MyBalanceCell : UITableViewCell
- (void)setCellWithData:(MyBalanceModel *)model;

@end
