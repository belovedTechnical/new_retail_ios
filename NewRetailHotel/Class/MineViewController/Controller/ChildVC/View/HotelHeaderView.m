//
//  HotelHeaderView.m
//  BelovedBao
//
//  Created by 至爱 on 16/9/8.
//  Copyright © 2016年 zhiai. All rights reserved.
//

#import "HotelHeaderView.h"
#import "UIColor+Hex.h"

@implementation HotelHeaderView

//重写
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"HotelHeaderView" owner:self options:nil];
        self = viewArray[0];
        self.frame = frame;
        self.unboltedBtn.tag = kUnbolted_Tag;
        self.boltedBtn.tag = kBolted_Tag;
      
 
               
    }
    return self;
}

- (IBAction)hotelBtnHeaderView:(UIButton *)sender {
    
    if (sender.tag == kUnbolted_Tag) {
        
        [self.unboltedBtn setTitleColor:[UIColor colorWithHexString:@"#E22425"] forState:(UIControlStateNormal)];
        [self.boltedBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        
        
        
        // 默认线的初始位置
        self.lineLeftFrame.constant = 0;
        // 移动下方的线
        [UIView animateWithDuration:0.15 animations:^{
            
            self.lineView.frame = CGRectMake(0,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height);
            
        }];
    }else if (sender.tag == kBolted_Tag){
        
        [self.unboltedBtn setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:(UIControlStateNormal)];
        [self.boltedBtn setTitleColor:[UIColor colorWithHexString:@"#E22425"] forState:(UIControlStateNormal)];
       
        self.lineLeftFrame.constant = self.lineView.frame.size.width;
        // 移动线的位置
        [UIView animateWithDuration:0.15 animations:^{
            self.lineView.frame = CGRectMake(self.boltedBtn.frame.origin.x,self.lineView.frame.origin.y, self.lineView.frame.size.width, self.lineView.frame.size.height);
            
        }];
        
    }
    // 传值
    self.clickIncomeBtn(sender.tag);
}

























@end
