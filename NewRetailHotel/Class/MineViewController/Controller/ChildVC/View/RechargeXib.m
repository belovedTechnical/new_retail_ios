//
//  RechargeXib.m
//  BelovedHotel
//
//  Created by BDSir on 2017/6/16.
//  Copyright © 2017年 BeloverdSpace. All rights reserved.
//

#import "RechargeXib.h"
#import "UIColor+Hex.h"
@interface RechargeXib()

/** 100 */
@property (weak, nonatomic) IBOutlet UIButton *oneBtn;
/** 300 */
@property (weak, nonatomic) IBOutlet UIButton *twoBtn;
/** 500 */
@property (weak, nonatomic) IBOutlet UIButton *threeBtn;
/** 1000 */
@property (weak, nonatomic) IBOutlet UIButton *fourBtn;
/** 3000 */
@property (weak, nonatomic) IBOutlet UIButton *fiveBtn;
/** 5000 */
@property (weak, nonatomic) IBOutlet UIButton *sixBtn;

@end

@implementation RechargeXib

+ (instancetype)loadRechargeXib
{
    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] firstObject];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
//    self.oneBtn.selected = YES;
}

// 充值金额选择按钮
- (IBAction)rechargeClickselectedBtn:(UIButton *)sender {
    
    self.selectedBtn.selected = NO;
    sender.selected = YES;
    self.selectedBtn = sender;
    
    [self normalButton];
    if (sender.selected == YES) {
        sender.layer.cornerRadius = 4;
        sender.layer.masksToBounds = YES;
        sender.layer.borderWidth = 0.5;
        sender.layer.borderColor = [UIColor colorWithHexString:@"#a6926b"].CGColor;
        sender.backgroundColor = [UIColor colorWithHexString:@"#a6926b" alpha:0.3];
    }
}

- (void)normalButton
{
    self.oneBtn.backgroundColor = [UIColor whiteColor];
    self.twoBtn.backgroundColor = [UIColor whiteColor];
    self.threeBtn.backgroundColor = [UIColor whiteColor];
    self.fourBtn.backgroundColor = [UIColor whiteColor];
    self.fiveBtn.backgroundColor = [UIColor whiteColor];
    self.sixBtn.backgroundColor = [UIColor whiteColor];
    self.oneBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    self.twoBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    self.threeBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    self.fourBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    self.fiveBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    self.sixBtn.layer.borderColor = [UIColor whiteColor].CGColor;
}

#pragma mark -选中按钮
- (void)selButton:(UIButton *)button
{
    self.selectedBtn.selected = NO;
    button.selected = YES;
    self.selectedBtn = button;
    
    // 修改下划线位置
    [UIView animateWithDuration:0.25 animations:^{
        
        
    }];
}



- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
}

@end
