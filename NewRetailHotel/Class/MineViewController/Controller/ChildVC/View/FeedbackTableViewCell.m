//
//  FeedbackTableViewCell.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/17.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "FeedbackTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "FeebackModel.h"

@interface FeedbackTableViewCell()

@property (nonatomic, strong) UILabel *lbUserID;
@property (nonatomic, strong) UILabel *lbDate;
@property (nonatomic, strong) UILabel *lbContent;
@property (nonatomic, strong) UILabel *lbAnswer;
@property (nonatomic, strong) UIView *answerView;
@property (nonatomic, strong) UIImageView *pictureImageView;

@end

@implementation FeedbackTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:@"FeedbackTableViewCell"]) {
        self.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    UILabel *lbUserID = [[UILabel alloc] init];
    lbUserID.textColor = [UIColor colorWithHexString:@"#333333"];
    lbUserID.font = BDNameFont;
    [self.contentView addSubview:lbUserID];
    self.lbUserID = lbUserID;
    
    UILabel *lbDate = [[UILabel alloc] init];
    lbDate.textColor = [UIColor colorWithHexString:@"#999999"];
    lbDate.font = BDDateFont;
    [self.contentView addSubview:lbDate];
    self.lbDate = lbDate;
    
    UILabel *lbContent = [[UILabel alloc] init];
    lbContent.textColor = [UIColor colorWithHexString:@"#333333"];
    lbContent.numberOfLines = 0;
    lbContent.font = BDContentFont;
    [self.contentView addSubview:lbContent];
    self.lbContent = lbContent;
    
    /** 配图 */
    UIImageView *pictureImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:pictureImageView];
    self.pictureImageView = pictureImageView;
    
    /*
    UIView *answerView = [[UIView alloc] init];
    answerView.backgroundColor =  [UIColor redColor];
    [self.contentView addSubview:answerView];
    self.answerView = answerView;

    UILabel *lbAnswer = [[UILabel alloc] init];
    lbAnswer.font = [UIFont systemFontOfSize:12];
    lbAnswer.numberOfLines = 0;
    lbAnswer.textColor = [UIColor colorWithHexString:@"#333333"];
    lbAnswer.backgroundColor = randomColor;
    [answerView addSubview:lbAnswer];
    self.lbAnswer = lbAnswer;
     */
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.lbUserID.frame = self.feedbackModel.nameFrame;
    self.lbDate.frame = self.feedbackModel.dateFrame;
    self.lbContent.frame = self.feedbackModel.textFrame;
    self.pictureImageView.frame = self.feedbackModel.pictureFrame;

}

- (void)setFeedbackModel:(FeebackModel *)feedbackModel
{
    _feedbackModel = feedbackModel;
    self.lbUserID.text = feedbackModel.backUserName;
    self.lbDate.text = feedbackModel.backTime;
    self.lbContent.text = feedbackModel.content;
    [self.pictureImageView sd_setImageWithURL:[NSURL URLWithString:feedbackModel.content]];
}





@end
