//
//  MyBalanceCell.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/8.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "MyBalanceCell.h"


@interface MyBalanceCell ()
@property (weak, nonatomic) IBOutlet UILabel *dateLab;

@property (weak, nonatomic) IBOutlet UILabel *typeLab;
@property (weak, nonatomic) IBOutlet UILabel *moneyLab;

@end
@implementation MyBalanceCell

- (void)setCellWithData:(MyBalanceModel *)model{
    self.dateLab.text = [NSString stringWithFormat:@"%@",model.transactionDate];
    
    NSString *str = [NSString stringWithFormat:@"%@",model.reason];
    if ([str isEqualToString:@"FatrRedEnvelopeBack"]) {
        self.typeLab.text = @"红包退回";

    }else if ([str isEqualToString:@"FatrRedEnvelopeOut"]){
        self.typeLab.text = @"红包提现";
    }else if ([str isEqualToString:@"FatrRedEnvelopeIn"]){
        self.typeLab.text = @"红包收入";
    }else if ([str isEqualToString:@"FatrRechargeDeposit"]){
        self.typeLab.text = @"充值存入";
    }else if ([str isEqualToString:@"FatrRechargeGet"]){
        self.typeLab.text = @"充值获赠";
    }else if ([str isEqualToString:@"FatrDepositDisbursement"]){
        self.typeLab.text = @"订单抵扣";
    }else if ([str isEqualToString:@"FatrRedEnvelopeDisbursement"]){
        self.typeLab.text = @"红包抵扣";
    }else if ([str isEqualToString:@"FatrRefundBalance"]){
        self.typeLab.text = @"余额退回";
    }
    else{
        self.typeLab.text = @"其他";
    }
    
    NSMutableString *moneyStr = [NSMutableString stringWithFormat:@"%@",model.amount];
    
    if ([moneyStr containsString:@"-"]) {
        [moneyStr insertString:@"¥" atIndex:1];
        self.moneyLab.text = moneyStr;
    }else {
        self.moneyLab.text = [NSString stringWithFormat:@"¥%@",moneyStr];
    }
}

- (NSString *)getDate:(NSString *)date{// date传进的参数时间戳
    double time = [date doubleValue];
    NSDate *tDate = [NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [formatter stringFromDate:tDate];// 返回年月日
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
