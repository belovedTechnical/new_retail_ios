//
//  ProductCollectionViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 17/3/7.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "ProductCollectionViewController.h"
#import "WebRoomDetailsViewController.h"
#import "CustomCollectionCell.h"
#import "ProductCollectModel.h"
#import "MJRefresh.h"

@interface ProductCollectionViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property(nonatomic,strong)NSMutableArray *dataArr;
@property(nonatomic,copy)NSString *apikey;
@property (weak, nonatomic) IBOutlet UIView *coverView;
@property(nonatomic,strong)ProductCollectModel *modelC;
@property(nonatomic,strong)NSMutableDictionary *paramDic;
// YES:上拉  NO:下拉
@property (nonatomic, assign) BOOL isUp;
@end

@implementation ProductCollectionViewController
- (NSMutableDictionary *)paramDic{
    if (!_paramDic) {
        self.paramDic = [NSMutableDictionary dictionary];
    }
    return _paramDic;
}
- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        self.dataArr = [NSMutableArray array];
    }
    return _dataArr;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.coverView.hidden = YES;
    self.title = @"商品收藏";
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    self.apikey = [NSString stringWithFormat:@"%@",[user objectForKey:@"apikey"]];
    self.tableview.tableFooterView = [[UIView alloc]init];
    
    [self loadProductList];
    
    [self.tableview registerNib:[UINib nibWithNibName:@"CustomCollectionCell" bundle:nil] forCellReuseIdentifier:@"CustomCollectionCell"];
    
    [self addRefresh];
    [self.tableview.mj_header beginRefreshing];
    
}


#pragma mark - UITableview

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CustomCollectionCell *cell = [self.tableview dequeueReusableCellWithIdentifier:@"CustomCollectionCell" forIndexPath:indexPath];
    
    ProductCollectModel *model = self.dataArr[indexPath.row];
    
    [cell setProductCell:model];
    
    cell.deletaBtnClick =^(){ 
        
//        BDLog(@"删除");
        self.coverView.hidden = NO;
         __block  ProductCollectModel *model = self.dataArr[indexPath.row];
        self.modelC = model;

        
    };
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ProductCollectModel *model = self.dataArr[indexPath.row];
    WebRoomDetailsViewController *detailsVC = [[WebRoomDetailsViewController alloc] init];
    detailsVC.roomDetailsURL = [NSString stringWithFormat:@"%@shopDetails/%@",HTML_URL_Base, model.productId];
    [self.navigationController pushViewController:detailsVC animated:YES];
}

// 取消
- (IBAction)cancleBtn:(UIButton *)sender {
    
    self.coverView.hidden = YES;
}

// 确定删除
- (IBAction)confirmDeleteBtn:(UIButton *)sender {
    
    self.coverView.hidden = YES;
    [self loadDeleteWith:[NSString stringWithFormat:@"%@",self.modelC.productFavoriteId]];
}

- (void)loadDeleteWith:(NSString *)goodsFavoritesId{
    NSDictionary *dic = @{@"goodsFavoritesId":goodsFavoritesId};
    [[BDNetworkTools sharedInstance] postCollectShopDeleteWithParameter:dic Block:^(NSDictionary *responseObject, NSError *error) {
        //  重新请求数据
        [self loadProductList];
    }];
}

#pragma mark - MJRefresh.h 第三方上拉加载和下拉刷新
// 添加上拉加载和下拉刷新
- (void)addRefresh{
    self.tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self.paramDic setObject:@"0" forKey:@"pageIndex"];
        
        [self requestDataList];
        _isUp = NO;
        
    }];
    self.tableview.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        NSInteger pageIndex = [[self.paramDic objectForKey:@"pageIndex"] integerValue];
        
        pageIndex++;
        
        [self.paramDic setObject:[NSString stringWithFormat:@"%ld", (long)pageIndex] forKey:@"pageIndex"];
        
        [self requestDataList];
        
        // 表示上拉
        _isUp = YES;
    }];
}
- (void)loadProductList{
    
    NSDictionary *paramDic =  @{@"pageIndex":@"0",@"pageSize":@"15"};;
    
    self.paramDic = [NSMutableDictionary dictionaryWithDictionary:paramDic];
    
    [self requestDataList];
    
    
}
- (void)requestDataList {
    [[BDNetworkTools sharedInstance] getCollectShopDataWithParameter:self.paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        NSArray *arr = [responseObject objectForKey:@"favoritesList"];
        if (!_isUp) {
            // 清空之前的数据
            [self.dataArr removeAllObjects];
        }
        for (NSDictionary *dic in arr) {
            ProductCollectModel *model = [[ProductCollectModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [self.dataArr addObject:model];
        }
        [self.tableview reloadData];
        [self.tableview.mj_header endRefreshing];
        if (arr.count < 15) {
            [self.tableview.mj_footer endRefreshingWithNoMoreData];
        }else
        {
            [self.tableview.mj_footer endRefreshing];
        }
    }];
}


@end
