//
//  MyBalanceViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/8.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "MyBalanceViewController.h"
#import "MyBalanceCell.h"
//#import "MyBalanceDetailViewController.h"
#import "BDNetworkTools.h"
#import "MJRefresh.h"
#import "MyBalanceModel.h"


@interface MyBalanceViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *balanceLab;

@property (nonatomic, strong) NSMutableArray *listArr;
@property (assign, nonatomic) NSInteger pageIndex;
@property (assign, nonatomic) NSInteger pageSize;

@property (assign, nonatomic) BOOL isRefresh;
@end

@implementation MyBalanceViewController
- (NSMutableArray *)listArr{
    if (!_listArr) {
        self.listArr = [NSMutableArray array];
    }
    return _listArr;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的余额";
    
    self.pageIndex = 0;
    self.pageSize = 15;
    self.isRefresh = YES;
    // 余额账户信息
    [self addRefresh];
    [self loadDataWithPageIndxe:self.pageIndex pageSize:self.pageSize];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerNib:[UINib nibWithNibName:@"MyBalanceCell" bundle:nil] forCellReuseIdentifier:@"MyBalanceCell"];

}



#pragma mark - Tableview
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.listArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyBalanceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyBalanceCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    MyBalanceModel *model = self.listArr[indexPath.row];
    [cell setCellWithData:model];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    MyBalanceDetailViewController *myVC = [[MyBalanceDetailViewController alloc]init];
//    
//    MyBalanceModel *model = self.listArr[indexPath.row];
//    // 传值
//    myVC.transId = model.finAccountTransId;
//  
//    [self.navigationController pushViewController:myVC animated:YES];
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark -账户余额请求
- (void)loadDataWithPageIndxe:(NSInteger)pageIdxe pageSize:(NSInteger)pageSize{
    NSDictionary *paramDic =  @{@"pageIndex":[NSString stringWithFormat:@"%ld",(long)self.pageIndex],@"pageSize":[NSString stringWithFormat:@"%ld",(long)self.pageSize]};
    [[BDNetworkTools sharedInstance] getAccountRemainingSumWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        if (self.isRefresh) {
            [self.listArr removeAllObjects];
        }
        self.balanceLab.text = [NSString stringWithFormat:@"¥%@",[responseObject objectForKey:@"availableBalance"]];
        NSArray *arr = [responseObject objectForKey:@"transList"];
        for (NSDictionary *dic in arr) {
            MyBalanceModel *model = [[MyBalanceModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            
            [self.listArr addObject:model];
        }
        
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        if (arr.count < 15) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }else
        {
            [self.tableView.mj_footer endRefreshing];
        }
    }];
}


#pragma mark -第三方上拉加载  下拉刷新
- (void)addRefresh
{
    __weak typeof(self) weakSelf = self;
    // 下拉刷新
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.isRefresh = YES;
        [weakSelf loadDataWithPageIndxe:weakSelf.pageIndex pageSize:weakSelf.pageSize];
    }];
    // 上拉加载
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        weakSelf.isRefresh = NO;
        weakSelf.pageIndex++;
        [weakSelf loadDataWithPageIndxe:weakSelf.pageIndex pageSize:weakSelf.pageSize];
    }];
}

@end
