//
//  UpdatePhoneViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/4.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "UpdatePhoneViewController.h"
#import "UpdatePhoneNumViewController.h"
#import "BDMobileNumberTools.h"

@interface UpdatePhoneViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (weak, nonatomic) IBOutlet UITextField *codeFeild;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
@property (weak, nonatomic) IBOutlet UILabel *codeLab;
@property(nonatomic, strong) NSString *useriPhone;
@end

@implementation UpdatePhoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"修改手机号码";
    self.codeLab.layer.borderColor = [UIColor colorWithHexString:@"#E22425"].CGColor;
    self.codeLab.layer.borderWidth = 0.5;
    
    [self getUserRequest];
    // 自定义清楚按钮
    UIButton *button = [self.codeFeild
                        valueForKey:@"_clearButton"];
    
    
    [button setImage:[UIImage imageNamed:@"icon_clear@3x"] forState:UIControlStateNormal];
    self.codeFeild.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    [self.codeFeild setValue:[UIColor colorWithHexString:@"#777777"] forKeyPath:@"_placeholderLabel.textColor"];
    [self.codeFeild setValue:[UIFont systemFontOfSize:13] forKeyPath:@"_placeholderLabel.font"];
    self.codeFeild.borderStyle = UITextBorderStyleNone;
    
}

// 获取验证码
- (IBAction)codeBtn:(UIButton *)sender {
    if ([BDMobileNumberTools isMobileNumber:self.phoneLab.text] == YES) {
        [self timerLifecycle];
        [self loadGetSmsCode];
    }
}

- (void)loadGetSmsCode{
    [[BDNetworkTools sharedInstance] postLoginSendCodeWihtProduct:self.phoneLab.text Block:^(NSDictionary *responseObject, NSError *error) {
        
    }];
}

// 下一步
- (IBAction)nextBtn:(UIButton *)sender {
    NSDictionary *dic = @{@"verifyCode":self.codeFeild.text};
    [[BDNetworkTools sharedInstance] getSmsCheckingWithParameter:dic Block:^(NSDictionary *responseObject, NSError *error) {
        // 验证码传值
        UpdatePhoneNumViewController *upVC = [[UpdatePhoneNumViewController alloc]init];
        upVC.codeStr = self.codeFeild.text;
        [self.navigationController pushViewController:upVC animated:YES];
    }];
  
}
- (void)timerLifecycle{
    __block long timeout=60; //倒计时时间
    __block NSInteger creatTime = [[NSDate new] timeIntervalSince1970];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                self.codeLab.text =@"获取验证码";
                self.codeLab.textColor = [UIColor colorWithHexString:@"#E22425"];
                self.codeLab.layer.borderColor = [UIColor colorWithHexString:@"#E22425"].CGColor;
                
                
                self.codeBtn.userInteractionEnabled = YES;
            });
        }else{
            int seconds;
            if (timeout == 60) {
                seconds =  60;
            }else{
                seconds = timeout % 60;
            }
            
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                self.codeLab.text =[NSString stringWithFormat:@"重新获取(%@)",strTime];
                
                self.codeLab.textColor = [UIColor colorWithHexString:@"#777777"];
                self.codeLab.layer.borderColor = [UIColor colorWithHexString:@"#777777"].CGColor;
                
                self.codeBtn.userInteractionEnabled = NO;
            });
            NSInteger a =[[NSDate new] timeIntervalSince1970];
            timeout = 59-(a-creatTime);
        }
    });
    dispatch_resume(_timer);
}

// 获取用户信息
- (void)getUserRequest
{
    [[BDNetworkTools sharedInstance] getUserAcountWithBlock:^(NSDictionary *responseObject, NSError *error) {
        _useriPhone = [responseObject objectForKey:@"userFullName"];
        self.phoneLab.text = _useriPhone;
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return YES;
}


@end
