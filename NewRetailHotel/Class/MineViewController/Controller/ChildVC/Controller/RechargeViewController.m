//
//  RechargeViewController.m
//  BelovedHotel
//
//  Created by BDSir on 2017/6/13.
//  Copyright © 2017年 BeloverdSpace. All rights reserved.
//

#import "RechargeViewController.h"
#import "RechargeXib.h"
#import <AlipaySDK/AlipaySDK.h>
#import "WXApi.h"
#import "WeChatAPIParameterModel.h"

@interface RechargeViewController ()<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) RechargeXib *rechargeView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, strong) NSString *orderNo;

@end

@implementation RechargeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"充值";
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    [self setupUI];
}

- (void)setupUI
{
    RechargeXib *rechargeView = [RechargeXib loadRechargeXib];
    rechargeView.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    [self.view addSubview:rechargeView];
    _rechargeView = rechargeView;
    rechargeView.remainingSumLabel.text = self.Balance;
    [rechargeView.rechargeBtn addTarget:self action:@selector(rechargeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [rechargeView.rechargeText addTarget:self action:@selector(clickRechargeText) forControlEvents:UIControlEventEditingDidBegin];
    [rechargeView.rechargeText addTarget:self action:@selector(clickRechargeTextEnd) forControlEvents:UIControlEventEditingDidEnd];
    
    UITableView *tableView = [[UITableView alloc] init];
    tableView.frame = CGRectMake(0, kScreenH, kScreenW, 100);
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.rowHeight = 50;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.view addSubview:tableView];
    self.tableView = tableView;
}

#pragma mark -UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    cell.textLabel.textColor = [UIColor colorWithHexString:@"#333333"];
    if (indexPath.row == 0) {
        cell.textLabel.text = @"微信";
        cell.imageView.image = [UIImage imageNamed:@"pay_wechat"];
    }else {
        cell.textLabel.text = @"支付宝";
        cell.imageView.image = [UIImage imageNamed:@"pay_alipay"];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        [self requestNetworkRechargePaySortWith:@"WeChat"];
    }else {
        [self requestNetworkRechargePaySortWith:@"Alipay"];
    }
}

//  解决cell分割线左边短20px的问题
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (void)clickRechargeText
{
    [UIView animateWithDuration:0.3 animations:^{
        self.rechargeView.frame = CGRectMake(0, -280, kScreenW, kScreenH);
    }];
}

- (void)clickRechargeTextEnd
{
    [UIView animateWithDuration:0.3 animations:^{
        self.rechargeView.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    }];
}

// 充值
- (void)rechargeBtnClick
{
    NSString *sumMoney = @"";
    if (_rechargeView.rechargeText.text.length > 0) {
        sumMoney = _rechargeView.rechargeText.text;
    }else {
        sumMoney = [_rechargeView.selectedBtn.titleLabel.text substringFromIndex:1];
    }
    [self requestNetworkRechargeOrderSumWithMoney:sumMoney];
}
#pragma mark -网络请求
- (void)requestNetworkRechargeOrderSumWithMoney:(NSString *)sumMoney
{
    NSDictionary *parameter = @{@"amount":sumMoney};
    [[BDNetworkTools sharedInstance] postRechargeFountWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        BDLog(@"%@", responseObject);
        self.orderId = [responseObject objectForKey:@"orderId"];
        self.orderNo = [responseObject objectForKey:@"orderNo"];
        self.tableView.bd_y = kScreenH - 100;
    }];
}

- (void)requestNetworkRechargePaySortWith:(NSString *)payType
{
    NSDictionary *parameter = @{
                                @"payType":payType,
                                @"clientSource":@"APP",
                                @"orderId":_orderId
                                };
    [[BDNetworkTools sharedInstance] postRechargeAccountWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
//        BDLog(@"%@", responseObject);
        if ([payType isEqualToString:@"Alipay"]) {
            NSString *acObjStr = [responseObject objectForKey:@"alipayApp"];
            NSDictionary *alipayDict = [self stringTransformDictWithString:acObjStr];
            NSString *payOrder = [alipayDict objectForKey:@"payParams"];
            // NOTE: 调用支付结果开始支付
            [[AlipaySDK defaultService] payOrder:payOrder fromScheme:kAliPayURLScheme callback:^(NSDictionary *resultDic) {
                BDLog(@"reslut = %@",resultDic);
            }];
            
        }else if ([payType isEqualToString:@"WeChat"]) {
            if ([WXApi isWXAppInstalled]) {
                BDLog(@"已经安装了微信...");
                NSString *acObjStr = [responseObject objectForKey:@"weChatApp"];
                NSDictionary *weChatDict = [self stringTransformDictWithString:acObjStr];
               WeChatAPIParameterModel *wxModel = [WeChatAPIParameterModel weChatAPIParameterModelWithDict:weChatDict];
                //这里调用后台接口获取订单的详细信息，然后调用微信支付方法
                [self WXPayWithAppid:wxModel.appId
                           partnerid:wxModel.partnerId
                            prepayid:wxModel.prepayId
                             package:wxModel.packageValue
                            noncestr:wxModel.nonceStr
                           timestamp:wxModel.timeStamp
                                sign:wxModel.sign];
            }
        }
    }];
}

#pragma mark 微信支付方法
- (void)WXPayWithAppid:(NSString *)appid partnerid:(NSString *)partnerid prepayid:(NSString *)prepayid package:(NSString *)package noncestr:(NSString *)noncestr timestamp:(NSString *)timestamp sign:(NSString *)sign{
    //需要创建这个支付对象
    PayReq *req = [[PayReq alloc] init];
    //由用户微信号和AppID组成的唯一标识，用于校验微信用户
    req.openID = appid;
    // 商家id，在注册的时候给的
    req.partnerId = partnerid;
    // 预支付订单这个是后台跟微信服务器交互后，微信服务器传给你们服务器的，你们服务器再传给你
    req.prepayId = prepayid;
    // 根据财付通文档填写的数据和签名
    req.package = package;
    // 随机编码，为了防止重复的，在后台生成
    req.nonceStr = noncestr;
    // 这个是时间戳，也是在后台生成的，为了验证支付的
    NSString * stamp = timestamp; req.timeStamp = stamp.intValue;
    // 这个签名也是后台做的
    req.sign = sign;
    if ([WXApi sendReq:req]) {
        //发送请求到微信，等待微信返回onResp
        NSLog(@"吊起微信成功...");
    }else{
        NSLog(@"吊起微信失败...");
    }
}

- (NSDictionary *)stringTransformDictWithString:(NSString *)string {
    NSRange range = NSMakeRange(0, string.length);
    NSString *subStr = [string substringWithRange:range];
    NSData *JSONData = [subStr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *DictString = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableLeaves error:nil];
    return DictString;
}

@end


