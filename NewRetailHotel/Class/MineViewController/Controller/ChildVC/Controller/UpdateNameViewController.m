//
//  UpdateNameViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/4.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "UpdateNameViewController.h"

@interface UpdateNameViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topOneViewConstraints;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UIButton *manbtn;
@property (weak, nonatomic) IBOutlet UIButton *womenBnt;
@property (weak, nonatomic) IBOutlet UILabel *dateLab;// 出生日期

@property (nonatomic,assign)BOOL IsMan;
@property (nonatomic,assign)BOOL isWomen;

@end

@implementation UpdateNameViewController

- (void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = YES;
    self.topOneViewConstraints.constant = SafeAreaTopHeight;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"个人信息";
    [self getUserInfoData];

    UIButton *button = [self.nameField valueForKey:@"_clearButton"];
    [button setImage:[UIImage imageNamed:@"icon_clear"] forState:UIControlStateNormal];
    self.nameField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self.nameField setValue:[UIColor colorWithHexString:@"#777777"] forKeyPath:@"_placeholderLabel.textColor"];
    [self.nameField setValue:[UIFont boldSystemFontOfSize:13] forKeyPath:@"_placeholderLabel.font"];
    self.nameField.borderStyle = UITextBorderStyleNone;
    //出生日期
    self.dateLab.userInteractionEnabled = YES;
    UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(labelTouchUpInside:)];
    [_dateLab addGestureRecognizer:labelTapGestureRecognizer];
}

- (void)labelTouchUpInside:(UITapGestureRecognizer *)recognizer{
    [self dateClick];
    
}

// 确定按钮
- (IBAction)confirmbtn:(UIButton *)sender {
    
    [self updateAccountInfo];
}

- (IBAction)menBtn:(UIButton *)sender {
    
    //   去除按钮的按下效果（阴影）
    self.manbtn.adjustsImageWhenHighlighted = NO;
    self.womenBnt.adjustsImageWhenHighlighted = NO;
    
    if (_IsMan) {
        [self.womenBnt setImage:[UIImage imageNamed:@"icon_select_selected"] forState:UIControlStateNormal];
        [self.manbtn setImage:[UIImage imageNamed:@"icon_select"] forState:UIControlStateNormal];

        _IsMan = NO;
        _isWomen = YES;
        
    }else {
        [self.manbtn setImage:[UIImage imageNamed:@"icon_select_selected"] forState:UIControlStateNormal];
        [self.womenBnt setImage:[UIImage imageNamed:@"icon_select"] forState:UIControlStateNormal];
        _IsMan = YES;
        _isWomen = NO;
        
    }
    
}
- (IBAction)womenBtn:(UIButton *)sender {
    self.womenBnt.adjustsImageWhenHighlighted = NO;
    
    if (!_isWomen) {
        [self.womenBnt setImage:[UIImage imageNamed:@"icon_select_selected"] forState:UIControlStateNormal];
        [self.manbtn setImage:[UIImage imageNamed:@"icon_select"] forState:UIControlStateNormal];
        
        _IsMan = NO;
        _isWomen = YES;
        
    }else {
        [self.manbtn setImage:[UIImage imageNamed:@"icon_select_selected"] forState:UIControlStateNormal];
        [self.womenBnt setImage:[UIImage imageNamed:@"icon_select"] forState:UIControlStateNormal];
        
        _IsMan = YES;
        _isWomen = NO;
        
    }

}

- (void)dateClick{
    UIDatePicker *picker = [[UIDatePicker alloc]init];
    picker.datePickerMode = UIDatePickerModeDate;
    
    picker.frame = CGRectMake(0, 0, kScreenW, 200);
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        NSDate *date = picker.date;
        
        _dateLab.text = [self stringFromDate:date];
        
    }];
    
    [alertController.view addSubview:picker];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

// NSDate转NSString
- (NSString *)stringFromDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *destDateString = [dateFormatter stringFromDate:date];
    return destDateString;
}

#pragma mark - 更新账户信息
- (void)updateAccountInfo{
    NSString *sex = @"";
    if (_isWomen) {
        sex = @"女";
    }else{
        sex = @"男";
    }
    NSDictionary *parameter = @{@"nickname":self.nameField.text,@"gender":sex,@"birthDate":self.dateLab.text};
    [[BDNetworkTools sharedInstance] putUserInfoUpdateWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)getUserInfoData
{
    [[BDNetworkTools sharedInstance] getUserAcountBasicWithBlock:^(NSDictionary *responseObject, NSError *error) {
        self.nameField.text = [responseObject objectForKey:@"nickname"];
        NSString *sexStr = [responseObject objectForKey:@"gender"];
        NSString *birtStr = [responseObject objectForKey:@"birthDate"];
        if (birtStr.length > 10) {
            self.dateLab.text = [birtStr substringToIndex:10];
        }
        // 性别
        if ([sexStr isEqualToString:@"男"]) {
            [self menBtn:_manbtn];
        }else {
            [self womenBtn:_womenBnt];
        }
    }];
}

@end
