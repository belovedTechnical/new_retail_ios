//
//  SafetyAccoutViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/8.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "SafetyAccoutViewController.h"
#import "UpdatePhoneViewController.h"
#import "RetrievePasswordViewController.h"

@interface SafetyAccoutViewController ()

@end

@implementation SafetyAccoutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"账户安全";
    self.tableView.rowHeight = 50;
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.tableFooterView = [[UIView alloc] init];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    NSArray *oneSection = @[@"修改手机号码",@"设置登录密码"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    cell.textLabel.textColor = [UIColor colorWithHexString:@"#333333"];
    cell.textLabel.text = oneSection[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        UpdatePhoneViewController *updatePhoneVC = [[UpdatePhoneViewController alloc] init];
        [self.navigationController pushViewController:updatePhoneVC animated:YES];
    }else if (indexPath.row == 1) {
        RetrievePasswordViewController *retrieveVC = [[RetrievePasswordViewController alloc] init];
        [self.navigationController pushViewController:retrieveVC animated:YES];
    }
}


@end
