//
//  ZhiaiBiListViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/16.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "ZhiaiBiListViewController.h"
#import "BDNetworkTools.h"
#import "MJRefresh.h"
#import "IncomeListCell.h"
#import "HotelHeaderView.h"
#import "ZhiAiBiListModel.h"
#import "HotelSearchData.h"

@interface ZhiaiBiListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *zhiaibiLab;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)HotelHeaderView *headerView;


// 动态参数字典
@property (nonatomic, strong) NSMutableDictionary *paramDic;
@property(nonatomic, strong)NSMutableArray *listArray;

@property (assign, nonatomic) NSInteger pageIndex;
@property (assign, nonatomic) NSInteger pageSize;
@property (assign, nonatomic) NSUInteger jiFenMingXi;

@property (assign, nonatomic) NSUInteger pageIndexShouRu;
@property (assign, nonatomic) NSUInteger PageIndexZhiChu;

@property (assign, nonatomic) BOOL jiFenMingXiMiddle;

@end

@implementation ZhiaiBiListViewController
- (NSMutableArray *)listArray{
    if (!_listArray) {
        self.listArray = [NSMutableArray array];
    }
    return _listArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.title = @"积分明细";

    self.pageSize = 15;
    self.jiFenMingXi = JiFenMingXiShouRu;
    [self resetPageIndex];
    
    // 默认加载
    [self loadPointListIncomeWithPageIndex:self.pageIndexShouRu];
    [self addRefresh];
    
    self.headerView = [[HotelHeaderView alloc]initWithFrame:CGRectMake(0, 10, kScreenW, 44)];
    
    __weak typeof(self) weakSelf = self;
    _headerView.clickIncomeBtn = ^(NSInteger index){
        weakSelf.jiFenMingXiMiddle = YES;
        switch (index) {
            case JiFenMingXiShouRu: //收入
                
                [weakSelf loadPointListIncomeWithPageIndex:weakSelf.pageIndexShouRu];
                weakSelf.jiFenMingXi = JiFenMingXiShouRu;
                break;
            case JiFenMingXiZhiChu: // 支出
                
                [weakSelf loadPointListOutWithPageIndex:weakSelf.PageIndexZhiChu];
                weakSelf.jiFenMingXi = JiFenMingXiZhiChu;
                break;
    
            default:
                break;
        }
        
    };
    //    [self.tableView.mj_header beginRefreshing];
    
    self.tableView.tableFooterView = [[UIView alloc]init];
    
    // 注册cell
    [_tableView registerNib:[UINib nibWithNibName:@"IncomeListCell" bundle:nil] forCellReuseIdentifier:@"IncomeListCell"];

}

//收入
- (void)loadPointListIncomeWithPageIndex:(NSUInteger)pageIndex{
    
    NSDictionary *paramDic =  @{@"transType":@"FattDeposit",@"pageIndex":[NSString stringWithFormat:@"%lu", (unsigned long)pageIndex],@"pageSize":[NSString stringWithFormat:@"%ld",(long)(long)self.pageSize]};
    
    self.paramDic = [NSMutableDictionary dictionaryWithDictionary:paramDic];
    
    [self requestDataList];
}

// 支出
- (void)loadPointListOutWithPageIndex:(NSUInteger)pageIndex {

    NSDictionary *paramDic =  @{@"transType":@"FattWithdraw", @"pageIndex":[NSString stringWithFormat:@"%lu", (unsigned long)pageIndex],@"pageSize":[NSString stringWithFormat:@"%ld",(long)self.pageSize] };
    
    self.paramDic = [NSMutableDictionary dictionaryWithDictionary:paramDic];
    
    [self requestDataList];
}

- (void)requestDataList{
    [[BDNetworkTools sharedInstance] getAccountIntegralWithParameter:self.paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        if (self.jiFenMingXiMiddle) {
            [self.listArray removeAllObjects];
        }
        self.zhiaibiLab.text = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"availablePoints"]];
        NSArray *arr = [responseObject objectForKey:@"pointsList"];
        for (NSDictionary *dic in arr) {
            ZhiAiBiListModel *model = [[ZhiAiBiListModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [self.listArray addObject:model];
        }
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        if (arr.count < 15) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }else
        {
            [self.tableView.mj_footer endRefreshing];
        }
    }];
}

- (IBAction)zhiAiBiBookHotel:(UIButton *)sender {
    
    self.navigationController.tabBarController.selectedIndex = 0;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.listArray.count;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    return self.headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    IncomeListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"IncomeListCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    ZhiAiBiListModel *model = self.listArray[indexPath.row];
    [cell setZhiAiBiListCell:model];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    ZhiAIBiDetailVC *detailVC = [[ZhiAIBiDetailVC alloc]init];
//    ZhiAiBiListModel *model = self.listArray[indexPath.row];
//    detailVC.pointsId = model.pointsId;
//    [self.navigationController pushViewController:detailVC animated:YES];
}

//  解决cell分割线左边短20px的问题
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)loadSubViews{
    // 规则说明按钮
    UIView *rightView = [[UIView alloc] init];
    rightView.bounds = CGRectMake(0, 0, 52, 44);
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 2, SafeAreaTopHeight, 44);
    [rightButton setTitle:@"规则说明" forState:(UIControlStateNormal)];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:14];
    
    [rightButton addTarget:self action:@selector(detailBtn:)
          forControlEvents:UIControlEventTouchUpInside];
    [rightView addSubview:rightButton];
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightView];
    [self.navigationItem setRightBarButtonItem:rightBarButtonItem animated:YES];
    
}
- (void)detailBtn:(UIBarButtonItem *)sender{
    
    
}

- (void)resetPageIndex
{
    self.pageIndexShouRu = 0;
    self.PageIndexZhiChu = 0;
}

#pragma mark -第三方上拉加载下拉刷新
- (void)addRefresh
{
    // 下拉刷新
    __weak typeof(self) weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        weakSelf.jiFenMingXiMiddle = YES;
        [weakSelf resetPageIndex];
        switch (weakSelf.jiFenMingXi) {
            case JiFenMingXiShouRu:
                [weakSelf loadPointListIncomeWithPageIndex:weakSelf.pageIndexShouRu];
                break;
            case JiFenMingXiZhiChu:
                [weakSelf loadPointListOutWithPageIndex:weakSelf.PageIndexZhiChu];
                break;
            default:
                break;
        }
    }];
    
    // 上拉加载
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        
        weakSelf.jiFenMingXiMiddle = NO;
        switch (weakSelf.jiFenMingXi) {
            case JiFenMingXiShouRu:
                weakSelf.pageIndexShouRu++;
                [weakSelf loadPointListIncomeWithPageIndex:weakSelf.pageIndexShouRu];
                break;
            case JiFenMingXiZhiChu:
                self.PageIndexZhiChu++;
                [weakSelf loadPointListOutWithPageIndex:weakSelf.PageIndexZhiChu];
                break;
            default:
                break;
        }
        
    }];
}


@end
