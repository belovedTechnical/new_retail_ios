//
//  ZhiaiBiListViewController.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/16.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef  NS_ENUM(NSUInteger, JiFenMingXi)
{
    JiFenMingXiShouRu = 201,
    JiFenMingXiZhiChu = 202
};

@interface ZhiaiBiListViewController : UIViewController

@end
