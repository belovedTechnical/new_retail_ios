//
//  FeedbackViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/17.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "FeedbackViewController.h"
#import "FeedbackTableViewCell.h"

#import "FeebackModel.h"
#import "QNUploadManager.h"

@interface FeedbackViewController ()<
                                    UITableViewDataSource,
                                    UITableViewDelegate,
                                    UITextFieldDelegate,
                                    UIImagePickerControllerDelegate,
                                    UINavigationControllerDelegate
                                    >

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *feekbackArray;
@property (nonatomic, strong) UITextField *fbText;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UIImagePickerController *imgPickerCon;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIImage *uploadImage;
@property (nonatomic, strong) NSMutableArray *uploadIconKey;

@end

static NSString *const feedbackCell= @"FeedbackTableViewCell";
@implementation FeedbackViewController

- (UIImagePickerController *)imgPickerCon
{
    if (!_imgPickerCon) {
        _imgPickerCon = [[UIImagePickerController alloc] init];
        _imgPickerCon.delegate = self;
        _imgPickerCon.allowsEditing = YES;
    }
    return _imgPickerCon;
}

- (NSMutableArray *)uploadIconKey
{
    if (!_uploadIconKey) {
        _uploadIconKey = [NSMutableArray array];
    }
    return _uploadIconKey;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    self.title = @"客服反馈";
    [self inintUI];
    [self getFeedbackData];
}

static CGFloat bottomH = 50;
- (void)inintUI
{
    UITableView *tableView = [[UITableView alloc] init];
    tableView.frame = CGRectMake(0, 0, kScreenW, kScreenH-bottomH);
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.rowHeight = 200;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    UIView *bottomView = [[UIView alloc] init];
    bottomView.frame = CGRectMake(0, kScreenH-bottomH-SafeAreaBottomHeight, kScreenW, bottomH);
    bottomView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:bottomView];
    self.bottomView = bottomView;
    
    UIButton *btnAddImage = [[UIButton alloc] init];
    btnAddImage.frame = CGRectMake(0, 5, 50, bottomH-10);
    [btnAddImage setImage:[UIImage imageNamed:@"addImage_icon"] forState:UIControlStateNormal];
    [btnAddImage addTarget:self action:@selector(btnAddImageClick) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:btnAddImage];
    
    CGFloat btnSendW = 50;
    UIButton *btnSend = [[UIButton alloc] init];
    btnSend.frame = CGRectMake(bottomView.bd_width-btnSendW-15, 5, btnSendW, bottomH-10);
    [btnSend setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnSend setTitle:@"发送" forState:UIControlStateNormal];
    btnSend.titleLabel.font = [UIFont systemFontOfSize:13];
    btnSend.backgroundColor = [UIColor colorWithHexString:@"#a6926b"];
    [btnSend addTarget:self action:@selector(btnSendClick) forControlEvents:UIControlEventTouchUpInside];
    btnSend.layer.cornerRadius = 2.5;
    btnSend.layer.masksToBounds = YES;
    [bottomView addSubview:btnSend];
    
    UITextField *fbText = [[UITextField alloc] init];
    fbText.frame = CGRectMake(btnAddImage.bd_right, 5, bottomView.bd_width-btnSendW-btnAddImage.bd_width-30, bottomH-10);
    fbText.borderStyle = UITextBorderStyleRoundedRect;
    fbText.delegate = self;
    [bottomView addSubview:fbText];
    self.fbText = fbText;
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake(0, kScreenH-200, 150, 150);
    [self.view addSubview:imageView];
    self.imageView = imageView;
    
    [self.tableView registerClass:[FeedbackTableViewCell class] forCellReuseIdentifier:feedbackCell];
}

#pragma mark -UITableViewDataSource, UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _feekbackArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FeedbackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:feedbackCell];
    if (cell == nil) {
        cell = [[FeedbackTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:feedbackCell];
    }
    cell.feedbackModel = _feekbackArray[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FeebackModel *feedModel = self.feekbackArray[indexPath.row];
    
    return feedModel.cellHeight;
}

#pragma mark -按钮点击监听
- (void)btnAddImageClick
{
    self.imgPickerCon.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imgPickerCon animated:YES completion:nil];
    
    // 判断相机是否可用
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
    }
}

- (void)btnSendClick
{
    NSDictionary *parameter = @{@"content":_fbText.text,@"feedbackType":@"string"};
    if (self.uploadImage == nil) {
        [self postFeedbackContentWithParameter:parameter];
    }else if (_fbText.text == nil)
    {
        [self requestNetUploadImage];
    }else {
        [self requestNetUploadImage];
        [self postFeedbackContentWithParameter:parameter];
    }
}

#pragma mark -UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [self.imgPickerCon dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    self.imageView.image = image;
    self.uploadImage = image;
}

#pragma mark -UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.bottomView.frame = CGRectMake(0, kScreenH-330, kScreenW, bottomH);
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.bottomView.frame = CGRectMake(0, kScreenH-bottomH, kScreenW, bottomH);
}

//照片获取本地路径转换
- (NSString *)getImagePath:(UIImage *)Image {
    NSString *filePath = nil;
    NSData *data = nil;
    if (UIImagePNGRepresentation(Image) == nil) {
        data = UIImageJPEGRepresentation(Image, 1.0);
    } else {
        data = UIImagePNGRepresentation(Image);
    }
    
    NSString *DocumentsPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager createDirectoryAtPath:DocumentsPath withIntermediateDirectories:YES attributes:nil error:nil];
    NSString *ImagePath = [[NSString alloc] initWithFormat:@"/theFirstImage.png"];
    [fileManager createFileAtPath:[DocumentsPath stringByAppendingString:ImagePath] contents:data attributes:nil];
    filePath = [[NSString alloc] initWithFormat:@"%@%@", DocumentsPath, ImagePath];
    return filePath;
}

#pragma mark -网络请求
- (void)getFeedbackData
{
    NSDictionary *parameter = @{@"pageIndex":@"0",@"pageSize":@"30"};
    [[BDNetworkTools sharedInstance] getFeedbackGetWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        NSArray *dataArrary = [responseObject objectForKey:@"feedbackList"];
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *dict in dataArrary) {
            [tempArray addObject:[FeebackModel feebackModelWithDict:dict]];
        }
        self.feekbackArray = tempArray;
        [self.tableView reloadData];
    }];
}

- (void)postFeedbackContentWithParameter:(NSDictionary *)parameter
{
    [[BDNetworkTools sharedInstance] postFeedfackCreateWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
//        BDLog(@"%@", responseObject);
    }];
}

- (void)requestNetUploadImage
{
    [[BDNetworkTools sharedInstance] getQiNiuYunTokenWithParameter:nil Block:^(NSDictionary *responseObject, NSError *error) {
        NSString *token = [responseObject objectForKey:@"token"];
        QNUploadManager *uploadManger = [[QNUploadManager alloc] init];
        NSString *imagePath = [self getImagePath:_uploadImage];
        [uploadManger putFile:imagePath key:token token:token complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
            [self.uploadIconKey addObject:[resp objectForKey:@"key"]];
            // 用户上传图片数组转Json类型
            NSData *data=[NSJSONSerialization dataWithJSONObject:_uploadIconKey options:NSJSONWritingPrettyPrinted error:nil];
            NSString *jsonStr=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            NSDictionary *parameter = @{@"content":jsonStr, @"feedbackType":@"image"};
            [self postFeedbackContentWithParameter:parameter];
            [self.tableView reloadData];
        } option:nil];
    }];
}





@end
