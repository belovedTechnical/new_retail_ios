//
//  ScanViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 17/3/13.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "ScanViewController.h"
#import "ShoppingHeaderView.h"
#import "BDNetworkTools.h"
#import "CustomCollectionCell.h"
#import "ScanModel.h"
#import "HotelSearchData.h"

@interface ScanViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,copy)NSString *userId;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic,strong)ShoppingHeaderView *shoppingHeaderView;
@property(nonatomic,strong)NSMutableArray *dataArr;
@property(nonatomic,assign)BOOL isHotel;
@property(nonatomic,copy)NSString *apikey;

@property (weak, nonatomic) IBOutlet UIView *noDataView;
@property (weak, nonatomic) IBOutlet UIButton *goToHomeBtn;


@end

@implementation ScanViewController
- (ShoppingHeaderView *)shoppingHeaderView{
    if (!_shoppingHeaderView) {
        self.shoppingHeaderView = [[ShoppingHeaderView alloc]initWithFrame:CGRectMake(0, SafeAreaTopHeight, kScreenW, 40)];
        
    }
    return _shoppingHeaderView;
    
}
- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        self.dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *defaul = [NSUserDefaults standardUserDefaults];
    self.apikey = [NSString stringWithFormat:@"%@",[defaul objectForKey:@"apikey"]];
    
    self.tableview.tableFooterView = [[UIView alloc]init];
    
    self.title = @"浏览记录";// 最多加载最新的20条
    [self loadAccont];//请求partyId
    
    self.isHotel = YES;
    
    [self loadSubviews];
    

    __weak ScanViewController *scanVC = self;
    self.shoppingHeaderView.shoppingClickedBtn = ^(NSInteger index){
        switch (index) {
            case kOutboxBtn_tag:
                [scanVC loadScanData:@"0"];
                scanVC.isHotel = YES;
                
                
                break;
            case kResignedBtn_tag:
                [scanVC loadScanData:@"1"];
                scanVC.isHotel = NO;
                
                break;
            default:
                break;
        }
        
        
        [scanVC.tableview reloadData];
    };

}
-(void)loadSubviews{
    self.noDataView.hidden = YES;
    self.goToHomeBtn.layer.cornerRadius = 5;
    self.goToHomeBtn.layer.masksToBounds = YES;
    self.goToHomeBtn.layer.borderColor = [UIColor colorWithHexString:@"#E22425"].CGColor;
    self.goToHomeBtn.layer.borderWidth = 1.5;
    
    [self.tableview registerNib:[UINib nibWithNibName:@"CustomCollectionCell" bundle:nil] forCellReuseIdentifier:@"CustomCollectionCell"];
    // 清除
    UIView *rightView = [[UIView alloc] init];
    rightView.bounds = CGRectMake(0, 0, 52, 44);
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(20, 0, 44, 44);
    [rightButton setTitle:@"清空" forState:(UIControlStateNormal)];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [rightButton addTarget:self action:@selector(clearBtn:)
          forControlEvents:UIControlEventTouchUpInside];
    [rightView addSubview:rightButton];
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightView];
    [self.navigationItem setRightBarButtonItem:rightBarButtonItem animated:YES];
    
}
- (void)clearBtn:(UIButton *)sender{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:@"是否要清空记录？" preferredStyle:UIAlertControllerStyleAlert];
    [alertVC addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        NSDictionary *dic = [NSDictionary dictionary];
        if (self.isHotel){
            dic = @{@"type":@"0",@"userId":self.userId};
        }else{
            dic = @{@"type":@"1",@"userId":self.userId};
        }
        [[BDNetworkTools sharedInstance] getScanClearRecordWithParameter:dic Block:^(NSDictionary *responseObject, NSError *error) {
            self.noDataView.hidden = NO;
            [self.dataArr removeAllObjects];
            [self.tableview reloadData];
        }];
    }]];
     [alertVC addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertVC animated:YES completion:nil];
}
- (IBAction)goToHomeVC:(UIButton *)sender {

    self.navigationController.tabBarController.selectedIndex = 0;  //0
    [self.navigationController popToRootViewControllerAnimated:NO];
}

// 浏览记录数据请求
- (void)loadScanData:(NSString *)type{
    
    [self.dataArr removeAllObjects];
    if (self.userId == nil) {
        return;
    }
    NSDictionary *dic = @{@"type":type,@"userId":self.userId};
    [[BDNetworkTools sharedInstance] getScanRecordDataWithParameter:dic Block:^(NSDictionary *responseObject, NSError *error) {
        NSArray *arr = [responseObject objectForKey:@"results"];
        for (NSDictionary *dic in arr) {
            ScanModel *model = [[ScanModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [self.dataArr addObject:model];
        }
        if (self.dataArr.count >0) {
            self.noDataView.hidden = YES;
            
        }else{
            self.noDataView.hidden = NO;
        }
        [self.tableview reloadData];
    }];
}

#pragma mark- UITableviewDelegate
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    
    return self.shoppingHeaderView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    
    return self.dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CustomCollectionCell *cell = [self.tableview dequeueReusableCellWithIdentifier:@"CustomCollectionCell" forIndexPath:indexPath];
    cell.deleteBtn.hidden = YES;
    ScanModel *model = self.dataArr[indexPath.row];
    
    [cell setScanCell:model];
    
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
     ScanModel *model = self.dataArr[indexPath.row];
    if (self.isHotel) {
         NSString *value = [NSString stringWithFormat:@"%@",model.partyId];
       
        [self loadDetail:value];

        
    }else{
        
    }
}
#pragma mark -跳转 请求酒店详情
- (void)loadDetail:(NSString *)value{
   
}

- (void)loadAccont{
    [[BDNetworkTools sharedInstance] getUserAcountWithBlock:^(NSDictionary *responseObject, NSError *error) {
        self.userId = [responseObject objectForKey:@"partyId"];
        [self loadScanData:@"0"];
    }];
}


@end
