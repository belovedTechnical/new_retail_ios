//
//  StoreCollectionViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 17/3/7.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import "StoreCollectionViewController.h"
#import "WebHotelDetailsViewController.h"
#import "StoreCollectModel.h"
#import "BDNetworkTools.h"
#import "MJRefresh.h"
#import "StoreCollectionCell.h"

@interface StoreCollectionViewController ()<UITableViewDelegate,UITableViewDataSource>
// YES:上拉  NO:下拉
@property (nonatomic, assign) BOOL isUp;
@property (nonatomic, copy) NSString *apikey;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) StoreCollectModel *modelS;
@property (nonatomic, strong) NSMutableDictionary *paramDic;
@property (weak, nonatomic) IBOutlet UIView *coverView;
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end

@implementation StoreCollectionViewController
- (NSMutableDictionary *)paramDic{
    if (!_paramDic) {
        self.paramDic = [NSMutableDictionary dictionary];
    }
    return _paramDic;
}
- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        self.dataArr = [NSMutableArray array];
    }
    return _dataArr;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"店铺收藏";
    
    self.coverView.hidden = YES;
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    self.apikey = [NSString stringWithFormat:@"%@",[user objectForKey:@"apikey"]];
    self.tableview.tableFooterView = [[UIView alloc]init];
    
    [self addRefresh];
    [self loadStoreList];
    [self.tableview registerNib:[UINib nibWithNibName:@"StoreCollectionCell" bundle:nil] forCellReuseIdentifier:@"StoreCollectionCell"];

}
#pragma mark - UITableview

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    StoreCollectionCell *cell = [self.tableview dequeueReusableCellWithIdentifier:@"StoreCollectionCell" forIndexPath:indexPath];
    StoreCollectModel *model = self.dataArr[indexPath.row];
    [cell setStoreCell:model];
    cell.deleteStoreBtn =^(){
        
        self.coverView.hidden = NO;
        __block  StoreCollectModel *model = self.dataArr[indexPath.row];
        self.modelS = model;
    };
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    StoreCollectModel *model = self.dataArr[indexPath.row];
    WebHotelDetailsViewController *detailVC = [[WebHotelDetailsViewController alloc] init];
    detailVC.hotelDetailsURL = [NSString stringWithFormat:@"%@shop/%@",HTML_URL_Base, model.productStoreId];
    [self.navigationController pushViewController:detailVC animated:YES];
}

// 取消
- (IBAction)cancleBtn:(UIButton *)sender {
    
    self.coverView.hidden = YES;
}

// 确定删除
- (IBAction)confirmDeleteBtn:(UIButton *)sender {
    
    self.coverView.hidden = YES;
    [self loadDeleteWith:[NSString stringWithFormat:@"%@",self.modelS.goodsFavoritesId]];
}

- (void)loadDeleteWith:(NSString *)goodsFavoritesId{
    NSDictionary *dic = @{@"goodsFavoritesId":goodsFavoritesId};
    [[BDNetworkTools sharedInstance] postCollectStoreDeleteWithParameter:dic Block:^(NSDictionary *responseObject, NSError *error) {
        //  重新请求数据
        [self loadStoreList];
    }];
}
#pragma mark - MJRefresh.h 第三方上拉加载和下拉刷新
// 添加上拉加载和下拉刷新
- (void)addRefresh{
    self.tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self.paramDic setObject:@"0" forKey:@"pageIndex"];
        
        [self requestDataList];
        _isUp = NO;
        
    }];
    self.tableview.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        NSInteger pageIndex = [[self.paramDic objectForKey:@"pageIndex"] integerValue];
        
        pageIndex++;
        
        [self.paramDic setObject:[NSString stringWithFormat:@"%ld", pageIndex] forKey:@"pageIndex"];
        
        [self requestDataList];
        
        // 表示上拉
        _isUp = YES;
    }];
}

- (void)loadStoreList{
    
    NSDictionary *paramDic =  @{@"pageIndex":@"0",@"pageSize":@"15"};;
    self.paramDic = [NSMutableDictionary dictionaryWithDictionary:paramDic];
    [self requestDataList];
}

// 店铺收藏
- (void)requestDataList{
    [[BDNetworkTools sharedInstance] getCollectStoreDataWithParameter:self.paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        NSArray *arr = [responseObject objectForKey:@"favoritesList"];
        if (!_isUp) {
            // 清空之前的数据
            [self.dataArr removeAllObjects];
        }
        for (NSDictionary *dic in arr) {
            StoreCollectModel *model = [[StoreCollectModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [self.dataArr addObject:model];
        }
        [self.tableview reloadData];
        [self.tableview.mj_header endRefreshing];
        if (arr.count < 15) {
            [self.tableview.mj_footer endRefreshingWithNoMoreData];
        }else
        {
            [self.tableview.mj_footer endRefreshing];
        }
    }];
}

@end
