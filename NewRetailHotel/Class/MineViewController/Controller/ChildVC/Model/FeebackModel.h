//
//  FeebackModel.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/20.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

#define BDNameFont [UIFont systemFontOfSize:15]
#define BDContentFont [UIFont systemFontOfSize:13]
#define BDDateFont [UIFont systemFontOfSize:12]
@interface FeebackModel : NSObject

@property (nonatomic, strong) NSString *feedbackId;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *feedbackType;
@property (nonatomic, strong) NSString *backTime;
@property (nonatomic, strong) NSString *feedbackStatus;
@property (nonatomic, strong) NSString *backPartyId;
@property (nonatomic, strong) NSString *backUserName;
@property (nonatomic, strong) NSString *replyPartyId;
@property (nonatomic, strong) NSString *replyUserName;
@property (nonatomic, strong) NSString *replyAuto;

+ (instancetype)feebackModelWithDict:(NSDictionary *)dict;

/*** frame数据 **/
@property(nonatomic, assign) CGRect iconFrame; /**< 图像的frame */
@property(nonatomic, assign) CGRect nameFrame; /**< 昵称的frame */
@property(nonatomic, assign) CGRect dateFrame; /**< 时间的frame */
@property(nonatomic, assign) CGRect textFrame; /**< 正文的frame */
@property(nonatomic, assign) CGRect pictureFrame; /**< 配图的frame */

@property(nonatomic, assign) CGFloat cellHeight; /**< cell的高度 */

@end
