//
//  ZhiAiBiListModel.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/16.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZhiAiBiListModel : NSObject

@property (nonatomic,copy)NSString *pointsId;
@property (nonatomic,copy)NSString *reason;
@property (nonatomic,copy)NSString *amount;
@property (nonatomic,copy)NSString *entryDate;

@end
