//
//  WeChatAPIParameterModel.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/5/17.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "WeChatAPIParameterModel.h"

@implementation WeChatAPIParameterModel

+ (instancetype)weChatAPIParameterModelWithDict:(NSDictionary *)dict {
    WeChatAPIParameterModel *model = [[self alloc] init];
    [model setValuesForKeysWithDictionary:dict];
    NSDictionary *acObjDict = [dict objectForKey:@"payParams"];
    [model setValuesForKeysWithDictionary:acObjDict];
    return model;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}

@end
