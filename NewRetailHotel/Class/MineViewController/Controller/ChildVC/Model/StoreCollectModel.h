//
//  StoreCollectModel.h
//  BelovedHotel
//
//  Created by 至爱 on 17/3/7.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreCollectModel : NSObject
@property(nonatomic,copy)NSString *goodsFavoritesId;
@property(nonatomic,copy)NSString *productStoreId;
@property(nonatomic,copy)NSString *storeLogo;
@property(nonatomic,copy)NSString *storeName;
@end
