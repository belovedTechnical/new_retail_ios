//
//  MyBalanceModel.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/16.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyBalanceModel : NSObject

@property (nonatomic,copy)NSString *finAccountTransId;
@property (nonatomic,copy)NSString *reason;
@property (nonatomic,copy)NSString *amount;
@property (nonatomic,copy)NSString *transactionDate;

@end
