//
//  ScanModel.h
//  BelovedHotel
//
//  Created by 至爱 on 17/3/13.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScanModel : NSObject

@property(nonatomic,copy)NSString *img;
@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *price;

@property(nonatomic,copy)NSString *partyId;
//@property(nonatomic,copy)NSString *;
//@property(nonatomic,copy)NSString *;

@end
