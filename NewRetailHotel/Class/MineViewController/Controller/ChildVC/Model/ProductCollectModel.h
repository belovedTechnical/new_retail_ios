//
//  ProductCollectModel.h
//  BelovedHotel
//
//  Created by 至爱 on 17/3/7.
//  Copyright © 2017年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductCollectModel : NSObject

@property(nonatomic,copy)NSString *productFavoriteId;
@property(nonatomic,copy)NSString *productId;
@property(nonatomic,copy)NSString *productImg;
@property(nonatomic,copy)NSString *productName;
@property(nonatomic,copy)NSString *productPrice;
@property(nonatomic,copy)NSString *productStatus;

@property(nonatomic,copy)NSString *productStore;

@end
