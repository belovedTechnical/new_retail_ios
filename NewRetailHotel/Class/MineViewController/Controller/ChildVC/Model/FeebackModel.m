//
//  FeebackModel.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/20.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "FeebackModel.h"

@implementation FeebackModel

+ (instancetype)feebackModelWithDict:(NSDictionary *)dict
{
    FeebackModel *model = [[self alloc] init];
    [model setValuesForKeysWithDictionary:dict];
    return model;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}

- (CGFloat)cellHeight
{
    if (_cellHeight == 0) {
        CGFloat space = 15;
        NSDictionary *nameAttr = @{NSFontAttributeName : BDNameFont};
        CGSize nameSize = [self.backUserName sizeWithAttributes:nameAttr];
        self.nameFrame = CGRectMake(space, space, nameSize.width, nameSize.height);
        
        NSDictionary *dateAttr = @{NSFontAttributeName : BDDateFont};
        CGSize dateSize = [self.backTime sizeWithAttributes:dateAttr];
        self.dateFrame = CGRectMake(kScreenW-dateSize.width-10, space, dateSize.width, dateSize.height);
        
        CGFloat textW = kScreenW - 2 * space;
        NSDictionary *contentAttr = @{NSFontAttributeName : BDContentFont};
        CGSize maxText = CGSizeMake(textW, MAXFLOAT);
        CGFloat textH = [self.content boundingRectWithSize:maxText options:NSStringDrawingUsesLineFragmentOrigin attributes:contentAttr context:nil].size.height;
        self.textFrame = CGRectMake(space, CGRectGetMaxY(self.nameFrame)+space, textW, textH);
        
        if ([self.feedbackType isEqualToString:@"image"]) {
            self.pictureFrame = CGRectMake(space, CGRectGetMaxY(self.textFrame), 80, 80);
            _cellHeight = CGRectGetMaxY(self.pictureFrame)+space;
        }else {
            _cellHeight = CGRectGetMaxY(self.textFrame)+space;
        }
        
    }
    return _cellHeight;
}

@end
