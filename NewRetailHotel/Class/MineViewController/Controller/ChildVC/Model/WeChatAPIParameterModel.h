//
//  WeChatAPIParameterModel.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/5/17.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeChatAPIParameterModel : NSObject
@property (nonatomic, strong) NSString *resCode;
@property (nonatomic, strong) NSString *retMsg;
@property (nonatomic, strong) NSString *payOrderId;
@property (nonatomic, strong) NSString *prepayId;
@property (nonatomic, strong) NSString *sign;
@property (nonatomic, strong) NSString *retCode;

@property (nonatomic, strong) NSDictionary *payParams;
@property (nonatomic, strong) NSString *nonceStr;
@property (nonatomic, strong) NSString *partnerId;
@property (nonatomic, strong) NSString *timeStamp;
@property (nonatomic, strong) NSString *appId;
@property (nonatomic, strong) NSString *packageValue;
//@property (nonatomic, strong) NSString *prepayId;
//@property (nonatomic, strong) NSString *sign;

+ (instancetype)weChatAPIParameterModelWithDict:(NSDictionary *)dict;

@end
