//
//  MeHeaderView.m
//  BelovedHotel
//
//  Created by 王保栋 on 2017/6/5.
//  Copyright © 2017年 BeloverdSpace. All rights reserved.
//

#import "MeHeaderView.h"
#import "UserInfoItem.h"

@interface MeHeaderView()

@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIImageView *meberIcon;

@end

@implementation MeHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor colorWithHexString:@"#f6f6f6" alpha:1];
    self.loginBtn.layer.cornerRadius = 2.5;
    self.loginBtn.layer.masksToBounds = YES;
    self.loginBtn.backgroundColor = [UIColor colorWithHexString:@"#a6926b"];
    [self.loginBtn setTitleColor:[UIColor colorWithHexString:@"#ffffff"] forState:UIControlStateNormal];
    
    self.userImage.layer.cornerRadius = self.userImage.bd_height / 2;
    self.userImage.layer.masksToBounds = YES;
    
    [self.signBtn setTitleColor:[UIColor colorWithHexString:@"#ffffff"] forState:UIControlStateNormal];
    self.signBtn.backgroundColor = [UIColor colorWithHexString:@"#a6926b"];
    [self graphicsViewClipWithButton:self.signBtn];
}

+ (instancetype)headerLoggedInView
{
    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] firstObject];
}

+ (instancetype)headerNotLoggedInView
{
    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
}

- (void)setItem:(UserInfoItem *)item
{   _item = item;
    
    self.userName.text = item.nickname;
//    [self.userImage sd_setImageWithURL:[NSURL URLWithString:item.headImage]];
}

- (void)graphicsViewClipWithButton:(UIButton *)clipButton
{
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:clipButton.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerBottomLeft cornerRadii:CGSizeMake(15, 15)];
    CAShapeLayer *layer = [CAShapeLayer layer];
    layer.frame = clipButton.bounds;
    layer.path = path.CGPath;
    clipButton.layer.mask = layer;
}

@end
