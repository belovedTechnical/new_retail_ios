//
//  MeContentView.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/4.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BottomButton.h"

@class UserInfoItem;
@interface MeContentView : UIView

/** 酒店订单 */
@property (weak, nonatomic) IBOutlet UIButton *hotleOrderBtn;
/** 电商订单 */
@property (weak, nonatomic) IBOutlet UIButton *onlineOrderBtn;
/** 积分 */
@property (weak, nonatomic) IBOutlet UIButton *integralBtn;
/** 优惠券 */
@property (weak, nonatomic) IBOutlet UIButton *couponBtn;
/** 账户余额 */
@property (weak, nonatomic) IBOutlet UIButton *balanceBtn;
/** 充值 */
@property (weak, nonatomic) IBOutlet UIButton *rechargeBtn;
/** 我的视频 */
@property (weak, nonatomic) IBOutlet BottomButton *welfareBtn;
/** 客房服务 */
@property (weak, nonatomic) IBOutlet BottomButton *guestBtn;
/** 商品收藏 */
@property (weak, nonatomic) IBOutlet BottomButton *goodsBtn;
/** 店铺收藏 */
@property (weak, nonatomic) IBOutlet BottomButton *storeBtn;
/** 客服反馈 */
@property (weak, nonatomic) IBOutlet BottomButton *suggestBtn;
/** 浏览记录 */
@property (weak, nonatomic) IBOutlet BottomButton *browseBtn;
/** 账户设置 */
@property (weak, nonatomic) IBOutlet BottomButton *settingUpBtn;

+ (instancetype)contentViewToLoad;
@property(nonatomic, strong) UserInfoItem *item;

@end
