//
//  SettingUpView.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/5.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "SettingUpView.h"
#import "RightBaseButton.h"

@interface SettingUpView()

@end

@implementation SettingUpView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self == [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    UIView *oneView = [[UIView alloc] init];
    oneView.frame = CGRectMake(0, 0, kScreenW, 100);
    oneView.backgroundColor = [UIColor whiteColor];
    [self addSubview:oneView];
    
    UIView *twoView = [[UIView alloc] init];
    twoView.frame = CGRectMake(0, oneView.bd_bottom+10, kScreenW, 100);
    twoView.backgroundColor = [UIColor whiteColor];
    [self addSubview:twoView];
    
    UIView *threeView = [[UIView alloc] init];
    threeView.frame = CGRectMake(0, twoView.bd_bottom+10, kScreenW, 50);
    threeView.backgroundColor = [UIColor whiteColor];
    [self addSubview:threeView];
    
    UIButton *accountD = [self setupRightarrowBtnWithAddView:oneView Y:0 title:@""];
    [accountD setImage:[UIImage imageNamed:@"icon_rightarrow"] forState:UIControlStateNormal];
    UIButton *spreadD = [self setupRightarrowBtnWithAddView:oneView Y:50 title:@""];
    [spreadD setImage:[UIImage imageNamed:@"icon_rightarrow"] forState:UIControlStateNormal];
    UIButton *pushD = [self setupRightarrowBtnWithAddView:twoView Y:0 title:@"推送 "];
    [pushD setImage:[UIImage imageNamed:@"icon_rightarrow"] forState:UIControlStateNormal];
    UIButton *cacheD = [self setupRightarrowBtnWithAddView:twoView Y:50 title:@""];
    [cacheD setTitle:@"0M" forState:UIControlStateNormal];
    UIButton *editionD = [self setupRightarrowBtnWithAddView:threeView Y:0 title:@""];
    // 获取当前版本
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [editionD setTitle:[NSString stringWithFormat:@"当前版本%@",version] forState:UIControlStateNormal];
    self.cacheD = cacheD;
    
    _accountTBtn = [self setupTitleLabeWithSuperView:oneView Text:@"账户管理" Y:0];
    _spreadTBtn = [self setupTitleLabeWithSuperView:oneView Text:@"我的推广" Y:50];
    _pushTBtn = [self setupTitleLabeWithSuperView:twoView Text:@"推送消息设置" Y:0];
    _cacheTBtn = [self setupTitleLabeWithSuperView:twoView Text:@"清除本地缓存" Y:50];
    _editionTBtn = [self setupTitleLabeWithSuperView:threeView Text:@"关于至爱" Y:0];
    [self setupLineView:oneView Y:50];
    [self setupLineView:twoView Y:50];
}

- (UIButton *)setupTitleLabeWithSuperView:(UIView *)superView Text:(NSString *)text Y:(CGFloat)y
{
    UIButton *btn = [[UIButton alloc] init];
    btn.frame = CGRectMake(15, y, kScreenW-50, 50);
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [btn setTitle:text forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [superView addSubview:btn];
    return btn;
}

- (UIButton *)setupRightarrowBtnWithAddView:(UIView *)view Y:(CGFloat)y title:(NSString *)title
{
    UIButton *rigBtn = [[UIButton alloc] init];
    rigBtn.frame = CGRectMake(kScreenW-120, y, 100, 50);
    rigBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [rigBtn setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
    rigBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [view addSubview:rigBtn];
    
    UILabel *titleLb = [[UILabel alloc] init];
    titleLb.frame = CGRectMake(kScreenW-120, y, 90, 50);
    titleLb.font = [UIFont systemFontOfSize:15];
    titleLb.textAlignment = NSTextAlignmentRight;
    titleLb.textColor = [UIColor colorWithHexString:@"#333333"];
    titleLb.text = title;
    [view addSubview:titleLb];
    return rigBtn;
}

- (void)setupLineView:(UIView *)addView Y:(CGFloat)y
{
    UIView *line = [[UIView alloc] init];
    line.frame = CGRectMake(15, y, kScreenW- 15, 0.5);
    line.backgroundColor = [UIColor colorWithHexString:@"#e1e1e1"];
    [addView addSubview:line];
}








@end
