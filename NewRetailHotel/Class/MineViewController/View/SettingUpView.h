//
//  SettingUpView.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/5.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingUpView : UIView
/** 账户管理 */
@property (nonatomic, strong) UIButton *accountTBtn;
/** 我的推广 */
@property (nonatomic, strong) UIButton *spreadTBtn;
/** 推送消息设置 */
@property (nonatomic, strong) UIButton *pushTBtn;
/** 清除本地缓存 */
@property (nonatomic, strong) UIButton *cacheTBtn;
/** 关于至爱 */
@property (nonatomic, strong) UIButton *editionTBtn;
@property (nonatomic, strong) UIButton *cacheD;

@end
