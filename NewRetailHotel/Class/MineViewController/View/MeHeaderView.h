//
//  MeHeaderView.h
//  BelovedHotel
//
//  Created by 王保栋 on 2017/6/5.
//  Copyright © 2017年 BeloverdSpace. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserInfoItem;
@interface MeHeaderView : UIView
/** 登录 */
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
/** 签到 */
@property (weak, nonatomic) IBOutlet UIButton *signBtn;
@property(nonatomic, strong) UserInfoItem *item;

+ (instancetype)headerLoggedInView;
+ (instancetype)headerNotLoggedInView;

@end
