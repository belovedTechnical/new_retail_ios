//
//  MeContentView.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/4.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "MeContentView.h"
#import "UserInfoItem.h"

@interface MeContentView()
/** 我的订单View */
@property (weak, nonatomic) IBOutlet UIView *OrderView;
/** 我的账户View */
@property (weak, nonatomic) IBOutlet UIView *AccountView;
/** 功能设置View*/
@property (weak, nonatomic) IBOutlet UIView *UtilityView;

@end

@implementation MeContentView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}

+ (instancetype)contentViewToLoad
{
    return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] firstObject];
}

- (void)initUI
{
    self.backgroundColor = [UIColor colorWithHexString:@"#f6f6f6"];
    
    [self.welfareBtn setTitle:@"我的公益" forState:UIControlStateNormal];
    [self.welfareBtn setImage:[UIImage imageOriginalWhitImageName:@"iconbottom_welfare"] forState:UIControlStateNormal];
    
    [self.guestBtn setTitle:@"客房服务" forState:UIControlStateNormal];
    [self.guestBtn setImage:[UIImage imageOriginalWhitImageName:@"myservice_icon"] forState:UIControlStateNormal];
    
    [self.goodsBtn setTitle:@"商品收藏" forState:UIControlStateNormal];
    [self.goodsBtn setImage:[UIImage imageOriginalWhitImageName:@"icon_concerngoods"] forState:UIControlStateNormal];
    
    [self.storeBtn setTitle:@"店铺收藏" forState:UIControlStateNormal];
    [self.storeBtn setImage:[UIImage imageOriginalWhitImageName:@"icon_concernshops"] forState:UIControlStateNormal];
    
    [self.suggestBtn setTitle:@"客服/反馈" forState:UIControlStateNormal];
    [self.suggestBtn setImage:[UIImage imageOriginalWhitImageName:@"icon_customerservice"] forState:UIControlStateNormal];
    
    [self.browseBtn setTitle:@"浏览记录" forState:UIControlStateNormal];
    [self.browseBtn setImage:[UIImage imageOriginalWhitImageName:@"icon_browsingrecord"] forState:UIControlStateNormal];
    
    [self.settingUpBtn setTitle:@"账户设置" forState:UIControlStateNormal];
    [self.settingUpBtn setImage:[UIImage imageOriginalWhitImageName:@"account_manage"] forState:UIControlStateNormal];
    
    [self setupLineView:self.OrderView Y:50.05];
    [self setupLineView:self.OrderView Y:100];
    [self setupLineView:self.AccountView Y:50];
}

- (void)setupLineView:(UIView *)addView Y:(CGFloat)y
{
    UIView *line = [[UIView alloc] init];
    line.frame = CGRectMake(15, y, kScreenW- 15, 0.5);
    line.backgroundColor = [UIColor colorWithHexString:@"#e1e1e1"];
    [addView addSubview:line];
}

- (void)setItem:(UserInfoItem *)item
{   _item = item;
    [self.integralBtn setTitle:item.availablePoints forState:UIControlStateNormal];
    [self.couponBtn setTitle:[NSString stringWithFormat:@"%@张",item.couponCount] forState:UIControlStateNormal];
    [self.balanceBtn setTitle:item.availableBalance forState:UIControlStateNormal];
}



@end
