//
//  SeeReplaceStatusCell.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SeeReplaceModel;
@interface SeeReplaceStatusCell : UITableViewHeaderFooterView
@property (nonatomic, strong) UIButton *btnFillout;
@property (nonatomic, strong) SeeReplaceModel *seeReplaceModel;
@end
