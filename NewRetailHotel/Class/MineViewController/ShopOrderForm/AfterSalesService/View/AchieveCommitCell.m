//
//  AchieveCommitCell.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "AchieveCommitCell.h"
#import "UIImageView+WebCache.h"
#import "ShopSortModel.h"

@interface AchieveCommitCell()
@property (nonatomic, strong) UILabel *lbShopName;
@property (nonatomic, strong) UILabel *lbPersonNumber;
@property (nonatomic, strong) UILabel *lbShopMoney;
@property (nonatomic, strong) UIImageView *imgIcon;
@end

@implementation AchieveCommitCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self initUI];
    }
    return self;
}

- (void)setShopModel:(ShopSortModel *)shopModel {
    _shopModel = shopModel;
    self.lbShopName.text = shopModel.productName;
    self.lbShopMoney.text = shopModel.price;
    self.lbPersonNumber.text = [NSString stringWithFormat:@"%@人已买过", shopModel.salesVolume];
    [self.imgIcon sd_setImageWithURL:[NSURL URLWithString:shopModel.productImg]];
}

- (void)initUI {
    UIImageView *imgIcon = [[UIImageView alloc] init];
    imgIcon.frame = CGRectMake(0, 0, self.bd_width, self.bd_width);
    [self addSubview:imgIcon];
    self.imgIcon = imgIcon;
    
    CGFloat margin = 10;
    UILabel *lbShopName = [[UILabel alloc] init];
    lbShopName.frame = CGRectMake(margin, imgIcon.bd_bottom, self.bd_width-margin*2, 40);
    lbShopName.textColor = [UIColor colorWithHexString:@"#222222"];
    lbShopName.textAlignment = NSTextAlignmentCenter;
    lbShopName.font = [UIFont systemFontOfSize:15];
//    lbShopName.text = @"苹果Apple iPhone7手机黑色移动电信版";
    lbShopName.numberOfLines = 0;
    [self addSubview:lbShopName];
    self.lbShopName = lbShopName;
    
    UILabel *lbShopMoney = [[UILabel alloc] init];
    lbShopMoney.frame = CGRectMake(margin, lbShopName.bd_bottom, self.bd_width/2-margin*2, 20);
    lbShopMoney.textColor = [UIColor colorWithHexString:@"#b4272d"];
    lbShopMoney.textAlignment = NSTextAlignmentLeft;
    lbShopMoney.font = [UIFont systemFontOfSize:12];
//    lbShopMoney.text = @"¥5688.00";
    [self addSubview:lbShopMoney];
    self.lbShopMoney = lbShopMoney;
    
    UILabel *lbPersonNumber = [[UILabel alloc] init];
    lbPersonNumber.frame = CGRectMake(lbShopMoney.bd_right, lbShopMoney.bd_y, self.bd_width-margin-lbShopMoney.bd_width, lbShopMoney.bd_height);
    lbPersonNumber.textColor = [UIColor colorWithHexString:@"#999999"];
    lbPersonNumber.textAlignment = NSTextAlignmentCenter;
    lbPersonNumber.font = [UIFont systemFontOfSize:12];
//    lbPersonNumber.text = @"500人买过";
    [self addSubview:lbPersonNumber];
    self.lbPersonNumber = lbPersonNumber;
}

@end
