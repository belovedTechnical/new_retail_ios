//
//  ReturnDetailsFooterView.m
//  BelovedHotel
//
//  Created by BDSir on 2018/4/3.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "ReturnDetailsFooterView.h"
#import "UIView+RGSize.h"

@interface ReturnDetailsFooterView()
@property (nonatomic, strong) UILabel *lbCause;
@property (nonatomic, strong) UILabel *lbExplain;
@property (nonatomic, strong) UILabel *lbProof;
@property (nonatomic, strong) UILabel *lbSum;
@property (nonatomic, strong) UILabel *lbSumDeclare;
@property (nonatomic, strong) UIView *estimateSumView;
@property (nonatomic, strong) UIView *fdLine;
@property (nonatomic, strong) UIButton *btnDownIcon;
@end

static CGFloat titleH = 25;
@implementation ReturnDetailsFooterView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self initUI];
    }
    return self;
}

- (void)setTitleName:(NSString *)titleName {
    _titleName = titleName;
    _lbCause.text = [NSString stringWithFormat:@"%@原因", titleName];
    _lbExplain.text = [NSString stringWithFormat:@"%@说明", titleName];

    if ([titleName isEqualToString:@"换货"]) {
        self.estimateSumView.hidden = YES;
        self.lbExplain.bd_y = _lbCause.bottom+10;
        self.lbProof.bd_y = _lbExplain.bottom+10;
        self.fdExplain.bd_y = self.lbExplain.bd_y;
        self.btnSelectIconOne.bd_y = _lbProof.bottom+10;
        self.btnSelectIconTwo.bd_y = _lbProof.bottom+10;
        self.btnSelectIconThree.bd_y = _lbProof.bottom+10;
        self.fdLine.bd_y = _lbExplain.bottom;
    }else {
        _lbCause.size = CGSizeMake([self textLabelWidthWithLabel:_lbCause], titleH);
        _lbExplain.size = CGSizeMake([self textLabelWidthWithLabel:_lbExplain], titleH);
        self.btnSelectCause.bd_x = _lbCause.right+10;
        self.fdExplain.bd_x = _lbExplain.right+10;
        self.fdLine.bd_x = self.fdExplain.bd_x;
    }
}

- (void)setProductPrice:(NSString *)productPrice {
    _productPrice = productPrice;
    self.lbSum.text = [NSString stringWithFormat:@"预计退款金额:¥%@", productPrice];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.btnDownIcon.frame = CGRectMake(_btnSelectCause.bd_right-15, _btnSelectCause.top+0.5, 15, _btnSelectCause.height-1);
}

- (void)initUI {
    
    UIImageView *imgBackView = [[UIImageView alloc] init];
    imgBackView.frame = CGRectMake(15, 20, self.width-30, self.height-40);
    imgBackView.image = [UIImage imageNamed:@"rectangularbox"];
    [self addSubview:imgBackView];
    
    CGFloat estimateSumH = 30;
    UILabel *lbCause = [self titleLabelWithY:55 text:@"换货原因"];
    UILabel *lbExplain = [self titleLabelWithY:lbCause.bottom+estimateSumH+5 text:@"换货说明"];
    UILabel *lbProof = [self titleLabelWithY:lbExplain.bottom+10 text:@"上传凭证"];
    self.lbCause = lbCause;
    self.lbExplain = lbExplain;
    self.lbProof = lbProof;
    
    UIView *estimateSumView = [[UIView alloc] init];
    estimateSumView.frame = CGRectMake(50, lbCause.bottom+10, 200, estimateSumH);
    [self addSubview:estimateSumView];
    self.estimateSumView = estimateSumView;
    
    UILabel *lbSum = [[UILabel alloc] init];
    lbSum.frame = CGRectMake(0, 0, estimateSumView.width, estimateSumView.height/2);
    lbSum.textColor = [UIColor colorWithHexString:@"#666666"];
    lbSum.textAlignment = NSTextAlignmentLeft;
    lbSum.font = [UIFont systemFontOfSize:14];
//    lbSum.text = @"预计退款金额:¥1000.00";
    [estimateSumView addSubview:lbSum];
    self.lbSum = lbSum;
    
//    UILabel *lbSumDeclare = [[UILabel alloc] init];
//    lbSumDeclare.frame = CGRectMake(0, lbSum.bottom, estimateSumView.width, estimateSumView.height/2);
//    lbSumDeclare.text = @"最多退¥1000.00,含发货邮费¥10.00";
//    lbSumDeclare.textColor = [UIColor colorWithHexString:@"#b4272d"];
//    lbSumDeclare.textAlignment = NSTextAlignmentLeft;
//    lbSumDeclare.font = [UIFont systemFontOfSize:12];
//    [estimateSumView addSubview:lbSumDeclare];
//    self.lbSumDeclare = lbSumDeclare;
    
    UIButton *btnSelectCause = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectCause.frame = CGRectMake(lbCause.right+10, lbCause.top, 150, lbCause.height);
    [btnSelectCause setTitle:@"请选择退款原因" forState:UIControlStateNormal];
    [btnSelectCause setTitleColor:[UIColor colorWithHexString:@"#666666"] forState:UIControlStateNormal];
    [btnSelectCause addTarget:self action:@selector(btnSelectCauseClick:) forControlEvents:UIControlEventTouchUpInside];
    btnSelectCause.layer.borderColor = [UIColor colorWithHexString:@"#666666"].CGColor;
    btnSelectCause.titleLabel.font = [UIFont systemFontOfSize:12];
    btnSelectCause.layer.borderWidth = 0.5;
    [self addSubview:btnSelectCause];
    self.btnSelectCause = btnSelectCause;
    
    UIView *fdLine = [[UIView alloc] init];
    fdLine.frame = CGRectMake(lbCause.right+10, lbExplain.bottom, 150, 1);
    fdLine.backgroundColor = [UIColor colorWithHexString:@"666666"];
    [self addSubview:fdLine];
    self.fdLine = fdLine;
    
    UIButton *btnDownIcon = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDownIcon setImage:[UIImage imageNamed:@"drop down"] forState:UIControlStateNormal];
    btnDownIcon.backgroundColor = [UIColor colorWithHexString:@"#cccccc"];
    [self addSubview:btnDownIcon];
    self.btnDownIcon = btnDownIcon;
    
    UITextField *fdExplain = [[UITextField alloc] initWithFrame:CGRectMake(lbExplain.right+10, lbExplain.top, btnSelectCause.width, lbExplain.height)];
    [self addSubview:fdExplain];
    self.fdExplain = fdExplain;
    
    CGFloat selectIconX = 50;
    CGFloat selectIconY = lbProof.bottom+10;
    UIButton *btnSelectIconOne = [self buttonSelectIconWithX:selectIconX y:selectIconY];
    UIButton *btnSelectIconTwo = [self buttonSelectIconWithX:btnSelectIconOne.right+10 y:selectIconY];
    UIButton *btnSelectIconThree = [self buttonSelectIconWithX:btnSelectIconTwo.right+10 y:selectIconY];
    btnSelectIconOne.tag = 1;
    btnSelectIconTwo.tag = 2;
    btnSelectIconThree.tag = 3;
    self.btnSelectIconOne = btnSelectIconOne;
    self.btnSelectIconTwo = btnSelectIconTwo;
    self.btnSelectIconThree = btnSelectIconThree;
    
    UIButton *btnSubmit = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSubmit.frame = CGRectMake(imgBackView.center.x-35, imgBackView.bottom-50, 71, 29);
    [btnSubmit setTitleColor:[UIColor colorWithHexString:@"#ffffff"] forState:UIControlStateNormal];
    [btnSubmit setTitle:@"提交申请" forState:UIControlStateNormal];
    btnSubmit.titleLabel.font = [UIFont systemFontOfSize:14];
    btnSubmit.backgroundColor = [UIColor redColor];
    btnSubmit.layer.cornerRadius = 1.5;
    btnSubmit.layer.masksToBounds = YES;
    [self addSubview:btnSubmit];
    self.btnSubmit = btnSubmit;
}

- (UILabel *)titleLabelWithY:(CGFloat)y text:(NSString *)text {
    UILabel *lb = [[UILabel alloc] init];
    lb.text = text;
    lb.font = [UIFont systemFontOfSize:14];
    CGFloat lbWidth = [self textLabelWidthWithLabel:lb];
    lb.frame = CGRectMake(50, y, lbWidth, titleH);
    lb.textColor = [UIColor colorWithHexString:@"#666666"];
    lb.textAlignment = NSTextAlignmentLeft;
    [self addSubview:lb];
    return lb;
}

- (UIButton *)buttonSelectIconWithX:(CGFloat)x y:(CGFloat)y {
    UIButton *btnSelectIcon = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectIcon.frame = CGRectMake(x, y, 60, 60);
//    btnSelectIcon.transform = CGAffineTransformMakeScale(.5, .5);
    [btnSelectIcon setBackgroundImage:[UIImage imageNamed:@"add_icon"] forState:UIControlStateNormal];
//    btnSelectIcon.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:btnSelectIcon];
    return btnSelectIcon;
}

- (void)btnSelectCauseClick:(UIButton *)sender {
    if (self.reasonSelect) {
        self.reasonSelect(sender);
    }
}

- (CGFloat)textLabelWidthWithLabel:(UILabel *)textLabel {
    CGFloat width = [textLabel.text boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:textLabel.font} context:nil].size.width;
    //限制最小的文字框大小
//    if (width < 90) {
//        width = 90;
//    }
    return width;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self endEditing:YES];
}

@end
