
//
//  PopupSelectedView.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/13.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "PopupSelectedView.h"
#import "ReasonSelectModel.h"

@interface PopupSelectedView()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *listArray;
@end
@implementation PopupSelectedView

- (instancetype)initWithFrame:(CGRect)frame ListArray:(NSArray *)array {
    if (self = [super initWithFrame:frame]) {
        self.listArray = array;
        [self initUI];
    }
    return self;
}

- (void)initUI {
    UITableView *tableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.tableFooterView = [[UIView alloc] init];
    [self addSubview:tableView];
}

#pragma mark -<UITableViewDataSource, UITableViewDelegate>
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _listArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"popupCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"popupCell"];
    }
    cell.backgroundColor = [UIColor groupTableViewBackgroundColor];
    ReasonSelectModel *reasonModel = _listArray[indexPath.row];
    cell.textLabel.text = reasonModel.asrComment==nil? reasonModel.exName:reasonModel.asrComment;
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 36;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ReasonSelectModel *reasonModel = _listArray[indexPath.row];
    if (self.cellSelectBlcok) {
        NSString *code = @"";
        NSString *text = @"";
        if (reasonModel.asrId == nil) {
            code = reasonModel.exCode;
            text = reasonModel.exName;
        }else {
            code = reasonModel.asrId;
            text = reasonModel.asrComment;
        }
        self.cellSelectBlcok(code, text);
    }
}

@end
