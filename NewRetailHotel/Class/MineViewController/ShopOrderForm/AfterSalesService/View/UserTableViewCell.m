//
//  UserTableViewCell.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "UserTableViewCell.h"
#import "ReplaceUserModel.h"
#import "UIImageView+WebCache.h"

@interface UserTableViewCell()
@property (nonatomic, strong) UIView *partitionView;
@property (nonatomic, assign) CGFloat lbHeight;
@end

@implementation UserTableViewCell

- (void)setFrame:(CGRect)frame {
    frame.size.height -= 20;    // 减掉的值就是分隔线的高度
    [super setFrame:frame];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
    }
    return self;
}

- (void)setReplaceModel:(ReplaceUserModel *)replaceModel {//isActionType
    _replaceModel = replaceModel;
    NSArray *titleArray = [self titleSelectWithModel:replaceModel];
    if (titleArray.count > 0 ) {
        [self TextLabelWithTitle:titleArray];
    }
    if (replaceModel.imgList.count > 0) {
        [self imageViewWithImageArray:replaceModel.imgList];
    }
    if ([replaceModel.isActionType isEqualToString:@"userApply"]) {
        [self buttonEdit];
    }
}

- (void)buttonEdit {
    UIButton *btnEdit = [UIButton buttonWithType:UIButtonTypeCustom];
    btnEdit.frame = CGRectMake(self.bd_width-50-10, 5, 50, 20);
    [btnEdit setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
    btnEdit.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
    [btnEdit setTitle:@"编辑" forState:UIControlStateNormal];
    btnEdit.titleLabel.font = [UIFont systemFontOfSize:14];
    btnEdit.layer.borderWidth = 1;
    btnEdit.hidden = YES;
    [self addSubview:btnEdit];
    self.btnEdit = btnEdit;
}

- (void)TextLabelWithTitle:(NSArray *)titleArray {
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    CGFloat lbH = 30;
    for (int i=0; i < titleArray.count; i++) {
        UILabel *lb = [[UILabel alloc] init];
        lb.text = titleArray[i];
        lb.font = [UIFont systemFontOfSize:14];
        lb.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        lb.textAlignment = NSTextAlignmentLeft;
        lb.textColor = [UIColor colorWithHexString:@"#666666"];
        lb.frame = CGRectMake(35, i*lbH, self.bd_width-35-10, lbH);
        self.lbHeight = i*lbH;
        [self addSubview:lb];
    }
}

- (void)imageViewWithImageArray:(NSArray *)imageArray {
    CGFloat row = 3;
    CGFloat margin = 30;
    CGFloat marginX = 35;
    CGFloat imgWH = ((kScreenW-marginX)-margin*row)/row;
    for (int i = 0; i<imageArray.count; i++) {
        UIImageView *imageView = [[UIImageView alloc] init];
        CGFloat imgX = i*(imgWH+margin)+marginX;
        imageView.frame = CGRectMake(imgX, _lbHeight+30, imgWH, imgWH);
        [imageView sd_setImageWithURL:[NSURL URLWithString:imageArray[i]]];
        [self addSubview:imageView];
    }
}

/*
 <isActionType操作类型
 【userApply:用户申请;
 userEditor:用户修改;
 merchantReply:商家回复;
 userExpress:用户物流;
 merchantReplyRefund:商家退款
 */
- (NSArray *)titleSelectWithModel:(ReplaceUserModel *)model {
    NSArray *titleArray = [NSArray array];
    if ([model.isActionType isEqualToString:@"userApply"]) {
        titleArray = @[
                       [NSString stringWithFormat:@"用户: %@", model.asoUserName],
                       [NSString stringWithFormat:@"提交时间: %@", model.actionTime],
//                       [NSString stringWithFormat:@"货物状态: %@", model.asrIdStr],
                       [NSString stringWithFormat:@"%@原因: %@", model.afterSidStr,model.asrIdStr],
                       [NSString stringWithFormat:@"%@说明: %@", model.afterSidStr,model.asExplain],
                       [NSString stringWithFormat:@"上传凭证"],
                       ];
    }else if ([model.isActionType isEqualToString:@"merchantReply"]) {
        titleArray = @[
                       [NSString stringWithFormat:@"商家"],
                       [NSString stringWithFormat:@"处理时间: %@", model.actionTime],
                       [NSString stringWithFormat:@"处理内容: %@", model.asoMhResStr],
                       [NSString stringWithFormat:@"退货地址: %@", model.detailAddress],
                       [NSString stringWithFormat:@"联系人: %@", model.contactMerchantName],
                       [NSString stringWithFormat:@"电话 %@", model.contactNumber],
                       ];
    }else if ([model.isActionType isEqualToString:@"userExpress"]) {
        titleArray = @[
                       [NSString stringWithFormat:@"用户: %@", model.asoUserName],
                       [NSString stringWithFormat:@"提交时间: %@", model.actionTime],
                       [NSString stringWithFormat:@"物流信息"],
//                       [NSString stringWithFormat:@"换货原因: %@", model.asExplain],
                       [NSString stringWithFormat:@"%@物流公司: %@", model.afterSidStr,model.comName],
                       [NSString stringWithFormat:@"%@物流单号: %@", model.afterSidStr,model.comNumber],
                       ];
    }else if ([model.isActionType isEqualToString:@"merchantReplyRefund"]) {
        titleArray = @[
                       [NSString stringWithFormat:@"商家"],
                       [NSString stringWithFormat:@"处理时间: %@", model.actionTime],
                       [NSString stringWithFormat:@"处理内容: %@", model.asrIdStr],
                       ];
    }
    return titleArray;
}

@end
