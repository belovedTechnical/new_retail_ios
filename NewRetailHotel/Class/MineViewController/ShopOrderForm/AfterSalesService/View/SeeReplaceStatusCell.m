//
//  SeeReplaceStatusCell.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "SeeReplaceStatusCell.h"
#import "SeeReplaceModel.h"

@interface SeeReplaceStatusCell()
@property (nonatomic, strong) UILabel *lbStatus;
@end
@implementation SeeReplaceStatusCell

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}

- (void)setSeeReplaceModel:(SeeReplaceModel *)seeReplaceModel {
    _seeReplaceModel = seeReplaceModel;
    /** 售后状态【1:用户售后申请中;2:商家同意;3:商家拒绝;4:确认完成;】 */
    switch ([seeReplaceModel.asoStatus integerValue]) {
        case 1:
            self.lbStatus.text = @"用户售后申请中";
            break;
        case 2:
            self.lbStatus.text = @"商家已同意换货，请填写物流信息";
            self.btnFillout.hidden = NO;
            break;
        case 3:
            self.lbStatus.text = @"商家拒绝";
            break;
        case 4:
            self.lbStatus.text = @"确认完成";
            break;
        default:
            break;
    }
}

- (void)initUI {
    UILabel *lbStatus = [[UILabel alloc] init];
    lbStatus.textColor = [UIColor colorWithHexString:@"#b4267b"];
    lbStatus.textAlignment = NSTextAlignmentLeft;
    lbStatus.font = [UIFont systemFontOfSize:15];
//    lbStatus.text = @"商家已同意换货，请填写物流信息";
    [self addSubview:lbStatus];
    self.lbStatus = lbStatus;
    
    UIButton *btnFillout = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnFillout setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
    btnFillout.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
    [btnFillout setTitle:@"去填写" forState:UIControlStateNormal];
    btnFillout.titleLabel.font = [UIFont systemFontOfSize:14];
    btnFillout.layer.borderWidth = 1;
    btnFillout.hidden = YES;
    [self addSubview:btnFillout];
    self.btnFillout = btnFillout;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.lbStatus.frame = CGRectMake(10, 0, self.bd_width-70, self.bd_height);
    CGFloat filloutW = 50;
    CGFloat filloutH = 20;
    self.btnFillout.frame = CGRectMake(self.bd_width-filloutW-10, self.bd_centerY-filloutH/2, filloutW, filloutH);
}

@end
