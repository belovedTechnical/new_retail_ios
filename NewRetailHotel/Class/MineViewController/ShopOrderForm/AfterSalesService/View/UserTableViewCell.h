//
//  UserTableViewCell.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReplaceUserModel;
@interface UserTableViewCell : UITableViewCell
@property (nonatomic, strong) UIButton *btnEdit;
@property (nonatomic, strong) ReplaceUserModel *replaceModel;
@end
