//
//  SeeReplaceRecord.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "SeeReplaceRecord.h"

@interface SeeReplaceRecord()
@property (nonatomic, strong) UIButton *btnRecord;
@end
@implementation SeeReplaceRecord

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self initUI];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.btnRecord.frame = CGRectMake(10, 0, self.bd_width-10, self.bd_height);
}

- (void)initUI {
    UIButton *btnRecord = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRecord setTitleColor:[UIColor colorWithHexString:@"#b4267b"] forState:UIControlStateNormal];;
    [btnRecord setImage:[UIImage imageNamed:@"aftersale"] forState:UIControlStateNormal];
    [btnRecord setTitle:@" 售后记录" forState:UIControlStateNormal];
    btnRecord.titleLabel.font = [UIFont systemFontOfSize:15];
    btnRecord.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btnRecord.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
    btnRecord.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    btnRecord.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [self addSubview:btnRecord];
    self.btnRecord = btnRecord;
}

@end
