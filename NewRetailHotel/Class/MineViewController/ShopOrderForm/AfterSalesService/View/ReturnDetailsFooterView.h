//
//  ReturnDetailsFooterView.h
//  BelovedHotel
//
//  Created by BDSir on 2018/4/3.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^reasonSelectedBlcok)(UIButton *sender);
@interface ReturnDetailsFooterView : UIView
@property (nonatomic, strong) NSString *titleName;
@property (nonatomic, strong) NSString *productPrice;
@property (nonatomic, strong) UIButton *btnSubmit;
@property (nonatomic, strong) UITextField *fdExplain;
@property (nonatomic, strong) UIButton *btnSelectIconOne;
@property (nonatomic, strong) UIButton *btnSelectIconTwo;
@property (nonatomic, strong) UIButton *btnSelectIconThree;
@property (nonatomic, strong) UIButton *btnSelectCause;

@property (nonatomic, strong) reasonSelectedBlcok reasonSelect;
@end
