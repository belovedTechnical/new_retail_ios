//
//  AchieveCommitCell.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShopSortModel;
@interface AchieveCommitCell : UICollectionViewCell
@property (nonatomic, strong) ShopSortModel *shopModel;
@end
