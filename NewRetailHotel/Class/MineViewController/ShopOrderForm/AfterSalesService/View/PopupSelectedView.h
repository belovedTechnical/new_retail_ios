//
//  PopupSelectedView.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/13.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CellSelectBlcok)(NSString *asrId, NSString *text);
@interface PopupSelectedView : UIView
@property (nonatomic, strong) CellSelectBlcok cellSelectBlcok;

- (instancetype)initWithFrame:(CGRect)frame ListArray:(NSArray *)array;
@end
