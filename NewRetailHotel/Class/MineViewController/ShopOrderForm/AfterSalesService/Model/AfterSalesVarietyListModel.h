//
//  AfterSalesVarietyListModel.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/13.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AfterSalesVarietyListModel : NSObject
@property (nonatomic, strong) NSString *afterSid;
@property (nonatomic, strong) NSString *afterSName;
@property (nonatomic, strong) NSString *afterSIcoUrl;
@property (nonatomic, strong) NSString *afterSComment;
@property (nonatomic, strong) NSString *afterSort;

+ (instancetype)afterSalesVarietyListModelWithDict:(NSDictionary *)dict;
@end
