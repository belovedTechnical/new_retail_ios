//
//  SeeReplaceModel.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ReplaceUserModel;
@interface SeeReplaceModel : NSObject
/** 售后ID */
@property (nonatomic, strong) NSString *asoId;
/** 售后状态【1:用户售后申请中;2:商家同意;3:商家拒绝;4:确认完成;】 */
@property (nonatomic, strong) NSString *asoStatus;
/** 详细规格ID */
@property (nonatomic, strong) NSString *productId;
/** 订单ID */
@property (nonatomic, strong) NSString *orderId;

/** 用户数据数组 */
@property (nonatomic, strong) NSArray *resMapList;

@property (nonatomic, strong) ReplaceUserModel *replaceUserModel;

+ (instancetype)seeReplaceModelWithDict:(NSDictionary *)dict;

@end
