//
//  ReasonSelectModel.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/13.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReasonSelectModel : NSObject
// 售后原因列表
@property (nonatomic, strong) NSString *asrId;
@property (nonatomic, strong) NSString *asrComment;

// 所有物流公司名称
@property (nonatomic, strong) NSString *exName;
@property (nonatomic, strong) NSString *exCode;

+ (instancetype)reasonSelectModelWithDict:(NSDictionary *)dict;
@end
