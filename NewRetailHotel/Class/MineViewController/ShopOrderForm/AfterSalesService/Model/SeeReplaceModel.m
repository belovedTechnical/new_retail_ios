//
//  SeeReplaceModel.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "SeeReplaceModel.h"
#import "ReplaceUserModel.h"

@implementation SeeReplaceModel

+ (instancetype)seeReplaceModelWithDict:(NSDictionary *)dict {
    SeeReplaceModel *model = [[self alloc] init];
    [model setValuesForKeysWithDictionary:dict];
    
    NSMutableArray *tempArray = [NSMutableArray array];
    for (NSDictionary *userDict in dict[@"resMapList"]) {
        ReplaceUserModel *userModel = [ReplaceUserModel replaceUSerModelWithDict:userDict];
        [tempArray addObject:userModel];
        model.replaceUserModel = userModel;
    }
    model.resMapList = tempArray;
    
    return model;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}

@end
