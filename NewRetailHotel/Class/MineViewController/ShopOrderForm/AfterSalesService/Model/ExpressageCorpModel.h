//
//  ExpressageCorpModel.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/14.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExpressageCorpModel : NSObject
@property (nonatomic, strong) NSString *exName;
@property (nonatomic, strong) NSString *exCode;

+ (instancetype)expressageCorpModelWithDict:(NSDictionary *)dict;
@end
