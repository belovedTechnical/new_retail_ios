//
//  ReasonSelectModel.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/13.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "ReasonSelectModel.h"

@implementation ReasonSelectModel

+ (instancetype)reasonSelectModelWithDict:(NSDictionary *)dict {
    ReasonSelectModel *reasonModel = [[self alloc] init];
    [reasonModel setValuesForKeysWithDictionary:dict];
    return reasonModel;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}

@end
