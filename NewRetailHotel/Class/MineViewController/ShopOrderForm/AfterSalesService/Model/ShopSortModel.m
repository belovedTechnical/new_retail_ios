//
//  ShopSortModel.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/23.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "ShopSortModel.h"

@implementation ShopSortModel

+ (instancetype)shopSortModelWithDict:(NSDictionary *)dict {
    ShopSortModel *model = [[self alloc] init];
    [model setValuesForKeysWithDictionary:dict];
    return model;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}

@end
