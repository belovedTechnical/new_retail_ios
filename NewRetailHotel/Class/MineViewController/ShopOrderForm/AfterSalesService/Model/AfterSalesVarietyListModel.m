//
//  AfterSalesVarietyListModel.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/13.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "AfterSalesVarietyListModel.h"

@implementation AfterSalesVarietyListModel

+ (instancetype)afterSalesVarietyListModelWithDict:(NSDictionary *)dict {
    AfterSalesVarietyListModel *listModel = [[self alloc] init];
    [listModel setValuesForKeysWithDictionary:dict];
    return listModel;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}

@end
