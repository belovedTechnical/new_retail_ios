//
//  ReplaceUserModel.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReplaceUserModel : NSObject
/** 用户名称 */
@property (nonatomic, strong) NSString *asoUserName;
@property (nonatomic, strong) NSString *isUser;
/** 类型 */
@property (nonatomic, strong) NSString *isActionType;
/** 提交时间 */
@property (nonatomic, strong) NSString *actionTime;
@property (nonatomic, strong) NSString *acObj;

#pragma mark -userApply
@property (nonatomic, strong) NSString *afterSid;
@property (nonatomic, strong) NSString *afterSidStr;
@property (nonatomic, strong) NSString *asrId;
@property (nonatomic, strong) NSString *asrIdStr;
@property (nonatomic, strong) NSString *asExplain;
@property (nonatomic, strong) NSArray  *imgList;

#pragma mark -merchantReply
@property (nonatomic, strong) NSString *asoMhRes;
@property (nonatomic, strong) NSString *contactNumber;
@property (nonatomic, strong) NSString *mhRefundAmount;
@property (nonatomic, strong) NSString *detailAddress;
@property (nonatomic, strong) NSString *contactMerchantName;
@property (nonatomic, strong) NSString *asoMhResStr;

#pragma mark -userExpress
@property (nonatomic, strong) NSString *comNumber;
@property (nonatomic, strong) NSString *comName;

+ (instancetype)replaceUSerModelWithDict:(NSDictionary *)dict;

@end
