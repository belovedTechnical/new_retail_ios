//
//  ShopSortModel.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/23.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShopSortModel : NSObject
/** 商品id */
@property (nonatomic, strong) NSString *productId;
/** 销售量 */
@property (nonatomic, strong) NSString *salesVolume;
/** 价格 */
@property (nonatomic, strong) NSString *price;
/** 商品图片 */
@property (nonatomic, strong) NSString *productImg;
@property (nonatomic, strong) NSString *originalCost;
/** 店铺id */
@property (nonatomic, strong) NSString *productStoreId;
/** 商品名 */
@property (nonatomic, strong) NSString *productName;
@property (nonatomic, strong) NSString *isCateTopPro;
@property (nonatomic, strong) NSString *subtitle;

+ (instancetype)shopSortModelWithDict:(NSDictionary *)dict;
@end
