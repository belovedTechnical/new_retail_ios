//
//  ReplaceUserModel.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "ReplaceUserModel.h"

@implementation ReplaceUserModel

+ (instancetype)replaceUSerModelWithDict:(NSDictionary *)dict {
    ReplaceUserModel *model = [[self alloc] init];
    [model setValuesForKeysWithDictionary:dict];
    NSString *acObjStr = [dict objectForKey:@"acObj"];
    NSRange range = NSMakeRange(0, acObjStr.length);
    NSString *accobj = [acObjStr substringWithRange:range];
    NSData *JSONData = [accobj dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *accObjDict = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableLeaves error:nil];
//    BDLog(@"%@", accObjDict);
    [model setValuesForKeysWithDictionary:accObjDict];

    return model;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}

@end
