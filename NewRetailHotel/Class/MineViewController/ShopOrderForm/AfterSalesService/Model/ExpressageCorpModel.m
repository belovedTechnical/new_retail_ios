//
//  ExpressageCorpModel.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/14.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "ExpressageCorpModel.h"

@implementation ExpressageCorpModel

+ (instancetype)expressageCorpModelWithDict:(NSDictionary *)dict {
    ExpressageCorpModel *corpModel = [[self alloc] init];
    [corpModel setValuesForKeysWithDictionary:dict];
    return corpModel;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}

@end
