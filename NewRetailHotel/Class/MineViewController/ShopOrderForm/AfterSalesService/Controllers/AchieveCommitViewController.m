//
//  AchieveCommitViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "AchieveCommitViewController.h"
#import "WebShopDetailsViewController.h"
#import "SeeReplaceViewController.h"
#import "AchieveCommitCell.h"
#import "ShopSortModel.h"

@interface AchieveCommitViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) NSArray *dataListArray;
@property (nonatomic, strong) UICollectionView *collectionView;
@end

static NSString * const ID = @"achieveCell";
static CGFloat margin = 5;
static NSInteger cols = 2;
#define itemW ([UIScreen mainScreen].bounds.size.width - (cols - 1) * margin) / cols
@implementation AchieveCommitViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addTopView];
    [self requestNetShopDetails];
}

#pragma mark -<UICollectionViewDataSource,UISearchControllerDelegate>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _dataListArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AchieveCommitCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ID forIndexPath:indexPath];
    cell.shopModel = _dataListArray[indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ShopSortModel *shopModel = _dataListArray[indexPath.row];
    NSString *urlStr = [NSString stringWithFormat:@"%@c-shopDetiles/%@",HTML_URL_Base, shopModel.productId];
    WebShopDetailsViewController *shopDetaVC = [[WebShopDetailsViewController alloc] init];
    shopDetaVC.shopDetailsURL = urlStr;
    [self.navigationController pushViewController:shopDetaVC animated:YES];
}

- (void)addTopView {
    UIView *topView = [[UIView alloc] init];
    topView.frame = CGRectMake(0, StatusBarHeight, kScreenW, 180);
    topView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:topView];
    self.topView = topView;
    
    UIButton *btnAchieve = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAchieve.frame = CGRectMake(0, 20, kScreenW, 30);
    [btnAchieve setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
    [btnAchieve setImage:[UIImage imageNamed:@"submit"] forState:UIControlStateNormal];
    [btnAchieve setTitle:@"  提交成功,等待商家处理" forState:UIControlStateNormal];
    btnAchieve.titleLabel.font = [UIFont systemFontOfSize:15];
    btnAchieve.contentMode = UIViewContentModeCenter;
    [topView addSubview:btnAchieve];
    
    CGFloat btnH = 30;
    CGFloat btnW = 100;
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(40, btnAchieve.bd_bottom+20, btnW, btnH);
    [btnBack setTitleColor:[UIColor colorWithHexString:@"#99ccff"] forState:UIControlStateNormal];
    [btnBack setTitle:@"返回首页" forState:UIControlStateNormal];
    btnBack.titleLabel.font = [UIFont systemFontOfSize:15];
    btnBack.titleLabel.textAlignment = NSTextAlignmentLeft;
    [btnBack addTarget:self action:@selector(btnGotoBackClick) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:btnBack];
    
    UIButton *btnDetails = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDetails.frame = CGRectMake(kScreenW-btnW-40, btnBack.bd_y, btnW, btnH);
    [btnDetails setTitleColor:[UIColor colorWithHexString:@"#99ccff"] forState:UIControlStateNormal];
    [btnDetails setTitle:@"查看详情" forState:UIControlStateNormal];
    btnDetails.titleLabel.font = [UIFont systemFontOfSize:15];
    btnDetails.titleLabel.textAlignment = NSTextAlignmentRight;
    [btnDetails addTarget:self action:@selector(btnDetailsClick) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:btnDetails];
    
    UIView *line = [[UIView alloc] init];
    line.frame = CGRectMake(0, btnBack.bd_bottom+10, kScreenW, 1);
    line.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [topView addSubview:line];
    
    UILabel *lbTitle = [[UILabel alloc] init];
    lbTitle.frame = CGRectMake(10, line.bd_bottom+15, kScreenW, 30);
    lbTitle.textColor = [UIColor colorWithHexString:@"#666666"];
    lbTitle.font = [UIFont systemFontOfSize:15];
    lbTitle.textAlignment = NSTextAlignmentLeft;
    lbTitle.text = @"随便逛逛";
    [topView addSubview:lbTitle];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(itemW, itemW+60);
    flowLayout.minimumLineSpacing = margin;
    flowLayout.minimumInteritemSpacing = margin;
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, _topView.bd_bottom+10, kScreenW, kScreenH-_topView.bd_height-SafeAreaBottomHeight-49) collectionViewLayout:flowLayout];
    _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    [self.view addSubview:_collectionView];
    [self.collectionView registerClass:[AchieveCommitCell class] forCellWithReuseIdentifier:ID];
}

#pragma mark -内部控制方法
- (void)btnGotoBackClick {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)btnDetailsClick {
    SeeReplaceViewController *replaceVC = [[SeeReplaceViewController alloc] init];
    replaceVC.asoId  = _asoId; 
    replaceVC.orderListModelArray = _orderListModelArray;
    [self.navigationController pushViewController:replaceVC animated:YES];
}

#pragma mark -网络请求
- (void)requestNetShopDetails {
    NSDictionary *parameter = @{
                                @"productCategoryTypeEnumId":@"ToResearchProduct",
                                @"sort":@"",
                                @"productCategoryId":@"",
                                @"pageIndex":@"",
                                @"pageSize":@""
                                };
    [[BDNetworkTools sharedInstance] postShopDetailsSortWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        NSArray *listArray = [responseObject objectForKey:@"productList"];
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *dict in listArray) {
            [tempArray addObject:[ShopSortModel shopSortModelWithDict:dict]];
        }
        
        self.dataListArray = tempArray;
        [self.collectionView reloadData];
    }];
}

@end
