//
//  ReturnExpressageViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/14.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "ReturnExpressageViewController.h"
#import "ReasonSelectModel.h"
#import "PopupSelectedView.h"
#import "UIView+RGSize.h"

@interface ReturnExpressageViewController ()
@property (nonatomic, strong)UILabel *lbExpressage;
@property (nonatomic, strong)UILabel *lbExpressageNumber;
@property (nonatomic, strong) UIButton *btnSelectExpressage;
@property (nonatomic, strong) UIButton *btnSubmit;
@property (nonatomic, strong) UIImageView *imgBackView;
@property (nonatomic, strong) UITextField *fdExplain;
@property (nonatomic, strong) NSArray *CorpArray;
@property (nonatomic, strong) NSString *corpCoder;
@end

@implementation ReturnExpressageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"退换物流填写";
    self.view.backgroundColor = [UIColor whiteColor];
    [self getExpressageCorpList];
    [self initUI];
}

- (void)initUI {
    UILabel *lbTitle = [[UILabel alloc] init];
    lbTitle.frame = CGRectMake(-1, 64, kScreenW+2, 45);
    lbTitle.font = [UIFont systemFontOfSize:15];
    lbTitle.textColor = [UIColor colorWithHexString:@"#666666"];
    lbTitle.textAlignment = NSTextAlignmentLeft;
    lbTitle.text = @"  退货物流信息";
    lbTitle.layer.borderWidth = 1;
    lbTitle.layer.borderColor = [UIColor colorWithHexString:@"#cccccc"].CGColor;
    [self.view addSubview:lbTitle];
    
    UIImageView *imgBackView = [[UIImageView alloc] init];
    imgBackView.frame = CGRectMake(15, lbTitle.bd_bottom+20, kScreenW-30, 300);
    imgBackView.image = [UIImage imageNamed:@"rectangularbox"];
    imgBackView.userInteractionEnabled = YES;
    [self.view addSubview:imgBackView];
    self.imgBackView = imgBackView;
    
    self.lbExpressage = [self titleLabelWithY:50 text:@"退货物流公司"];
    self.lbExpressageNumber = [self titleLabelWithY:_lbExpressage.bd_bottom+25 text:@"退货物流单号"];
    
    UIButton *btnSelectExpressage = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSelectExpressage.frame = CGRectMake(_lbExpressage.right+10, _lbExpressage.top, 150, _lbExpressage.height);
    [btnSelectExpressage setTitle:@"请选择物流公司" forState:UIControlStateNormal];
    [btnSelectExpressage setTitleColor:[UIColor colorWithHexString:@"#666666"] forState:UIControlStateNormal];
    [btnSelectExpressage addTarget:self action:@selector(btnSelectExpressageClick:) forControlEvents:UIControlEventTouchUpInside];
    btnSelectExpressage.layer.borderColor = [UIColor colorWithHexString:@"#666666"].CGColor;
    btnSelectExpressage.titleLabel.font = [UIFont systemFontOfSize:12];
    btnSelectExpressage.layer.borderWidth = 0.5;
    [imgBackView addSubview:btnSelectExpressage];
    self.btnSelectExpressage = btnSelectExpressage;
    
    UIButton *btnDownBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDownBack.frame = CGRectMake(btnSelectExpressage.right-15, btnSelectExpressage.top, 15, btnSelectExpressage.height);
    [btnDownBack setImage:[UIImage imageNamed:@"drop down"] forState:UIControlStateNormal];
    btnDownBack.backgroundColor = [UIColor colorWithHexString:@"#cccccc"];
    [imgBackView addSubview:btnDownBack];
    
    UITextField *fdExplain = [[UITextField alloc] initWithFrame:CGRectMake(_lbExpressageNumber.right+10, _lbExpressageNumber.top, btnSelectExpressage.width, _lbExpressageNumber.height)];
    fdExplain.keyboardType = UIKeyboardTypeNumberPad;
    [imgBackView addSubview:fdExplain];
    self.fdExplain = fdExplain;
    
    UIView *fdLine = [[UIView alloc] init];
    fdLine.frame = CGRectMake(_lbExpressageNumber.right+10, fdExplain.bottom, 150, 1);
    fdLine.backgroundColor = [UIColor colorWithHexString:@"666666"];
    [imgBackView addSubview:fdLine];
    
    UIButton *btnSubmit = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSubmit.frame = CGRectMake(imgBackView.center.x-35, _lbExpressageNumber.bottom+50, 71, 29);
    [btnSubmit setTitleColor:[UIColor colorWithHexString:@"#ffffff"] forState:UIControlStateNormal];
    [btnSubmit addTarget:self action:@selector(postNetExpressageInfo:) forControlEvents:UIControlEventTouchUpInside];
    [btnSubmit setTitle:@"提交" forState:UIControlStateNormal];
    btnSubmit.titleLabel.font = [UIFont systemFontOfSize:14];
    btnSubmit.backgroundColor = [UIColor redColor];
    btnSubmit.layer.cornerRadius = 13;
    btnSubmit.layer.masksToBounds = YES;
    [imgBackView addSubview:btnSubmit];
    self.btnSubmit = btnSubmit;
}

- (UILabel *)titleLabelWithY:(CGFloat)y text:(NSString *)text {
    UILabel *lb = [[UILabel alloc] init];
    lb.text = text;
    lb.font = [UIFont systemFontOfSize:14];
    lb.frame = CGRectMake(30, y, 90, 30);
    lb.textColor = [UIColor colorWithHexString:@"#666666"];
    lb.textAlignment = NSTextAlignmentLeft;
    [self.imgBackView addSubview:lb];
    return lb;
}

#pragma mark -控制方法
- (void)btnSelectExpressageClick:(UIButton *)sender {
    CGFloat popViewH = _CorpArray.count*50 > (kScreenH-80) ? kScreenH-80 :_CorpArray.count*50;
    PopupSelectedView *popView = [[PopupSelectedView alloc] initWithFrame:CGRectMake(sender.bd_x, _btnSelectExpressage.bd_bottom, sender.bd_width, popViewH) ListArray:_CorpArray];
    [self.view addSubview:popView];
    typeof(popView) weakPopView = popView;
    popView.cellSelectBlcok = ^(NSString *asrId, NSString *text) {
        self.corpCoder = asrId;
        [sender setTitle:text forState:UIControlStateNormal];
        [weakPopView removeFromSuperview];
    };
}

#pragma mark -网络请求
- (void)getExpressageCorpList {
    [[BDNetworkTools sharedInstance] getExpressageCorpListWithBlock:^(NSDictionary *responseObject, NSError *error) {
        NSArray *corpArray = [responseObject objectForKey:@"exList"];
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *dict in corpArray) {
            [tempArray addObject:[ReasonSelectModel reasonSelectModelWithDict:dict]];
        }
        self.CorpArray = tempArray;
    }];
}

- (void)postNetExpressageInfo:(UIButton *)btn {
    btn.userInteractionEnabled = NO;
    btn.backgroundColor = [UIColor blackColor];
    NSDictionary *parameter = @{
                                @"asoId":_asoId,
                                @"comCode":_corpCoder,
                                @"comNumber":_fdExplain.text
                                };
    [[BDNetworkTools sharedInstance] postAfterSaleExpressWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
//        BDLog(@"%@", responseObject);
        btn.userInteractionEnabled = YES;
        btn.backgroundColor = [UIColor redColor];
        NSString *errmsg = [responseObject objectForKey:@"errmsg"];
        if ([errmsg isEqualToString:@"ok"]) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


@end
