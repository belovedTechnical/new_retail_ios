//
//  SeeReplaceViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "SeeReplaceViewController.h"
#import "ReturnExpressageViewController.h"
#import "ReturnDetailsViewController.h"
#import "SeeReplaceStatusCell.h"
#import "UserTableViewCell.h"
#import "OrderFromViewCell.h"
#import "SeeReplaceRecord.h"
#import "SeeReplaceModel.h"
#import "ReplaceUserModel.h"

@interface SeeReplaceViewController ()
@property (nonatomic, strong) SeeReplaceModel *replaceModel;
@property (nonatomic, strong) NSString *afterSid;
@property (nonatomic, strong) NSString *afterSidStr;
@end

@implementation SeeReplaceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"售后详情";
    [self requestNetReplaceDetails];
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerClass:[OrderFromViewCell class] forCellReuseIdentifier:@"shopInfoID"];
    [self.tableView registerClass:[UserTableViewCell class] forCellReuseIdentifier:@"detailsID"];
    [self.tableView registerClass:[SeeReplaceStatusCell class] forHeaderFooterViewReuseIdentifier:@"headerID"];
    [self.tableView registerClass:[SeeReplaceRecord class] forHeaderFooterViewReuseIdentifier:@"recordHeaderID"];
}

#pragma mark - Table view data source
- (instancetype)initWithStyle:(UITableViewStyle)style {
    return [super initWithStyle:UITableViewStyleGrouped];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
     return 1;
    }else {
        return _replaceModel.resMapList.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        OrderFromViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shopInfoID"];
        if (self.orderListModelArray.count > 0) {
            cell.listModel = self.orderListModelArray[indexPath.row];
        }
        return cell;
    }else {
        UserTableViewCell *userCell = [tableView dequeueReusableCellWithIdentifier:@"detailsID"];
        ReplaceUserModel *detailsModel = _replaceModel.resMapList[indexPath.row];
        self.afterSid = detailsModel.afterSid;
        self.afterSidStr = detailsModel.afterSidStr;
        userCell.replaceModel = detailsModel;
        if ([_replaceModel.asoStatus integerValue] == 3) {
            userCell.btnEdit.hidden = NO;
            [userCell.btnEdit addTarget:self action:@selector(btnEditClick:) forControlEvents:UIControlEventTouchUpInside];
        }
        return userCell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SeeReplaceStatusCell *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"headerID"];
    [headerView.btnFillout addTarget:self action:@selector(btnFilloutClick) forControlEvents:UIControlEventTouchUpInside];
    headerView.seeReplaceModel = self.replaceModel;
    SeeReplaceRecord *recordHeaderView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"recordHeaderID"];
    switch (section) {
        case 0:
            return headerView;
        case 1:
            return recordHeaderView;
        default:
            return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 96;
    }else {
        ReplaceUserModel *detailsModel = _replaceModel.resMapList[indexPath.row];
        // 根据不同类型给高度(暂时写死)
        CGFloat cellLabelHeight = 30;
        CGFloat cellImageHeight = 35;
        CGFloat cellMarginHeight = 20;
        if ([detailsModel.isActionType isEqualToString:@"userApply"]) {
            return cellLabelHeight*6+cellImageHeight+cellMarginHeight+50;
        }else if ([detailsModel.isActionType isEqualToString:@"merchantReply"]) {
            return cellLabelHeight*6+cellMarginHeight;
        }else if ([detailsModel.isActionType isEqualToString:@"userExpress"]) {
            return cellLabelHeight*6+cellMarginHeight;
        }else if ([detailsModel.isActionType isEqualToString:@"merchantReplyRefund"]) {
            return cellLabelHeight*3+cellMarginHeight;
        }else if ([detailsModel.isActionType isEqualToString:@"userEditor"]) {
            return cellLabelHeight*3+cellMarginHeight;
        }else {
            return 0;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 50;
            break;
        case 1:
            return 30;
            break;
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 20;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
     [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark -控制方法
// 填写物流信息
- (void)btnFilloutClick {
    ReturnExpressageViewController *expreessInput = [[ReturnExpressageViewController alloc] init];
    expreessInput.asoId = _asoId;
    [self.navigationController pushViewController:expreessInput animated:YES];
}

// 商家不同意 更新信息
- (void)btnEditClick:(UIButton *)sender {
    ReturnDetailsViewController *updateReturnVC = [[ReturnDetailsViewController alloc] init];
    updateReturnVC.orderListModelArray = _orderListModelArray;
    updateReturnVC.orderID = _replaceModel.orderId;
    updateReturnVC.titleName = _afterSidStr;
    updateReturnVC.afterSid = _afterSid;
    updateReturnVC.isUpdateInfo = YES;
    [self.navigationController pushViewController:updateReturnVC animated:YES];
}

/**
 name="asoStatus"售后单状态【1:用户售后申请中;2:商家同意;3:商家拒绝;4:确认完成;】
 */
#pragma mark -网络请求
- (void)requestNetReplaceDetails {
    
    NSDictionary *parameter = @{@"asoId":_asoId};
    [[BDNetworkTools sharedInstance] getAfterSaleOrderDetailsWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
//        BDLog(@"%@", responseObject);
        NSDictionary *data = [responseObject objectForKey:@"data"];
        self.replaceModel = [SeeReplaceModel seeReplaceModelWithDict:data];
        [self.tableView reloadData];
    }];
}

@end
