//
//  AfterSalesViewController.m
//  BelovedHotel
//
//  Created by BDSir on 2018/4/3.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "AfterSalesViewController.h"
#import "ReturnDetailsViewController.h"
#import "AfterSalesVarietyListModel.h"
#import "OrderFromViewCell.h"
#import "OnlineModel.h"

@interface AfterSalesViewController ()
@property (nonatomic, strong) NSArray *varietyListArray;
@end

@implementation AfterSalesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"申请售后";
    [self requestAfterSalesListing];
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.estimatedSectionHeaderHeight = 0;
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.tableView registerClass:[OrderFromViewCell class] forCellReuseIdentifier:@"shopInfoID"];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }else {
        return _varietyListArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        OrderFromViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shopInfoID"];
        cell.listModel = self.orderListModelArray[indexPath.row];
        return cell;
    }
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    NSArray *imageArray = @[@"exchange",@"refund",@"money"];
    AfterSalesVarietyListModel *listModel = _varietyListArray[indexPath.row];
    cell.textLabel.text = listModel.afterSName;
    cell.detailTextLabel.text = listModel.afterSComment;
    cell.imageView.image = [UIImage imageNamed:imageArray[indexPath.row]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 96;
    }else {
        return 60;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    AfterSalesVarietyListModel *listModel = _varietyListArray[indexPath.row];
    ReturnDetailsViewController *returnVC = [[ReturnDetailsViewController alloc] init];
    returnVC.orderListModelArray = _orderListModelArray;
    returnVC.titleName = cell.textLabel.text;
    returnVC.afterSid = listModel.afterSid;
    returnVC.orderID = _orderID;
    returnVC.isUpdateInfo = NO;
    [self.navigationController pushViewController:returnVC animated:YES];
}

#pragma mark -网络请求
- (void)requestAfterSalesListing {
    [[BDNetworkTools sharedInstance] getAfterSaleVarietyWithBlock:^(NSDictionary *responseObject, NSError *error) {
        NSArray *listArray = [responseObject objectForKey:@"afterList"];
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *dict in listArray) {
            [tempArray addObject:[AfterSalesVarietyListModel afterSalesVarietyListModelWithDict:dict]];
        }
        self.varietyListArray = tempArray;
        [self.tableView reloadData];
    }];
}

@end
