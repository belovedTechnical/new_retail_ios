//
//  ReturnDetailsViewController.h
//  BelovedHotel
//
//  Created by BDSir on 2018/4/3.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReturnDetailsViewController : UITableViewController
@property (nonatomic, assign) BOOL isUpdateInfo;
@property (nonatomic, strong) NSString *orderID;
@property (nonatomic, strong) NSString *titleName;
@property (nonatomic, strong) NSString *afterSid;
@property (nonatomic, strong) NSArray *orderListModelArray;
@end
