//
//  ReturnDetailsViewController.m
//  BelovedHotel
//
//  Created by BDSir on 2018/4/3.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "ReturnDetailsViewController.h"
#import "AchieveCommitViewController.h"
#import "ReturnDetailsFooterView.h"
#import "OnlineProListModel.h"
#import "ReasonSelectModel.h"
#import "OrderFromViewCell.h"
#import "PopupSelectedView.h"
#import "BDFilePathImage.h"
//#import "OnlineModel.h"
#import "QiniuSDK.h"

@interface ReturnDetailsViewController ()<UIImagePickerControllerDelegate,
                                          UINavigationControllerDelegate
                                         >
@property (nonatomic, strong) UIImagePickerController *imgPickerCon;
@property (nonatomic, strong) ReturnDetailsFooterView *tableFootView;
@property (nonatomic, strong) NSMutableArray *qiNiuImageKeyArray;
@property (nonatomic, strong) NSArray *reasonArray;
@property (nonatomic, assign) NSInteger selectBtnTag;
@property (nonatomic, strong) NSString *qiNiuToken;
@property (nonatomic, strong) NSString *productId;
@property (nonatomic, strong) NSString *asoId;          // 创建售后成功id
@property (nonatomic, strong) NSString *asrId;
@end

@implementation ReturnDetailsViewController
- (UIImagePickerController *)imgPickerCon {
    if (!_imgPickerCon) {
        _imgPickerCon = [[UIImagePickerController alloc] init];
        _imgPickerCon.delegate = self;
        _imgPickerCon.allowsEditing = YES;
    }
    return _imgPickerCon;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = _titleName;
    [self getQiNiuYunToken];
    [self requestReasonSelect];
    self.tableView.estimatedSectionHeaderHeight = 0;
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.tableView registerClass:[OrderFromViewCell class] forCellReuseIdentifier:@"shopInfoID"];
    [self initUI];
}

- (void)initUI {
    ReturnDetailsFooterView *tableFootView = [[ReturnDetailsFooterView alloc] initWithFrame:CGRectMake(0, 20, kScreen_Width, 370)];
    tableFootView.titleName = _titleName;
    [tableFootView.btnSubmit addTarget:self action:@selector(btnSubmitClick) forControlEvents:UIControlEventTouchUpInside];
    [tableFootView.btnSelectIconOne addTarget:self action:@selector(btnSelectIconClick:) forControlEvents:UIControlEventTouchUpInside];
    [tableFootView.btnSelectIconTwo addTarget:self action:@selector(btnSelectIconClick:) forControlEvents:UIControlEventTouchUpInside];
    [tableFootView.btnSelectIconThree addTarget:self action:@selector(btnSelectIconClick:) forControlEvents:UIControlEventTouchUpInside];
    self.tableFootView = tableFootView;
    self.tableView.tableFooterView = tableFootView;
    
    tableFootView.reasonSelect = ^(UIButton *sender) {
       CGFloat popViewH = _reasonArray.count*50 > (kScreenH-60) ? kScreenH-60 :_reasonArray.count*50;
        PopupSelectedView *popView = [[PopupSelectedView alloc] initWithFrame:CGRectMake(sender.bd_x, sender.bd_y, sender.bd_width, popViewH) ListArray:_reasonArray];
        [self.view addSubview:popView];
        
        typeof(popView) weakPopView = popView;
        popView.cellSelectBlcok = ^(NSString *asrId, NSString *text) {
            self.asrId = asrId;
            [sender setTitle:text forState:UIControlStateNormal];
            [weakPopView removeFromSuperview];
        };
    };
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrderFromViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shopInfoID"];
    OnlineProListModel *model = self.orderListModelArray[indexPath.row];
    self.tableFootView.productPrice = model.productPrice;
    self.productId = model.productId;
    cell.listModel = model;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
     return 96;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

#pragma mark -UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [self.imgPickerCon dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    switch (self.selectBtnTag) {
        case 1:
            [self.tableFootView.btnSelectIconOne setBackgroundImage:image forState:UIControlStateNormal];
            break;
        case 2:
            [self.tableFootView.btnSelectIconTwo setBackgroundImage:image forState:UIControlStateNormal];
            break;
        case 3:
            [self.tableFootView.btnSelectIconThree setBackgroundImage:image forState:UIControlStateNormal];
            break;
        default:
            break;
    }
    [SVProgressHUD showWithStatus:@"图片加载中"];
    NSString *imagePath = [BDFilePathImage getImagePathWithIamge:image];
    [self imageUploadToQiNiuWithPath:imagePath];
}

#pragma mark -内部控制方法
- (void)btnSubmitClick {
    if (_isUpdateInfo == NO) {
        [self creatAfterSaleEstablish];
    }else {
        [self updateAfterSaleEstablish];
    }
}

- (void)btnSelectIconClick:(UIButton *)sender {
    self.selectBtnTag = sender.tag;
    self.imgPickerCon.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imgPickerCon animated:YES completion:nil];
    
    // 判断相机是否可用
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
    }
}

#pragma mark -网络请求
- (void)requestReasonSelect {
    [[BDNetworkTools sharedInstance] getAfterSaleReasonSelectWithAfterSid:_afterSid Block:^(NSDictionary *responseObject, NSError *error) {
        NSArray *listArray = [responseObject objectForKey:@"reasonList"];
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *dict in listArray) {
            [tempArray addObject:[ReasonSelectModel reasonSelectModelWithDict:dict]];
        }
        self.reasonArray = tempArray;
    }];
}

- (void)creatAfterSaleEstablish {
    NSString *imgKey = [_qiNiuImageKeyArray componentsJoinedByString:@","];
    NSDictionary *parameter = @{
                                @"orderId":_orderID,
                                @"productId":_productId,
                                @"asrId":_asrId,
                                @"afterSid":_afterSid,
                                @"asExplain":_tableFootView.fdExplain.text,
                                @"asOrderImages":imgKey,
                                @"asRefundAmount":@""
                                };
    [[BDNetworkTools sharedInstance] postAfterSaleEstablishWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
//        BDLog(@"%@", responseObject);
        self.asoId = [responseObject objectForKey:@"asoId"];
        if (self.asoId) {
            AchieveCommitViewController *achieveVC = [[AchieveCommitViewController alloc] init];
            achieveVC.asoId = self.asoId;
            achieveVC.orderListModelArray = _orderListModelArray;
            [self.navigationController pushViewController:achieveVC animated:YES];
        }
    }];
}

- (void)updateAfterSaleEstablish {
    NSString *imgKey = [_qiNiuImageKeyArray componentsJoinedByString:@","];
    NSDictionary *parameter = @{
                                @"orderId":_orderID,
                                @"productId":_productId,
                                @"asrId":_asrId,
                                @"afterSid":_afterSid,
                                @"asExplain":_tableFootView.fdExplain.text,
                                @"asOrderImages":imgKey,
                                @"asRefundAmount":@""
                                };
    [[BDNetworkTools sharedInstance] postAfterSaleUpdateWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        //        BDLog(@"%@", responseObject);
        self.asoId = [responseObject objectForKey:@"asoId"];
        if (self.asoId) {
            AchieveCommitViewController *achieveVC = [[AchieveCommitViewController alloc] init];
            achieveVC.asoId = self.asoId;
            achieveVC.orderListModelArray = _orderListModelArray;
            [self.navigationController pushViewController:achieveVC animated:YES];
        }
    }];
}


// 上传图片到七牛云
- (void)imageUploadToQiNiuWithPath:(NSString *)filePath {
    QNUploadManager *qnUpManager = [[QNUploadManager alloc] init];
    QNUploadOption *uploadOption = [[QNUploadOption alloc] initWithMime:nil                      progressHandler:^(NSString *key, float percent) {}
                                                        params:nil
                                                        checkCrc:NO
                                                        cancellationSignal:nil];
    [qnUpManager putFile:filePath key:nil token:_qiNiuToken complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        NSString *imgKey = [resp objectForKey:@"key"];
        [self.qiNiuImageKeyArray addObject:imgKey];
        [SVProgressHUD dismiss];
    } option:uploadOption];
}

// 获取七牛云上传token
- (void)getQiNiuYunToken {
    [[BDNetworkTools sharedInstance] getQiNiuYunTokenWithParameter:nil Block:^(NSDictionary *responseObject, NSError *error) {
        self.qiNiuToken = [responseObject objectForKey:@"token"];
    }];
}

- (NSMutableArray *)qiNiuImageKeyArray {
    if (!_qiNiuImageKeyArray) {
        _qiNiuImageKeyArray = [NSMutableArray array];
    }
    return _qiNiuImageKeyArray;
}

@end
