//
//  AfterSalesViewController.h
//  BelovedHotel
//
//  Created by BDSir on 2018/4/3.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AfterSalesViewController : UITableViewController
@property (nonatomic, strong) NSArray *orderListModelArray;
@property (nonatomic, strong) NSString *orderID;
@end
