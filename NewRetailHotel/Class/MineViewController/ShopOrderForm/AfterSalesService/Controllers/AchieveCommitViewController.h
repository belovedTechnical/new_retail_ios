//
//  AchieveCommitViewController.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AchieveCommitViewController : UIViewController
@property (nonatomic, strong) NSString *asoId;
@property (nonatomic, strong) NSArray *orderListModelArray;
@end
