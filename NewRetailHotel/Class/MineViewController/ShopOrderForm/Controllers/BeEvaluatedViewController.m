//
//  BeEvaluatedViewController.m
//  BelovedHotel
//
//  Created by BDSir on 2018/3/27.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "BeEvaluatedViewController.h"

@interface BeEvaluatedViewController ()

@end

@implementation BeEvaluatedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"待评价";
    // 待评价
    self.orderType = @"OrderCompleted";
    self.sendStatus = @"";
    self.commentStatus = @"0";
    [self requestNetworkData];
}

@end
