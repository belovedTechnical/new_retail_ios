//
//  PendingPaymentViewController.m
//  BelovedHotel
//
//  Created by BDSir on 2018/3/27.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "PendingPaymentViewController.h"

@interface PendingPaymentViewController ()

@end

@implementation PendingPaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"待付款";
    // 待付款
    self.orderType = @"OrderPlaced";
    [self requestNetworkData];
}




@end
