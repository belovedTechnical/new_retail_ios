//
//  AllOrdersViewController.h
//  BelovedHotel
//
//  Created by BDSir on 2018/3/30.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllOrdersViewController : UITableViewController

@property (nonatomic, strong) NSString *orderType;
@property (nonatomic, strong) NSString *sendStatus;
@property (nonatomic, strong) NSString *commentStatus;
@property (nonatomic, assign) BOOL isChildVC;
- (void)requestNetworkData;
@end
