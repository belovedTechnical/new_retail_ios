//
//  OrderStatusViewCell.m
//  BelovedHotel
//
//  Created by BDSir on 2018/3/31.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "OrderStatusViewCell.h"
#import "OrderDetailsModel.h"
#import "UIView+RGSize.h"

static CGFloat const space = 15;
static CGFloat const titleH = 25;
static CGFloat const titleW = 80;
@interface OrderStatusViewCell() {
    dispatch_source_t _timer;
}
@property (nonatomic, strong) UILabel *lbHour;
@property (nonatomic, strong) UILabel *lbMinute;
@property (nonatomic, strong) UILabel *lbSecond;
@property (nonatomic, strong) UILabel *lbStatus;
@property (nonatomic, strong) UILabel *lbOrderTime;
@property (nonatomic, strong) UILabel *lbOrderNumber;
@property (nonatomic, strong) UILabel *lbResidueTime;
@property (nonatomic, strong) OrderDetailsModel *modelDetails;
@end

static CGFloat const timeW = 35;
static CGFloat const timeSpace = 10;
static CGFloat const lbStatusW = 130;
@implementation OrderStatusViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier model:(OrderDetailsModel *)model {
    self.modelDetails = model;
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        if ([model.orderStatus isEqualToString:@"OrderPlaced"]) {// 下单
            if ([model.payStatus integerValue] == 0) {// 未支付
                [self initUIPendingPaymentWithStatus:@"订单取消倒计时"];
            }else if ([model.payStatus integerValue] == 1) {// 已支付
                [self initUIConsignmentWithStatus:@"等待卖家发货"];
            }
        }else if ([model.orderStatus isEqualToString:@"OrderApproved"]) {// 确认
            if ([model.sendStatus integerValue] == 0) {// 待发货
                [self initUIConsignmentWithStatus:@"等待卖家发货"];
            }else if ([model.sendStatus integerValue] == 1) {// 待收货
                [self initUIConsignmentWithStatus:@"卖家已发货"];
            }
        }else if ([model.orderStatus isEqualToString:@"OrderCompleted"]) {// 完成
            [self initUIFinish];
            if ([model.commentStatus integerValue] == 0) {// 未评论
                BDLog(@"未评论");
            }else if ([model.commentStatus integerValue] == 1) {// 已评论
                BDLog(@"已评论");
            }
        }else if ([model.orderStatus isEqualToString:@"OrderCancelled"]) {// 取消
                BDLog(@"取消");
        }
    }
    return self;
}

#pragma mark - 待付款状态UI
- (void)initUIPendingPaymentWithStatus:(NSString *)status {
    UILabel *lbStatus = [[UILabel alloc] initWithFrame:CGRectMake(0, self.center.y, lbStatusW, 22)];
    lbStatus.backgroundColor = [UIColor colorWithHexString:@"#b4272d"];
    lbStatus.font = [UIFont systemFontOfSize:15];
    lbStatus.textColor = [UIColor whiteColor];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:lbStatus.bounds
                                           byRoundingCorners:UIRectCornerTopRight | UIRectCornerBottomRight
                                           cornerRadii:CGSizeMake(7, 7)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = lbStatus.bounds;
    maskLayer.path = maskPath.CGPath;
    lbStatus.layer.mask = maskLayer;
    lbStatus.text = status;
    [self addSubview:lbStatus];
    
    UILabel *lbHour = [self timeLabelWithX:lbStatusW+timeSpace];
    UILabel *lbMinute = [self timeLabelWithX:lbHour.right+timeSpace];
    UILabel *lbSecond = [self timeLabelWithX:lbMinute.right+timeSpace];
    self.lbHour = lbHour;
    self.lbMinute = lbMinute;
    self.lbSecond = lbSecond;
    
    long ChaTime = [self.modelDetails.currentSystemDate longLongValue] - [self.modelDetails.placedDate longLongValue];
    long timeCha = 24 * 60 * 60 * 1000 - ChaTime;
    if (ChaTime !=0) {
        [self shengYuTime:timeCha / 1000];
    }
}

- (UILabel *)timeLabelWithX:(CGFloat)x {
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(x, self.center.y, timeW, 22)];
    lb.layer.borderColor = [UIColor colorWithHexString:@"#b4272d"].CGColor;
    lb.textColor = [UIColor colorWithHexString:@"#b4272d"];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.font = [UIFont systemFontOfSize:14];
    lb.layer.masksToBounds = YES;
    lb.layer.cornerRadius = 5;
    lb.layer.borderWidth = 1;
    [self addSubview:lb];
    return lb;
}

#pragma mark -待发货、待收货
- (void)initUIConsignmentWithStatus:(NSString *)status {
    UILabel *lbStatus = [[UILabel alloc] initWithFrame:CGRectMake(space, 14, 200, titleH)];
    lbStatus.textColor = [UIColor colorWithHexString:@"#b4272d"];
    lbStatus.font = [UIFont systemFontOfSize:15];
    lbStatus.textAlignment = NSTextAlignmentLeft;
    lbStatus.text = status;
    [self addSubview:lbStatus];
    
    UILabel *lbTitleOrderTime = [self titleLabelWithY:lbStatus.bottom];
    UILabel *lbTitleOrderNumber = [self titleLabelWithY:lbTitleOrderTime.bottom];
    UILabel *lbTitleResidueTime = [self titleLabelWithY:lbTitleOrderNumber.bottom];
    lbTitleOrderTime.text = @"下单时间:";
    lbTitleOrderNumber.text = @"订单号:";
    lbTitleResidueTime.text = @"剩余时间:";
    UILabel *lbOrderTime = [self resultLabelWithY:lbTitleOrderTime.frame.origin.y];
    UILabel *lbOrderNumber = [self resultLabelWithY:lbTitleOrderNumber.frame.origin.y];
    UILabel *lbResidueTime = [self resultLabelWithY:lbTitleResidueTime.frame.origin.y];
    self.lbOrderTime = lbOrderTime;
    self.lbOrderNumber = lbOrderNumber;
    self.lbResidueTime = lbResidueTime;
    if ([status isEqualToString:@"等待卖家发货"]) {
        lbTitleResidueTime.hidden = YES;
        lbResidueTime.hidden = YES;
    }else {
        lbTitleResidueTime.hidden = NO;
        lbResidueTime.hidden = NO;
    }
    
    NSString *payDate = [NSString stringWithFormat:@"%@", self.modelDetails.placedDate];
    NSString *orderTime = [payDate substringToIndex:10];
    NSTimeInterval time = [orderTime doubleValue];
    NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    lbOrderTime.text = [dateFormatter stringFromDate: detaildate];
    lbOrderNumber.text = self.modelDetails.orderNo;
    
    long ChaTime = [self.modelDetails.currentSystemDate longLongValue] - [self.modelDetails.orderDeliveryDate longLongValue];
    long timeCha = 15 *24 *60 *60* 1000 - ChaTime;
    if (ChaTime !=0) {
        [self shengYuTime:timeCha / 1000];
    }
}

- (UILabel *)titleLabelWithY:(CGFloat)y {
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(space, y, titleW, titleH)];
    lb.textColor = [UIColor colorWithHexString:@"#333333"];
    lb.textAlignment = NSTextAlignmentLeft;
    lb.font = [UIFont systemFontOfSize:13];
    [self addSubview:lb];
    return lb;
}

- (UILabel *)resultLabelWithY:(CGFloat)y {
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(space+titleW, y, self.frame.size.width-space-titleW, titleH)];
    lb.textColor = [UIColor colorWithHexString:@"#333333"];
    lb.textAlignment = NSTextAlignmentLeft;
    lb.font = [UIFont systemFontOfSize:13];
    [self addSubview:lb];
    return lb;
}

#pragma mark -完成状态
- (void)initUIFinish {
    UILabel *lbFinish = [[UILabel alloc] init];
    lbFinish.frame = CGRectMake(38, 0, self.width, self.height-10);
    lbFinish.text = @"交易已完成";
    lbFinish.textColor = [UIColor colorWithHexString:@"#b4272d"];
    lbFinish .textAlignment = NSTextAlignmentLeft;
    lbFinish.font = [UIFont systemFontOfSize:14];
    [self addSubview:lbFinish];
}

#pragma mark -计算剩余时间
- (void)shengYuTime:(long )time {
    if (_timer==nil) {
        __block long timeout = time; //倒计时时间
        if (timeout!=0) {
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
            dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
            dispatch_source_set_event_handler(_timer, ^{
                if(timeout<=0){ //倒计时结束，关闭
                    dispatch_source_cancel(_timer);
                    _timer = nil;
                    dispatch_async(dispatch_get_main_queue(), ^{
//                        self.dayLabel.text = @"";
                        self.lbHour.text = @"00";
                        self.lbMinute.text = @"00";
                        self.lbSecond.text = @"00";
                    });
                }else{
                    int days = (int)(timeout/(3600*24));
                    if (days==0) {
//                        self.dayLabel.text = @"";
                    }
                    int hours = (int)((timeout-days*24*3600)/3600);
                    int minute = (int)(timeout-days*24*3600-hours*3600)/60;
                    int second = (int)timeout-days*24*3600-hours*3600-minute*60;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (days==0) {
//                            self.dayLabel.text = @"0天";
                        }else{
//                            self.dayLabel.text = [NSString stringWithFormat:@"%d天",days];
                        }
                        if (hours<10) {
                            self.lbHour.text = [NSString stringWithFormat:@"0%d时",hours];
                        }else{
                            self.lbHour.text = [NSString stringWithFormat:@"%d时",hours];
                        }
                        if (minute<10) {
                            self.lbMinute.text = [NSString stringWithFormat:@"0%d分",minute];
                        }else{
                            self.lbMinute.text = [NSString stringWithFormat:@"%d分",minute];
                        }
                        if (second<10) {
                            self.lbSecond.text = [NSString stringWithFormat:@"0%d秒",second];
                        }else{
                            self.lbSecond.text = [NSString stringWithFormat:@"%d秒",second];
                        }
                        if (days == 0) {
                            self.lbResidueTime.text = [NSString stringWithFormat:@"%d:%d:%d", hours, minute, second];
                        }else
                        {
                            self.lbResidueTime.text = [NSString stringWithFormat:@"%d天%d:%d:%d",days, hours, minute, second];
                        }
                    });
                    timeout--;
                }
            });
            dispatch_resume(_timer);
        }
    }
}


@end
