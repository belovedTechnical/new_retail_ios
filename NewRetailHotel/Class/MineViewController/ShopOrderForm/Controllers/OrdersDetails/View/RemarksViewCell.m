//
//  RemarksViewCell.m
//  BelovedHotel
//
//  Created by BDSir on 2018/3/31.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "RemarksViewCell.h"
#import "OrderDetailsModel.h"
#import "UIView+RGSize.h"

@interface RemarksViewCell()
@property (nonatomic, strong) UILabel *lbInvoiceInfo;
@property (nonatomic, strong) UILabel *lbRemarksInfo;
@end

static CGFloat const space = 15;
static CGFloat const titleH = 45;
static CGFloat const titleW = 80;
static CGFloat const btnH = 29;
static CGFloat const btnW = 71;
static CGFloat const btnSpace = 10;
@implementation RemarksViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}

- (void)setModelDetail:(OrderDetailsModel *)modelDetail {
    _modelDetail = modelDetail;
    self.lbInvoiceInfo.text = modelDetail.invoice == nil ? @"不需要发票":modelDetail.invoice;
    self.lbRemarksInfo.text = modelDetail.remarks;
    [self displayDisponse:modelDetail];
}

- (void)initUI {
    UILabel *lbInvoice = [self titleLabelWithY:0];
    lbInvoice.text = @"发票信息:";
    UILabel *lbRemarks = [self titleLabelWithY:titleH+1];
    lbRemarks.text = @"留言:";
    UILabel *lbInvoiceInfo = [self resultLabelWithY:0];
    UILabel *lbRemarksInfo = [self resultLabelWithY:titleH+1];
    self.lbInvoiceInfo = lbInvoiceInfo;
    self.lbRemarksInfo = lbRemarksInfo;
    
    [self lineViewWithY:lbInvoice.bottom];
    [self lineViewWithY:lbRemarksInfo.bottom];
    
    UIButton *btnRight = [self bottomFunctionBtnWithX:kScreenW-btnSpace-btnW];
    UIButton *btnCenter = [self bottomFunctionBtnWithX:kScreenW-(btnSpace*2)-(btnW*2)];
    UIButton *btnLeft = [self bottomFunctionBtnWithX:kScreenW-(btnSpace*3)-(btnW*3)];
    self.btnLeft = btnLeft;
    self.btnRight = btnRight;
    self.btnCenter = btnCenter;
}

- (UILabel *)titleLabelWithY:(CGFloat)y {
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(space, y, titleW, titleH)];
    lb.textColor = [UIColor colorWithHexString:@"#333333"];
    lb.textAlignment = NSTextAlignmentLeft;
    lb.font = [UIFont systemFontOfSize:15];
    [self addSubview:lb];
    return lb;
}

- (UILabel *)resultLabelWithY:(CGFloat)y {
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(space+titleW, y, kScreenW-space-titleW, titleH)];
    lb.textColor = [UIColor colorWithHexString:@"#333333"];
    lb.textAlignment = NSTextAlignmentLeft;
    lb.font = [UIFont systemFontOfSize:15];
    [self addSubview:lb];
    return lb;
}

- (UIView *)lineViewWithY:(CGFloat)y {
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(15, y, kScreenW, 0.5)];
    line.backgroundColor = [UIColor colorWithHexString:@"#cccccc"];
    [self addSubview:line];
    return line;
}

- (UIButton *)bottomFunctionBtnWithX:(CGFloat)x {
    UIButton *btnFunction = [[UIButton alloc] initWithFrame:CGRectMake(x, self.lbRemarksInfo.bottom+space, btnW, btnH)];
    [btnFunction setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
    [btnFunction setTitleColor:[UIColor colorWithHexString:@"#cc3333"] forState:UIControlStateHighlighted];
    btnFunction.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
    btnFunction.titleLabel.font = [UIFont systemFontOfSize:14];
    btnFunction.layer.masksToBounds = YES;
    btnFunction.layer.cornerRadius = 1.5;
    btnFunction.layer.borderWidth = 1;
    [self addSubview:btnFunction];
    return btnFunction;
}

- (void)displayDisponse:(OrderDetailsModel *)model
{
    NSString *orderStatus = model.orderStatus;
    NSString *sendStatus = [model.sendStatus description];
    NSString *backString = @"";
    if ([@"OrderApproved" isEqualToString:orderStatus] && [@"1" isEqualToString:sendStatus]) {
        [self.btnLeft setTitle:@"联系商家" forState:UIControlStateNormal];
        [self.btnCenter setTitle:@"查看物流" forState:UIControlStateNormal];
        [self.btnRight setTitle:@"确认收货" forState:UIControlStateNormal];
    }else if ([@"OrderApproved" isEqualToString:orderStatus] && [@"0" isEqualToString:sendStatus]) {
        self.btnLeft.hidden = YES;
        self.btnCenter.hidden = YES;
        [self.btnRight setTitle:@"联系商家" forState:UIControlStateNormal];
    }else if ([@"OrderPlaced" isEqualToString:orderStatus]) {
        self.btnLeft.hidden = YES;
        [self.btnRight setTitle:@"去付款" forState:UIControlStateNormal];
        [self.btnCenter setTitle:@"联系商家" forState:UIControlStateNormal];
    }else if ([@"OrderCancelled" isEqualToString:orderStatus]) {
        backString = @"已取消";
    }else if ([@"OrderCompleted" isEqualToString:orderStatus]) {
        backString = @"已完成";
    } if ([@"OrderCompleted" isEqualToString:orderStatus]) {
        backString = @"待评价";
        self.btnLeft.hidden = YES;
        [self.btnCenter setTitle:@"申请售后" forState:UIControlStateNormal];
        [self.btnRight setTitle:@"去评价" forState:UIControlStateNormal];
    } if ([@"OrderAfterSale" isEqualToString:orderStatus]) {
        backString = @"退款/退货";
        self.btnLeft.hidden = YES;
        self.btnCenter.hidden = YES;
        [self.btnRight setTitle:@"查看详情" forState:UIControlStateNormal];
    }
}

@end
