//
//  AddressViewCell.m
//  BelovedHotel
//
//  Created by BDSir on 2018/3/31.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "AddressViewCell.h"
#import "OrderDetailsModel.h"
#import "UIView+RGSize.h"

@interface AddressViewCell()
@property (nonatomic, weak) UILabel *lbName;
@property (nonatomic, weak) UILabel *lbPhone;
@property (nonatomic, weak) UILabel *lbAddress;
@end

@implementation AddressViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}

- (void)setModelDetail:(OrderDetailsModel *)modelDetail {
    _modelDetail = modelDetail;
    self.lbName.text = modelDetail.attnName;
    self.lbPhone.text = modelDetail.telePhone;
    self.lbAddress.text = modelDetail.address;
    CGSize labelSize = [self.lbAddress sizeThatFits:CGSizeMake(self.frame.size.width-45, MAXFLOAT)];
    self.lbAddress.size = labelSize;
}

- (void)initUI {
    UILabel *lbName = [[UILabel alloc] initWithFrame:CGRectMake(38, 19, 80, 30)];
    lbName.textColor = [UIColor colorWithHexString:@"#666666"];
    lbName.textAlignment = NSTextAlignmentLeft;
    lbName.font = [UIFont systemFontOfSize:15];
    [self addSubview:lbName];
    self.lbName = lbName;
    
    UILabel *lbPhone = [[UILabel alloc] initWithFrame:CGRectMake(38+lbName.frame.size.width, 19, 150, 30)];
    lbPhone.textColor = [UIColor colorWithHexString:@"#666666"];
    lbPhone.textAlignment = NSTextAlignmentLeft;
    lbPhone.font = [UIFont systemFontOfSize:15];
    [self addSubview:lbPhone];
    self.lbPhone = lbPhone;
    
    UIImageView *imgAddress = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"place"]];
    imgAddress.origin = CGPointMake(15, lbName.frame.size.height+19);
    [self addSubview:imgAddress];
    
    UILabel *lbAddress = [[UILabel alloc] init];
    lbAddress.origin = CGPointMake(38, imgAddress.origin.y);
    lbAddress.textColor = [UIColor colorWithHexString:@"#999999"];
    lbAddress.lineBreakMode = NSLineBreakByWordWrapping;
    lbAddress.textAlignment = NSTextAlignmentLeft;
    lbAddress.font = [UIFont systemFontOfSize:14];
    lbAddress.numberOfLines = 0;
    [self addSubview:lbAddress];
    self.lbAddress = lbAddress;
}

@end
