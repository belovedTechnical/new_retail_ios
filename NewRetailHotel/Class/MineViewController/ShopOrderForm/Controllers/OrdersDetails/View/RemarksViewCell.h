//
//  RemarksViewCell.h
//  BelovedHotel
//
//  Created by BDSir on 2018/3/31.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OrderDetailsModel;
@interface RemarksViewCell : UITableViewCell

@property (nonatomic, weak) UIButton *btnRight;
@property (nonatomic, weak) UIButton *btnCenter;
@property (nonatomic, weak) UIButton *btnLeft;

@property (nonatomic, strong) OrderDetailsModel *modelDetail;
@end
