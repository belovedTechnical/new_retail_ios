//
//  OrderDetailsViewController.m
//  BelovedHotel
//
//  Created by BDSir on 2018/3/31.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "OrderDetailsViewController.h"
#import "ExpressInformationViewController.h"
#import "WebShopDetailsViewController.h"
#import "AfterSalesViewController.h"
#import "PayOderViewController.h"
#import "OrderFormHeaderView.h"
#import "OrderStatusViewCell.h"
#import "OrderFromViewCell.h"
#import "OnlineProListModel.h"
#import "OrderDetailsModel.h"
#import "AddressViewCell.h"
#import "RemarksViewCell.h"
#import "OnlineModel.h"
#import <objc/runtime.h>

@interface OrderDetailsViewController ()
@property (nonatomic, strong) OrderDetailsModel *detailsModel;
@property (nonatomic, strong) NSArray *orderListModelArray;

@end

static NSString * const shopInfoID = @"shopInfoID";
@implementation OrderDetailsViewController

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"订单详情";
    [self loadData];
    self.tableView.contentInset = UIEdgeInsetsMake(10, 0, 0, 0);
    self.tableView.tableFooterView = [[UIView alloc] init];
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.tableView registerClass:[OrderFromViewCell class] forCellReuseIdentifier:shopInfoID];
    [self.tableView registerClass:[AddressViewCell class] forCellReuseIdentifier:@"addressID"];
    [self.tableView registerClass:[RemarksViewCell class] forCellReuseIdentifier:@"remarksID"];
    [self.tableView registerClass:[OrderFormHeaderView class] forHeaderFooterViewReuseIdentifier:@"headerViewID"];
}

#pragma mark --UITableViewDataSource/UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        OrderStatusViewCell *cellStatus = [[OrderStatusViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"statusID" model:self.detailsModel];
        return cellStatus;
    }else  if (indexPath.section == 1) {
        AddressViewCell *cellAddress = [tableView dequeueReusableCellWithIdentifier:@"addressID" forIndexPath:indexPath];
        cellAddress.modelDetail = self.detailsModel;
        return cellAddress;
    }else  if (indexPath.section == 2) {
        OrderFromViewCell *cellShop = [tableView dequeueReusableCellWithIdentifier:shopInfoID forIndexPath:indexPath];
        cellShop.backgroundColor = [UIColor groupTableViewBackgroundColor];
        cellShop.listModel = self.orderListModelArray[_index];
        return cellShop;
    }else  if (indexPath.section == 3) {
        RemarksViewCell *cellRemark = [tableView dequeueReusableCellWithIdentifier:@"remarksID" forIndexPath:indexPath];
        cellRemark.selectionStyle = UITableViewCellSelectionStyleNone;
        cellRemark.modelDetail = self.detailsModel;
        [cellRemark.btnRight addTarget:self action:@selector(btnRightAndbtnCenterAndBntLeftClick:) forControlEvents:UIControlEventTouchUpInside];
        [cellRemark.btnCenter addTarget:self action:@selector(btnRightAndbtnCenterAndBntLeftClick:) forControlEvents:UIControlEventTouchUpInside];
        [cellRemark.btnLeft addTarget:self action:@selector(btnRightAndbtnCenterAndBntLeftClick:) forControlEvents:UIControlEventTouchUpInside];
        OnlineProListModel *shopModel = self.orderListModelArray[_index];
        objc_setAssociatedObject(cellRemark.btnRight, @"phoneNumber", shopModel.contactNumber, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        return cellRemark;
    }else {
        return nil;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 2) {
        OrderFormHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"headerViewID"];
        [headerView.btnStore setTitle:[NSString stringWithFormat:@" %@", self.detailsModel.productStoreName] forState:UIControlStateNormal];
        return headerView;
    }else {
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if ([self.detailsModel.orderStatus isEqualToString:@"OrderPlaced"]) {// 下单
            if ([self.detailsModel.payStatus integerValue] == 0) {// 未支付
                return 76;
            }else if ([self.detailsModel.payStatus integerValue] == 1) {// 已支付
                return 130;
            }
        }else if ([self.detailsModel.orderStatus isEqualToString:@"OrderCompleted"]) {// 完成
            return 30;
        }
        return 130;
    }else  if (indexPath.section == 1) {

        return 100;
    }else  if (indexPath.section == 2) {

        return 100;
    }else  if (indexPath.section == 3) {
        return 150;
    }else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 2) {
        return 40;
    }else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 2) {
        OnlineProListModel *shopModel = self.orderListModelArray[_index];
        NSString *urlStr = [NSString stringWithFormat:@"%@c-shopDetails/%@",HTML_URL_Base, shopModel.productId];
        WebShopDetailsViewController *shopDetaVC = [[WebShopDetailsViewController alloc] init];
        shopDetaVC.shopDetailsURL = urlStr;
        [self.navigationController pushViewController:shopDetaVC animated:YES];
    }
}

#pragma mark -按钮点击方法
- (void)btnRightAndbtnCenterAndBntLeftClick:(UIButton *)sender
{
    if ([sender.titleLabel.text isEqualToString:@"联系商家"]) {
        [self contactBusinessWithBtn:sender];
    }else if ([sender.titleLabel.text isEqualToString:@"去付款"]) {
        [self goToCloseAnAccountWithBtn:sender];
    }else if ([sender.titleLabel.text isEqualToString:@"去评价"]) {
        [self goToCommentWithBtn:sender];
    }else if ([sender.titleLabel.text isEqualToString:@"确认收货"]) {
        [self confirmReceiptWithBtn:sender];
    }else if ([sender.titleLabel.text isEqualToString:@"查看物流"]) {
        [self seeLogisticsWithBtn:sender];
    }else if ([sender.titleLabel.text isEqualToString:@"查看详情"]) {
        [self seeOrderDetailsWithBtn:sender];
    }if ([sender.titleLabel.text isEqualToString:@"申请售后"]) {
        [self afterSalesWithBtn:sender];
    }
}
/** 去付款 */
- (void)goToCloseAnAccountWithBtn:(UIButton *)sender {
    PayOderViewController *payVC = [[PayOderViewController alloc]init];
    payVC.orderId = _orderId;
    [self.navigationController pushViewController:payVC animated:YES];
}
/** 联系商家 */
- (void)contactBusinessWithBtn:(UIButton *)sender {
    NSString *phone = objc_getAssociatedObject(sender, @"phoneNumber");
    [self showAlertWithTitle:@"联系商家" message:phone AscertainHandler:^(UIAlertAction *action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", phone]]];
    } CancelHandler:^(UIAlertAction *action) {
        
    }];
}
/** 去评价 */
- (void)goToCommentWithBtn:(UIButton *)sender {
    BDLog(@"去评价");
}
/** 确认收货 */
- (void)confirmReceiptWithBtn:(UIButton *)sender {
    NSDictionary *parameter = @{@"orderId":[NSString stringWithFormat:@"%lu", (long)sender.tag]};
    [[BDNetworkTools sharedInstance] putOnlineOrderVerifyWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
/** 查看物流 */
- (void)seeLogisticsWithBtn:(UIButton *)sender {
    ExpressInformationViewController *expressVC = [[ExpressInformationViewController alloc] init];
    expressVC.orderId = self.detailsModel.orderId;
    expressVC.expressNumber = self.detailsModel.expressNumber;
    expressVC.expressCompany = self.detailsModel.expressCompany;
    [self.navigationController pushViewController:expressVC animated:YES];
}
/** 查看详情 */
- (void)seeOrderDetailsWithBtn:(UIButton *)sender {
    BDLog(@"查看详情");
}

/** 申请售后 */
- (void)afterSalesWithBtn:(UIButton *)sender {
    AfterSalesViewController *afterSalesVC = [[AfterSalesViewController alloc] init];
    afterSalesVC.orderListModelArray = _orderListModelArray;
    afterSalesVC.orderID = _orderId;
    [self.navigationController pushViewController:afterSalesVC animated:YES];
}

#pragma mark -网络请求
- (void)loadData
{
    [SVProgressHUD show];
    NSDictionary *parameter = @{@"orderId":self.orderId};
    [[BDNetworkTools sharedInstance] getOnlineListDetailWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        [SVProgressHUD dismiss];
        BDLog(@"%@", responseObject);
        self.detailsModel = [OrderDetailsModel orderDetailsModelWith:responseObject];
        self.orderListModelArray = self.detailsModel.productList;
        [self.tableView reloadData];
    }];
}

@end
