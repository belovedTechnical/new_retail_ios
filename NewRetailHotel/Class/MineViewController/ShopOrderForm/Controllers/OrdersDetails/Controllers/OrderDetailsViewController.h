//
//  OrderDetailsViewController.h
//  BelovedHotel
//
//  Created by BDSir on 2018/3/31.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailsViewController : UITableViewController
@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) NSString *orderStatus;
@end
