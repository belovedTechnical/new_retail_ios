//
//  OrderDetailsModel.m
//  BelovedHotel
//
//  Created by BDSir on 2018/4/2.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "OrderDetailsModel.h"
#import "OnlineProListModel.h"

@implementation OrderDetailsModel

+ (instancetype)orderDetailsModelWith:(NSDictionary *)dict
{
    OrderDetailsModel *model = [[self alloc] init];
    [model setValuesForKeysWithDictionary:dict];
    NSMutableArray *tempArray = [NSMutableArray array];
    for (NSDictionary *cellDict in dict[@"productList"]) {
        OnlineProListModel *cell = [[OnlineProListModel alloc] init];
        [cell setValuesForKeysWithDictionary:cellDict];
        [tempArray addObject:cell];
    }
    model.productList = tempArray;
    return model;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}

@end
