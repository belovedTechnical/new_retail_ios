//
//  OrderDetailsModel.h
//  BelovedHotel
//
//  Created by BDSir on 2018/4/2.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderDetailsModel : NSObject
/** 订单状态 */
@property (nonatomic, strong) NSString *orderStatus;
/** 支付状态 0未支付，1已支付 */
@property (nonatomic, strong) NSString *payStatus;
/** 发货状态 0待发货，1待收货 */
@property (nonatomic, strong) NSString *sendStatus;
/** 评论状态 0未评论，1已评论  */
@property (nonatomic, strong) NSString *commentStatus;
/** 收货人名字 */
@property (nonatomic, strong) NSString *attnName;
/** 收货电话 */
@property (nonatomic, strong) NSString *telePhone;
/** 收货地址 */
@property (nonatomic, strong) NSString *address;
/** 发票信息 */
@property (nonatomic, strong) NSString *invoice;
/** 留言 */
@property (nonatomic, strong) NSString *remarks;
/** 订单ID */
@property (nonatomic, strong) NSString *orderId;
/** 订单号 */
@property (nonatomic, strong) NSString *orderNo;
/** 下单时间 */
@property (nonatomic, strong) NSString *placedDate;
/** 当前系统时间 */
@property (nonatomic, strong) NSString *currentSystemDate;
/** 订单发货时间 */
@property (nonatomic, strong) NSString *orderDeliveryDate;
/** 订单完成时间 */
@property (nonatomic, strong) NSString *orderCompleteDate;
/** 店铺名称 */
@property (nonatomic, strong) NSString *productStoreName;
/** 店铺ID */
@property (nonatomic, strong) NSString *productStoreId;
/** 店铺类型 */
@property (nonatomic, strong) NSString *productStoreType;
/** 物流单号 */
@property (nonatomic, strong) NSString *expressNumber;
/** 物流公司 */
@property (nonatomic, strong) NSString *expressCompany;
/** 商品数组 */
@property (nonatomic, strong) NSArray *productList;

+ (instancetype)orderDetailsModelWith:(NSDictionary *)dict;

@end
