//
//  ShopOrderFormViewController.m
//  BelovedHotel
//
//  Created by BDSir on 2018/3/27.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "ShopOrderFormViewController.h"
#import "PendingPaymentViewController.h"
#import "ToBeDeliveredViewController.h"
#import "ToReceiveGoodsViewController.h"
#import "BeEvaluatedViewController.h"
#import "ReplacePurchaseViewController.h"
#import "AllOrdersViewController.h"
#import "UIView+Frame.h"

static NSString *const ID = @"cell";
@interface ShopOrderFormViewController ()<UICollectionViewDataSource, UICollectionViewDelegate>
@property(nonatomic, weak) UIScrollView *topTitleView;
@property(nonatomic, weak) UICollectionView *collectionView;
@property(nonatomic, weak) UIButton *selectButton;
@property(nonatomic, weak) UIView *underLineView;
@property(nonatomic, strong) NSMutableArray *buttons;
@property(nonatomic, assign) BOOL isInitial;
/** 待付款 */
@property (nonatomic, strong) NSString *unPay;
/** 待发货 */
@property (nonatomic, strong) NSString *unSend;
/** 待收货 */
@property (nonatomic, strong) NSString *unReceive;
/** 待评价 */
@property (nonatomic, strong) NSString *unComment;
/** 退货/退款 */
@property (nonatomic, strong) NSString *inAfterSale;

@end

#define BDScreenW [UIScreen mainScreen].bounds.size.width
@implementation ShopOrderFormViewController

- (NSMutableArray *)buttons
{
    if (!_buttons) {
        _buttons = [NSMutableArray array];
    }
    return _buttons;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    if (_isInitial == NO) {
//        [self setupAllTitleButton];
//        _isInitial = YES;
//    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"电商订单";
    
    [self requestNetworkData];
    [self setupNavigationBar];
    // 不需要自动添加额外滚动区域
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
}

- (void)initUI {
    [self setupAddChildViewController];
    [self setupBottomView];
    [self setupTopView];
    [self setupAllTitleButton];
}

#pragma mark - 设置导航条内容
- (void)setupNavigationBar {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"全部订单" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithHexString:@"#666666"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(orderAllButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [btn sizeToFit];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
    imageView.frame = CGRectMake(0, 0, MAXFLOAT, MAXFLOAT);
    [imageView sizeToFit];
    imageView.bd_left = btn.bd_right;
    imageView.bd_centerY = btn.bd_centerY;
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, btn.bd_width+imageView.bd_width, btn.bd_height)];
    [customView addSubview:btn];
    [customView addSubview:imageView];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:customView];
}

// 全部订单点击
- (void)orderAllButtonClick {
    AllOrdersViewController *allOrderVC = [[AllOrdersViewController alloc] init];
    allOrderVC.orderType = @"";
    allOrderVC.sendStatus = @"";
    allOrderVC.commentStatus = @"";
    allOrderVC.isChildVC = YES;
    [allOrderVC requestNetworkData];
    [self.navigationController pushViewController:allOrderVC animated:YES];
}

- (void)setupAddChildViewController
{
    PendingPaymentViewController *paymentVC = [[PendingPaymentViewController alloc] init];
    paymentVC.title = [NSString stringWithFormat:@"待付款\n  (%@)", self.unPay];
    [self addChildViewController:paymentVC];
    
    ToBeDeliveredViewController *deliveredVC = [[ToBeDeliveredViewController alloc] init];
    deliveredVC.title = [NSString stringWithFormat:@"待发货\n  (%@)", self.unSend];
    [self addChildViewController:deliveredVC];
    
    ToReceiveGoodsViewController *receiveVC = [[ToReceiveGoodsViewController alloc] init];
    receiveVC.title = [NSString stringWithFormat:@"待收货\n   (%@)", self.unReceive];
    [self addChildViewController:receiveVC];
    
    BeEvaluatedViewController *evaluatedVC = [[BeEvaluatedViewController alloc] init];
    evaluatedVC.title = [NSString stringWithFormat:@"待评价\n  (%@)", self.unComment];
    [self addChildViewController:evaluatedVC];
    
    ReplacePurchaseViewController *replaceVC = [[ReplacePurchaseViewController alloc] init];
    replaceVC.title = [NSString stringWithFormat:@"退款/退货\n  (%@)", self.inAfterSale];
    [self addChildViewController:replaceVC];
}

#pragma mark -设置所有标题
- (void)setupAllTitleButton
{
    NSUInteger count = self.childViewControllers.count;
    CGFloat btnX = 0;
    CGFloat btnY = 0;
    CGFloat btnW = BDScreenW / count;
    CGFloat btnH = self.topTitleView.bd_height;
    for (int i = 0; i < count; i++) {
        UIButton *titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        titleBtn.tag = i;
        UIViewController *vc = self.childViewControllers[i];
        [titleBtn setTitle:vc.title forState:UIControlStateNormal];
        [titleBtn setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
        [titleBtn setTitleColor:[UIColor colorWithHexString:@"#b4272d"] forState:UIControlStateSelected];
        titleBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        titleBtn.titleLabel.numberOfLines = 0;
        btnX = i * btnW; 
        titleBtn.frame = CGRectMake(btnX, btnY, btnW, btnH);
        
        [self.topTitleView addSubview:titleBtn];
        [self.buttons addObject:titleBtn];
        
        // 监听按钮
        [titleBtn addTarget:self action:@selector(titleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        if (i == 0) {
            
            // 添加下划线
            UIView *underLineView = [[UIView alloc] init];
            underLineView.backgroundColor = [UIColor redColor];
            [self.topTitleView addSubview:underLineView];
            _underLineView = underLineView;
            
            //中心点
            underLineView.bd_height = 2;
            underLineView.bd_y = self.topTitleView.bd_height - underLineView.bd_height;
            [titleBtn.titleLabel sizeToFit];
            underLineView.bd_width = titleBtn.bd_width;
            underLineView.bd_centerX = titleBtn.bd_centerX;
            
            [self titleBtnClick:titleBtn];
        }
    }
}

#pragma mark -点击标题
- (void)titleBtnClick:(UIButton *)button
{
    NSInteger i = button.tag;
    // 1.选中按钮
    [self selButton:button];
    
    // 2.设置collectionView偏移量
    CGFloat offsetX = i * BDScreenW;
    _collectionView.contentOffset = CGPointMake(offsetX, 0);
    
    NSDictionary *dict = @{@"btnTitle": button.titleLabel.text};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"couponSelectNoti" object:nil userInfo:dict];
}

#pragma mark -选中按钮
- (void)selButton:(UIButton *)button
{
    _selectButton.selected = NO;
    button.selected = YES;
    _selectButton = button;
    
    // 修改下划线位置
    [UIView animateWithDuration:0.25 animations:^{
        _underLineView.bd_centerX = button.bd_centerX;
        
    }];
}

#pragma mark -UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.childViewControllers.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ID forIndexPath:indexPath];
    
    // 移除其他子控制器view
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    // 添加对应的子控制器View 到对应cell
    UIViewController *vc = self.childViewControllers[indexPath.row];
    
    // 默认控制器frame有y值, 每次添加的时候，必须重新设置子控制器的frame
    vc.view.frame = [UIScreen mainScreen].bounds;
    
    [cell.contentView addSubview:vc.view];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate
// 滚动完成的时候就会调用
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // 获取页码
    NSInteger page = scrollView.contentOffset.x / BDScreenW;
    
    // 获取标题按钮
    UIButton *titleButton = self.buttons[page];
    
    // 选中标题
    [self selButton:titleButton];
}

#pragma mark -添加顶部标题View
- (void)setupTopView
{
    CGFloat x = 0;
    CGFloat y = SafeAreaTopHeight;
    CGFloat w = self.view.frame.size.width;
    CGFloat h = 60;
    
    UIScrollView *topTitleView = [[UIScrollView alloc] initWithFrame:CGRectMake(x, y, w, h)];
    topTitleView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:topTitleView];
    self.topTitleView = topTitleView;
    
}

#pragma mark -添加底部内容View
- (void)setupBottomView
{
    // 布局
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = [UIScreen mainScreen].bounds.size;
    // 设置滚动方向
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 0;
    
    // UICollectionView
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:[UIScreen mainScreen].bounds collectionViewLayout:layout];
    collectionView.backgroundColor = [UIColor colorWithRed:(215)/255.0 green:(215)/255.0 blue:(215)/255.0 alpha:1];
    [self.view addSubview:collectionView];
    
    collectionView.showsHorizontalScrollIndicator = NO;
    collectionView.pagingEnabled = YES;
    
    _collectionView = collectionView;
    collectionView.dataSource = self;
    collectionView.delegate = self;
    
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:ID];
}

#pragma mark --请求网络数据--
- (void)requestNetworkData
{
    [SVProgressHUD show];
    [[BDNetworkTools sharedInstance] getOnlineOrderCountWithBlock:^(NSDictionary *responseObject, NSError *error) {
        [SVProgressHUD dismiss];
        self.unPay = [responseObject objectForKey:@"unPay"];
        self.unSend = [responseObject objectForKey:@"unSend"];
        self.unReceive = [responseObject objectForKey:@"unReceive"];
        self.unComment = [responseObject objectForKey:@"unComment"];
        self.inAfterSale = [responseObject objectForKey:@"inAfterSale"];
        [self initUI];
    }];
}


@end
