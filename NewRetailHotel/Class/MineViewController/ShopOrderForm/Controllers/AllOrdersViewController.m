//
//  AllOrdersViewController.m
//  BelovedHotel
//
//  Created by BDSir on 2018/3/30.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "AllOrdersViewController.h"
#import "ExpressInformationViewController.h"
#import "OnlineDetailViewController.h"
#import "OrderDetailsViewController.h"
#import "SeeReplaceViewController.h"
#import "PayOderViewController.h"
#import "OrderFormHeaderView.h"
#import "OrderFormFooterView.h"
#import "OnlineProListModel.h"
#import "OrderFromViewCell.h"
#import "OnlineModel.h"
#import "MJRefresh.h"
#import <objc/runtime.h>
#import "CommentViewController.h"

@interface AllOrdersViewController ()

@property (nonatomic, strong) NSMutableArray *orderListModelArray;
@property (nonatomic, strong) OnlineProListModel *modelSection;
@property (nonatomic, assign) BOOL isRefresh;
@property (nonatomic, assign) NSUInteger pageIndex;

@end

static NSString * const shopInfoID = @"shopInfoID";
static NSString * const cellHeaderID = @"cellHeaderID";
static NSString * const cellFooterID = @"cellFooterID";
#define TopHeight (kScreenH == 812.0 ? 88+60 : 64+60)
@implementation AllOrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isRefresh = YES;
    self.pageIndex = 0;
    [self addRefresh];
    self.tableView.rowHeight = 93;
    if (_isChildVC) {
        self.title = @"全部订单";
    }else {
     self.tableView.contentInset = UIEdgeInsetsMake(TopHeight, 0, 0, 0);
        if (@available(iOS 11.0, *)) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
    }
    [self.tableView registerClass:[OrderFromViewCell class] forCellReuseIdentifier:shopInfoID];
    [self.tableView registerClass:[OrderFormHeaderView class] forHeaderFooterViewReuseIdentifier:cellHeaderID];
    [self.tableView registerClass:[OrderFormFooterView class] forHeaderFooterViewReuseIdentifier:cellFooterID];
}

#pragma mark - Table view data source

- (instancetype)initWithStyle:(UITableViewStyle)style {
    return [super initWithStyle:UITableViewStyleGrouped];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.orderListModelArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    OnlineModel *model = self.orderListModelArray[section];
    return model.productList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrderFromViewCell *cell = [tableView dequeueReusableCellWithIdentifier:shopInfoID forIndexPath:indexPath];
    cell.backgroundColor = [UIColor groupTableViewBackgroundColor];
    OnlineModel *model = self.orderListModelArray[indexPath.section];
    self.modelSection = model.productList[indexPath.row];
    cell.listModel = _modelSection;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    OrderFormHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:cellHeaderID];
    OnlineModel *model = self.orderListModelArray[section];
    [headerView.btnStore setTitle:[NSString stringWithFormat:@" %@", model.storeName] forState:UIControlStateNormal];
    headerView.lbStatus.text = [self displayDisponse:model];
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    OrderFormFooterView *footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:cellFooterID];
    OnlineModel *model = self.orderListModelArray[section];
    footerView.lbPrice.text = [NSString stringWithFormat:@"合计:¥%@", model.orderTotalPrice];
    footerView.lbQuantity.text = [NSString stringWithFormat:@"共%@件商品", model.orderTotalNum];
    footerView.btnRight.tag = [model.orderId integerValue];
    footerView.btnLeft.tag = [model.orderId integerValue];
    footerView.btnCenter.tag = [model.orderId integerValue];
    objc_setAssociatedObject(self, @"phoneNumber", model.contactNumber, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    objc_setAssociatedObject(self, @"productList", model.productList, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (model.productList.count >0) {
        OnlineProListModel *shopModel = model.productList[0];
        objc_setAssociatedObject(footerView.btnRight, @"asoId", shopModel.asoId, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    
    NSString *statusStr = [self displayDisponse:model];
    if ([@"待付款" isEqualToString:statusStr]) {
        footerView.btnLeft.hidden = YES;
        [footerView.btnRight setTitle:@"去付款" forState:UIControlStateNormal];
        [footerView.btnCenter setTitle:@"联系商家" forState:UIControlStateNormal];
    }else if ([@"待发货" isEqualToString:statusStr]) {
        footerView.lbPrice.hidden = YES;
        footerView.lbQuantity.hidden = YES;
        footerView.lbPrepare.hidden = NO;
        footerView.btnLeft.hidden = YES;
        footerView.btnCenter.hidden = YES;
        [footerView.btnRight setTitle:@"联系商家" forState:UIControlStateNormal];
    }else if ([@"卖家已发货" isEqualToString:statusStr]) {
        [footerView.btnLeft setTitle:@"联系商家" forState:UIControlStateNormal];
        [footerView.btnCenter setTitle:@"查看物流" forState:UIControlStateNormal];
        [footerView.btnRight setTitle:@"确认收货" forState:UIControlStateNormal];
    }else if ([@"已完成" isEqualToString:statusStr]) {
        footerView.btnLeft.hidden = YES;
        footerView.btnCenter.hidden = YES;
        [footerView.btnRight setTitle:@"去评价" forState:UIControlStateNormal];
    }else if ([statusStr containsString:@"退货"]||[statusStr containsString:@"退款"]||[statusStr containsString:@"换货"]||[statusStr containsString:@"仅退款"]) {
        footerView.btnLeft.hidden = YES;
        footerView.btnCenter.hidden = YES;
        [footerView.btnRight setTitle:@"查看详情" forState:UIControlStateNormal];
    }

    [footerView.btnRight addTarget:self action:@selector(btnRightAndbtnCenterAndBntLeftClick:) forControlEvents:UIControlEventTouchUpInside];
    [footerView.btnCenter addTarget:self action:@selector(btnRightAndbtnCenterAndBntLeftClick:) forControlEvents:UIControlEventTouchUpInside];
    [footerView.btnLeft addTarget:self action:@selector(btnRightAndbtnCenterAndBntLeftClick:) forControlEvents:UIControlEventTouchUpInside];
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 110;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OnlineModel *model = self.orderListModelArray[indexPath.section];
    OnlineProListModel *shopModel = model.productList[indexPath.row];
    if ([@"OrderAfterSale" isEqualToString:model.orderStatus]) {
        SeeReplaceViewController *replaceVC = [[SeeReplaceViewController alloc] init];
        replaceVC.orderListModelArray = model.productList;
        replaceVC.asoId = shopModel.asoId;
        [self.navigationController pushViewController:replaceVC animated:YES];
    }else {
        NSString *orderStatus = model.orderStatus;
        OrderDetailsViewController *orderDetailsVC = [[OrderDetailsViewController alloc] init];
        orderDetailsVC.orderId = model.orderId;
        orderDetailsVC.index = indexPath.row;
        orderDetailsVC.orderStatus = orderStatus;
        [self.navigationController pushViewController:orderDetailsVC animated:YES];
    }
}

- (NSString *)displayDisponse:(OnlineModel *)model
{
    NSString *orderStatus = model.orderStatus;
    NSString *sendStatus = [model.sendStatus description];
    NSString *backString = @"";
    if ([@"OrderApproved" isEqualToString:orderStatus] && [@"1" isEqualToString:sendStatus]) {
        backString = @"卖家已发货";
    }else if ([@"OrderApproved" isEqualToString:orderStatus] && [@"0" isEqualToString:sendStatus]) {
        backString = @"待发货";
    }else if ([@"OrderPlaced" isEqualToString:orderStatus]) {
        backString = @"待付款";
    }else if ([@"OrderCancelled" isEqualToString:orderStatus]) {
        backString = @"已取消";
    }else if ([@"OrderCompleted" isEqualToString:orderStatus]) {
        backString = @"已完成";
    }else if ([@"OrderAfterSale" isEqualToString:orderStatus]) {
        backString = @"退货/退款";
        if (model.productList.count >0) {
            OnlineProListModel *shopModel = model.productList[0];
            backString = shopModel.afterSidStr;
        }
    }
    return backString;
}

#pragma mark -按钮点击方法
- (void)btnRightAndbtnCenterAndBntLeftClick:(UIButton *)sender
{
    if ([sender.titleLabel.text isEqualToString:@"联系商家"]) {
        [self contactBusinessWithBtn:sender];
    }else if ([sender.titleLabel.text isEqualToString:@"去付款"]) {
        [self goToCloseAnAccountWithBtn:sender];
    }else if ([sender.titleLabel.text isEqualToString:@"去评价"]) {
        [self goToCommentWithBtn:sender];
    }else if ([sender.titleLabel.text isEqualToString:@"确认收货"]) {
        [self confirmReceiptWithBtn:sender];
    }else if ([sender.titleLabel.text isEqualToString:@"查看物流"]) {
        [self seeLogisticsWithBtn:sender];
    }else if ([sender.titleLabel.text isEqualToString:@"查看详情"]) {
        [self seeOrderDetailsWithBtn:sender];
    }
}
/** 去付款 */
- (void)goToCloseAnAccountWithBtn:(UIButton *)sender {
    PayOderViewController *payVC = [[PayOderViewController alloc]init];
    payVC.orderId = [NSString stringWithFormat:@"%lu", (long)sender.tag];
    [self.navigationController pushViewController:payVC animated:YES];
}
/** 联系商家 */
- (void)contactBusinessWithBtn:(UIButton *)sender {
   NSString *phone = objc_getAssociatedObject(self, @"phoneNumber");
    [self showAlertWithTitle:@"联系商家" message:phone AscertainHandler:^(UIAlertAction *action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", phone]]];
    } CancelHandler:^(UIAlertAction *action) {

    }];
}
/** 去评价 */
- (void)goToCommentWithBtn:(UIButton *)sender {
    CommentViewController *commentVC = [[CommentViewController alloc] init];
    commentVC.orderId = [NSString stringWithFormat:@"%lu", sender.tag];
    commentVC.isHotelOrder = NO;
    [self.navigationController pushViewController:commentVC animated:YES];
}
/** 确认收货 */
- (void)confirmReceiptWithBtn:(UIButton *)sender {
    NSDictionary *parameter = @{@"orderId":[NSString stringWithFormat:@"%lu", (long)sender.tag]};
    [[BDNetworkTools sharedInstance] putOnlineOrderVerifyWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        [self.tableView.mj_header beginRefreshing];
    }];
}
/** 查看物流 */
- (void)seeLogisticsWithBtn:(UIButton *)sender {
    ExpressInformationViewController *expressVC = [[ExpressInformationViewController alloc] init];
    expressVC.orderId = [NSString stringWithFormat:@"%lu", (long)sender.tag];
    [self.navigationController pushViewController:expressVC animated:YES];
}
/** 查看详情 */
- (void)seeOrderDetailsWithBtn:(UIButton *)sender {
    NSArray *productList = objc_getAssociatedObject(self, @"productList");
    NSString *asoId = objc_getAssociatedObject(sender, @"asoId");
    if (productList.count==0 || asoId == nil) {
        return;
    }
    SeeReplaceViewController *replaceVC = [[SeeReplaceViewController alloc] init];
    replaceVC.orderListModelArray = productList;
    replaceVC.asoId = asoId;
    [self.navigationController pushViewController:replaceVC animated:YES];
}



// 添加上拉加载和下拉刷新
- (void)addRefresh
{
    __weak typeof(self) weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.pageIndex = 0;
        self.isRefresh = YES;
        [weakSelf requestNetworkData];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.pageIndex++;
        self.isRefresh = NO;
        [weakSelf requestNetworkData];
    }];
}

- (NSMutableArray *)orderListDataArray
{
    if (!_orderListModelArray) {
        self.orderListModelArray = [NSMutableArray array]; 
    }
    return _orderListModelArray;
}

#pragma mark --请求网络数据--
- (void)requestNetworkData
{
    [SVProgressHUD show];
    NSDictionary *paramDic =  @{
                                @"orderType":_orderType,
                                @"pageIndex":[NSString stringWithFormat:@"%lu", (unsigned long)_pageIndex],
                                @"pageSize":@"20",
                                @"sendStatus":_sendStatus==nil? @"":_sendStatus,
                                @"commentStatus":_commentStatus==nil? @"":_commentStatus
                                };
    [[BDNetworkTools sharedInstance] getOnlineListWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        [SVProgressHUD dismiss];
        if (_isRefresh == YES) {
            [self.orderListModelArray removeAllObjects];
        }
        NSArray *arrOrderList = [responseObject objectForKey:@"orderList"];
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *dict in arrOrderList) {
            OnlineModel *model = [[OnlineModel alloc]init];
            [model setValuesForKeysWithDictionary:dict];
            [tempArray addObject:model];
        }
        self.orderListModelArray = tempArray;
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        if (self.orderListModelArray.count < 20) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }else {
            [self.tableView.mj_footer endRefreshing];
        }
    }];
}



@end
