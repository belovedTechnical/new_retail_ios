//
//  ReplacePurchaseViewController.m
//  BelovedHotel
//
//  Created by BDSir on 2018/3/27.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "ReplacePurchaseViewController.h"

@interface ReplacePurchaseViewController ()

@end

@implementation ReplacePurchaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"退货/退款";
    // 退货/退款
    self.orderType = @"OrderAfterSale";
    [self requestNetworkData];
}

@end
