//
//  ToBeDeliveredViewController.m
//  BelovedHotel
//
//  Created by BDSir on 2018/3/27.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "ToBeDeliveredViewController.h"

@interface ToBeDeliveredViewController ()

@end

@implementation ToBeDeliveredViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"待发货";
    // 待发货
    self.orderType = @"OrderApproved";
    self.sendStatus = @"0";
    [self requestNetworkData];
}



@end
