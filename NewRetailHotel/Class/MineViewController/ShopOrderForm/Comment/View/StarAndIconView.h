//
//  StarAndIconView.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/26.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StarAndIconView : UIView
@property (nonatomic, weak) UIImageView *imageView;
@property (nonatomic, strong) NSString *grade;
@end
