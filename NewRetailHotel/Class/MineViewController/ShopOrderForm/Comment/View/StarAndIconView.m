//
//  StarAndIconView.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/26.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "StarAndIconView.h"
#import "RatingBarView.h"

@interface StarAndIconView()<RatingBarDelegate>
@property (nonatomic, strong) RatingBarView *ratingView;
@property (nonatomic, weak) UILabel *lb;
@end
@implementation StarAndIconView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat margin = 10;
    CGFloat lbH = 30;
    self.imageView.frame = CGRectMake(margin, margin, 80, 80);
    self.lb.frame = CGRectMake(_imageView.bd_right+10, _imageView.bd_centerY-lbH/2, 60, lbH);
    self.ratingView.frame = CGRectMake(_lb.bd_right, _lb.bd_top+5, 150, lbH);
}

- (void)initUI {
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.backgroundColor = randomColor;
    [self addSubview:imageView];
    self.imageView = imageView;
    
    UILabel *lb = [[UILabel alloc] init];
    lb.text = @"满意度";
    [self addSubview:lb];
    self.lb = lb;
    
    self.ratingView = [[RatingBarView alloc] init];
    self.ratingView.isIndicator = NO;
    [self.ratingView setImageDeselected:@"ratingbar_unselected" halfSelected:@"ratingbar_selected" fullSelected:@"ratingbar_selected" andDelegate:self];
    [self addSubview:_ratingView];
}

#pragma mark - <RatingBarDelegate>评分协议
-(void)ratingBar:(RatingBarView *)ratingBar ratingChanged:(float)newRating{
    float current = 0;
    
    if (newRating == 0.5 ||newRating == 1.5||newRating == 2.5||newRating == 3.5||newRating == 4.5) {
        current = newRating + 0.5;
        self.grade = [NSString stringWithFormat:@"%.1f",current];
//        self.lb.text = [NSString stringWithFormat:@"%.1f",current];
    }else{
//        self.lb.text = [NSString stringWithFormat:@"%.1f",newRating];
        self.grade = [NSString stringWithFormat:@"%.1f",newRating];
    }
}

@end
