//
//  SelectedImageView.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/26.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "SelectedImageView.h"
#import "BDFilePathImage.h"
#import "QiniuSDK.h"

@interface SelectedImageView()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (nonatomic, strong) UIImagePickerController *imgPickerCon;
@property (nonatomic, strong) UIButton *selectButton;
@property (nonatomic, strong) NSString *qiNiuToken;
@end
@implementation SelectedImageView
- (UIImagePickerController *)imgPickerCon {
    if (!_imgPickerCon) {
        _imgPickerCon = [[UIImagePickerController alloc] init];
        _imgPickerCon.delegate = self;
        _imgPickerCon.allowsEditing = YES;
    }
    return _imgPickerCon;
}

- (NSMutableArray *)qiNiuImageKeyArray {
    if (!_qiNiuImageKeyArray) {
        _qiNiuImageKeyArray = [NSMutableArray array];
    }
    return _qiNiuImageKeyArray;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initUI];
        [self getQiNiuYunToken];
    }
    return self;
}

- (void)initUI {
    CGFloat btnWH = 70;
    for (int i=0; i<3; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(i*(btnWH+20)+20, 0, btnWH, btnWH);
        btn.tag = i;
        [btn setBackgroundImage:[UIImage imageNamed:@"add_icon"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(addImageBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];

}

- (void)addImageBtnClick:(UIButton *)sender {
    
    self.selectButton = sender;
    if (self.addImageBlock) {
        self.addImageBlock(sender);
    }
    self.imgPickerCon.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [[self viewController:self] presentViewController:self.imgPickerCon animated:YES completion:nil];
    // 判断相机是否可用
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
    }
}

- (UIViewController*)viewController:(UIView *)view {
    
    for (UIView* next = [view superview]; next; next = next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController*)nextResponder;
        }
    }
    return nil;
}

#pragma mark -UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [SVProgressHUD showWithStatus:@"图片加载中"];
    [self.imgPickerCon dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    [self.selectButton setBackgroundImage:image forState:UIControlStateNormal];
    
    NSString *imagePath = [BDFilePathImage getImagePathWithIamge:image];
    [self imageUploadToQiNiuWithPath:imagePath];
}

#pragma mark -网络请求
// 上传图片到七牛云
- (void)imageUploadToQiNiuWithPath:(NSString *)filePath {
    QNUploadManager *qnUpManager = [[QNUploadManager alloc] init];
    QNUploadOption *uploadOption = [[QNUploadOption alloc] initWithMime:nil                      progressHandler:^(NSString *key, float percent) {}
                                                                 params:nil
                                                               checkCrc:NO
                                                     cancellationSignal:nil];
    [qnUpManager putFile:filePath key:nil token:_qiNiuToken complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
        
        NSString *imgKey = [resp objectForKey:@"key"];
        [self.qiNiuImageKeyArray addObject:imgKey];
        [SVProgressHUD dismiss];
    } option:uploadOption];
}

// 获取七牛云上传token
- (void)getQiNiuYunToken {
    [[BDNetworkTools sharedInstance] getQiNiuYunTokenWithParameter:nil Block:^(NSDictionary *responseObject, NSError *error) {
        self.qiNiuToken = [responseObject objectForKey:@"token"];
    }];
}




@end
