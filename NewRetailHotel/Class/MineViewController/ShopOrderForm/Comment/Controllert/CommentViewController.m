//
//  CommentViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/26.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "CommentViewController.h"
#import "SelectedImageView.h"
#import "StarAndIconView.h"
#import "UIImageView+WebCache.h"

@interface CommentViewController ()<UITextViewDelegate>
@property (nonatomic, strong) SelectedImageView *selectedImageView;
@property (nonatomic, strong) StarAndIconView *starAndIconView;
@property (nonatomic, strong) UILabel *lbPlaceholder;
@property (nonatomic, strong) NSString *productStoreId;
@property (nonatomic, strong) NSString *productImg;
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UIButton *btnRefer;
@end

@implementation CommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"订单评价";
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self initUI];
    if (_isHotelOrder ==NO) {
        [self loadShopDetailsData];
    }
}

- (void)initUI {
    // 商品图片 星级 View
    self.starAndIconView = [[StarAndIconView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, kScreenW, 100)];
    [self.view addSubview:_starAndIconView];
    
    // 评论文字
    UITextView *textView = [[UITextView alloc] init];
    textView.frame = CGRectMake(0, self.starAndIconView.bd_bottom+20, kScreenW, 100);
    textView.backgroundColor = [UIColor whiteColor];
    textView.delegate = self;
    [self.view addSubview:textView];
    self.textView = textView;
    
    UILabel *lbPlaceholder = [[UILabel alloc] init];
    lbPlaceholder.frame = CGRectMake(10, 0, textView.bd_width, 50);
    lbPlaceholder.textColor = [UIColor lightGrayColor];
    lbPlaceholder.text = @"评价一下，可获得积分，1到100个字之间";
    lbPlaceholder.font = [UIFont systemFontOfSize:13];
    lbPlaceholder.numberOfLines = 0;
    [textView addSubview:lbPlaceholder];
    self.lbPlaceholder = lbPlaceholder;
   
    // 选择照片
    SelectedImageView *selectedImageView = [[SelectedImageView alloc] init];
    selectedImageView.frame = CGRectMake(0, textView.bd_bottom+10, kScreenW, 80);
    [self.view addSubview:selectedImageView];
    self.selectedImageView = selectedImageView;
    
    // 提交评价
    UIButton *btnRefer = [UIButton buttonWithType:UIButtonTypeCustom];
    btnRefer.frame = CGRectMake(30, selectedImageView.bd_bottom+20, kScreenW-30*2, 50);
    [btnRefer setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnRefer addTarget:self action:@selector(btnReferClick:) forControlEvents:UIControlEventTouchUpInside];
    [btnRefer setTitle:@"提交评价" forState:UIControlStateNormal];
    btnRefer.backgroundColor = [UIColor redColor];
    [self.view addSubview:btnRefer];
    self.btnRefer = btnRefer;
}

// 提交按钮点击
- (void)btnReferClick:(UIButton *)btn {
    if (_isHotelOrder == YES) {
        [self requestNetHotelOrderComment];
    }else {
        [self requestNetCommentCreat];
    }
    self.btnRefer.userInteractionEnabled = NO;
}

#pragma mark -UITextViewDelegate
//将要开始编辑
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    self.lbPlaceholder.hidden = YES;
    return YES;
}

//将要结束编辑
- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    if (textView.text.length == 0) {
        self.lbPlaceholder.hidden = NO;
    }
    return YES;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark -网络请求
// 根据商品id请求详情
- (void)loadShopDetailsData
{
    [SVProgressHUD show];
    NSDictionary *parameter = @{@"orderId":self.orderId};
    [[BDNetworkTools sharedInstance] getOnlineListDetailWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        [SVProgressHUD dismiss];
        self.productStoreId = [responseObject objectForKey:@"productStoreId"];
        NSArray *listArray = [responseObject objectForKey:@"productList"];
        for (NSDictionary *dict in listArray) {
           NSString *image = [dict objectForKey:@"productImg"];
            [self.starAndIconView.imageView sd_setImageWithURL:[NSURL URLWithString:image]];
        }
    }];
}

// 电商创建评论
- (void)requestNetCommentCreat {
    NSMutableArray *listArray = [NSMutableArray array];
    NSDictionary *dict = @{
                           @"productId":_orderId,
                           @"commentContent":self.textView.text,
                           @"commentLevel":self.starAndIconView.grade,
                           @"productCommentImage":self.selectedImageView.qiNiuImageKeyArray.count==0?@"":self.selectedImageView.qiNiuImageKeyArray
                           };
    [listArray addObject:dict];
    NSData *data = [NSJSONSerialization dataWithJSONObject:listArray options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSDictionary *parameter = @{
                                @"orderId":self.orderId,
                                @"productStoreId":self.productStoreId,
                                @"commentContent":self.textView.text,
                                @"commentList":jsonStr
                                };
    [[BDNetworkTools sharedInstance] postCommentShopCreatParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        self.btnRefer.userInteractionEnabled = YES;
        BDLog(@"%@", responseObject);
        [self showAlertWithTitle:@"评论成功" message:@"" AscertainHandler:^(UIAlertAction *action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }];
}

// 创建酒店评论
- (void)requestNetHotelOrderComment {
    NSString *imageKay = @"";
    if (self.selectedImageView.qiNiuImageKeyArray.count > 0) {
        imageKay = [self.selectedImageView.qiNiuImageKeyArray componentsJoinedByString:@","];
    }
    NSDictionary *parameter = @{
                                @"orderId":_orderId,
                                @"commentContent":self.textView.text,
                                @"commentLevel":self.starAndIconView.grade,
                                @"productCommentImage":imageKay
                                };
    [[BDNetworkTools sharedInstance] postCommentGetHotelParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        
    }];
}

@end
