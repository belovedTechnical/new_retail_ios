//
//  CommentViewController.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/26.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentViewController : UIViewController
@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, assign) BOOL isHotelOrder;
@end
