//
//  OrderFormHeaderView.m
//  BelovedHotel
//
//  Created by BDSir on 2018/3/28.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "OrderFormHeaderView.h"

@interface OrderFormHeaderView()

@end

@implementation OrderFormHeaderView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self initUI];
    }
    return self;
}

- (void)initUI {

    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:topView];
    
    UIButton *btnStore = [[UIButton alloc] init];
    [btnStore setTitleColor:[UIColor colorWithHexString:@"#222222"] forState:UIControlStateNormal];
    [btnStore setImage:[UIImage imageNamed:@"store"] forState:UIControlStateNormal];
//    [btnStore setTitle:@" 小米手机官方商城" forState:UIControlStateNormal];
    btnStore.titleLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:btnStore];
    self.btnStore = btnStore;

    UIImageView *directIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
    [self addSubview:directIcon];
    
    UILabel *lbStatus = [[UILabel alloc] init];
    lbStatus.textColor = [UIColor colorWithHexString:@"#b4272d"];
    lbStatus.textAlignment = NSTextAlignmentRight;
    lbStatus.font = [UIFont systemFontOfSize:14];
//    lbStatus.text = @"待付款";
    [self addSubview:lbStatus];
    self.lbStatus = lbStatus;
    
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(0);
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.height.equalTo(@5);
    }];
    
    [btnStore mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(5);
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.left.equalTo(self.mas_left).offset(15);
        make.right.equalTo(directIcon.mas_left).offset(-10);
    }];
    
    [directIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(btnStore.mas_centerY);
        make.left.equalTo(btnStore.mas_right).offset(10);
//        make.height.equalTo(@20);
//        make.width.equalTo(@12);
    }];

    [lbStatus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(5);
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.right.equalTo(self.mas_right).offset(-15);
        make.width.equalTo(@100);
    }];
}

@end
