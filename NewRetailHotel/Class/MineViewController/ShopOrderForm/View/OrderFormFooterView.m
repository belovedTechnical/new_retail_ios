//
//  OrderFormFooterView.m
//  BelovedHotel
//
//  Created by BDSir on 2018/3/28.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "OrderFormFooterView.h"

@interface OrderFormFooterView()

@end

@implementation OrderFormFooterView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self initUI];
    }
    return self;
}

- (void)initUI {
    
    UILabel *lbPrepare = [[UILabel alloc] init];
    lbPrepare.textColor = [UIColor colorWithHexString:@"#333333"];
    lbPrepare.textAlignment = NSTextAlignmentCenter;
    lbPrepare.font = [UIFont systemFontOfSize:12];
    lbPrepare.text = @"卖家正在安排发货，请耐心等待";
    lbPrepare.hidden = YES;
    [self addSubview:lbPrepare];
    self.lbPrepare = lbPrepare;
    
    UILabel *lbQuantity = [[UILabel alloc] init];
    lbQuantity.textColor = [UIColor colorWithHexString:@"#222222"];
    lbQuantity.textAlignment = NSTextAlignmentRight;
    lbQuantity.font = [UIFont systemFontOfSize:12];
    lbQuantity.text = @"共2件商品";
    [self addSubview:lbQuantity];
    self.lbQuantity = lbQuantity;
    
    UILabel *lbPrice = [[UILabel alloc] init];
    lbPrice.textColor = [UIColor colorWithHexString:@"#222222"];
    lbPrice.textAlignment = NSTextAlignmentLeft;
    lbPrice.font = [UIFont systemFontOfSize:15];
    lbPrice.text = @"合计:¥11376.00";
    [self addSubview:lbPrice];
    self.lbPrice = lbPrice;
    
    UIView *line = [[UIView alloc] init];
    line.backgroundColor = [UIColor colorWithHexString:@"#999999"];
    [self addSubview:line];
    
    UIButton *btnRight = [self bottomFunctionBtn];
    [btnRight setTitle:@"联系商家" forState:UIControlStateNormal];
    UIButton *btnCenter = [self bottomFunctionBtn];
    [btnCenter setTitle:@"去付款" forState:UIControlStateNormal];
    UIButton *btnLeft = [self bottomFunctionBtn];
    [btnLeft setTitle:@"查看详情" forState:UIControlStateNormal];
    self.btnLeft = btnLeft;
    self.btnRight = btnRight;
    self.btnCenter = btnCenter;
    
    UIView *spaceView = [[UIView alloc] init];
    spaceView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:spaceView];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY).offset(-15);
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.height.equalTo(@1);
    }];
    
    [lbPrepare mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(0);
        make.bottom.equalTo(line.mas_top).offset(0);
        make.left.equalTo(self.mas_left).offset(10);
        make.right.equalTo(self.mas_right).offset(-10);
    }];
    
    [lbPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(0);
        make.bottom.equalTo(line.mas_top).offset(0);
        make.right.equalTo(self.mas_right).offset(-10);
        make.width.equalTo(@130);
    }];
    
    [lbQuantity mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(0);
        make.bottom.equalTo(line.mas_top).offset(0);
        make.right.equalTo(lbPrice.mas_left).offset(-10);
        make.width.equalTo(@100);
    }];
    
    [btnRight mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(line.mas_bottom).offset(10);
        make.right.equalTo(self.mas_right).offset(-10);
        make.height.equalTo(@29);
        make.width.equalTo(@71);
    }];
    
    [btnCenter mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(btnRight.mas_height);
        make.width.equalTo(btnRight.mas_width);
        make.top.equalTo(btnRight.mas_top);
        make.right.equalTo(btnRight.mas_left).offset(-10);
    }];
    
    [btnLeft mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(btnRight.mas_height);
        make.width.equalTo(btnRight.mas_width);
        make.top.equalTo(btnRight.mas_top);
        make.right.equalTo(btnCenter.mas_left).offset(-10);
    }];
    
    [spaceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom).offset(0);
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.height.equalTo(@20);
    }];
}

- (UIButton *)bottomFunctionBtn
{
    UIButton *btnFunction = [[UIButton alloc] init];
    [btnFunction setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
    [btnFunction setTitleColor:[UIColor colorWithHexString:@"#cc3333"] forState:UIControlStateHighlighted];
    btnFunction.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
    btnFunction.titleLabel.font = [UIFont systemFontOfSize:14];
    btnFunction.layer.masksToBounds = YES;
    btnFunction.layer.cornerRadius = 1.5;
    btnFunction.layer.borderWidth = 1;
    [self addSubview:btnFunction];
    return btnFunction;
}

@end
