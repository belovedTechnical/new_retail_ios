//
//  OrderFormFooterView.h
//  BelovedHotel
//
//  Created by BDSir on 2018/3/28.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderFormFooterView : UITableViewHeaderFooterView
@property (nonatomic, weak) UILabel *lbPrepare;
@property (nonatomic, weak) UILabel *lbQuantity;
@property (nonatomic, weak) UILabel *lbPrice;
@property (nonatomic, weak) UIButton *btnRight;
@property (nonatomic, weak) UIButton *btnCenter;
@property (nonatomic, weak) UIButton *btnLeft;

@end
