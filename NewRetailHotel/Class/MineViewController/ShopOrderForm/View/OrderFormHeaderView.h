//
//  OrderFormHeaderView.h
//  BelovedHotel
//
//  Created by BDSir on 2018/3/28.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderFormHeaderView : UITableViewHeaderFooterView

@property (nonatomic, weak) UIButton *btnStore;
@property (nonatomic, weak) UILabel *lbStatus;

@end
