//
//  OrderFromViewCell.m
//  BelovedHotel
//
//  Created by BDSir on 2018/3/28.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import "OrderFromViewCell.h"
#import "OnlineProListModel.h"
#import "UIImageView+WebCache.h"

@interface OrderFromViewCell()

@property (nonatomic, weak) UIImageView *shopImageView;
@property (nonatomic, weak) UILabel *lbShopName;
@property (nonatomic, weak) UILabel *lbMoney;
@property (nonatomic, weak) UILabel *lbSpecification;
@property (nonatomic, weak) UILabel *quantityLb;

@end

@implementation OrderFromViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}

- (void)setListModel:(OnlineProListModel *)listModel {
    _listModel = listModel;
    NSString *imgURLStr = [NSString stringWithFormat:@"%@", listModel.productImg];
    [self.shopImageView sd_setImageWithURL:[NSURL URLWithString:imgURLStr]];
    self.lbMoney.text = [NSString stringWithFormat:@"¥%@",listModel.productPrice];
    self.quantityLb.text = [NSString stringWithFormat:@"x%@",listModel.quantity];
    self.lbShopName.text = [NSString stringWithFormat:@"%@",listModel.productName];
    self.lbSpecification.text  = [NSString stringWithFormat:@"%@",listModel.featureName];
}

- (void)initUI {
    UIImageView *shopImageView = [[UIImageView alloc] init];
    shopImageView.backgroundColor = randomColor;
    [self addSubview:shopImageView];
    self.shopImageView = shopImageView;
    
    UILabel *lbShopName = [[UILabel alloc] init];
    lbShopName.textColor = [UIColor colorWithHexString:@"#222222"];
    lbShopName.font = [UIFont systemFontOfSize:15];
    lbShopName.textAlignment = NSTextAlignmentLeft;
    lbShopName.numberOfLines = 0;
//    lbShopName.text = @"苹果Apple iPhone7手机黑色移动电信";
    [self addSubview:lbShopName];
    self.lbShopName = lbShopName;
    
    UILabel *lbMoney = [[UILabel alloc] init];
    lbMoney.textColor = [UIColor colorWithHexString:@"#222222"];
    lbMoney.font = [UIFont systemFontOfSize:12];
    lbMoney.textAlignment = NSTextAlignmentRight;
//    lbMoney.text = @"¥5688.00";
    [self addSubview:lbMoney];
    self.lbMoney = lbMoney;
    
    UILabel *lbSpecification = [[UILabel alloc] init];
    lbSpecification.textColor = [UIColor colorWithHexString:@"#999999"];
    lbSpecification.font = [UIFont systemFontOfSize:12];
    lbSpecification.textAlignment = NSTextAlignmentLeft;
//    lbSpecification.text = @"颜色:银色 版本:全网通4G";
    [self addSubview:lbSpecification];
    self.lbSpecification = lbSpecification;
    
    UILabel *quantityLb = [[UILabel alloc] init];
    quantityLb.textColor = [UIColor colorWithHexString:@"#999999"];
    quantityLb.font = [UIFont systemFontOfSize:12];
    quantityLb.textAlignment = NSTextAlignmentRight;
    quantityLb.text = @"x1";
    [self addSubview:quantityLb];
    self.quantityLb = quantityLb;
    
    [shopImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(10);
        make.left.equalTo(self.mas_left).offset(15);
        make.height.equalTo(@73);
        make.width.equalTo(@73);
    }];
    
    [lbShopName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(8);
        make.left.equalTo(shopImageView.mas_right).offset(10);
        make.right.equalTo(self.mas_right).offset(-80);
        make.height.equalTo(@50);
    }];

    [lbMoney mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lbShopName.mas_top).offset(0);
        make.right.equalTo(self.mas_right).offset(-10);
        make.left.equalTo(lbShopName.mas_right).offset(0);
        make.height.equalTo(@23);
    }];

    [lbSpecification mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lbShopName.mas_bottom).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(-10);
        make.left.equalTo(lbShopName.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(-50);
    }];

    [quantityLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lbSpecification.mas_top).offset(0);
        make.left.equalTo(lbSpecification.mas_right).offset(0);
        make.right.equalTo(self.mas_right).offset(-10);
        make.height.equalTo(@23);
    }];
}



@end
