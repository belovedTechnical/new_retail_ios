//
//  OrderFromViewCell.h
//  BelovedHotel
//
//  Created by BDSir on 2018/3/28.
//  Copyright © 2018年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OnlineProListModel;
@interface OrderFromViewCell : UITableViewCell

@property (nonatomic, strong) OnlineProListModel *listModel;

@end
