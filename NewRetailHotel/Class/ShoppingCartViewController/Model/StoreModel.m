//
//  StoreModel.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/23.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "StoreModel.h"
#import "ShopBuyModel.h"

@implementation StoreModel

- (void)setCartItems:(NSMutableArray *)cartItems{
    NSMutableArray *temArr = [NSMutableArray array];
    for (NSDictionary *dic in cartItems) {
        ShopBuyModel *model = [[ShopBuyModel alloc]init];
        [model setValuesForKeysWithDictionary:dic];
        self.item = model;
        [temArr addObject:model];
    }
    
    _cartItems = [NSMutableArray arrayWithArray:temArr];
}

@end
