//
//  StoreModel.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/23.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ShopBuyModel;
@interface StoreModel : NSObject

@property (assign,nonatomic) BOOL select;
@property (nonatomic,strong) NSMutableArray *cartItems;
@property (nonatomic,assign) NSInteger productAmount;
@property (nonatomic,copy) NSString *productPrice;
@property (nonatomic,copy) NSString * productStoreId;
@property (nonatomic,copy) NSString *storeName;
@property(nonatomic, strong) ShopBuyModel *item;


- (void)setCartItems:(NSMutableArray *)cartItems;

@end
