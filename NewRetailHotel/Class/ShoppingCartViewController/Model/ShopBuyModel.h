//
//  ShopBuyModel.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/13.
//  Copyright © 2016年 至爱. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface ShopBuyModel : NSObject

@property (nonatomic,copy) NSString *imageUrl;
@property (nonatomic,copy) NSString *productId;
@property (nonatomic,copy) NSString *productName;
@property (nonatomic,assign)NSInteger quantity;
@property (nonatomic,copy) NSString *spec;
@property (nonatomic,assign)float unitPrice;
@property (nonatomic, copy) NSString *productStatus;

@property (nonatomic,assign) BOOL select;

@end
