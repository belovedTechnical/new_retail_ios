//
//  ShoppingCartViewController.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/7.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingCartViewController : UIViewController

@property (nonatomic, assign) BOOL isRootVC;
@property(nonatomic, strong) NSString *storeID;

@end
