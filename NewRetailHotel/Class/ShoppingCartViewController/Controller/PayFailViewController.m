//
//  PayFailViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/12/2.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "PayFailViewController.h"
//#import "RechargeViewController.h"

@interface PayFailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *orderNum;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;

@end

@implementation PayFailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"支付失败";
   
    self.orderNum.text = [NSString stringWithFormat:@"订单号:%@",self.orderID];
    self.priceLab.text = [NSString stringWithFormat:@"订单金额:%@",self.price];
 
}
- (IBAction)finishBtn:(UIButton *)sender {
    
    if ([self.vcTitle isEqualToString:@"充值"]) {
//        RechargeViewController *reVC = [[RechargeViewController alloc]init];
//        [self.navigationController pushViewController:reVC animated:YES];
    }else if([self.vcTitle isEqualToString:@"支付订单"]){
        self.navigationController.tabBarController.selectedIndex = 4;
        [self.navigationController popToRootViewControllerAnimated:YES];
//        OnlineProductListViewController *listVC = [[OnlineProductListViewController alloc] init];
//        [self.navigationController pushViewController:listVC animated:YES];
        
    }else{
        

    }
}


@end
