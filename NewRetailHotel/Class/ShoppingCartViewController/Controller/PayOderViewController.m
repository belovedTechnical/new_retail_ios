//
//  PayOderViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/21.
//  Copyright © 2016年 至爱. All rights reserved.
//
//#define kWeChatURLScheme @"wx4b2322692fe85433"
//#define kAliPayURLScheme @"AliPay2016080501709547"
#import "PayOderViewController.h"
#import "PayHotelOrderCell.h"
#import "BDNetworkTools.h"
#import "PayStatusViewController.h"
#import "PayFailViewController.h"
#import "MinesViewController.h"
#import <AlipaySDK/AlipaySDK.h>
#import "WXApi.h"
#import "WeChatAPIParameterModel.h"

@interface PayOderViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    BOOL _jump;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, copy) NSString *apikey;
@property (nonatomic, strong) NSDictionary *dic;
@property (nonatomic, strong) NSDictionary *payDic;
@property (nonatomic, copy) NSString *payType;// 支付类型 支付宝或微信
@property (nonatomic, copy) NSString *isPoi;// 积分 Y N
@property (nonatomic, copy) NSString *isBal;//  我的余额 Y N

@property (nonatomic, assign) CGFloat balance;
@property (nonatomic, assign) CGFloat points;
@property (nonatomic, assign) CGFloat payPrice;

@property (nonatomic, assign) CGFloat shengyuPrice;


@end

@implementation PayOderViewController
#pragma mark -懒加载
- (NSDictionary *)dic{
    if (!_dic) {
        self.dic = [NSDictionary dictionary];
    }
    return _dic;
}
- (NSDictionary *)payDic{
    if (!_payDic) {
        self.payDic = [NSDictionary dictionary];
    }
    return _payDic;
}

#pragma mark -程序加载
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"支付订单";
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    self.apikey = [user objectForKey:@"apikey"];
    
    _jump = NO;
   
    [self loadOrderData];
    _tableView.tableFooterView = [[UIView alloc]init];
    //去掉分割线
    _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"PayHotelOrderCell" bundle:nil] forCellReuseIdentifier:@"PayHotelOrderCell"];
    
    /// 支付成功通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paySuccess) name:@"paySuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(achievePay) name:PAY_NOTIFICATION object:@"success"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 316;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PayHotelOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PayHotelOrderCell"];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if ( self.payDic.count >0) {
         [cell setCellWith:self.dic withPayDic:self.payDic];
    }
    __weak PayHotelOrderCell *cell1 = cell;
    cell.payBtn = ^(NSInteger index){
        switch (index) {
            case kPointBtn_tag:// 积分
            {
                if (_points > 0 && cell1.pointBtn.selected == YES) {
                    if (cell1.balanceBtn.selected == YES) {
                        if ((self.payPrice - self.points- self.balance) <= 0) {
                            [self pointAndBalanceDeductibleHideWith:cell1];
                        }else {
                            cell1.totalPriceLab.text = [NSString stringWithFormat:@"¥%.2f",self.payPrice - self.points- self.balance];
                        }
                    }else {
                        if ((self.payPrice - self.points) <= 0) {
                            cell1.balanceBtn.userInteractionEnabled = NO;
                            [self pointAndBalanceDeductibleHideWith:cell1];
                        }else {
                            if (cell1.zhifuBaoBtn.selected == YES) {
                                cell1.wechatBtn.selected = NO;
                            }else {
                                cell1.wechatBtn.selected = YES;
                            }
                            cell1.totalPriceLab.text = [NSString stringWithFormat:@"¥%.2f",self.payPrice - self.points];
                        }
                    }
                    
                }else {
                    if (_balance > 0 && cell1.balanceBtn.selected == YES) {
                        cell1.totalPriceLab.text = [NSString stringWithFormat:@"¥%.2f",(_payPrice - _balance) <= 0 ? 0.00 : _payPrice - _balance];
                        cell1.pointBtn.selected = NO;
                        [self weChatAndAlipayNoHideWith:cell1];
                    }else {
                        cell1.totalPriceLab.text = [NSString stringWithFormat:@"¥%.2f",_payPrice];
                        cell1.balanceBtn.userInteractionEnabled = YES;
                        cell1.pointBtn.selected = NO;
                        [self weChatAndAlipayNoHideWith:cell1];
                    }
                }
            }
                break;
                // 我的余额
            case kBalanceBtn_tag:
            {
                if (_balance > 0 && cell1.balanceBtn.selected == YES) {
                    if (cell1.pointBtn.selected == YES) {
                        if ((self.payPrice - self.points- self.balance) <= 0) {
                            [self pointAndBalanceDeductibleHideWith:cell1];
                        }else {
                            if (cell1.zhifuBaoBtn.selected == YES) {
                                cell1.wechatBtn.selected = NO;
                            }else {
                                cell1.wechatBtn.selected = YES;
                            }
                            cell1.totalPriceLab.text = [NSString stringWithFormat:@"¥%.2f",self.payPrice - self.points- self.balance];
                        }
                    }else {
                        if ((self.payPrice - self.balance) <= 0) {
                            cell1.pointBtn.userInteractionEnabled = NO;
                            [self pointAndBalanceDeductibleHideWith:cell1];
                        }else {
                            cell1.totalPriceLab.text = [NSString stringWithFormat:@"¥%.2f",self.payPrice - self.balance];
                        }
                    }
                }else {
                    if (_points > 0 && cell1.pointBtn.selected == YES) {
                        cell1.totalPriceLab.text = [NSString stringWithFormat:@"¥%.2f",(_payPrice - _points) <= 0 ? 0.00 : _payPrice - _points];
                        cell1.balanceBtn.selected = NO;
                        [self weChatAndAlipayNoHideWith:cell1];
                    }else {
                        cell1.totalPriceLab.text = [NSString stringWithFormat:@"¥%.2f",_payPrice];
                        cell1.pointBtn.userInteractionEnabled = YES;
                        cell1.balanceBtn.selected = NO;
                        [self weChatAndAlipayNoHideWith:cell1];
                    }
                }
            }
                break;
            case kWechatBtn_tag:
                self.payType = @"WeChat";
                break;
                
            case kAliPayBtn_tag:
                self.payType = @"Alipay";
                break;
                
            case kConfirmPayBtn_tag:
                [self judgingConditionsWith:cell1];
                break;
                
            default:
                break;
        }
    };
    
    return cell;
}

- (void)weChatAndAlipayNoHideWith:(PayHotelOrderCell *)cell1
{
    cell1.wxView.hidden = NO;
    cell1.zhifubaoView.hidden = NO;
    if (cell1.zhifuBaoBtn.selected == YES) {
        cell1.wechatBtn.selected = NO;
    }else {
        cell1.wechatBtn.selected = YES;
    }
}

- (void)pointAndBalanceDeductibleHideWith:(PayHotelOrderCell *)cell1
{
    cell1.totalPriceLab.text = @"¥0.00";
    cell1.zhifuBaoBtn.selected = NO;
    cell1.wechatBtn.selected = NO;
    cell1.wxView.hidden = YES;
    cell1.zhifubaoView.hidden = YES;
}

- (void)judgingConditionsWith:(PayHotelOrderCell *)cell1{
    
    self.isPoi = cell1.pointBtn.selected == YES ? @"Y" : @"N";
    self.isBal = cell1.balanceBtn.selected == YES ? @"Y" : @"N";
    self.payType = cell1.wechatBtn.selected == YES ? @"WeChat" : @"Alipay";
    
    if (_payPrice == 0) {
        [self loadOrderPay];
    }
    // 余额 或 积分 或 相加 可以抵扣商品总价
    if ((self.payPrice <= _points && cell1.pointBtn.selected == YES) || (_payPrice <= _balance && cell1.balanceBtn.selected == YES)) {
            self.payType = @"";
            [self loadOrderPay];
    }else {
        // 支付宝 或 微信 任何一种选择 都可以
        if (cell1.zhifuBaoBtn.selected == YES || cell1.wechatBtn.selected == YES) {
            [self loadOrderPay];
        }else {
            if (_payPrice <= (_points + _balance) && cell1.pointBtn.selected == YES && cell1.balanceBtn.selected == YES) {
                [self loadOrderPay];
            }else {
                [self showMessage:@"请选择微信或支付宝其中一种支付方式"];
            }
        }
    }

}
- (void)showMessage:(NSString *)string{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"" message:string preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertVC addAction:action1];
    [self presentViewController:alertVC animated:YES completion:nil];
}


// 微信/支付宝
- (void)loadOrderPay {
    
    if (_isManyStore == YES) {  // 多店铺支付
        [self manyStoreRequestPay];
        
    }else { // 单店铺支付
        [self singleStoreRequestPay];
    }
}

// pay money
- (void)loadPayStatus:(NSString *)charge{
    
    NSString *urlStr = [NSString string];
    if ([self.payType isEqualToString:@"WeChat"]) {
        urlStr = kWeChatURLScheme;
    }else {
        urlStr = kAliPayURLScheme;
    }
    
}

- (void)showMessageSuceess:(NSString *)string{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"" message:string preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
        self.navigationController.tabBarController.selectedIndex = 4;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
    [alertVC addAction:action1];
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark - pay
- (void)paySuccess {
    if (!_jump) {
        _jump = YES;
        if ([self.payType isEqualToString:@"WeChat"]) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        } else if ([self.payType isEqualToString:@"Alipay"]) {
            PayStatusViewController *payVC = [[PayStatusViewController alloc] init];
            payVC.orderID = self.orderId;
            payVC.price = [self.payDic objectForKey:@"totalPrices"];
            [self.navigationController pushViewController:payVC animated:YES];
        }
    }
}

- (void)achievePay {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -网络请求
#pragma mark -查询订单支付信息
- (void)loadOrderData{
    NSDictionary *paramDic  = @{@"orderId":self.orderId};
    [[BDNetworkTools sharedInstance] getPayOrderInfoWithParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        self.payDic = [NSDictionary dictionaryWithDictionary:responseObject];
        self.payPrice = [[self.payDic objectForKey:@"totalPrice"] floatValue];
        self.balance = [[self.payDic objectForKey:@"balance"] floatValue];
        self.points = [[self.payDic objectForKey:@"points"] floatValue];
        [self.tableView reloadData];
    }];
}

#pragma mark -单店铺支付
- (void)singleStoreRequestPay 
{
    NSDictionary *parameter = @{
                                @"orderId":self.orderId,
                                @"payType":self.payType,
                                @"isAccountPrice":self.isBal,
                                @"clientSource":@"APP",
                                @"isPayPoints":self.isPoi
                                };
    [[BDNetworkTools sharedInstance] putHotelPaymentOrderRoomWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
//        BDLog(@"%@", responseObject);
        if ([self.payType isEqualToString:@"WeChat"]) {
            NSString *acObjStr = [responseObject objectForKey:@"weChatApp"];
            NSDictionary *weChatDict = [self stringTransformDictWithString:acObjStr];
            WeChatAPIParameterModel *wxModel = [WeChatAPIParameterModel weChatAPIParameterModelWithDict:weChatDict];
            //这里调用后台接口获取订单的详细信息，然后调用微信支付方法
            [self WXPayWithAppid:wxModel.appId
                       partnerid:wxModel.partnerId
                        prepayid:wxModel.prepayId
                         package:wxModel.packageValue
                        noncestr:wxModel.nonceStr
                       timestamp:wxModel.timeStamp
                            sign:wxModel.sign];
            
        }else if([self.payType isEqualToString:@"Alipay"]){
            NSString *acObjStr = [responseObject objectForKey:@"alipayApp"];
            NSDictionary *alipayDict = [self stringTransformDictWithString:acObjStr];
            NSString *payOrder = [alipayDict objectForKey:@"payParams"];
            // NOTE: 调用支付结果开始支付18719353007
            [[AlipaySDK defaultService] payOrder:payOrder fromScheme:kAliPayURLScheme callback:^(NSDictionary *resultDic) {
                BDLog(@"reslut = %@",resultDic);
            }];

        }else{
            // 积分和余额支付 支付成功跳转到 我的 界面
            if ([[responseObject objectForKey:@"errmsg"] isEqualToString:@"ok"]) {
                [self showMessageSuceess:@"支付成功"];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }
    }];
}

#pragma mark -多店铺支付
- (void)manyStoreRequestPay
{
    NSDictionary *parameter = @{
                                @"orderId":self.orderId,
                                @"payType":self.payType,
                                @"isAccountPrice":self.isBal,
                                @"clientSource":@"APP",
                                @"isPayPoints":self.isPoi
                                };
    [[BDNetworkTools sharedInstance] postPayManyOrderWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        // 支付成功后跳转到 界面
        if ([self.payType isEqualToString:@"WeChat"]) {
            NSString *acObjStr = [responseObject objectForKey:@"weChatApp"];
            NSDictionary *weChatDict = [self stringTransformDictWithString:acObjStr];
            WeChatAPIParameterModel *wxModel = [WeChatAPIParameterModel weChatAPIParameterModelWithDict:weChatDict];
            //这里调用后台接口获取订单的详细信息，然后调用微信支付方法
            [self WXPayWithAppid:wxModel.appId
                       partnerid:wxModel.partnerId
                        prepayid:wxModel.prepayId
                         package:wxModel.packageValue
                        noncestr:wxModel.nonceStr
                       timestamp:wxModel.timeStamp
                            sign:wxModel.sign];
            
        }else if([self.payType isEqualToString:@"Alipay"]){
            NSString *acObjStr = [responseObject objectForKey:@"alipayApp"];
            NSDictionary *alipayDict = [self stringTransformDictWithString:acObjStr];
            NSString *payOrder = [alipayDict objectForKey:@"payParams"];
            // NOTE: 调用支付结果开始支付18719353007
            [[AlipaySDK defaultService] payOrder:payOrder fromScheme:kAliPayURLScheme callback:^(NSDictionary *resultDic) {
                BDLog(@"reslut = %@",resultDic);
            }];
            
        }else{
            // 积分和余额支付 支付成功跳转到 我的 界面
            if ([[responseObject objectForKey:@"errmsg"] isEqualToString:@"ok"]) {
                [self showMessageSuceess:@"支付成功"];
            }
        }
    }];
}

#pragma mark 微信支付方法
- (void)WXPayWithAppid:(NSString *)appid partnerid:(NSString *)partnerid prepayid:(NSString *)prepayid package:(NSString *)package noncestr:(NSString *)noncestr timestamp:(NSString *)timestamp sign:(NSString *)sign{
    //需要创建这个支付对象
    PayReq *req = [[PayReq alloc] init];
    //由用户微信号和AppID组成的唯一标识，用于校验微信用户
    req.openID = appid;
    // 商家id，在注册的时候给的
    req.partnerId = partnerid;
    // 预支付订单这个是后台跟微信服务器交互后，微信服务器传给你们服务器的，你们服务器再传给你
    req.prepayId = prepayid;
    // 根据财付通文档填写的数据和签名
    req.package = package;
    // 随机编码，为了防止重复的，在后台生成
    req.nonceStr = noncestr;
    // 这个是时间戳，也是在后台生成的，为了验证支付的
    NSString * stamp = timestamp; req.timeStamp = stamp.intValue;
    // 这个签名也是后台做的
    req.sign = sign;
    if ([WXApi sendReq:req]) {
        //发送请求到微信，等待微信返回onResp
        NSLog(@"吊起微信成功...");
    }else{
        NSLog(@"吊起微信失败...");
    }
}

- (NSDictionary *)stringTransformDictWithString:(NSString *)string {
    NSRange range = NSMakeRange(0, string.length);
    NSString *subStr = [string substringWithRange:range];
    NSData *JSONData = [subStr dataUsingEncoding:NSUTF8StringEncoding];
    if (JSONData == nil) return nil;
    NSDictionary *DictString = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableLeaves error:nil];
    return DictString;
}


@end







