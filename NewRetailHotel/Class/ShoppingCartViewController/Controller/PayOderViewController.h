//
//  PayOderViewController.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/21.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayOderViewController : UIViewController

@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *totalPrice;
@property (nonatomic, copy) NSString *isCart;
@property (nonatomic, assign) BOOL isManyStore;

@end
