//
//  PayStatusViewController.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/29.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "PayStatusViewController.h"

@interface PayStatusViewController ()
@property (weak, nonatomic) IBOutlet UILabel *orderNum;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;

@end

@implementation PayStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.orderNum.text = [NSString stringWithFormat:@"订单号:%@",self.orderID];
    self.priceLab.text = [NSString stringWithFormat:@"订单金额:%@",self.price];
    
}

// 支付完成按钮点击
- (IBAction)finishBtn:(UIButton *)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}



@end
