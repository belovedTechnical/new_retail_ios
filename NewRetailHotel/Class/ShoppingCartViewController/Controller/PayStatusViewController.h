//
//  PayStatusViewController.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/29.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayStatusViewController : UIViewController

@property (nonatomic,copy)NSString *orderID;
@property (nonatomic,copy)NSString *price;

@end
