//
//  ShoppingCartViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/7.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "ShoppingCartViewController.h"
#import "ManyStoreOrderCofirmController.h"
#import "WebHotelDetailsViewController.h"
#import "OrderCofirmViewController.h"
#import "CartTableViewCell.h"
#import "TableHeaderView.h"
#import "BDNetworkTools.h"
#import "ShopBuyModel.h"
#import "StoreModel.h"
#import "SaveTool.h"
#import "NoteLoginViewController.h"

@interface ShoppingCartViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSString *apikey;
@property (nonatomic, strong) NSArray *storeIdArray;
@property (nonatomic, strong) UIButton *allSellectedButton;
@property (nonatomic, strong) UILabel *totlePriceLabel;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSMutableArray *selectedArray;
@property (nonatomic, strong) NSMutableArray *sectionArray;

@end

static CGFloat const bottomViewHeight = 49;
//static CGFloat const naigationBarHeight = 64;
static NSString *const tableHeaderView = @"TableHeaderView";
static NSString *const cartReusableCell = @"CartReusableCell";
#define  TAG_CartEmptyView 100
@implementation ShoppingCartViewController

- (void)viewWillAppear:(BOOL)animated
{   [super viewWillAppear:animated];
    
    self.tabBarController.tabBar.hidden = NO;
    [BDShowHUD showSVPMaskWithStatus:@"正在加载"];
    //当进入购物车的时候判断是否有已选择的商品,有就清空
    //主要是提交订单后再返回到购物车,如果不清空,还会显示
    if (self.selectedArray.count > 0) {
        for (ShopBuyModel *model in self.selectedArray) {
            model.select = NO;
        }
        [self.selectedArray removeAllObjects];
    }
    //初始化显示状态
    _allSellectedButton.selected = NO;
    self.navigationItem.rightBarButtonItem = nil;
    _totlePriceLabel.attributedText = [self SetString:@"￥0.00"];
    [self requestCartData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"购物车";
    [self changeView];
}

#pragma mark -自定义底部视图
- (void)setupCustomBottomView
{
    CGFloat bottomViewY = kScreenH - bottomViewHeight - 49 - SafeAreaBottomHeight;
    if (_isRootVC == YES) {
        bottomViewY = kScreenH - bottomViewHeight - SafeAreaBottomHeight;
        self.tabBarController.tabBar.hidden = YES;
    }
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, bottomViewY, kScreenW, bottomViewHeight)];
    bottomView.backgroundColor = [UIColor whiteColor];
    bottomView.tag = TAG_CartEmptyView + 1;
    
    [self.view addSubview:bottomView];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(0, 0, kScreenW, 1);
    lineView.backgroundColor = [UIColor lightGrayColor];
    [bottomView addSubview:lineView];
    
    //全选按钮
    UIButton *selectAll = [UIButton buttonWithType:UIButtonTypeCustom];
    selectAll.titleLabel.font = [UIFont systemFontOfSize:16];
    selectAll.frame = CGRectMake(10, 5, 80, bottomViewHeight - 10);
    [selectAll setTitle:@" 全选" forState:UIControlStateNormal];
    [selectAll setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
    [selectAll setImage:[UIImage imageNamed:shopingCartIcon] forState:UIControlStateNormal];
    [selectAll setImage:[UIImage imageNamed:shopingCartSelecdIcon] forState:UIControlStateSelected];
    [selectAll setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [selectAll addTarget:self action:@selector(selectAllBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:selectAll];
    self.allSellectedButton = selectAll;
    
    //结算按钮
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = [UIColor redColor];
    btn.frame = CGRectMake(kScreenW - 120, 1, 120, bottomViewHeight-1);
    btn.backgroundColor = [UIColor colorWithHexString:@"#e1e1e1"];
    btn.titleLabel.font = [UIFont systemFontOfSize:16];
    [btn setTitle:@"去结算" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithHexString:@"#a6926b"] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithHexString:@"#ffffff"] forState:UIControlStateSelected];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [btn addTarget:self action:@selector(goToPayButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:btn];
    
    //合计
    UILabel *label = [[UILabel alloc]init];
    label.font = [UIFont systemFontOfSize:16];
    label.textAlignment = NSTextAlignmentRight;
    label.textColor = [UIColor colorWithHexString:@"#333333"];
    [bottomView addSubview:label];
    
    label.attributedText = [self SetString:@"¥0.00"];
    CGFloat maxWidth = kScreenW - selectAll.bounds.size.width - btn.bounds.size.width - 10;
    label.frame = CGRectMake(selectAll.bounds.size.width + 5, 0, maxWidth, bottomViewHeight);
    self.totlePriceLabel = label;
}

#pragma mark -Label富文本属性
- (NSMutableAttributedString *)SetString:(NSString *)string {
    
    NSString *text = [NSString stringWithFormat:@"合计:%@",string];
    NSMutableAttributedString *String = [[NSMutableAttributedString alloc]initWithString:text];
    NSRange rang = [text rangeOfString:@"合计:"];
    [String addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#333333"] range:rang];
    [String addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:rang];
    return String;
}

#pragma mark -- 购物车为空时的默认视图
- (void)changeView {
    if (self.dataArray.count > 0) {
        UIView *view = [self.view viewWithTag:TAG_CartEmptyView];
        if (view != nil) {
            [view removeFromSuperview];
        }
        
        [self setupCartView];
    } else {
        UIView *bottomView = [self.view viewWithTag:TAG_CartEmptyView + 1];
        [bottomView removeFromSuperview];
        
        [self.tableView removeFromSuperview];
        self.tableView = nil;
        NSString *userApi = [SaveTool objectForKey:apikey];
        if (userApi.length > 6) {
            [self setupCartEmptyViewWithJumpTitle:@"去首页看看" warnText:@"购物车都快饿扁啦!好东西，手慢无哦"];
        }else {
            [self setupCartEmptyViewWithJumpTitle:@"去登录" warnText:@"您还没有登录,请先登录后查看"];
        }
    }
}

#pragma mark -- 购物车无商品时的视图
- (void)setupCartEmptyViewWithJumpTitle:(NSString *)jumpTitle warnText:(NSString *)warnText {
    //默认视图背景
    UIView *backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, SafeAreaTopHeight, kScreenW, kScreenH - SafeAreaTopHeight)];
    backgroundView.tag = TAG_CartEmptyView;
    backgroundView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:backgroundView];
    
    //默认图片
    UIImageView *img = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_cart_empty.png"]];
    img.center = CGPointMake(kScreenW/2.0, kScreenH/2.0 - 120);
    img.bounds = CGRectMake(0, 0, 150, 150);
    [backgroundView addSubview:img];
    
    UILabel *warnLabel = [[UILabel alloc]init];
    warnLabel.center = CGPointMake(kScreenW/2.0, kScreenH/2.0 - 10);
    warnLabel.bounds = CGRectMake(0, 0, kScreenW, 30);
    warnLabel.textAlignment = NSTextAlignmentCenter;
    warnLabel.text = warnText;
    warnLabel.font = [UIFont systemFontOfSize:15];
    warnLabel.textColor = [UIColor lightGrayColor];
    [backgroundView addSubview:warnLabel];
    
    // 去首页
    UIButton *jumpBtn = [[UIButton alloc] initWithFrame:CGRectMake(kScreenW/2.0 - 80, warnLabel.frame.origin.y + 50, 160, 50)];
    [jumpBtn setTitle:jumpTitle forState:UIControlStateNormal];
    [jumpBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [jumpBtn addTarget:self action:@selector(jumpHomePageClick) forControlEvents:UIControlEventTouchUpInside];
    [jumpBtn.layer setMasksToBounds:YES];
    [jumpBtn.layer setCornerRadius:25];
    jumpBtn.backgroundColor = [UIColor redColor];
    [backgroundView addSubview:jumpBtn];
    

    
}

#pragma mark -跳转到首页
- (void)jumpHomePageClick
{
    //设置按钮点击事件
    NSString *userApi = [SaveTool objectForKey:apikey];
    if (userApi.length > 6) {
        //首页
        [self.tabBarController setSelectedIndex:0];
    } else {
        //登录
        NoteLoginViewController *loginVC = [[NoteLoginViewController alloc] init];
        [self.navigationController pushViewController:loginVC animated:YES];
    }
}

#pragma mark -购物车有商品时视图
- (void)setupCartView
{
    UITableView *tableView;
    if (_isRootVC == YES) {
        tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, kScreenW, kScreenH - bottomViewHeight - SafeAreaTopHeight)];
    }else {
        tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, kScreenW, kScreenH - bottomViewHeight - SafeAreaTopHeight - 50)];
    }
    tableView.dataSource = self;
    tableView.delegate = self;
    
    tableView.rowHeight = 115;
    tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    [self.tableView registerClass:[CartTableViewCell class] forCellReuseIdentifier:cartReusableCell];
    [self.tableView registerClass:[TableHeaderView class] forHeaderFooterViewReuseIdentifier:tableHeaderView];
    
    // 创建底部视图
    [self setupCustomBottomView];
}

#pragma mark --- UITableViewDataSource & UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    StoreModel *modelS = self.dataArray[section];
    return modelS.cartItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cartReusableCell];
    if (cell == nil) {
        cell = [[CartTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cartReusableCell];
    }
    
    StoreModel *model = self.dataArray[indexPath.section];
    ShopBuyModel *buyModel = model.cartItems[indexPath.row];
    
    __block typeof(cell)wsCell = cell;
    
    [cell numberAddWithBlock:^(NSInteger number) {
        wsCell.lzNumber = number;
        buyModel.quantity = number;
        
        [model.cartItems replaceObjectAtIndex:indexPath.row withObject:buyModel];
        if ([self.selectedArray containsObject:buyModel]) {
            [self.selectedArray removeObject:buyModel];
            [self.selectedArray addObject:buyModel];
            [self countPrice];
        }
        [self addCartShopRequestWithProductID:buyModel.productId number:@"1"];
    }];
    
    [cell numberCutWithBlock:^(NSInteger number) {
        
        wsCell.lzNumber = number;
        buyModel.quantity = number;
        
        [model.cartItems replaceObjectAtIndex:indexPath.row withObject:buyModel];
        
        //判断已选择数组里有无该对象,有就删除  重新添加
        if ([self.selectedArray containsObject:buyModel]) {
            [self.selectedArray removeObject:buyModel];
            [self.selectedArray addObject:buyModel];
            [self countPrice];
        }
        [self addCartShopRequestWithProductID:buyModel.productId number:@"-1"];
    }];
    
    [cell cellSelectedWithBlock:^(BOOL select) {
        buyModel.select = select;
        if (select) {
            [self.selectedArray addObject:buyModel];
        } else {
            [self.selectedArray removeObject:buyModel];
        }
        [self verityAllSelectState];
        [self verityGroupSelectState:indexPath.section];
        [self countPrice];
    }];
    [cell reloadDataWithModel:buyModel];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    TableHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:tableHeaderView];
    [headerView.contentView setBackgroundColor:[UIColor clearColor]];
    StoreModel *model = self.dataArray[section];
    headerView.title =[NSString stringWithFormat:@"%@",model.storeName];
    headerView.select = model.select;
    
    headerView.entranceBlock = ^{
        if ([self isPureInt:model.productStoreId]) {
            WebHotelDetailsViewController *storeVC = [[WebHotelDetailsViewController alloc] init];
            storeVC.hotelDetailsURL = [NSString stringWithFormat:@"%@shop/%@",HTML_URL_Base, model.productStoreId];
            [self.navigationController pushViewController:storeVC animated:YES];
        }else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    };
    
    headerView.lzClickBlock = ^(BOOL select) {
        model.select = select;
        if (select) {
            for (ShopBuyModel *buyModel in model.cartItems) {
                buyModel.select = YES;
                if (![self.selectedArray containsObject:buyModel]) {
                    
                    [self.selectedArray addObject:buyModel];
                }
            }
            
        } else {
            for (ShopBuyModel *buyModel in model.cartItems) {
                buyModel.select = NO;
                if ([self.selectedArray containsObject:buyModel]) {
                    
                    [self.selectedArray removeObject:buyModel];
                }
            }
        }
        
        [self verityAllSelectState];
        
        [tableView reloadData];
        [self countPrice];
    };
    
    return headerView;
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示"
                                                                       message:@"确定要删除该商品?删除后无法恢复!"
                                                                preferredStyle:1];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             
                                                             StoreModel *shop = [self.dataArray objectAtIndex:indexPath.section];
                                                             ShopBuyModel *model = [shop.cartItems objectAtIndex:indexPath.row];
                                                             
                                                             [shop.cartItems removeObjectAtIndex:indexPath.row];
                                                             //    删除
                                                             [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                                                             
                                                             if (shop.cartItems.count == 0) {
                                                                 [self.dataArray removeObjectAtIndex:indexPath.section];
                                                             }
                                                             
                                                             //判断删除的商品是否已选择
                                                             if ([self.selectedArray containsObject:model]) {
                                                                 //从已选中删除,重新计算价格
                                                                 [self.selectedArray removeObject:model];
                                                                 [self countPrice];
                                                             }
                                                             
                                                             NSInteger count = 0;
                                                             for (StoreModel *shop in self.dataArray) {
                                                                 count += shop.cartItems.count;
                                                             }
                                                             
                                                             if (self.selectedArray.count == count) {
                                                                 _allSellectedButton.selected = YES;
                                                             } else {
                                                                 _allSellectedButton.selected = NO;
                                                             }
                                                             
                                                             if (count == 0) {
                                                                 [self changeView];
                                                             }
                                                             //如果删除的时候数据紊乱,可延迟0.5s刷新一下
                                                             [self removeCartShopWith:model.productId];
                                                             
                                                         }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:okAction];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

#pragma mark --- 全选按钮点击事件
- (void)selectAllBtnClick:(UIButton*)button {
    button.selected = !button.selected;
    //点击全选时,把之前已选择的全部删除
    for (ShopBuyModel *buyModel in self.selectedArray) {
        buyModel.select = NO;
    }
    
    [self.selectedArray removeAllObjects];
    
    if (button.selected) {
        
        for (StoreModel *shop in self.dataArray) {
            shop.select = YES;
            for (ShopBuyModel *buyModell in shop.cartItems) {
                buyModell.select = YES;
                [self.selectedArray addObject:buyModell];
            }
        }
        
    } else {
        for (StoreModel *shop in self.dataArray) {
            shop.select = NO;
        }
    }
    
    [self.tableView reloadData];
    [self countPrice];
    if (button.selected == YES) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"全部删除" style:UIBarButtonItemStylePlain target:self action:@selector(allRemoveClick)];
    }else {
        self.navigationItem.rightBarButtonItem = nil;
    }
}

// 全部删除
- (void)allRemoveClick
{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"确定要删除全部已选商品？" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *determine = [UIAlertAction actionWithTitle:@"确定"
                                                        style:UIAlertActionStyleDestructive
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          [self removeAllShops];
                                                      }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消"
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * _Nonnull action) {
                                                       
                                                   }];
    [alertVC addAction:determine];
    [alertVC addAction:cancel];
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark -计算选中价格
/**
 *  计算已选中商品金额
 */
-(void)countPrice {
    double totlePrice = 0.0;
    
    for (ShopBuyModel *buyModel in self.selectedArray) {
        if ([buyModel.productStatus isEqualToString:@"normal"]) {
            double price = buyModel.unitPrice;
            totlePrice += price * buyModel.quantity;
        }
    }
    NSString *string = [NSString stringWithFormat:@"￥%.2f",totlePrice];
    self.totlePriceLabel.attributedText = [self SetString:string];
}

- (void)verityGroupSelectState:(NSInteger)section {
    
    // 判断某个区的商品是否全选
    StoreModel *tempShop = self.dataArray[section];
    // 是否全选标示符
    BOOL isShopAllSelect = YES;
    for (ShopBuyModel *model in tempShop.cartItems) {
        // 当有一个为NO的是时候,将标示符置为NO,并跳出循环
        if (model.select == NO) {
            isShopAllSelect = NO;
            break;
        }
    }
    
    TableHeaderView *header = (TableHeaderView *)[self.tableView headerViewForSection:section];
    header.select = isShopAllSelect;
    tempShop.select = isShopAllSelect;
}

- (void)verityAllSelectState {
    
    NSInteger count = 0;
    for (StoreModel *shop in self.dataArray) {
        count += shop.cartItems.count;
    }
    
    if (self.selectedArray.count == count) {
        _allSellectedButton.selected = YES;
    } else {
        _allSellectedButton.selected = NO;
    }
}

#pragma mark --- 服务员，买单
- (void)goToPayButtonClick:(UIButton*)button {
    NSString *productStoreId = @"";
    NSMutableArray *sectionArray = [NSMutableArray array];
    NSMutableArray *rowArray = [NSMutableArray array];
    NSMutableArray *storeIDArray = [NSMutableArray array];
    for (StoreModel *shop in self.dataArray) {
        if (shop.select) {
            if (![sectionArray containsObject:shop]) {
                [sectionArray addObject:shop];
                productStoreId = shop.productStoreId;
            }
            [storeIDArray addObject:shop.productStoreId];
        } else {
            if ([sectionArray containsObject:shop]) {
                [sectionArray removeObject:shop];
            }
            for (ShopBuyModel *buyModel in shop.cartItems) {
                if (buyModel.select) {
                    if (![rowArray containsObject:shop]) {
                        [rowArray addObject:shop];
                        productStoreId = shop.productStoreId;
                    }
                    if (![storeIDArray containsObject:shop.productStoreId]) {
                        [storeIDArray addObject:shop.productStoreId];
                    }
                } else {
                    if ([rowArray containsObject:shop]) {
                        [rowArray removeObject:shop];
                    }
                }
            }
        }
    }
    if (self.selectedArray.count > 0) {
        NSMutableArray *productIdArr = [NSMutableArray array];
        for (ShopBuyModel *model in self.selectedArray) {
            if ([model.productStatus isEqualToString:@"normal"]) {
                [productIdArr addObject:model.productId];
            }
        }
        NSString *productId = [productIdArr componentsJoinedByString:@","];
            if (storeIDArray.count < 2) { // 单个店铺
                OrderCofirmViewController *oneOrderVC = [[OrderCofirmViewController alloc] init];
                oneOrderVC.idStr = productId;
                oneOrderVC.isCart = @"Y";
                oneOrderVC.productStoreId = productStoreId;
                [self.navigationController pushViewController:oneOrderVC animated:YES];
            }else { // 多个店铺
                ManyStoreOrderCofirmController *manyStoreOrderVC = [[ManyStoreOrderCofirmController alloc] init];
                manyStoreOrderVC.productId = productId;
                manyStoreOrderVC.storeIDArray = storeIDArray;
                [self.navigationController pushViewController:manyStoreOrderVC animated:YES];
            }
    } else {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示"
                                                                         message:@"你还没有选择任何商品!"
                                                                  preferredStyle:1];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:okAction];
        [self presentViewController:alertVC animated:YES completion:nil];
    }
}


#pragma mark -网络请求
#pragma mark - 购物车列表数据
- (void)requestCartData{
    [self.dataArray removeAllObjects];
    [[BDNetworkTools sharedInstance] getShopCartDataWithStoreID:_storeID Block:^(NSDictionary *responseObject, NSError *error) {
        NSArray *arrS =  [[responseObject objectForKey:@"shoppingCart"] objectForKey:@"storeCarts"];
        for (NSDictionary *mdic in arrS) {
            StoreModel *storeModel = [[StoreModel alloc] init];
            [storeModel setValuesForKeysWithDictionary:mdic];
            [self.dataArray addObject:storeModel];
        }
        [self changeView];
        [self requestCommonwealStoreIDListWithApikey:apikey];
        [self.tableView reloadData];
        [BDShowHUD dismissSVP];
    }];
}

#pragma mark - 移除 选中的  购物车中的商品
- (void)removeCartShopWith:(NSString *)productIds
{
    NSDictionary *dic = @{@"productIds":productIds};
    [[BDNetworkTools sharedInstance] postShopCartDeleteSelectParameter:dic Block:^(NSDictionary *responseObject, NSError *error) {
        [self.tableView reloadData];
    }];
}

#pragma mark -添加购物车
- (void)addCartShopRequestWithProductID:(NSString *)productID number:(NSString *)number
{
    NSDictionary *paramDic = @{@"productId":productID, @"number": number};
    [[BDNetworkTools sharedInstance] postShopCartAddShopParameter:paramDic Block:^(NSDictionary *responseObject, NSError *error) {
        
    }];
}

#pragma mark - 清空购物车
- (void)removeAllShops{
    [[BDNetworkTools sharedInstance] deleteShopCartClearWithParameter:nil Block:^(NSDictionary *responseObject, NSError *error) {
        self.navigationItem.rightBarButtonItem = nil;
        [self.dataArray removeAllObjects];
        [self.tableView removeFromSuperview];
        [self changeView];
    }];
}

#pragma mark -获取全民公益店铺ID
- (void)requestCommonwealStoreIDListWithApikey:(NSString *)apikey
{

}

#pragma mark -判断是否为纯数字
//判断是否为整形
- (BOOL)isPureInt:(NSString*)string{
    
    NSScanner *scan = [NSScanner scannerWithString:string];
    int val;
    return[scan scanInt:&val] && [scan isAtEnd];
}

#pragma mark - 初始化数组
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataArray;
}

- (NSMutableArray *)selectedArray {
    if (_selectedArray == nil) {
        _selectedArray = [NSMutableArray arrayWithCapacity:0];
    }
    return _selectedArray;
}

- (NSArray *)storeIdArray
{
    if (!_storeIdArray) {
        _storeIdArray = [NSArray array];
    }
    return _storeIdArray;
}




@end
