//
//  PayFailViewController.h
//  BelovedHotel
//
//  Created by 至爱 on 16/12/2.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayFailViewController : UIViewController

@property (nonatomic,copy)NSString *orderID;
@property (nonatomic,copy)NSString *price;
@property(nonatomic,copy)NSString *vcTitle;
@property (nonatomic,copy)NSString *Balance;
@property (nonatomic,copy)NSString *zhiaibi;

@end
