//
//  TableHeaderView.m
//  购物车DEMO
//
//  Created by BDSir on 2017/8/16.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "TableHeaderView.h"
#import "RightBaseButton.h"

@interface TableHeaderView()

@property (strong,nonatomic)UILabel *titleLabel;
@property (strong,nonatomic)UIButton *button;

@end

@implementation TableHeaderView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    
    UIView *topView = [[UIView alloc] init];
    topView.frame = CGRectMake(0, 0, kScreenW, 10);
    topView.backgroundColor = [UIColor colorWithHexString:@"#f6f6f6"];
    [self addSubview:topView];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(15, 10, 30, 30);
    
    [button setImage:[UIImage imageNamed:shopingCartIcon] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:shopingCartSelecdIcon] forState:UIControlStateSelected];
    [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:button];
    self.button = button;
    
    UILabel *label = [[UILabel alloc]init];
    label.frame = CGRectMake(60, 10, kScreenW - 100, 30);
    label.font = [UIFont systemFontOfSize:13];
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.contentView addSubview:label];
    self.titleLabel = label;
    
    RightBaseButton *entranceBtn = [[RightBaseButton alloc] initWithFrame:CGRectMake(220, 10, 100, 30)];
    [entranceBtn setTitle:@"进入店铺" forState:UIControlStateNormal];
    [entranceBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [entranceBtn setImage:[UIImage imageNamed:@"icon_rightarrow02.png"] forState:UIControlStateNormal];
    entranceBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [entranceBtn addTarget:self action:@selector(entranceBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:entranceBtn];
}

#pragma mark -内部控制方法
- (void)buttonClick:(UIButton*)button {
    button.selected = !button.selected;
    
    if (self.lzClickBlock) {
        self.lzClickBlock(button.selected);
    }
}

// 进入店铺
- (void)entranceBtnClick
{
    if (self.entranceBlock) {
        self.entranceBlock();
    }
}

- (void)setSelect:(BOOL)select {
    
    self.button.selected = select;
    _select = select;
}

- (void)setTitle:(NSString *)title {
    self.titleLabel.text = title;
    _title = title;
}


@end

