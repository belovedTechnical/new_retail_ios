//
//  TableHeaderView.h
//  购物车DEMO
//
//  Created by BDSir on 2017/8/16.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^buttonClickBlock)(BOOL select);
typedef void(^entranceBtnBlock)(void);
@interface TableHeaderView : UITableViewHeaderFooterView

@property (copy,nonatomic)NSString *title;
@property (copy,nonatomic)buttonClickBlock lzClickBlock;
@property (nonatomic, copy) entranceBtnBlock entranceBlock;
@property (assign,nonatomic)BOOL select;

@end
