//
//  PayHotelOrderCell.m
//  BelovedHotel
//
//  Created by 至爱 on 16/11/21.
//  Copyright © 2016年 至爱. All rights reserved.
//

#import "PayHotelOrderCell.h"
@interface PayHotelOrderCell ()


@property(nonatomic,assign)BOOL isPoint;
@property(nonatomic,assign)BOOL isImg;

@end

@implementation PayHotelOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.pointBtn.tag = kPointBtn_tag;
    self.balanceBtn.tag = kBalanceBtn_tag;
    self.wechatBtn.tag = kWechatBtn_tag;
    self.zhifuBaoBtn.tag = kAliPayBtn_tag;
    self.confirmBtn.tag = kConfirmPayBtn_tag;
    self.wechatBtn.selected = YES;
    self.zhifuBaoBtn.selected = NO;
    self.pointBtn.selected = NO;
    self.balanceBtn.selected = NO;
}
- (void)setCellWith:(NSDictionary *)dic withPayDic:(NSDictionary *)payDic{

    
    if ([[payDic allKeys] containsObject:@"points"]) {
        NSString *pointStr = [NSString stringWithFormat:@"%@",[payDic objectForKey:@"points"]];
        CGFloat pointF = [pointStr floatValue];
        self.pointLab.text = [NSString stringWithFormat:@"可抵扣¥%.2f",pointF];
    }else{
        self.pointLab.text  = @"¥0.00";
    }
    
    if ([[payDic allKeys] containsObject:@"balance"]) {
        NSString *balanceStr = [NSString stringWithFormat:@"%@",[payDic objectForKey:@"balance"]];
        CGFloat balanceF = [balanceStr floatValue];
        self.balanceLab.text = [NSString stringWithFormat:@"可抵扣¥%.2f",balanceF];
    }else{
        self.balanceLab.text  = @"¥0.00";
    }
    
    if ([[payDic allKeys] containsObject:@"totalPrice"]) {
        
        NSString *priceStr = [NSString stringWithFormat:@"%@",[payDic objectForKey:@"totalPrice"]];
        if (priceStr == nil) {
            self.totalPriceLab.text  = @"¥0.00";
        }else {
            CGFloat priceF = [priceStr floatValue];
            self.totalPriceLab.text = [NSString stringWithFormat:@"¥%.2f", priceF];
        }
        
    }else{
        self.totalPriceLab.text  = @"¥0.00";
    }
}

- (IBAction)payBtn:(UIButton *)sender {
    
    if (sender.tag == kBalanceBtn_tag) {// 余额
        self.balanceBtn.selected = !sender.selected;

    }else if (sender.tag == kWechatBtn_tag ){
            self.wechatBtn.selected = !sender.selected;
            if (self.wechatBtn.selected == YES) {
                self.zhifuBaoBtn.selected = NO;
            }
    }else if (sender.tag == kAliPayBtn_tag){
            self.zhifuBaoBtn.selected = !sender.selected;
            if (self.zhifuBaoBtn.selected == YES) {
                self.wechatBtn.selected = NO;
            }
    }else if (sender.tag == kPointBtn_tag){// 积分
        self.pointBtn.selected = !sender.selected;
  
    }else{// 确认支付
        
        
    }
    self.payBtn(sender.tag);

}




@end
