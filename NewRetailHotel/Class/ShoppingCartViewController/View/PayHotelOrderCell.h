//
//  PayHotelOrderCell.h
//  BelovedHotel
//
//  Created by 至爱 on 16/11/21.
//  Copyright © 2016年 至爱. All rights reserved.
//
#define kPointBtn_tag 700
#define kBalanceBtn_tag 701
#define kWechatBtn_tag 702
#define kAliPayBtn_tag 703
#define kConfirmPayBtn_tag 704
#import <UIKit/UIKit.h>




@interface PayHotelOrderCell : UITableViewCell
- (void)setCellWith:(NSDictionary *)dic withPayDic:(NSDictionary *)payDic;

@property (nonatomic,copy)void(^payBtn)(NSInteger index);

// 积分
@property (weak, nonatomic) IBOutlet UILabel *pointLab;
@property (weak, nonatomic) IBOutlet UIButton *pointBtn;

// 余额
@property (weak, nonatomic) IBOutlet UIButton *balanceBtn;
@property (weak, nonatomic) IBOutlet UILabel *balanceLab;

// 微信
@property (weak, nonatomic) IBOutlet UIButton *wechatBtn;
// 支付宝
@property (weak, nonatomic) IBOutlet UIButton *zhifuBaoBtn;

@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@property (weak, nonatomic) IBOutlet UILabel *totalPriceLab;

@property (weak, nonatomic) IBOutlet UIView *wxView;

@property (weak, nonatomic) IBOutlet UIView *zhifubaoView;

@end
