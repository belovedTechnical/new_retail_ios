//
//  CartTableViewCell.m
//  购物车DEMO
//
//  Created by BDSir on 2017/8/16.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "CartTableViewCell.h"
#import "UIView+RGSize.h"
#import "ShopBuyModel.h"
#import "UIImageView+WebCache.h"

@interface CartTableViewCell()
{
    NumberChangedBlock numberAddBlock;
    NumberChangedBlock numberCutBlock;
    CellSelectedBlock cellSelectedBlock;
}
/** 删除按钮 */
@property(nonatomic, strong) UILabel *delegateLabel;
/** 状态显示 */
@property(nonatomic, strong) UILabel *statusLabel;
//选中按钮
@property (nonatomic,retain) UIButton *selectBtn;
//显示照片
@property (nonatomic,retain) UIImageView *lzImageView;
//商品名
@property (nonatomic,retain) UILabel *nameLabel;
//尺寸
@property (nonatomic,retain) UILabel *sizeLabel;
//时间
@property (nonatomic,retain) UILabel *dateLabel;
//价格
@property (nonatomic,retain) UILabel *priceLabel;
//数量
@property (nonatomic,retain)UILabel *numberLabel;

@end

@implementation CartTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupMainView];
    }
    return self;
}

#pragma mark - public method
- (void)reloadDataWithModel:(ShopBuyModel *)model {
    //productStatus  offSale 下架  sellOut 售罄  normal 正常有库存
    [self.lzImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.imageUrl]]];
    self.nameLabel.text = [NSString stringWithFormat:@"%@",model.productName];
    self.priceLabel.text = [NSString stringWithFormat:@"¥%.2f",model.unitPrice];
    self.dateLabel.text = [NSString stringWithFormat:@"%@",model.spec];
    self.numberLabel.text = [NSString stringWithFormat:@"%ld",(long)model.quantity];
    
    if ([model.productStatus isEqualToString:@"offSale"]) { // 下架
        [self noNormalStatusWith:@"已下架"];
    }else if ([model.productStatus isEqualToString:@"sellOut"]){ // 售空
        [self noNormalStatusWith:@"已售空"];
    }else if ([model.productStatus isEqualToString:@"normal"]) { // 正常
        [self normalStatusWithModel:model];
    }
}

- (void)noNormalStatusWith:(NSString *)title
{
    self.statusLabel.hidden = NO;
    self.delegateLabel.hidden = NO;
    self.selectBtn.hidden = YES;
    self.statusLabel.text = title;
}

- (void)normalStatusWithModel:(ShopBuyModel *)model
{
    self.statusLabel.hidden = YES;
    self.delegateLabel.hidden = YES;
    self.selectBtn.hidden = NO;
    self.selectBtn.selected = model.select;
}

- (void)numberAddWithBlock:(NumberChangedBlock)block {
    numberAddBlock = block;
}

- (void)numberCutWithBlock:(NumberChangedBlock)block {
    numberCutBlock = block;
}

- (void)cellSelectedWithBlock:(CellSelectedBlock)block {
    cellSelectedBlock = block;
}

#pragma mark - 重写setter方法
- (void)setLzNumber:(NSInteger)lzNumber {
    _lzNumber = lzNumber;
    
    self.numberLabel.text = [NSString stringWithFormat:@"%ld",(long)lzNumber];
}

- (void)setLzSelected:(BOOL)lzSelected {
    _lzSelected = lzSelected;
    self.selectBtn.selected = lzSelected;
}
#pragma mark - 按钮点击方法
- (void)selectBtnClick:(UIButton*)button {
    button.selected = !button.selected;
    
    if (cellSelectedBlock) {
        cellSelectedBlock(button.selected);
    }
}

- (void)addBtnClick:(UIButton*)button {
    
    NSInteger count = [self.numberLabel.text integerValue];
    count++;
    
    if (numberAddBlock) {
        numberAddBlock(count);
    }
}

- (void)cutBtnClick:(UIButton*)button {
    NSInteger count = [self.numberLabel.text integerValue];
    count--;
    if(count <= 0){
        return ;
    }
    
    if (numberCutBlock) {
        numberCutBlock(count);
    }
}
#pragma mark - 布局主视图
-(void)setupMainView {
    
    CGFloat lineH = 0.5;
    UIView *line = [[UIView alloc] init];
    line.frame = CGRectMake(15, 0, kScreenW-15, lineH);
    line.backgroundColor = [UIColor colorWithHexString:@"#e1e1e1" alpha:1];
    [self addSubview:line];
    
    //白色背景
    CGFloat bgViewHeight = 115-lineH;
    UIView *bgView = [[UIView alloc]init];
    bgView.frame = CGRectMake(0, lineH, kScreenW, bgViewHeight);
    bgView.backgroundColor = [UIColor whiteColor];
    //    bgView.layer.borderColor = [UIColor colorWithHexString:@"e1e1e1"].CGColor;
    //    bgView.layer.borderWidth = 0.5;
    [self addSubview:bgView];
    
    //选中按钮
    UIButton *selectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    selectBtn.center = CGPointMake(20, bgViewHeight/2.0);
    selectBtn.bounds = CGRectMake(0, 0, 30, 30);
    [selectBtn setImage:[UIImage imageNamed:shopingCartIcon] forState:UIControlStateNormal];
    [selectBtn setImage:[UIImage imageNamed:shopingCartSelecdIcon] forState:UIControlStateSelected];
    [selectBtn addTarget:self action:@selector(selectBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:selectBtn];
    self.selectBtn = selectBtn;
    
    //照片背景
    CGFloat space = 24;
    UIView *imageBgView = [[UIView alloc]init];
    imageBgView.frame = CGRectMake(selectBtn.right, space/2, bgView.height - space, bgView.height - space);
    imageBgView.backgroundColor = [UIColor whiteColor];
    [bgView addSubview:imageBgView];
    
    //显示图片
    UIImageView* imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"default_pic_1"];
    imageView.frame = CGRectMake(imageBgView.left, imageBgView.top, imageBgView.width, imageBgView.height);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [bgView addSubview:imageView];
    self.lzImageView = imageView;
    
    CGFloat width = 80;
    //商品名
    UILabel* nameLabel = [[UILabel alloc]init];
    nameLabel.frame = CGRectMake(imageBgView.right + 6, 10, bgView.width-imageView.width-selectBtn.width-40, 36);
    nameLabel.font = [UIFont systemFontOfSize:15];
    nameLabel.textColor = [UIColor colorWithHexString:@"#333333"];
    nameLabel.numberOfLines = 0;
    [bgView addSubview:nameLabel];
    self.nameLabel = nameLabel;
    
    //价格
    UILabel* priceLabel = [[UILabel alloc]init];
    priceLabel.frame = CGRectMake(nameLabel.left, bgView.height-30, width, 20);
    priceLabel.font = [UIFont boldSystemFontOfSize:16];
    priceLabel.textColor = [UIColor colorWithHexString:@"#b4282d"];
    priceLabel.textAlignment = NSTextAlignmentLeft;
    [bgView addSubview:priceLabel];
    self.priceLabel = priceLabel;
    
    //规格
    UILabel* dateLabel = [[UILabel alloc]init];
    dateLabel.frame = CGRectMake(nameLabel.left, 10+nameLabel.height, nameLabel.width, 18);
    dateLabel.font = [UIFont systemFontOfSize:12];
    dateLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    [bgView addSubview:dateLabel];
    self.dateLabel = dateLabel;
    
    //数量加按钮
    UIButton *addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    addBtn.frame = CGRectMake(bgView.width - 35, bgView.height - 35, 27, 25);
    [addBtn setImage:[UIImage imageNamed:@"cart_addBtn_nomal.png"] forState:UIControlStateNormal];
    [addBtn setImage:[UIImage imageNamed:@"cart_addBtn_highlight.png"] forState:UIControlStateHighlighted];
    [addBtn addTarget:self action:@selector(addBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:addBtn];
    
    //数量显示
    UILabel* numberLabel = [[UILabel alloc]init];
    numberLabel.frame = CGRectMake(addBtn.left - 30, addBtn.top, 30, 25);
    numberLabel.textAlignment = NSTextAlignmentCenter;
    numberLabel.text = @"1";
    numberLabel.font = [UIFont systemFontOfSize:15];
    [bgView addSubview:numberLabel];
    self.numberLabel = numberLabel;
    
    //数量减按钮
    UIButton *cutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cutBtn.frame = CGRectMake(numberLabel.left - 25, addBtn.top, 27, 25);
    [cutBtn setImage:[UIImage imageNamed:@"cart_cutBtn_nomal"] forState:UIControlStateNormal];
    [cutBtn setImage:[UIImage imageNamed:@"cart_cutBtn_highlight"] forState:UIControlStateHighlighted];
    [cutBtn addTarget:self action:@selector(cutBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:cutBtn];
    
    //删除按钮
    UILabel *delegateLabel = [[UILabel alloc] init];
    delegateLabel.center = CGPointMake(20, bgViewHeight/2.0);
    delegateLabel.bounds = CGRectMake(0, 0, 30, 60);
    delegateLabel.text = @"左滑删除";
    delegateLabel.font = [UIFont systemFontOfSize:10];
    delegateLabel.textColor = [UIColor darkGrayColor];
    delegateLabel.numberOfLines = 0;
    delegateLabel.hidden = YES;
    [bgView addSubview:delegateLabel];
    self.delegateLabel = delegateLabel;
    
    // 状态显示
    CGFloat statusWidth = kScreenW - cutBtn.left;
    UILabel *statusLabel = [[UILabel alloc] init];
    statusLabel.frame = CGRectMake(cutBtn.left, cutBtn.top, statusWidth-10, 25 );
    statusLabel.textAlignment = NSTextAlignmentCenter;
    statusLabel.backgroundColor = [UIColor whiteColor];
    statusLabel.textColor = [UIColor redColor];
    statusLabel.hidden = YES;
    [bgView addSubview:statusLabel];
    self.statusLabel = statusLabel;
}




@end

