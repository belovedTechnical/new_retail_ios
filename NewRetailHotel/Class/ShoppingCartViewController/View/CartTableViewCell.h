//
//  CartTableViewCell.h
//  购物车DEMO
//
//  Created by BDSir on 2017/8/16.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShopBuyModel;
typedef void(^NumberChangedBlock)(NSInteger number);
typedef void(^CellSelectedBlock)(BOOL select);

@interface CartTableViewCell : UITableViewCell

//商品数量
@property (assign,nonatomic)NSInteger lzNumber;
@property (assign,nonatomic)BOOL lzSelected;

- (void)reloadDataWithModel:(ShopBuyModel *)model;
- (void)numberAddWithBlock:(NumberChangedBlock)block;
- (void)numberCutWithBlock:(NumberChangedBlock)block;
- (void)cellSelectedWithBlock:(CellSelectedBlock)block;

@end
