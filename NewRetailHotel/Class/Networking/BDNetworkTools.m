//
//  BDNetworkTools.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/5.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "BDNetworkTools.h"
#import "AFNetworking.h"
#import "SaveTool.h"

                                  // 浏览记录

/*******************      登录     *******************/
#define URL_LoginSignInBasicAuth    @"account/account/signInBasicAuth"                  // 获取ApiKey
#define URL_LoginSendCode           @"sms/sms/sendCode"                                 // 发送短信验证码
#define URL_LoginSendCode2          @"sms/sms/sendCode2"                                // 发送短信验证码2
#define URL_LoginVerifyCodeSms      @"sms/sms/"                                         // 检测验证码的有效性
#define URL_LoginSignInApikey       @"account/account/signIn"                           // 短信登录
#define URL_LoginPassword           @"account/account/signInWithPassword"               // 密码登录
#define URL_LoginRegister           @"account/account/signUp"                           // 注册账号
#define URL_LoginResetPassword      @"account/account/resetPassword"                    // 重新设置密码
#define URL_LoginSignOut            @"account/account/signOut"                          // 退出
#define URL_LogOpenIdDetail         @"account/openId/detail"                            // 登录  绑定用户微信公众号openID
#define URL_LogOpendIdUpdate        @"account/openId/update"                            // 绑定  绑定用户微信公众号openID
#define URL_ResetPassword       @"account/account/resetPassword"                        // 找回密码
#define URL_ChangePassByOld     @"account/account/changePasswordByOld"     // 通过旧密码修改新密码，注意：必须是用户已登录状态，使用用户api_key调用该接口
#define URL_PassByCode          @"account/account/changePasswordByVerifyCode"    // 修改密码 1   --  通过短信验证码修改新密码
#define URL_PassByCode2         @"account/account/changePasswordByVerifyCode2"   // 修改密码 2   --  通过短信验证码修改新密码
#define URL_ChangeUsername      @"account/account/changeUsername"                // 修改手机号码  --  修改用户名即修改手机号码
#define URL_ChangeUsername2     @"account/account/changeUsername2"               // 修改手机号码2 --  修改用户名即修改手机号码
/*******************      用户     *******************/
#define URL_UserAcount          @"account/account"                               // 用户账户
#define URL_UserInfo            @"account/account/info"              //PUT为更新账户信息  GET为获取基础信息
#define URL_UpdataAccountInfo   @"account/account/info"                          // 更新用户个人信息
#define URL_MeMyCoupon          @"coupon/userCoupon/myCoupon"                    // 我的优惠券
#define URL_InvitationCode      @"account/account/invitationCode"                // 获取邀请码
#define URL_InvitationCode      @"account/account/invitationCode"                // 获取邀请码
#define URL_Invitations         @"account/account/invitations"                   // 获取邀请关系
#define URL_InvitationGain      @"account/account/invitationGain"                // 邀请收益
#define URL_InvitationDetailed  @"account/account/invitations"                   // 邀请收益详情
#define URL_List                @"account/financialAccount/list"                 // 余额账户信息
#define URL_BalanceDetail       @"account/financialAccount/detail"               // 余额账户信息详情
#define URL_Pointslist          @"account/pointsAccount/list"                    // 积分列表
#define URL_PointsDeatil        @"account/pointsAccount/detail"                  // 积分详情
#define URL_VCAccountList       @"account/VCAccount/list"                        // get 至爱币列表查询
#define URL_VCAccountDetail     @"account/VCAccount/detail"                      // get 至爱币记录明细
#define URL_RechargeCreate      @"recharge/recharge/create"                      // 创建充值订单
#define URL_RechargePay         @"recharge/recharge/pay"                         // 创建充值付款订单
#define URL_rechargeZhiAiBi     @"giftCard/giftCardRecharge"                     // 至爱购物卡充值
#define URL_Contact             @"account/contact"                               // 查询收货地址
#define URL_ContactDetail       @"account/contact/detail"                        // 查询地址信息
#define URL_ContactCreate       @"account/contact/create"                        // 创建收货地址
#define URL_ContactDelete       @"account/contact/delete"                        // 删除收货地址
#define URL_ContactUpdate       @"account/contact/update"                        // 修改收货地址
#define URL_ContactDefault      @"account/contact/default"                       // 默认收货地址
#define URL_CommentView         @"comments/comment/view"                         // 查询接口
#define URL_CommentCreat        @"comments/comment/create"                       // 创建评论
#define URL_CommentsProduct     @"comments/product"                              // 订单评论页面接口
#define URL_CommentsToken       @"comments/token"                                // 获取七牛Token用于图片上传
#define URL_GoodsList           @"goodsFavorites/goods/listFavorites"            // 商品收藏
#define URL_GoodsDelete         @"goodsFavorites/goods/delete"                   // 商品删除
#define URL_StoreList           @"storeFavorites/store/listFavorites"            // 店铺收藏
#define URL_StoreDelete         @"storeFavorites/store/delete"                   // 店铺删除
#define URL_FeedBackCreate      @"feedback/feedback/create"                      // 创建用户反馈
#define URL_FeedBackGet         @"feedback/feedback/get"                         // 获取用户反馈
#define URL_ScanHotel           @"/comment/historyList"                          // 浏览记录
#define URL_ScanClear           @"/comment/historyDelete"                        // 清除浏览记录
#define URL_ScanAdd             @"/comment/historyAdd"                           // 添加浏览记录
#define URL_KuaiDi100           @"kuaidi100/queryExpress"                        // 快递查询
#define URL_exchangeList        @"/exchange/exchange/list"                       // get至爱币账户查找
#define URL_exchangeTaglist     @"/exchange/exchange/taglist"                    // get兑换专区标签
#define URL_exchangeProductlist @"/exchange/exchange/productlist"                // get专区商品详情
#define URL_exchangeSubmitOrder @"/exchange/exchange/submitOrder"                // post专区商品订单结算
#define URL_OrderSelectCoupon   @"coupon/userCoupon/orderSelectCoupon"           // 单店铺优惠券选择列表
#define URL_ManySelectCoupon    @"coupon/userCoupon/manyOrderSelectCoupon"       // 多店铺优惠券选择列表
#define URL_GetCoupon           @"coupon/userCoupon/getCoupon"                   // 领取优惠券
#define URL_CouponMessage       @"coupon/userCoupon/couponMessage"               // 优惠券消息
#define URL_CouponMessageRead   @"coupon/userCoupon/couponMessageRead"           // 优惠券已读
#define URL_AddressIDNumber     @"account/contact/validationIdCard"              // 全球购验证收货人身份证号码

/*******************      酒店     *******************/
#define URL_HotelSeek           @"hotels/hotel"                                     // 酒店查询列表
#define URL_hotelPerson         @"hotels/hotel/person"                           // 查询入住人GET------创建入住人POST----删除入住人Delete
#define URL_hotels              @"hotels/hotel"                                  // 查询酒店列表
#define URL_hotelDetail         @"hotels/hotel/detail"                           // 查询洒店详情
#define URL_hotelroom_Detail    @"hotels/hotel/detail/room/detail"               // 查询房间详情
#define URL_CommentCreatHotel   @"comments/comment/createHotel"                  // 酒店评论
#define URL_CommentGetHotel     @"comments/comment/getHotel"                     // 酒店详情展示评论 条数和星级
#define URL_ConmentList         @"comments/comment/viewHotel"                    // 酒店评论列表
#define URL_provinceCityTown    @"provinceCityTown/town"                         // 根据城市code查询所辖区县
#define URL_HotelOrderList      @"hotels/hotel/order/list"                       // 酒店订单
#define URL_HotelOrderDetail    @"hotels/hotel/order/list/detail"                // 酒店订单详情
#define URL_HotelOrderCancel    @"hotels/hotel/order/cancel"                     // 取消酒店订单
#define URL_HotelAllIDList      @"hotels/hotel/hotelPsIdList"                    // 所有酒店id列表
#define URL_OrderData           @"hotels/hotel/order/data"                       // 查询订单支付信息
#define URL_OrderPay            @"hotels/hotel/order"          // PUT订单支付 POST创建订单  GET查询价格和政策
#define URL_ManyOrderData       @"hotels/hotel/order/manyStorePay"               // 多店铺支付订单
#define URL_OnlineListCount     @"order/order/orderTypeCount"                    // 每个状态的数量
#define URL_OnlineList          @"order/order/list"                              // 电商订单
#define URL_OnlineListDetail    @"order/order/list/details"                      // 电商订单详情
#define URL_OnlineCancle        @"order/order/cancel"                            // 取消订单
#define URL_OnlineComplete      @"order/order/complete"                          // 完成订单 确认收货
#define URL_OnlineDelete        @"order/order/remove"                            // 删除订单
#define URL_OrderProduct        @"order/order/product"                           // 订单确认页面产品数据
#define URL_OrderSubmit         @"order/order/submits"                           // 生成订单

/*******************      省市区     *******************/
#define URL_LocationProvince            @"provinceCityTown/province"             // 获取省列表
#define URL_LocationCity                @"provinceCityTown/city"                 // 获取市列表
#define URL_LocationTown                @"provinceCityTown/town"                 // 获取区列表

/*******************      获取分类列表商品信息     *******************/
#define URL_ShopDetailsSort             @"mall/categoryMallProduct"             // 获取商品请求列表

/*******************      购物车、订单     *******************/
#define URL_CartDetail          @"cart/cart/detail"                              // 查询购物车
#define URL_CartSpecial         @"cart/cart/storeDetail"                         // 查询特殊店铺购物车
#define URL_CartAmount          @"cart/cart/add"                                 // 添加
#define URL_CartClear           @"cart/cart/clear"                               // 清空
#define URL_CartRemove          @"cart/cart/remove"                              // 移除购物车
#define URL_CartCheckInfo       @"order/order/manyStoreProduct"                  // 多店铺收银产品价格计算
#define URL_CartManyOrder       @"order/order/manySubmits"                       // 多店铺生成订单
#define URL_ManyStoreFreight    @"order/manyStoreFreight"                        // 多店铺获取运费
#define URL_SingleStoreFreight  @"order/freight"                                 // 单店铺获取运费

#pragma mark -视频接口
#define URL_VideoUserPulish     @"shortvideo/user/push"                          // 用户上传视频
#define URL_VideoPulishSucceed  @":3001/user/upvideosuccess.html"                // 上传成功后界面
#define URL_VideoVerify         @":3001/user/myvideo.html"                       // 审核区域视频

#pragma mark -全球购店铺ID
#define URL_GlobalPurchaseID    @"102602"                                        // 全球购店铺ID

#pragma mark -售后接口
#define URL_AfterSaleEstablish  @"order/afterSales/createAs"                     // 创建售后申请
#define URL_AfterSaleUpdate     @"order/afterSales/userUpAfterSaleOrder"         // 更新售后申请
#define URL_AfterSaleExpress    @"order/afterSales/upExpress"                    // 售后物流申请
#define URL_AfterSaleDetails    @"order/afterSales/getAsDetail"                  // 售后详情
#define URL_AfterSaleItemList   @"order/afterSales/asItemList"                   // 售后项列表
#define URL_AfterSaleCause      @"order/afterSales/asItemList/itemForResonList"  // 类型原因列表

#pragma mark -快递查询
#define URL_ExpressageCorpList      @"kuaidi100/Express"                             // 查询所有快递公司

@interface BDNetworkTools()

@property (strong, nonatomic) AFHTTPSessionManager *manager;
@property (nonatomic, assign) AFNetworkReachabilityStatus currentNetworkState; //当前网络状态
@property (nonatomic, assign) AFNetworkReachabilityStatus lasttNetworkState; //之前网络状态

@end

@implementation BDNetworkTools

/** 单例对象 */
+ (instancetype)sharedInstance
{
    static BDNetworkTools* network = NULL;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        network = [[BDNetworkTools alloc] init];
    });
    return network;
}

- (NSString *)httpProtocol:(NSString *)protocol appendingBaseURL:(NSString *)serviceURL
{
    NSString *httpBaseURL = nil;
    if (protocol.length > 0 && serviceURL.length > 0) {
        httpBaseURL = [protocol stringByAppendingString:serviceURL];
    }
    return httpBaseURL;
}

- (NSString *)httpBaseURL
{
    return [self httpProtocol:HTTP appendingBaseURL:URL_Base];
}

//- (NSString *)httpBase3URL
//{
//    return [self httpProtocol:HTTP appendingBaseURL:URL_Base3];
//}

- (void)cancelAllRequests
{
    [[[AFHTTPSessionManager manager] operationQueue] cancelAllOperations];
}

#pragma mark --<layz>
- (AFHTTPSessionManager *)manager
{
    if (_manager == nil) {
        _manager = [AFHTTPSessionManager manager];
        _manager.responseSerializer = [AFJSONResponseSerializer serializer];
        _manager.requestSerializer = [AFJSONRequestSerializer serializer];
        _manager.requestSerializer.timeoutInterval = 7.f;
    }
    return _manager;
}

#pragma mark - ---最基础的网络请求————BD
- (void)sendHttpRequest:(AFHTTPSessionManager *)manager
                 method:(NSString *)method
                    url:(NSString *)url
             parameters:(NSDictionary *)parameters
           whileSuccess:(SuccessBlock)successBlock
                 orFail:(FailBlock)failBlock{
    
    // 打开网络加载菊花
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    if ([method isEqualToString:@"GET"])
    {
        [manager GET:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * task, id responseObject) {
            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
            
            successBlock(responseObject);
        } failure:^(NSURLSessionDataTask * task, NSError *error) {
            
            failBlock(error);
        }];
    }
    else if ([method isEqualToString:@"POST"])
    {
        [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * task, id responseObject) {
            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
            
            successBlock(responseObject);
        } failure:^(NSURLSessionDataTask * task, NSError * error) {
            
            failBlock(error);
        }];
    }
    
}

#pragma mark - ---带用户apikey网络请求头————BD
- (void)userApikeySendHttpRequest:(AFHTTPSessionManager *)manager
                           method:(NSString *)method
                              url:(NSString *)url
                       parameters:(NSDictionary *)parameters
                     whileSuccess:(SuccessBlock)successBlock
                           orFail:(FailBlock)failBlock{
    
    NSString *userApikey = [SaveTool objectForKey:apikey];
    BDLog(@"%@", userApikey);
    // 设置请求头
    [self.manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [self.manager.requestSerializer setValue:userApikey forHTTPHeaderField:@"api_key"];
    // 打开网络加载菊花
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    if ([method isEqualToString:@"GET"])
    {
        [manager GET:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * task, id responseObject) {
            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
            
            successBlock(responseObject);
        } failure:^(NSURLSessionDataTask * task, NSError *error) {
            
            failBlock(error);
        }];
    }
    else if ([method isEqualToString:@"POST"])
    {
        [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * task, id responseObject) {
            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
            
            successBlock(responseObject);
        } failure:^(NSURLSessionDataTask * task, NSError * error) {
            
            failBlock(error);
        }];
    }
    else if ([method isEqualToString:@"PUT"])
    {
        [manager PUT:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            successBlock(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            failBlock(error);
        }];
    }
    else if ([method isEqualToString:@"DELETE"])
    {
        [manager DELETE:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            successBlock(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            failBlock(error);
        }];
    }
}

#pragma mark -登录
/** 获取管理员Apikey */
- (void)getLoginSignInBasicAuthWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString * string = @"jishubu:jsb.86664866";
    NSData * stringData = [string dataUsingEncoding:NSUTF8StringEncoding];            //先转换成NSData类型再加密
    NSString * encodeToString = [stringData base64EncodedStringWithOptions:0];        //加密成 NSString  类型
    NSString *str = [NSString stringWithFormat:@"%@%@",@"Basic ",encodeToString];
    
    [self.manager.requestSerializer setValue:str forHTTPHeaderField:@"Authorization"];
    [self.manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSString *url = [[self httpBaseURL] stringByAppendingString:URL_LoginSignInBasicAuth];
    [self sendHttpRequest:self.manager method:@"GET" url:url parameters:nil whileSuccess:^(id responseObject) {
        
        [SaveTool setObject:[responseObject objectForKey:@"apikey"] forKey:superapi_key];
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil,error);
    }];
}

/** 用户注册 */
- (void)postUserRegisterWihtParameters:(NSDictionary *)parameters Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *superApikey = [SaveTool objectForKey:superapi_key];
    [self.manager.requestSerializer setValue:superApikey forHTTPHeaderField:@"api_key"];
    [self.manager.requestSerializer setValue:superApikey forHTTPHeaderField:@"Authorization"];
    [self.manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSString *url = [[self httpBaseURL] stringByAppendingString:URL_LoginRegister];
    [self sendHttpRequest:self.manager method:@"POST" url:url parameters:parameters whileSuccess:^(id responseObject) {
        
        [SaveTool setObject:[responseObject objectForKey:@"apikey"] forKey:apikey];
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil,error);
    }];
}

/** 发送验证码 */
- (void)postLoginSendCodeWihtProduct:(NSString *)product Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    
    NSString *superApikey = [SaveTool objectForKey:superapi_key];
    [self.manager.requestSerializer setValue:superApikey forHTTPHeaderField:@"api_key"];
    [self.manager.requestSerializer setValue:superApikey forHTTPHeaderField:@"Authorization"];
    [self.manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSString *url = [[self httpBaseURL] stringByAppendingString:URL_LoginSendCode];
    NSDictionary *parameters = @{@"phone":product};
    [self sendHttpRequest:self.manager method:@"POST" url:url parameters:parameters whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil,error);
    }];
}

// 密码登录
- (void)postPasswordLoginWihtParameters:(NSDictionary *)parameters Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *superApikey = [SaveTool objectForKey:superapi_key];
    [self.manager.requestSerializer setValue:superApikey forHTTPHeaderField:@"api_key"];
    [self.manager.requestSerializer setValue:superApikey forHTTPHeaderField:@"Authorization"];
    [self.manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSString *url = [[self httpBaseURL] stringByAppendingString:URL_LoginPassword];
    [self sendHttpRequest:self.manager method:@"POST" url:url parameters:parameters whileSuccess:^(id responseObject) {
        
        [SaveTool setObject:[responseObject objectForKey:@"apikey"] forKey:apikey];
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil,error);
    }];
}

// 短信登录
- (void)postLoginSignInWihtParameters:(NSDictionary *)parameters Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *superApikey = [SaveTool objectForKey:superapi_key];
    [self.manager.requestSerializer setValue:superApikey forHTTPHeaderField:@"api_key"];
    [self.manager.requestSerializer setValue:superApikey forHTTPHeaderField:@"Authorization"];
    [self.manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSString *url = [[self httpBaseURL] stringByAppendingString:URL_LoginSignInApikey];
    [self sendHttpRequest:self.manager method:@"POST" url:url parameters:parameters whileSuccess:^(id responseObject) {
        
        [SaveTool setObject:[responseObject objectForKey:@"apikey"] forKey:apikey];
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil,error);
    }];
}

/** 重新设置密码 */
- (void)postResetPasswordWihtParameters:(NSDictionary *)parameters Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *superApikey = [SaveTool objectForKey:superapi_key];
    [self.manager.requestSerializer setValue:superApikey forHTTPHeaderField:@"Authorization"];
    [self.manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSString *url = [[self httpBaseURL] stringByAppendingString:URL_LoginResetPassword];
    [self sendHttpRequest:self.manager method:@"POST" url:url parameters:parameters whileSuccess:^(id responseObject) {
        
        [SaveTool setObject:[responseObject objectForKey:@"apikey"] forKey:apikey];
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil,error);
    }];
}

// 微信登录  openId登录
- (void)loginOpenIdDetailWithUnionId:(NSString *)unionId Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *openIdURL = [[self httpBaseURL] stringByAppendingString:URL_LogOpenIdDetail];
    NSDictionary *parameter = @{@"unionId":unionId, };
    
    [self sendHttpRequest:self.manager method:@"GET" url:openIdURL parameters:parameter whileSuccess:^(id responseObject) {
        
        [SaveTool setObject:[responseObject objectForKey:@"apikey"] forKey:apikey];
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 用户信息
- (void)getUserAcountWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *userKey = [SaveTool objectForKey:apikey];
    [self.manager.requestSerializer setValue:userKey forHTTPHeaderField:@"api_key"];
    [self.manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSString *url = [[self httpBaseURL] stringByAppendingString:URL_UserAcount];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:url parameters:nil whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 用户基础信息(昵称、性别、生日)
- (void)getUserAcountBasicWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *url = [[self httpBaseURL] stringByAppendingString:URL_UserInfo];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:url parameters:nil whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 退出登陆
- (void)getLoginSigOutWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *url = [[self httpBaseURL] stringByAppendingString:URL_LoginSignOut];
    NSString *apik = [SaveTool objectForKey:apikey];
    NSDictionary *parameter = @{@"userApiKey":apik};
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:url parameters:parameter whileSuccess:^(id responseObject) {
        
        [SaveTool setObject:@"(null)" forKey:apikey];
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

#pragma mark -酒店
/**
 【2016年9月到11月区间,注意格式是Date 2016-09-20】
 fromDate:入住时间 thruDate:离店时间 city:城市 【SC249】 county:区 【CTU2262】
 cityCode:城市编码  sort:排序（传higt价格从高到低、传low价格从低到高）【默认是low】
 pageIndex:页码【默认是0】 pageSize:每页显示条数【默认是15】   tag:搜索文本框内容
 */
- (void)getHotelSeekWithFromDate:(NSDate *)fromDate ThruDate:(NSDate *)thruDate City:(NSString *)city County:(NSString *)county CityCode:(NSString *)cityCode Sort:(NSString *)sort PageIndex:(NSInteger)pageIndex PageSize:(NSInteger)pageSize Tag:(NSString *)tag Blcok:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *fromStr = [self stringFormatWithDate:fromDate];
    NSString *thruStr = [self stringFormatWithDate:thruDate];
    NSString *url = [[self httpBaseURL] stringByAppendingString:URL_HotelSeek];
    NSDictionary *parameter = @{
                                @"fromDate":[NSString stringWithFormat:@"%@", fromStr],
                                @"thruDate":[NSString stringWithFormat:@"%@", thruStr],
                                @"pageIndex":[NSString stringWithFormat:@"%lu", pageIndex],
                                @"pageSize":[NSString stringWithFormat:@"%lu", pageSize],
                                @"cityCode":[NSString stringWithFormat:@"%@", cityCode],
                                //                                @"county":[NSString stringWithFormat:@"%@", county],
                                //                                @"city":[NSString stringWithFormat:@"%@", city],
                                @"sort":[NSString stringWithFormat:@"%@", sort],
                                //                                @"tag":[NSString stringWithFormat:@"%@", tag]
                                };
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:url parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

/** 获取区列表 */
- (void)getLocationTownWithCity:(NSString *)cityCode Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *townUrl = [[self httpBaseURL] stringByAppendingString:URL_LocationTown];
    NSDictionary *parameter  = @{@"cityCode":cityCode};
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:townUrl parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 酒店订单列表
- (void)getHotelListWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *listURL = [[self httpBaseURL] stringByAppendingString:URL_HotelOrderList];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:listURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 所有酒店ID列表
- (void)getHotelAllIDListWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *listURL = [[self httpBaseURL] stringByAppendingString:URL_HotelAllIDList];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:listURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 酒店订单取消
- (void)putHotelOrderCancelWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *cancelURL = [[self httpBaseURL] stringByAppendingString:URL_HotelOrderCancel];
    [self userApikeySendHttpRequest:self.manager method:@"PUT" url:cancelURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 酒店订单详情数据请求
- (void)getHotelOrderDetailsWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *listURL = [[self httpBaseURL] stringByAppendingString:URL_HotelOrderDetail];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:listURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 订单删除 酒店 电商 共用
- (void)deleteOrderWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *deleteURL = [[self httpBaseURL] stringByAppendingString:URL_OnlineDelete];
    [self userApikeySendHttpRequest:self.manager method:@"DELETE" url:deleteURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 订单数量查询
- (void)getOnlineOrderCountWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *listURL = [[self httpBaseURL] stringByAppendingString:URL_OnlineListCount];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:listURL parameters:nil whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 电商订单列表
- (void)getOnlineListWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *listURL = [[self httpBaseURL] stringByAppendingString:URL_OnlineList];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:listURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 电商订单列表详情
- (void)getOnlineListDetailWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *listURL = [[self httpBaseURL] stringByAppendingString:URL_OnlineListDetail];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:listURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}


// 电商订单取消
- (void)putOnlineOrderCancelWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *cancelURL = [[self httpBaseURL] stringByAppendingString:URL_OnlineCancle];
    [self userApikeySendHttpRequest:self.manager method:@"PUT" url:cancelURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 电商订单确定
- (void)putOnlineOrderVerifyWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *cancelURL = [[self httpBaseURL] stringByAppendingString:URL_OnlineComplete];
    [self userApikeySendHttpRequest:self.manager method:@"PUT" url:cancelURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

#pragma mark -优惠券
- (void)getCouponMeDataWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *couponURL = [[self httpBaseURL] stringByAppendingString:URL_MeMyCoupon];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:couponURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 账户余额请求
- (void)getAccountRemainingSumWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *remainingURL = [[self httpBaseURL] stringByAppendingString:URL_List];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:remainingURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 账户积分请求
- (void)getAccountIntegralWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *remainingURL = [[self httpBaseURL] stringByAppendingString:URL_Pointslist];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:remainingURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 收藏  商品删除
- (void)postCollectShopDeleteWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *deleteURL = [[self httpBaseURL] stringByAppendingString:URL_GoodsDelete];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:deleteURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 收藏  店铺删除
- (void)postCollectStoreDeleteWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *deleteURL = [[self httpBaseURL] stringByAppendingString:URL_StoreDelete];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:deleteURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 商品收藏数据
- (void)getCollectShopDataWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *dataURL = [[self httpBaseURL] stringByAppendingString:URL_GoodsList];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:dataURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 浏览记录数据
- (void)getScanRecordDataWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *recordURL = [[self httpBaseURL] stringByAppendingString:URL_ScanHotel];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:recordURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 清除浏览记录
- (void)getScanClearRecordWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *recordURL = [NSString stringWithFormat:@"%@%@%@",[self httpBaseURL],URL_Base4,URL_ScanClear];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:recordURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 店铺收藏数据
- (void)getCollectStoreDataWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *dataURL = [[self httpBaseURL] stringByAppendingString:URL_StoreList];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:dataURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 购物车数据请求
- (void)getShopCartDataWithStoreID:(NSString *)storeId Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *cartURL = @"";
    NSDictionary *paramDic = @{};
    if (storeId == nil) {
        cartURL = [NSString stringWithFormat:@"%@%@",[self httpBaseURL],URL_CartDetail];
        paramDic = nil;
    }else {
        cartURL = [NSString stringWithFormat:@"%@%@",[self httpBaseURL],URL_CartSpecial];
        paramDic = @{@"productStoreId": storeId};
    }
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:cartURL parameters:paramDic whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 添加购物车
- (void)postShopCartAddShopParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *url = [NSString stringWithFormat:@"%@%@",[self httpBaseURL],URL_CartAmount];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:url parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 删除选中商品
- (void)postShopCartDeleteSelectParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *url = [NSString stringWithFormat:@"%@%@",[self httpBaseURL],URL_CartRemove];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:url parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 清空购物车
- (void)deleteShopCartClearWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *deleteURL = [[self httpBaseURL] stringByAppendingString:URL_CartClear];
    [self userApikeySendHttpRequest:self.manager method:@"DELETE" url:deleteURL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 更新个人信息
- (void)putUserInfoUpdateWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_UpdataAccountInfo];
    [self userApikeySendHttpRequest:self.manager method:@"PUT" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 效验验证码的是否有效正确
- (void)getSmsCheckingWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *url = [[self httpBaseURL] stringByAppendingString:URL_LoginVerifyCodeSms];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:url parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 修改手机号
- (void)putAmendPoneNumberAccoutWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_ChangeUsername2];
    [self userApikeySendHttpRequest:self.manager method:@"PUT" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

#pragma mark -收货地址
// 新建收货地址
- (void)postAddressNewWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_ContactCreate];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 查询收货地址
- (void)getAddressRedactWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_Contact];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:URL parameters:nil whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 设置默认地址
- (void)postAddressAcquiesceWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_ContactDefault];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 删除收货地址
- (void)postAddressDeleteWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_ContactDelete];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 修改收货地址
- (void)postAddressAmendWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_ContactUpdate];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

#pragma mark -订单结算
// 单店铺生成订单
- (void)postOrderMakeWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_OrderSubmit];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 多店铺生成订单
- (void)postOrderManyStoreMakeWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_CartManyOrder];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 单店铺价格计算
- (void)postOrderAlonePriceCountWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_OrderProduct];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 多店铺产品价格计算
- (void)postOrderManyPriceCountWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_CartCheckInfo];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 单店铺获取运费
- (void)postOrderAlonePriceFreightWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_SingleStoreFreight];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 多店铺获取运费
- (void)postOrderManyPriceFreightWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_ManyStoreFreight];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

#pragma mark -优惠券接口
// 单店铺优惠券选择接口
- (void)postOrderAloneCouponWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_OrderSelectCoupon];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 多店铺优惠券选择接口
- (void)postOrderManyCouponWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_ManySelectCoupon];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 全球购验证收货人身份证号码
- (void)postIdentityCardCheckingWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_AddressIDNumber];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

#pragma mark -账户充值
// 创建充值付款订单
- (void)postRechargeAccountWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_RechargePay];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 创建充值订单
- (void)postRechargeFountWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_RechargeCreate];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 查询酒店入住人
- (void)getHotelOccupantWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_hotelPerson];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:URL parameters:nil whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 创建酒店入住人
- (void)postHotelOccupantWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_hotelPerson];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}
// 删除酒店入住人
- (void)deleteHotelOccupantWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_hotelPerson];
    [self userApikeySendHttpRequest:self.manager method:@"DELETE" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 查询价格和政策
- (void)getRoomPirceWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_OrderPay];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 创建酒店订单
- (void)postRoomOrderWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_OrderPay];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 酒店订房支付
- (void)putHotelPaymentOrderRoomWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_OrderPay];
    [self userApikeySendHttpRequest:self.manager method:@"PUT" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        completeBlock(nil, error);
    }];
}

#pragma mark -获取商品列表
/**
 获取商品列表
 @param parameter 分类类型：
 productCategoryTypeEnumId: * MallRecommendProduct 精品推荐 * ToResearchProduct 百里挑一 * MallDailyProduct 每日上新 * PctCatalog 导航 * Special 特色好货 * GuessYouLikeProduct 猜你喜欢
 sort: 排序（传desc价格从高到低、传asc价格从低到高、传sale销量排序）
 productCategoryId:三级分类ID下面的商品,如果不传则返回指定类型分类下所有商品
 */
- (void)postShopDetailsSortWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_ShopDetailsSort];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

#pragma mark -支付
// 查询订单支付信息
- (void)getPayOrderInfoWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_OrderData];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 多店铺订单支付
- (void)postPayManyOrderWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_ManyOrderData];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

/**
 GET快递查询
 orderId:订单号
 */
- (void)getKuaiDiDelailsWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_KuaiDi100];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

/**
 post快递查询
 orderId:订单号
 */
- (void)postKuaiDiDelailsWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_KuaiDi100];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 获取用户反馈
- (void)getFeedbackGetWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *url = [[self httpBaseURL] stringByAppendingString:URL_FeedBackGet];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:url parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 创建用户反馈
- (void)postFeedfackCreateWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_FeedBackCreate];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}


/**
 获取售后项目选择列表
 */
- (void)getAfterSaleVarietyWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *url = [[self httpBaseURL] stringByAppendingString:URL_AfterSaleItemList];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:url parameters:nil whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}


/**
 获取售后不满意选择
 @param afterSid afterSid:售后项目选择ID
 */
- (void)getAfterSaleReasonSelectWithAfterSid:(NSString *)afterSid Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *url = [[self httpBaseURL] stringByAppendingString:URL_AfterSaleCause];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:url parameters:@{@"afterSid":afterSid} whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

/**
 创建售后申请
 
 @param parameter
 @"asrId":具体原因项
 @"orderId":订单ID
 @"afterSid":原因项
 @"asExplain":描述
 @"productId":产品ID[带规格的ID]
 @"asOrderImages":图片字符串[使用,分隔]
 @"asRefundAmount":预计退款金额【有退款才传递】
 */
- (void)postAfterSaleEstablishWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_AfterSaleEstablish];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

/**
 售后申请更新
 @param parameter 参数同售后申请
 */
- (void)postAfterSaleUpdateWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_AfterSaleUpdate];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 创建售后物流
- (void)postAfterSaleExpressWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_AfterSaleExpress];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 获取售后单详情
- (void)getAfterSaleOrderDetailsWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *url = [[self httpBaseURL] stringByAppendingString:URL_AfterSaleDetails];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:url parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

// 获取七牛云上传Token
- (void)getQiNiuYunTokenWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_CommentsToken];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

#pragma mark -快递查询
/**
 查询所有快递公司
 */
- (void)getExpressageCorpListWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_ExpressageCorpList];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:URL parameters:nil whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

#pragma mark -评论
/**
 创建商品评论
 @param parameter orderId 、 productStoreId  、commentList
 */
- (void)postCommentShopCreatParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_CommentCreat];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

/**
 创建酒店评论
 @param parameter orderId 、 productStoreId  、commentList
 */
- (void)postCommentHotelCreatParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_CommentCreatHotel];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

/**
 创建酒店 评论、星级、条数
 @param parameter orderId 、 productStoreId  、commentList
 */
- (void)postCommentGetHotelParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_CommentGetHotel];
    [self userApikeySendHttpRequest:self.manager method:@"POST" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

/**
 酒店评论列表
 */
- (void)getCommentHotelViewListWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_ConmentList];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

/**
 订单评论界面接口
 */
- (void)getCommentOrderWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock
{
    NSString *URL = [[self httpBaseURL] stringByAppendingString:URL_CommentsProduct];
    [self userApikeySendHttpRequest:self.manager method:@"GET" url:URL parameters:parameter whileSuccess:^(id responseObject) {
        
        completeBlock(responseObject,nil);
    } orFail:^(id error) {
        
        completeBlock(nil, error);
    }];
}

#pragma mark -date转字符串
- (NSString *)stringFormatWithDate:(NSDate *)date
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStr = [format stringFromDate:date];
    return dateStr;
}

#pragma mark -判断网络状态
- (void)startMonitoringNetworkState
{
    // AFNetworkingReachabilityDidChangeNotification其它地方通过监听这个通知可以得到网络状态改变
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    // 检测网络连接的单例,网络变化时的回调方法
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (status == AFNetworkReachabilityStatusNotReachable) {
            [SVProgressHUD showErrorWithStatus:@"网络连接已断开"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"网络断开" object:nil userInfo:nil];
        }
        self.lasttNetworkState = self.currentNetworkState;
        self.currentNetworkState = status;
    }];
}


@end

