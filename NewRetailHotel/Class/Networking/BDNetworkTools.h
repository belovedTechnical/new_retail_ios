//
//  BDNetworkTools.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/5.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>


#define HTTP                  @"http://"
#define HTTPS                 @"https://"
// 基本网络请求
#pragma mark -  正式服务器
//#define URL_Base3           @"newretailmanage.beloved999.com:" //原来的服务器地址
//#define URL_Base            @"newretailmanage.beloved999.com:80/api/s1/"

#define Environment 1       //1 为正式环境  0 为测试环境

#if Environment

#define URL_Base            @"manage.beloved999.com:80/api/s1/"
#define HTML_URL_Base       @"http://m.beloved999.com/"
#define HTML_HOST           @"m.beloved999.com"

#else

#define URL_Base            @"jh193manage.beloved999.com:80/api/s1/"
#define HTML_URL_Base       @"http://jh193m.beloved999.com/"
#define HTML_HOST           @"jh193m.beloved999.com"


#endif

#define URL_Base4               @":7002"


typedef void (^SuccessBlock)(id responseObject);
typedef void (^FailBlock)(id error);

@interface BDNetworkTools : NSObject

/**
 *  网络服务单例
 *  @return 网络服务对象
 */
+ (instancetype)sharedInstance;

- (void)cancelAllRequests;

/** 获取管理员Apikey */
- (void)getLoginSignInBasicAuthWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 用户注册 */
- (void)postUserRegisterWihtParameters:(NSDictionary *)parameters Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 发送验证码 */
- (void)postLoginSendCodeWihtProduct:(NSString *)product Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 短信登录 */
- (void)postLoginSignInWihtParameters:(NSDictionary *)parameters Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlockl;

/** 重新设置密码 */
- (void)postResetPasswordWihtParameters:(NSDictionary *)parameters Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 密码登录 */
- (void)postPasswordLoginWihtParameters:(NSDictionary *)parameters Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

///** 个人用户信息 */
//- (void)getMeUserInfoAcountWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;
/** 用户信息 */
- (void)getUserAcountWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 用户基础信息(昵称、性别、生日) */
- (void)getUserAcountBasicWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 微信登录  openId登录 */
- (void)loginOpenIdDetailWithUnionId:(NSString *)unionId Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 注销登陆 */
- (void)getLoginSigOutWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 查询酒店列表 */
- (void)getHotelSeekWithFromDate:(NSDate *)fromDate ThruDate:(NSDate *)thruDate City:(NSString *)city County:(NSString *)county CityCode:(NSString *)cityCode Sort:(NSString *)sort PageIndex:(NSInteger)pageIndex PageSize:(NSInteger)pageSize Tag:(NSString *)tag Blcok:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 获取区列表 */
- (void)getLocationTownWithCity:(NSString *)cityCode Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 酒店订单列表 */
- (void)getHotelListWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 所有酒店ID列表 */
- (void)getHotelAllIDListWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 酒店订单取消 */
- (void)putHotelOrderCancelWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 酒店订单详情数据请求 */
- (void)getHotelOrderDetailsWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 订单删除 酒店 电商 共用 */
- (void)deleteOrderWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 订单数量查询 */
- (void)getOnlineOrderCountWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 电商订单列表 */
- (void)getOnlineListWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 电商订单列表详情 */
- (void)getOnlineListDetailWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 电商订单取消 */
- (void)putOnlineOrderCancelWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 电商订单确定 */
- (void)putOnlineOrderVerifyWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 优惠券 */
- (void)getCouponMeDataWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 账户余额请求 */
- (void)getAccountRemainingSumWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 账户积分请求 */
- (void)getAccountIntegralWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 收藏  商品删除 */
- (void)postCollectShopDeleteWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 收藏  店铺删除 */
- (void)postCollectStoreDeleteWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 商品收藏数据 */
- (void)getCollectShopDataWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 浏览记录数据 */
- (void)getScanRecordDataWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 清除浏览记录 */
- (void)getScanClearRecordWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 店铺收藏数据 */
- (void)getCollectStoreDataWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 购物车数据请求 */
- (void)getShopCartDataWithStoreID:(NSString *)storeId Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 添加购物车 */
- (void)postShopCartAddShopParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 删除选中商品 */
- (void)postShopCartDeleteSelectParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 清空购物车 */
- (void)deleteShopCartClearWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 更新个人信息 */
- (void)putUserInfoUpdateWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 效验验证码的是否有效正确 */
- (void)getSmsCheckingWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 修改手机号 */
- (void)putAmendPoneNumberAccoutWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 新建收货地址 */
- (void)postAddressNewWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 查询收货地址 */
- (void)getAddressRedactWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 设置默认地址 */
- (void)postAddressAcquiesceWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 删除收货地址 */
- (void)postAddressDeleteWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 修改收货地址 */
- (void)postAddressAmendWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 单店铺生成订单 */
- (void)postOrderMakeWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 多店铺生成订单 */
- (void)postOrderManyStoreMakeWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 单店铺价格计算 */
- (void)postOrderAlonePriceCountWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 多店铺产品价格计算 */
- (void)postOrderManyPriceCountWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 单店铺获取运费 */
- (void)postOrderAlonePriceFreightWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 多店铺获取运费 */
- (void)postOrderManyPriceFreightWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 单店铺优惠券选择接口 */
- (void)postOrderAloneCouponWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 多店铺优惠券选择接口 */
- (void)postOrderManyCouponWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 全球购验证收货人身份证号码 */
- (void)postIdentityCardCheckingWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 账户充值 */
- (void)postRechargeAccountWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 创建充值订单 */
- (void)postRechargeFountWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 查询酒店入住人 */
- (void)getHotelOccupantWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 创建酒店入住人 */
- (void)postHotelOccupantWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 删除酒店入住人 */
- (void)deleteHotelOccupantWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 查询价格和政策 */
- (void)getRoomPirceWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 创建酒店订单 */
- (void)postRoomOrderWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 酒店订房支付 */
- (void)putHotelPaymentOrderRoomWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/**
 获取商品列表
 @param parameter 分类类型：
 productCategoryTypeEnumId: * MallRecommendProduct 精品推荐 * ToResearchProduct 百里挑一 * MallDailyProduct 每日上新 * PctCatalog 导航 * Special 特色好货 * GuessYouLikeProduct 猜你喜欢
 sort: 排序（传desc价格从高到低、传asc价格从低到高、传sale销量排序）
 productCategoryId:三级分类ID下面的商品,如果不传则返回指定类型分类下所有商品
 */
- (void)postShopDetailsSortWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 查询订单支付信息 */
- (void)getPayOrderInfoWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 多店铺订单支付 */
- (void)postPayManyOrderWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 获取用户反馈 */
- (void)getFeedbackGetWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 创建用户反馈 */
- (void)postFeedfackCreateWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 获取售后项目选择列表 */
- (void)getAfterSaleVarietyWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/**
 获取售后不满意选择
 @param afterSid afterSid:售后项目选择ID
 */
- (void)getAfterSaleReasonSelectWithAfterSid:(NSString *)afterSid Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 创建售后申请 */
- (void)postAfterSaleEstablishWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 售后申请更新 */
- (void)postAfterSaleUpdateWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 创建售后物流 */
- (void)postAfterSaleExpressWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 获取售后单详情 */
- (void)getAfterSaleOrderDetailsWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** 获取七牛云上传Token */
- (void)getQiNiuYunTokenWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/**
 查询所有快递公司
 */
- (void)getExpressageCorpListWithBlock:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/** GET快递查询 */
- (void)getKuaiDiDelailsWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;
/**
 post快递查询
 orderId:订单号
 */
- (void)postKuaiDiDelailsWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

#pragma mark -评论
/**
 创建商品评论
 @param parameter orderId 、 productStoreId  、commentList
 */
- (void)postCommentShopCreatParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/**
 创建酒店评论
 @param parameter orderId 、 productStoreId  、commentList
 */
- (void)postCommentHotelCreatParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/**
 创建酒店 评论、星级、条数
 @param parameter orderId 、 productStoreId  、commentList
 */
- (void)postCommentGetHotelParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/**
 酒店评论列表
 */
- (void)getCommentHotelViewListWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

/**
 订单评论界面接口
 */
- (void)getCommentOrderWithParameter:(NSDictionary *)parameter Block:(void(^)(NSDictionary *responseObject, NSError *error))completeBlock;

#pragma mark -判断网络状态
/** -判断网络状态 */
- (void)startMonitoringNetworkState;

@end

