//
//  WebHotelDetailsViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/18.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "WebHotelDetailsViewController.h"

@interface WebHotelDetailsViewController ()

@end

@implementation WebHotelDetailsViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, StatusBarHeight)];
    topView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:topView];
    
    UIView *tabarBottomView = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenH-SafeAreaBottomHeight, kScreenW, SafeAreaBottomHeight)];
    tabarBottomView.backgroundColor = [UIColor colorWithHexString:@"#f6f6f6"];
    [self.view addSubview:tabarBottomView];
    
    UIButton *btnCustomBack = [[UIButton alloc] initWithFrame:CGRectMake(0, StatusBarHeight, 36, 35)];
    [btnCustomBack addTarget:self action:@selector(webBackBtnClick) forControlEvents:UIControlEventTouchUpInside];
    btnCustomBack.backgroundColor = [UIColor clearColor];
    [self.view addSubview:btnCustomBack];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (NSURL *)webURL
{
    NSLog(@"酒店：%@", _hotelDetailsURL);
    NSURL *url = [NSURL URLWithString:_hotelDetailsURL];
    return url;
}

- (void)webBackBtnClick {
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
