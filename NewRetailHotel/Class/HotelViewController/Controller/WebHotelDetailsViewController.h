//
//  WebHotelDetailsViewController.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/18.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIWebBaseViewController.h"

@interface WebHotelDetailsViewController : UIWebBaseViewController

@property (nonatomic, strong) NSString *hotelDetailsURL;

@end
