//
//  ScanningViewController.m
//  QRcode
//
//  Created by 王保栋 on 2017/10/2.
//  Copyright © 2017年 BeloverdSpace. All rights reserved.
//

#import "ScanningViewController.h"
#import "QRCodeManager.h"
#import <AVFoundation/AVFoundation.h>
//#import "ShopdetailViewController.h"
//#import "CoinDetilesViewController.h"
//#import "SpecialViewController.h"

@interface ScanningViewController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scanViewBottomCons;
@property (nonatomic, strong) AVCaptureSession *session;
@property (weak, nonatomic) IBOutlet UIView *scanView;
@property (weak, nonatomic) IBOutlet UIImageView *qrcodeBacldropImageView;
@property (nonatomic, strong) UIButton *flashOpen;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerHeightConstraint;

@end

@implementation ScanningViewController

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabBarController.tabBar.hidden = YES;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"相册" style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonItemClick:)];
    
    UIImage *image = [UIImage imageNamed:@"qrcode_border.png"];
    image = [image stretchableImageWithLeftCapWidth: image.size.width * 0.5 topCapHeight: image.size.height * 0.5];
    self.qrcodeBacldropImageView.image = image;
    
    [self addScanningAnimation];
    [self startScanning];
}

#pragma mark -添加扫描动画
- (void)addScanningAnimation
{
    // 1.设置冲击波底部和容器视图顶部对齐
    self.scanViewBottomCons.constant = -self.containerHeightConstraint.constant;
    [self.view layoutIfNeeded];
    
    // 2.执行扫描动画
    [UIView animateWithDuration:2.0 animations:^{
        
        [UIView setAnimationRepeatCount:MAXFLOAT];
        self.scanViewBottomCons.constant = self.containerHeightConstraint.constant;
        [self.view layoutIfNeeded];
    } completion:nil];
}

#pragma mark -开始扫描内容
- (void)startScanning
{
    [[QRCodeManager shareInstance] startScanningQRCodeWithInView:self.view scanView:self.scanView resultCallback:^(NSArray *results) {
        
        NSString *resultsStr = [NSString stringWithFormat:@"%@", results];
        if ([resultsStr containsString:@"http://"] || [resultsStr containsString:@"https://"]) {
//            if ([resultsStr containsString:@"coinDetiles"]) {
//                CoinDetilesViewController *coninDetilesVC = [[CoinDetilesViewController alloc] init];
//                coninDetilesVC.coinUrl = resultsStr;
//                [self.navigationController pushViewController:coninDetilesVC animated:YES];
//
//            }else if ([resultsStr containsString:@"shopDetiles"]) {
//                ShopdetailViewController *fashionDetailVC = [[ShopdetailViewController alloc]init];
//                fashionDetailVC.currentUrl = resultsStr;
//                [self.navigationController pushViewController:fashionDetailVC animated:YES];
//
//            }else {
//                SpecialViewController *specialVC = [[SpecialViewController alloc] init];
//                specialVC.specialUrl = resultsStr;
//                specialVC.navTitle = @"扫描信息";
//                [self.navigationController pushViewController:specialVC animated:YES];
//            }

        }else {
//            [self alertShowWithMessage:@"照片中未发现二维码" AscertainBlock:nil];
        }
    }];
}

#pragma mark - 从相册选择
- (void)rightBarButtonItemClick:(UIBarButtonItem *)item
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        controller.delegate = self;
        
        [self presentViewController:controller animated:YES completion:NULL];
    }
    else
    {
        [self showAlertWithTitle:@"提示" message:@"设备不支持访问相册" AscertainHandler:nil];
    }
}

#pragma mark -UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        UIImage *image = info[UIImagePickerControllerOriginalImage];
        [self findQRCodeFromImage:image];
    }];
//    [SVProgressHUD showWithStatus:@"正在识别信息"];
    // 注意: 如果实现了该方法, 当选中一张图片时系统就不会自动关闭相册控制器
    [picker dismissViewControllerAnimated:NO completion:nil];
}

- (void)findQRCodeFromImage:(UIImage *)image
{
    CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeQRCode
                                              context:nil
                                              options:@{CIDetectorAccuracy:CIDetectorAccuracyHigh}];
    
    NSArray *features = [detector featuresInImage:[CIImage imageWithCGImage:image.CGImage]];
    if (features.count >= 1)
    {
//        [SVProgressHUD dismiss];
        CIQRCodeFeature *feature = [features firstObject];
        if ([feature.messageString containsString:@"http://"] || [feature.messageString containsString:@"https://"]) {
//            ShopdetailViewController *fashionDetailVC = [[ShopdetailViewController alloc]init];
//            fashionDetailVC.currentUrl = feature.messageString;
//            [self.navigationController pushViewController:fashionDetailVC animated:YES];
        }
        // 2.停止会话
        [self.session stopRunning];
    }
    else
    {
//         [SVProgressHUD dismiss];
//         [self alertShowWithMessage:@"图片里没有二维码" AscertainBlock:nil];
    }
    
}

#pragma mark - 开关闪光灯
- (void)rightBarButtonDidClick:(UIBarButtonItem *)item
{
//    self.flashOpen = !self.flashOpen;
    
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    if ([device hasTorch] && [device hasFlash])
    {
        [device lockForConfiguration:nil];
        
        if (self.flashOpen)
        {
            device.torchMode = AVCaptureTorchModeOn;
            device.flashMode = AVCaptureFlashModeOn;
        }
        else
        {
            device.torchMode = AVCaptureTorchModeOff;
            device.flashMode = AVCaptureFlashModeOff;
        }
        
        [device unlockForConfiguration];
    }
}


@end
