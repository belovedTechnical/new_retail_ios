//
//  WebRoomDetailsViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/18.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "WebRoomDetailsViewController.h"

@interface WebRoomDetailsViewController ()

@end

@implementation WebRoomDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (CGRect)webViewFrame
{
    CGRect frame = CGRectMake(0, 0, kScreenW, kScreenH);
    return frame;
}
- (NSURL *)webURL
{
    NSURL *url = [NSURL URLWithString:_roomDetailsURL];
    return url;
}






@end
