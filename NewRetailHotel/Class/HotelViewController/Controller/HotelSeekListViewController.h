//
//  HotelSeekListViewController.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/21.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelSeekListViewController : UIViewController

@property (nonatomic, strong) NSString *startStr;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic, strong) NSString *nuberDays;
@property (nonatomic, strong) NSString *cityCode;
@property (nonatomic, copy) NSString *cityName;

@end
