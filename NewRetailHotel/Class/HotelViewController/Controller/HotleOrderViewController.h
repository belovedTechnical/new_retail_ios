//
//  HotleOrderViewController.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/23.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotleOrderViewController : UIViewController

@property (nonatomic, strong) NSString *productId;
/** 支付类型 到店付/在线付CashPayment/PrepaidPayment */
@property (nonatomic, strong) NSString *payType;
/** 房型类型 */
@property (nonatomic, strong) NSString *room_type;
/** 酒店名称 */
@property (nonatomic, strong) NSString *hotelName;
/** 房型id */
@property (nonatomic, strong) NSString *roomTypeId;
/** 酒店id */
@property (nonatomic, strong) NSString *hotelPartyId;

@end
