//
//  BDCalendarViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/20.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "BDCalendarViewController.h"
#import "BDCalendarHeaderView.h"
#import "BDCalendarLogic.h"
#import "BDCalendarModel.h"
#import "BDCalendarCell.h"

@interface BDCalendarViewController ()<
                                        UICollectionViewDataSource,
                                        UICollectionViewDelegate,
                                        UICollectionViewDelegateFlowLayout
                                        >
{
    int daynumber;                   //天数
    int optiondaynumber;             //选择日期数量
    UICollectionView *_calendarView;
    NSMutableArray *_dataArr;
    NSMutableArray *_monthArr;
}
@property(nonatomic,strong) UIButton *btnConfirm; // 确定
@property(nonatomic,strong) BDCalendarLogic *Logic;
@property (nonatomic, strong) NSString *selectedDate;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;

@end

static NSString * const calendarID = @"calendar";
static NSString * const calendaerHeaderID = @"calendaerHeader";
@implementation BDCalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _dataArr = [NSMutableArray array];
    _monthArr = [NSMutableArray array];
    self.title = @"请选择日期";
    [self createData];
    [self createCalendarView];
}

-(void)createData
{
    _Logic = [[BDCalendarLogic alloc] init];
    _monthArr = [_Logic reloadCalendarView:nil needMonth:2];
    [_calendarView reloadData];
}

-(void)createCalendarView
{
    
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, kScreenW, kScreenH - SafeAreaTopHeight)];
    [self.view addSubview:backView];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.itemSize = CGSizeMake(kScreenW / 7,kScreenW / 7);
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    layout.minimumInteritemSpacing = 0.0f;
    layout.minimumLineSpacing = 0.0f;
    
    _calendarView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, kScreenW, kScreenH - SafeAreaTopHeight) collectionViewLayout:layout];
    _calendarView.backgroundColor = [UIColor whiteColor];
    _calendarView.delegate = self;
    _calendarView.dataSource = self;
    _calendarView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_calendarView];
    
    UIView *bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = [UIColor whiteColor];
    [backView addSubview:bottomView];
    
    
    UIView *lineView = [[UIView alloc] init];
    bottomView.backgroundColor = [UIColor colorWithHexString:@"#a6926b"];
    [backView addSubview:lineView];
    
    
    _btnConfirm  = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnConfirm setTitle:@"确定" forState:UIControlStateNormal];
    [_btnConfirm addTarget:self action:@selector(confrimOrderRoomDate) forControlEvents:UIControlEventTouchUpInside];
    [_btnConfirm setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    _btnConfirm.titleLabel.font = [UIFont systemFontOfSize:15];
    _btnConfirm.backgroundColor = [UIColor colorWithHexString:@"#a6926b"];
    _btnConfirm.layer.cornerRadius = 5;
    [bottomView addSubview:_btnConfirm];
    
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.bottom.mas_equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(@40);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bottomView.mas_top);
        make.left.mas_equalTo(bottomView.mas_left);
        make.right.mas_equalTo(bottomView.mas_right);
        make.height.equalTo(@1);
    }];
    
    [_calendarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(backView.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.bottom.mas_equalTo(bottomView.mas_top);
    }];
    
    [_btnConfirm mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@150);
        make.height.equalTo(@25);
        make.centerX.mas_equalTo(bottomView.mas_centerX);
        make.centerY.mas_equalTo(bottomView.mas_centerY);
    }];
    
    
    [_calendarView registerNib:[UINib nibWithNibName:@"BDCalendarCell" bundle:nil] forCellWithReuseIdentifier:calendarID];
    [_calendarView registerNib:[UINib nibWithNibName:@"BDCalendarHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:calendaerHeaderID];
}

- (void)confrimOrderRoomDate {
    if(self.startDate != nil && self.endDate != nil){
        NSTimeInterval interval = [_endDate timeIntervalSinceDate:_startDate];
        NSUInteger resultDays = ((NSInteger)interval)/(3600*24);
        NSString *numberDays = [NSString stringWithFormat:@"共%lu晚", (unsigned long)resultDays];
        NSString *changeStartDate = [self stringSwitchoverDate:_startDate];
        NSString *changeEndDateStr = [self stringSwitchoverDate:_endDate];
        [self.navigationController popViewControllerAnimated:YES];
        //发送变订房日期变更通知
        NSDictionary *dateDict = @{@"changeStartDate":changeStartDate, @"changeEndDateStr":changeEndDateStr, @"numberDays":numberDays, @"startDate":_startDate, @"endDate":_endDate};
        [[NSNotificationCenter defaultCenter] postNotificationName:kChangeOrderRoomDate object:nil userInfo:dateDict];
    }
}

#pragma mark -UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return _monthArr.count;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_monthArr[section] count];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BDCalendarCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:calendarID forIndexPath:indexPath];
    BDCalendarModel *model = [_monthArr[indexPath.section] objectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    BDCalendarModel *model = [_monthArr[indexPath.section] objectAtIndex:indexPath.row];
    if(model.style != CellDayTypePast){
        //还没有选择范围
        if(_startDate == nil && _endDate == nil){
            _startDate = [model date];
            model.style = CellDayTypeClick;
        }else if (_startDate != nil && _endDate == nil){
            NSDate * end_date = [model date];
            for (NSMutableArray *array in _monthArr) {
                for (BDCalendarModel *itemModel in array) {
                    if(itemModel.style == CellDayTypeClick){
                        itemModel.style = CellDayTypeFutur;
                    }

                    if(itemModel.style == CellDayTypeFutur){
                        NSTimeInterval interval = [[itemModel date] timeIntervalSinceDate:_startDate];
                        NSTimeInterval interval2 = [[itemModel date] timeIntervalSinceDate:end_date];
                        if(interval >= 0 && interval2 <=0){
                            itemModel.style = CellDayTypeClick;
                        }
                    }
                }
            }


            if([end_date timeIntervalSinceDate:_startDate] > 0){
                _endDate = end_date;
            }else{
                _startDate = [model date];
                _endDate = nil;
                model.style = CellDayTypeClick;
            }
        }else if (_startDate != nil && _endDate != nil){
            for (NSMutableArray *array in _monthArr) {
                for (BDCalendarModel *itemModel in array) {
                    if(itemModel.style == CellDayTypeClick){
                        NSTimeInterval interval = [[itemModel date] timeIntervalSinceDate:_startDate];
                        NSTimeInterval interval2 = [[itemModel date] timeIntervalSinceDate:_endDate];
                        if(interval >= 0 && interval2 <= 0){
                            itemModel.style = CellDayTypeFutur;
                        }
                    }
                }
            }
            _startDate = [model date];
            _endDate = nil;
            model.style = CellDayTypeClick;
        }
    }
    [_calendarView reloadData];
}

//显示月份头部信息
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    BDCalendarHeaderView *calendarHeaderView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:calendaerHeaderID forIndexPath:indexPath];
    BDCalendarModel *model = [_monthArr[indexPath.section] objectAtIndex:15];
    calendarHeaderView.titleStr = [NSString stringWithFormat:@"%ld年%ld月",(unsigned long)model.year,(unsigned long)model.month];
    return calendarHeaderView;
}

//月份头部 RectSize
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(self.view.frame.size.width, 70);
}

- (NSString *)stringSwitchoverDate:(NSDate *)date
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MM月dd日"];
    NSString *currentlyDate = [format stringFromDate:date];
    return currentlyDate;
}

@end
