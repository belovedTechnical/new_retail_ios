//
//  BDCalendarModel.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/20.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSDate+BDCalendar.h"

#define CAN_SHOW 1      //可以被点击
#define CANNOT_SHOW 0   //不能被点击

typedef NS_ENUM(NSInteger, CollectionViewCellDayType) {
    CellDayTypeEmpty,       //不显示
    CellDayTypePast,        //过去的日期
    CellDayTypeFutur,       //将来的日期
    CellDayTypeWeek,        //周末
    CellDayTypeClick,       //被点击的日期
    CellDayTypeStart,       //开始日期
    CellDayTypeEnd,         //结束日期
    CellDayTypeStartAndEnd, //起止日期
    CellDayTypeYuQi,        //逾期日期
};

@interface BDCalendarModel : NSObject

@property (nonatomic, assign) NSUInteger day;               //天
@property (nonatomic, assign) NSUInteger month;             //月
@property (nonatomic, assign) NSUInteger year;              //年
@property (nonatomic, assign) NSUInteger week;              //周
@property (nonatomic, strong) NSString *holiday;            //节日
@property (nonatomic, strong) NSString *Chinese_calendar;   //农历

@property (assign, nonatomic) CollectionViewCellDayType style; //显示的样式

+ (BDCalendarModel *)calendarDayWithYear:(NSUInteger)year month:(NSUInteger)month day:(NSUInteger)day;

- (NSDate *)date;            //返回当前model的NSDate对象
- (NSString *)toString;      //返回当前model的NSString对象
- (NSString *)getWeek;       //返回星期

@end
