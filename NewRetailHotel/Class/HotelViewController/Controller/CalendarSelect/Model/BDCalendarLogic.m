//
//  BDCalendarLogic.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/20.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "BDCalendarLogic.h"
#import "BDCalendarModel.h"
#import "HotelSearchData.h"
#import "NSDate+BDCalendar.h"

@interface BDCalendarLogic()
{
    NSDate *today;  //今天的日期
    NSDate *before; //2个月之后的日期
    NSDate *_yuqiDate;
    BDCalendarModel *selectcalendarDay;
    HotelSearchData *searchData;
}
@end

@implementation BDCalendarLogic

@synthesize startDate = _startDate;
@synthesize endDate = _endDate;

//计算当前日期之前几天或者是之后的几天（负数是之前几天，正数是之后的几天）
- (NSMutableArray *)reloadCalendarView:(NSDate *)date
                             needMonth:(int)month_number
{
    searchData = [HotelSearchData sharedHotelSearchData];
    //如果为空就从当天的日期开始
    if(date == nil){
        date = [NSDate date];
    }
    today = date;//起始日期
    before = [date dayInTheFollowingMonth:month_number];//计算它days天以后的时间
    NSDateComponents *todayDC = [today YMDComponents];
    NSDateComponents *beforeDC = [before YMDComponents];
    NSInteger todayYear = todayDC.year;
    NSInteger todayMonth = todayDC.month;
    NSInteger beforeYear = beforeDC.year;
    NSInteger beforeMonth = beforeDC.month;
    NSInteger months = (beforeYear-todayYear) * 12 + (beforeMonth - todayMonth); //选中与补选中相差的月份
    NSMutableArray *calendarMonth = [[NSMutableArray alloc] init]; //每个月的dayModel数组
    
    for (int i = 0; i <= months; i++) {
        NSDate *month = [today dayInTheFollowingMonth:i];
        NSMutableArray *calendarDays = [[NSMutableArray alloc] init];
        [self calculateDaysInPreviousMonthWithDate:month andArray:calendarDays];//计算上月份的天数
        [self calculateDaysInCurrentMonthWithDate:month andArray:calendarDays];//计算当月的天数
        [self calculateDaysInFollowingMonthWithDate:month andArray:calendarDays];//计算下月份的天数
        [calendarMonth insertObject:calendarDays atIndex:i];
    }
    return calendarMonth;
}


//计算上月份的天数
- (void)calculateDaysInPreviousMonthWithDate:(NSDate *)date andArray:(NSMutableArray *)array
{
    NSUInteger weeklyOrdinality = [[date firstDayOfCurrentMonth] weeklyOrdinality]; //计算这个的第一天是礼拜几,并转为int型
    NSDate *dayInThePreviousMonth = [date dayInThePreviousMonth];//上一个月的NSDate对象
    NSUInteger daysCount = [dayInThePreviousMonth numberOfDaysInCurrentMonth];//计算上个月有多少天
    NSUInteger partialDaysCount = weeklyOrdinality - 1;//获取上月在这个月的日历上显示的天数
    NSDateComponents *components = [dayInThePreviousMonth YMDComponents];//获取年月日对象
    for (int i = (int)daysCount - (int)partialDaysCount + 1; i < daysCount + 1; ++i) {
        BDCalendarModel *calendarDay = [BDCalendarModel calendarDayWithYear:components.year month:components.month day:i];
        //style不需要计算
        calendarDay.style = CellDayTypePast;
        [array addObject:calendarDay];
    }
}


//计算下月份的天数
- (void)calculateDaysInFollowingMonthWithDate:(NSDate *)date andArray:(NSMutableArray *)array
{
    NSUInteger weeklyOrdinality = [[date lastDayOfCurrentMonth] weeklyOrdinality];
    if (weeklyOrdinality == 7) return ;
    NSUInteger partialDaysCount = 7 - weeklyOrdinality;
    NSDateComponents *components = [[date dayInTheFollowingMonth] YMDComponents];
    for (int i = 1; i < partialDaysCount + 1; ++i) {
        BDCalendarModel *calendarDay = [BDCalendarModel calendarDayWithYear:components.year month:components.month day:i];
        calendarDay.style = CellDayTypePast;
        [array addObject:calendarDay];
    }
}

//计算当月的天数
- (void)calculateDaysInCurrentMonthWithDate:(NSDate *)date andArray:(NSMutableArray *)array
{
    NSUInteger daysCount = [date numberOfDaysInCurrentMonth]; //计算这个月有多少天
    NSDateComponents *components = [date YMDComponents];      //今天日期的年月日
    for (int i = 1; i < daysCount + 1; ++i){
        BDCalendarModel *calendarDay = [BDCalendarModel calendarDayWithYear:components.year month:components.month day:i];
        NSDate *calendarDayDate = [calendarDay date];
        NSDate *today = [[NSDate date] YMDCurrentlyDate];
        NSDate *tomorrow =  [[[NSDate date] dateByAddingTimeInterval:3600 *48] YMDCurrentlyDate];
        NSTimeInterval interval = [calendarDayDate timeIntervalSinceDate:today];
        NSTimeInterval interval2 = [calendarDayDate timeIntervalSinceDate:tomorrow];
        if(interval >= 0 && interval2 <= 0){
            calendarDay.style = CellDayTypeClick;
        }else{
            [self changStyle:calendarDay];
        }
        [array addObject:calendarDay];
    }
}


- (void)changStyle:(BDCalendarModel *)calendarDay
{
    NSDate *nowDate = [NSDate date];
    NSDateComponents *calendarNow = [nowDate YMDComponents]; //今天
    //    NSDateComponents *calendarStart = [_startDate YMDComponents];//
    //    NSDateComponents *calendarEnd = [_endDate YMDComponents];//
    //    NSDateComponents *calendarYuQi = [_yuqiDate YMDComponents];//
    if (calendarDay.year < calendarNow.year || (calendarDay.year==calendarNow.year && (calendarDay.month<calendarNow.month ||(calendarDay.month == calendarNow.month&&calendarDay.day < calendarNow.day)))){
        calendarDay.style = CellDayTypePast;
    } else {
        calendarDay.style=CellDayTypeFutur;
    }
}


@end
