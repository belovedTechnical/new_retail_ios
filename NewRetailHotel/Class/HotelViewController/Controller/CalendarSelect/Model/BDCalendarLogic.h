//
//  BDCalendarLogic.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/20.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BDCalendarLogic : NSObject

@property(nonatomic,strong) NSString *titleStr;
@property(nonatomic,strong) NSDate *startDate;
@property(nonatomic,strong) NSDate *endDate;

- (NSMutableArray *)reloadCalendarView:(NSDate *)date
                             needMonth:(int)month_number;

@end
