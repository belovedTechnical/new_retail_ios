//
//  BDCalendarModel.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/20.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "BDCalendarModel.h"

@interface BDCalendarModel()

@property (strong,nonatomic) NSCalendar *greCalendar;
@property (strong,nonatomic) NSDateComponents *dateComponentsForDate;

@end

@implementation BDCalendarModel

+ (BDCalendarModel *)calendarDayWithYear:(NSUInteger)year month:(NSUInteger)month day:(NSUInteger)day
{
    BDCalendarModel *calendarDay = [[self alloc] init];
    calendarDay.year = year;//年
    calendarDay.month = month;//月
    calendarDay.day = day;//日
    
    calendarDay.greCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [calendarDay.greCalendar setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Shanghai"]];
    calendarDay.dateComponentsForDate = [[NSDateComponents alloc] init];
    
    return calendarDay;
}

//返回当前model的NSDate对象
- (NSDate *)date
{
    self.dateComponentsForDate.year = self.year;
    self.dateComponentsForDate.month = self.month;
    self.dateComponentsForDate.day = self.day;
    NSDate *dateComponents = [self.greCalendar dateFromComponents:self.dateComponentsForDate];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMT];
    return [dateComponents dateByAddingTimeInterval:interval];
}

//返回当前model的NSString对象
- (NSString *)toString
{
    NSDate *date = [self date];
    NSString *string = [date stringFromDate:date];
    return string;
}

//返回星期
- (NSString *)getWeek
{
    NSDate *date = [self date];
    NSString *week_str = [date compareIfTodayWithDate];
    return week_str;
}

//判断是不是同一天
- (BOOL)isEqualTo:(BDCalendarModel *)day
{
    BOOL isEqual = (self.year == day.year) && (self.month == day.month) && (self.day == day.day);
    return isEqual;
}



@end
