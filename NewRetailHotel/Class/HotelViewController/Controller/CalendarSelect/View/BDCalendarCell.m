//
//  BDCalendarCell.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/20.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "BDCalendarCell.h"
#import "BDCalendarModel.h"

@interface BDCalendarCell()

@property (weak, nonatomic) IBOutlet UILabel *numberLab;
@property (weak, nonatomic) IBOutlet UIImageView *backImgView;

@end

@implementation BDCalendarCell

-(void)setModel:(BDCalendarModel *)model
{
    _model = model;
    _numberLab.text = [NSString stringWithFormat:@"%ld",(unsigned long)model.day];
    switch (model.style) {
        case CellDayTypeEmpty: //不显示
            _numberLab.hidden = YES;
            _numberLab.textColor = [UIColor grayColor];
            _backImgView.hidden = YES;
            
            break;
        case CellDayTypePast: //过去的日期
            _numberLab.hidden = NO;
            _numberLab.textColor = UIColorFromRGB(0x5d584a);
            _backImgView.hidden = YES;
            break;
        case CellDayTypeFutur: //将来的日期
            _numberLab.hidden = NO;
            _numberLab.textColor = UIColorFromRGB(0x000000);
            _backImgView.hidden = YES;
            break;
            
        case CellDayTypeClick:
            _numberLab.hidden = NO;
            _numberLab.textColor =  [UIColor colorWithHexString:@"#E22425"];
            _backImgView.hidden = NO;
            _backImgView.image = [UIImage imageNamed:@"day_icon.png"];
            //            if ([self.titleStr isEqualToString:@"逾期"]) {
            //                _backImgView.image=[UIImage imageNamed:@"ybxzdy"];
            //                _numberLab.textColor=[UIColor grayColor];
            //            }
            break;
            
        default:
            break;
    }
}


@end
