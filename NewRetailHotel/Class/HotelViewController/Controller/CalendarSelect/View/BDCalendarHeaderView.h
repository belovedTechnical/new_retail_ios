//
//  BDCalendarHeaderView.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/20.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BDCalendarHeaderView : UICollectionReusableView

@property(nonatomic,strong)NSString *titleStr;

@end
