//
//  BDCalendarHeaderView.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/20.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "BDCalendarHeaderView.h"

@interface BDCalendarHeaderView()

@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@end

@implementation BDCalendarHeaderView

-(void)setTitleStr:(NSString *)titleStr
{
    _titleStr = titleStr;
    _titleLab.text = titleStr;
}

@end
