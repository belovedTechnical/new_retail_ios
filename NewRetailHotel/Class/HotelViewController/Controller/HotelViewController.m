//
//  HotelViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/7.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "HotelViewController.h"
#import "ScanningViewController.h"
#import "SelectCityViewController.h"
#import "BDCalendarViewController.h"
#import "HotelSeekListViewController.h"
#import "WebSearchViewController.h"
#import "BottomButton.h"
#import "HotelTopView.h"
#import "HotelSeekView.h"
#import "SaveTool.h"

#import <CoreLocation/CoreLocation.h>

@interface HotelViewController ()<CLLocationManagerDelegate>

@property (nonatomic, strong) HotelTopView *hotelTopView;
@property (nonatomic, strong) HotelSeekView *hotelSeekView;
@property (nonatomic, strong) CLLocationManager *locationM;
/** 地理编码对象 */
@property (nonatomic, strong) CLGeocoder *geoc;
/** 顶部定位、搜索、扫一扫容器View */
@property (nonatomic, strong) UIView *topContainerView;
@property (nonatomic, strong) NSString *userLocationConcreteness;
@property (nonatomic, copy) NSString *cityCode;
@property (nonatomic, copy) NSString *cityName;

@end

@implementation HotelViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeOrderRoomDate:) name:kChangeOrderRoomDate object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initUI
{
    UIImageView *bgImage = [[UIImageView alloc] initWithFrame:self.view.bounds];
    bgImage.userInteractionEnabled = YES;
    bgImage.image = [UIImage imageNamed:@"bg"];
    [self.view addSubview:bgImage];
    
    [self.locationM startUpdatingLocation];
    self.hotelTopView = [[HotelTopView alloc] initWithFrame: CGRectMake(0, 0, kScreenW, SafeAreaTopHeight)];
    [bgImage addSubview:self.hotelTopView];
    
    CGFloat seekX = 20;
    HotelSeekView *seekView = [[HotelSeekView alloc] initWithFrame:CGRectMake(seekX, 0, kScreenW - seekX*2, 185)];
    seekView.center = self.view.center;
    [bgImage addSubview:seekView];
    self.hotelSeekView = seekView;
    
    UIImageView *bottomImage = [[UIImageView alloc] init];
    bottomImage.frame = CGRectMake(25, seekView.bd_bottom+75, kScreenW-50, 27);
    bottomImage.image = [UIImage imageNamed:@"icon_service"];
    [bgImage addSubview:bottomImage];
    
    [self.hotelTopView.searchBtn addTarget:self action:@selector(searchBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.hotelTopView.scanBtn addTarget:self action:@selector(scanBntClick) forControlEvents:UIControlEventTouchUpInside];
    [seekView.btnDestination addTarget:self action:@selector(destinationSelectClick) forControlEvents:UIControlEventTouchUpInside];
    [seekView.btnSeek addTarget:self action:@selector(wineshopSeekBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [seekView.btnLocation addTarget:self action:@selector(btnLocationClick) forControlEvents:UIControlEventTouchUpInside];
    [seekView.btnInputDate addTarget:self action:@selector(inputDateBtnClick) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark -扫一扫点击
- (void)scanBntClick
{
    self.navigationController.navigationBar.hidden = NO;
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Scanning" bundle:[NSBundle mainBundle]];
    ScanningViewController *scaningVC = [story instantiateViewControllerWithIdentifier:@"Scanning"];
    [self.navigationController pushViewController:scaningVC animated:YES];
}

#pragma mark -搜索点击
- (void)searchBtnClick
{
    WebSearchViewController *searchVC = [[WebSearchViewController alloc] init];
    self.navigationController.navigationBar.hidden = NO;
    [self.navigationController pushViewController:searchVC animated:YES];
}

// 我的位置
- (void)btnLocationClick
{
    [self.hotelSeekView.btnDestination setTitle:_userLocationConcreteness forState:UIControlStateNormal];
}

// 目的地选择
- (void)destinationSelectClick
{
    self.navigationController.navigationBar.hidden = NO;
    SelectCityViewController *seleCtiyVC = [[SelectCityViewController alloc] init];
    [self.navigationController pushViewController:seleCtiyVC animated:NO];
    seleCtiyVC.searchHostBlock = ^(NSString *cityName, NSString *cityCode) {
        [self.hotelSeekView.btnDestination setTitle:cityName forState:UIControlStateNormal];
        self.cityCode = cityCode;
        self.cityCode = cityName;
    };
}

// 输入住房时间
- (void)inputDateBtnClick
{
    self.navigationController.navigationBar.hidden = NO;
    BDCalendarViewController *calendarVC = [[BDCalendarViewController alloc] init];
    [self.navigationController pushViewController:calendarVC animated:NO];
}

// 酒店日期选择改变通知
- (void)changeOrderRoomDate:(NSNotification *)notif {
    NSDate *startDate = notif.userInfo[@"startDate"];
    NSDate *endDate = notif.userInfo[@"endDate"];
    NSString *startStr = notif.userInfo[@"changeStartDate"];
    NSString *endStr = notif.userInfo[@"changeEndDateStr"];
    NSString *nuberDays = notif.userInfo[@"numberDays"];
    _hotelSeekView.dateStart.text = startStr;
    _hotelSeekView.dateDeparture.text = endStr;
    _hotelSeekView.lbGross.text = nuberDays;
    _hotelSeekView.startDate = startDate;
    _hotelSeekView.endDate = endDate;
}

// 查询酒店
- (void)wineshopSeekBtnClick
{
    [SaveTool setObject:_hotelSeekView.dateStart.text forKey:checkInStrKey];
    [SaveTool setObject:_hotelSeekView.dateDeparture.text forKey:checkOutStrKey];
    [SaveTool setObject:_hotelSeekView.startDate forKey:checkInDateKey];
    [SaveTool setObject:_hotelSeekView.endDate forKey:checkOutDateKey];
    [SaveTool setObject:_hotelSeekView.lbGross.text forKey:numberDaysKey];
    HotelSeekListViewController *seekListVC = [[HotelSeekListViewController alloc] init];
    seekListVC.startDate = _hotelSeekView.startDate;
    seekListVC.endDate = _hotelSeekView.endDate;
    seekListVC.startStr = _hotelSeekView.dateStart.text;
    seekListVC.nuberDays = _hotelSeekView.lbGross.text;
    seekListVC.cityCode = _cityCode == nil ? @"440300":_cityCode;
    seekListVC.cityName = _cityName == nil ? @"深圳市": _cityName;
    [self.navigationController pushViewController:seekListVC animated:YES];
}

#pragma mark -CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    // 1.取出最新位置
    CLLocation *location = [locations lastObject];
    // 2.反地理编码
    [self.geoc reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        if (error == nil) {
            CLPlacemark *pl = [placemarks firstObject];
            self.userLocationConcreteness = [NSString stringWithFormat:@"%@%@%@",pl.locality,pl.subLocality,pl.name];
            _hotelTopView.lbLocation.text = pl.locality;
            _cityName = pl.locality;
            [self.locationM stopUpdatingLocation];
        }
    }];
}

- (NSDate *)dateFromString:(NSString *)dateStr
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MM月dd日"];
    NSDate *date = [format dateFromString:dateStr];
    return date;
}

#pragma mark -懒加载
- (CLLocationManager *)locationM
{
    if (!_locationM) {
        _locationM = [[CLLocationManager alloc] init];
        _locationM.delegate = self;
        [_locationM requestAlwaysAuthorization];
        _locationM.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    }
    return _locationM;
}

- (CLGeocoder *)geoc
{
    if (!_geoc) {
        _geoc = [[CLGeocoder alloc] init];
    }
    return _geoc;
}


@end
