//
//  HotleOrderViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/23.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "HotleOrderViewController.h"
#import "AddOccupantViewController.h"
#import "BDCalendarViewController.h"
#import "PayOderViewController.h"
#import "HotleOrderView.h"
#import "NSDate+BDCalendar.h"
#import "BDMobileNumberTools.h"
#import "HotelOrderListViewController.h"
#import "SaveTool.h"

@interface HotleOrderViewController ()

@property (nonatomic, strong) HotleOrderView *hotleOrderView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic, strong) UILabel *lbPrice;

@end

@implementation HotleOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f6f6f6"];
    self.title = @"订单填写";
    self.startDate = [SaveTool objectForKey:checkInDateKey];
    self.endDate = [SaveTool objectForKey:checkOutDateKey];
    [self initUI];
    [self computeRoomPrice];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeOrderRoomDate:) name:kChangeOrderRoomDate object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)initUI
{
    CGFloat bottomViewH = 48;
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH - bottomViewH)];
    scrollView.contentSize = CGSizeMake(0, kScreenH+120);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    self.scrollView = scrollView;
    
    _hotleOrderView = [[HotleOrderView alloc] initWithFrame:scrollView.bounds];
    _hotleOrderView.lbHotleName.text = _hotelName;
    [scrollView addSubview:_hotleOrderView];
    [_hotleOrderView.btnAddOccupant addTarget:self action:@selector(btnAddOccupantClick) forControlEvents:UIControlEventTouchUpInside];
    [_hotleOrderView.btnDateClick addTarget:self action:@selector(btnDateClickSelect) forControlEvents:UIControlEventTouchUpInside];
    [_hotleOrderView.btnSelectRoom addTarget:self action:@selector(btnSelectRoomClick:) forControlEvents:UIControlEventTouchUpInside];
    [_hotleOrderView.tfNumber addTarget:self action:@selector(textFieldTextDidBegin:) forControlEvents:UIControlEventEditingDidBegin];
    [_hotleOrderView.tfNumber addTarget:self action:@selector(textFieldTextDidEnd:) forControlEvents:UIControlEventEditingDidEnd];
    [_hotleOrderView.btnPhone addTarget:self action:@selector(calliPhoneClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *bottomView = [[UIView alloc] init];
    bottomView.frame = CGRectMake(0, kScreenH-bottomViewH-SafeAreaBottomHeight, kScreenW, bottomViewH);
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomView];
    
    UILabel *lbPrice = [[UILabel alloc] init];
    lbPrice.frame = CGRectMake(15, 0, 100, bottomViewH);
    lbPrice.textColor = [UIColor colorWithHexString:@"#b4282d"];
    lbPrice.font = [UIFont systemFontOfSize:15];
    lbPrice.text = @"总额:¥00.00";
    [bottomView addSubview:lbPrice];
    self.lbPrice = lbPrice;
    
    UIButton *btnDetailed = [[UIButton alloc] init];
    btnDetailed.frame = CGRectMake(lbPrice.bd_right, 0, 50, bottomViewH);
    [btnDetailed setTitleColor:[UIColor colorWithHexString:@"#a6926b"] forState:UIControlStateNormal];
    [btnDetailed addTarget:self action:@selector(btnDetailedClick) forControlEvents:UIControlEventTouchUpInside];
    [btnDetailed setTitle:@"明细" forState:UIControlStateNormal];
    btnDetailed.titleLabel.font = [UIFont systemFontOfSize:15];
    [bottomView addSubview:btnDetailed];
    
    CGFloat defrayW = 150;
    UIButton *btnDefray = [[UIButton alloc] init];
    btnDefray.frame = CGRectMake(kScreenW-defrayW, 0, defrayW, bottomViewH);
    [btnDefray setTitleColor:[UIColor colorWithHexString:@"#ffffff"] forState:UIControlStateNormal];
    btnDefray.backgroundColor = [UIColor redColor];
    [btnDefray setTitle:@"提交订单" forState:UIControlStateNormal];
    [btnDefray addTarget:self action:@selector(immediatelyPayment) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:btnDefray];
}

#pragma mark -按钮点击
- (void)btnAddOccupantClick
{
    AddOccupantViewController *occupantVC = [[AddOccupantViewController alloc] init];
    occupantVC.nameStr = _hotleOrderView.nameLb.text;
    [self.navigationController pushViewController:occupantVC animated:YES];
    occupantVC.occupantNameBlcok = ^(NSArray *nameArray) {
        _hotleOrderView.nameLb.text = [nameArray componentsJoinedByString:@" "];
    };
}

// 立即支付
- (void)immediatelyPayment
{
    if ( [BDMobileNumberTools isMobileNumber:_hotleOrderView.tfPhone.text] == YES
        && _hotleOrderView.nameLb.text != nil) {
                [self foundRoomOrder];
    }else {
        [self showAlertWithTitle:@"请检查" message:@"入住人或手机号不完整" AscertainHandler:nil CancelHandler:nil];
    }
}

// 明细按钮点击
- (void)btnDetailedClick
{
    
}

// 住房日期选择
- (void)btnDateClickSelect
{
    BDCalendarViewController *calendarVC = [[BDCalendarViewController alloc] init];
    [self.navigationController pushViewController:calendarVC animated:NO];
}

// 酒店日期选择改变通知
- (void)changeOrderRoomDate:(NSNotification *)notif
{
    NSDate *startDate = notif.userInfo[@"startDate"];
    NSDate *endDate = notif.userInfo[@"endDate"];
    NSString *startStr = notif.userInfo[@"changeStartDate"];
    NSString *endStr = notif.userInfo[@"changeEndDateStr"];
    NSString *nuberDays = notif.userInfo[@"numberDays"];
    _hotleOrderView.lbDateEnter.text = startStr;
    _hotleOrderView.lbDateOut.text = endStr;
    _hotleOrderView.lbDayAll.text = nuberDays;
    self.startDate = startDate;
    self.endDate = endDate;
    [SaveTool setObject:nuberDays forKey:numberDaysKey];
    [self computeRoomPrice];
}

- (void)calliPhoneClick
{
    NSString *phone = @"400-811-6519";
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@""
                                                                     message:phone
                                                              preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定"
                                                      style:(UIAlertActionStyleDefault)
                                                    handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phone]];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消"
                                                      style:(UIAlertActionStyleDefault) handler:nil];
                        [alertVC addAction:action1];
                        [alertVC addAction:action2];
    [self presentViewController:alertVC animated:YES completion:nil];
}

- (void)btnSelectRoomClick:(UIButton *)sender
{
    UIAlertController *alerVC = [UIAlertController alertControllerWithTitle:@"" message:@"请选择房间数量" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *alert1 = [UIAlertAction actionWithTitle:@"1间" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [sender setTitle:@"1 " forState:UIControlStateNormal];
        [self computeRoomPrice];
    }];
    UIAlertAction *alert2 = [UIAlertAction actionWithTitle:@"2间" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [sender setTitle:@"2 " forState:UIControlStateNormal];
        [self computeRoomPrice];
    }];
    UIAlertAction *alert3 = [UIAlertAction actionWithTitle:@"3间" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [sender setTitle:@"3 " forState:UIControlStateNormal];
        [self computeRoomPrice];
    }];
    [alerVC addAction:alert1];
    [alerVC addAction:alert2];
    [alerVC addAction:alert3];
    [self presentViewController:alerVC animated:YES completion:nil];
}

- (void)textFieldTextDidBegin:(UITextField *)textField
{
    self.scrollView.frame = CGRectMake(0, -120, kScreenW, kScreenH);
}

- (void)textFieldTextDidEnd:(UITextField *)textField
{
    self.scrollView.contentSize = CGSizeMake(0, kScreenH+200);
}

- (NSString *)achieveCurrentlyDate:(NSDate *)date
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"YYYY-MM-dd"];
    NSString *currentlyDate = [format stringFromDate:date];
    return currentlyDate;
}

#pragma mark -网络请求
- (void)computeRoomPrice
{
    if (_startDate == nil || _endDate == nil) {
        NSDate *date = [[NSDate alloc] init];
        NSDate *tomorrow = [[NSDate date] dateByAddingTimeInterval:3600*24];
        _startDate = date;
        _endDate = tomorrow;
    }
    NSString *strStart = [self achieveCurrentlyDate:_startDate];
    NSString *strEnd = [self achieveCurrentlyDate:_endDate];
    NSDictionary *parameter = @{
                                @"fromDate":strStart,
                                @"thruDate":strEnd,
                                @"payType":_payType,//【预付：PrepaidPayment 到店付：CashPayment】
                                @"productId":_roomTypeId,
                                @"quantity":[NSString stringWithFormat:@"%@", _hotleOrderView.btnSelectRoom.titleLabel.text]
                                };
    [[BDNetworkTools sharedInstance] getRoomPirceWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        _lbPrice.text = @"";
        _lbPrice.text = [NSString stringWithFormat:@"总额:¥%@", [responseObject objectForKey:@"totalPrice"]];
    }];
}

- (void)foundRoomOrder
{
    NSString *strStart = [self achieveCurrentlyDate:_startDate];
    NSString *strEnd = [self achieveCurrentlyDate:_endDate];
// BDLog(@"1%@\n2%@\n3%@\n4%@\n5%@\n6%@\n7%@\n8%@\n9%@\n10%@\n11%@\n12%@",strStart,strEnd,_payType,_hotelPartyId,_roomTypeId,_hotleOrderView.nameLb.text,_hotleOrderView.tfPhone.text,_hotleOrderView.tfRise.text,_hotleOrderView.tfNumber.text,_hotleOrderView.tfRemarks.text,_hotleOrderView.btnSelectRoom.titleLabel.text,_hotleOrderView.nameLb.text);
    NSDictionary *parameter = @{
                                @"fromDate":strStart,
                                @"thruDate":strEnd,
                                @"payType":_payType,
                                @"hotelPartyId":_hotelPartyId,
                                @"productId":_roomTypeId,
                                @"linkmanName":_hotleOrderView.nameLb.text,
                                @"linkmanPhone":_hotleOrderView.tfPhone.text,
                                @"receipt":_hotleOrderView.tfRise.text,
                                @"taxpayerNum":_hotleOrderView.tfNumber.text,
                                @"remark":_hotleOrderView.tfRemarks.text,
                                @"quantity":[NSString stringWithFormat:@"%@", _hotleOrderView.btnSelectRoom.titleLabel.text],
                                @"checkInPerson":_hotleOrderView.nameLb.text
                                };
    [[BDNetworkTools sharedInstance] postRoomOrderWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
//        BDLog(@"%@", responseObject);
        /*
        NSString *orderId = [responseObject objectForKey:@"orderId"];
        PayOderViewController *payVC = [[PayOderViewController alloc] init];
        payVC.orderId = orderId;
        payVC.isManyStore = NO;
        [self.navigationController pushViewController:payVC animated:YES];
        */
        
        //直接进入订单列表，不拉起支付
        HotelOrderListViewController *hotelOrderVC = [[HotelOrderListViewController alloc] init];
        NSMutableArray *childVCS = [NSMutableArray arrayWithArray:self.navigationController.childViewControllers];
        NSArray *newArray = @[childVCS[0], hotelOrderVC];
        [self.navigationController setViewControllers:newArray animated:true];

    }];
}



@end
