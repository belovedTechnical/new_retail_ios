//
//  AddOccupantViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/9.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "AddOccupantViewController.h"
#import "OccupantNameCell.h"
#import "OccupantModel.h"

@interface AddOccupantViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *occupantArray;
@property (nonatomic, strong) NSMutableArray *nameArray;
@property (nonatomic, strong) UITextField *nameFD;

@end

static NSString *const occupantID = @"OccupantNameCell";
@implementation AddOccupantViewController

- (NSMutableArray *)nameArray
{
    if (!_nameArray) {
        _nameArray = [NSMutableArray array];
    }
    return _nameArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self initUI];
    [self setNavigationItem];
    [self getHotelOccupant];
}

- (void)setNavigationItem
{
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemwithText:@"完成" target:self action:@selector(achieveBtnClick)];
}

- (void)achieveBtnClick
{
    if (self.occupantNameBlcok) {
        self.occupantNameBlcok(_nameArray);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)initUI
{
    UIView *topView = [[UIView alloc] init];
    topView.frame = CGRectMake(0, 65, kScreenW, 100);
    [self.view addSubview:topView];
    
    UILabel *titleLb = [[UILabel alloc] init];
    titleLb.frame = CGRectMake(0, 0, 100, 50);
    titleLb.font = [UIFont systemFontOfSize:15];
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.backgroundColor = [UIColor whiteColor];
    titleLb.text = @"新增入住人";
    [topView addSubview:titleLb];
    
    UITextField *nameFD = [[UITextField alloc] init];
    nameFD.frame = CGRectMake(titleLb.bd_right, 0, kScreenW-titleLb.bd_width, 50);
    nameFD.backgroundColor = [UIColor whiteColor];
    nameFD.font = [UIFont systemFontOfSize:13];
    nameFD.placeholder = @"请输入入住人姓名";
    [topView addSubview:nameFD];
    self.nameFD = nameFD;
    
    UIButton *btnAddOccupant = [[UIButton alloc] init];
    btnAddOccupant.frame = CGRectMake(topView.center.y, nameFD.bd_bottom+10, 80, 30);
    [btnAddOccupant setTitleColor:[UIColor colorWithHexString:@"#a6926b"] forState:UIControlStateNormal];
    [btnAddOccupant setTitle:@"添加" forState:UIControlStateNormal];
    btnAddOccupant.layer.borderColor = [UIColor colorWithHexString:@"#a6926b"].CGColor;
    [btnAddOccupant addTarget:self action:@selector(postHotelOccupant) forControlEvents:UIControlEventTouchUpInside];
    btnAddOccupant.layer.borderWidth = 1;
    [btnAddOccupant.layer setCornerRadius:5];
    btnAddOccupant.layer.masksToBounds = YES;
    [topView addSubview:btnAddOccupant];
    
    UITableView *tableView = [[UITableView alloc] init];
    tableView.frame = CGRectMake(0, topView.bd_y+topView.bd_height+10, kScreenW, kScreenH- topView.bd_height-10);
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.rowHeight = 50;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [tableView registerClass:[OccupantNameCell class] forCellReuseIdentifier:occupantID];
}

#pragma mark -UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _occupantArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OccupantNameCell *cell = [tableView dequeueReusableCellWithIdentifier:occupantID];
    if (cell == nil) {
        cell = [[OccupantNameCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:occupantID];
    }
    cell.occupantModel = _occupantArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    OccupantModel *occuModel = _occupantArray[indexPath.row];
    OccupantNameCell *cell = (OccupantNameCell *)[tableView cellForRowAtIndexPath:indexPath];
    occuModel.isSelect = NO;
    if ([self.nameArray containsObject:occuModel.checkInPersonName]) {
        occuModel.isSelect = NO;
        [self.nameArray removeObject:occuModel.checkInPersonName];
    }else {
        occuModel.isSelect = YES;
        [self.nameArray addObject:occuModel.checkInPersonName];
    }
    cell.occupantModel = occuModel;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        OccupantModel *occuModel = _occupantArray[indexPath.row];
        [self deleteHotelOccupantWithPersonId:occuModel.checkInPersonId];
    }
}

#pragma mark -网络请求
- (void)getHotelOccupant
{
    [[BDNetworkTools sharedInstance] getHotelOccupantWithBlock:^(NSDictionary *responseObject, NSError *error) {
        NSArray *occRes = [responseObject objectForKey:@"checkInPersonList"];
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *dict in occRes) {
            [tempArray addObject:[OccupantModel occupantModelWithDict:dict]];
        }
        self.occupantArray = tempArray;
        [self.tableView reloadData];
    }];
}

- (void)postHotelOccupant
{
    NSDictionary *parameter = @{@"checkInPersonName":self.nameFD.text};
    [[BDNetworkTools sharedInstance] postHotelOccupantWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        self.nameFD.text = @"";
        [self.view endEditing:YES];
        [self getHotelOccupant];
    }];
}

- (void)deleteHotelOccupantWithPersonId:(NSString *)personId
{
    NSDictionary *parameter = @{@"checkInPersonId":personId};
    [[BDNetworkTools sharedInstance] deleteHotelOccupantWithParameter:parameter Block:^(NSDictionary *responseObject, NSError *error) {
        
        [self getHotelOccupant];
    }];
}






















@end
