//
//  AddOccupantViewController.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/9.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^occupantNameBlock)(NSArray *nameArray);
@interface AddOccupantViewController : UIViewController

@property (nonatomic, copy) occupantNameBlock occupantNameBlcok;
@property (nonatomic, strong) NSString *nameStr;

@end
