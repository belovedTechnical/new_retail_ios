//
//  HotelSeekListViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/21.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "HotelSeekListViewController.h"
#import "BDCalendarViewController.h"
#import "SelectCityViewController.h"
#import "WebSearchViewController.h"
#import "WebHotelDetailsViewController.h"
#import "SeekListTopView.h"
#import "SeekListCell.h"
#import "PopListSelectView.h"
#import "HotelSeekListModel.h"
#import "WebHotelSearchListViewController.h"

@interface HotelSeekListViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) SeekListTopView *listTopView;
@property (nonatomic, strong) SeekListCell *seekListCell;
@property (nonatomic, strong) PopListSelectView *popListView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *arrayTown;
@property (nonatomic, strong) HotelSeekListModel *seekListModel;
@property (nonatomic, strong) NSArray *seekListArray;

@end

static NSString * const seekListCellID = @"seekListCell";
@implementation HotelSeekListViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    self.view.backgroundColor = [UIColor colorWithHexString:@"#f6f6f6"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
    [self requestNetHotelListWithCityCode:_cityCode sort:@"low"];
    [self requestNetLocationTownWithCity:_cityCode];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeOrderRoomDate:) name:kChangeOrderRoomDate object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initUI
{
    _listTopView = [[SeekListTopView alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.bd_y, kScreenW, 80)];
    [_listTopView.btnDate setTitle:[NSString stringWithFormat:@"%@%@", _startStr,_nuberDays] forState:UIControlStateNormal ];
    
    if (self.cityName.length > 0) {
        [_listTopView.btnCity setTitle:self.cityName forState:UIControlStateNormal];
    }
    
    [self.view addSubview:_listTopView];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, _listTopView.bd_bottom, kScreenW, kScreenH - _listTopView.bd_height-20);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.estimatedRowHeight = 230;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];

    [self.tableView registerNib:[UINib nibWithNibName:@"SeekListCell" bundle:nil] forCellReuseIdentifier:seekListCellID];
    [_listTopView.btnBack addTarget:self action:@selector(btnBackClick) forControlEvents:UIControlEventTouchUpInside];
    [_listTopView.btnSearch addTarget:self action:@selector(btnSearchClick) forControlEvents:UIControlEventTouchUpInside];
    [_listTopView.btnCity addTarget:self action:@selector(btnCityClick) forControlEvents:UIControlEventTouchUpInside];
    [_listTopView.btnDate addTarget:self action:@selector(btnDateClick) forControlEvents:UIControlEventTouchUpInside];
    [_listTopView.btnArea addTarget:self action:@selector(btnAreaClick:) forControlEvents:UIControlEventTouchUpInside];
    [_listTopView.btnTaxis addTarget:self action:@selector(btnTaxisClick:) forControlEvents:UIControlEventTouchUpInside];
    
}

#pragma mark -UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _seekListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SeekListCell *cell = [tableView dequeueReusableCellWithIdentifier:seekListCellID];
    cell.seekListModel = _seekListArray[indexPath.row];
    return cell;
}


#pragma mark -UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HotelSeekListModel *model = self.seekListArray[indexPath.row];
    WebHotelDetailsViewController *roomDetailVC = [[WebHotelDetailsViewController alloc] init];
    roomDetailVC.hotelDetailsURL = [NSString stringWithFormat:@"%@%@%@", HTML_URL_Base, urlWebHotelDetails,model.hotelPartyId];
    [self.navigationController pushViewController:roomDetailVC animated:YES];
}


#pragma mark -点击事件
- (void)btnBackClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)btnSearchClick {
//    WebSearchViewController *searchVC = [[WebSearchViewController alloc] init];
//    self.navigationController.navigationBar.hidden = NO;
//    [self.navigationController pushViewController:searchVC animated:YES];
    
    WebHotelSearchListViewController *searchVC = [[WebHotelSearchListViewController alloc] init];
    searchVC.hotelSearchListURL = [NSString stringWithFormat:@"%@hotel-search.html",HTML_URL_Base];
    self.navigationController.navigationBar.hidden = NO;
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (void)btnCityClick
{
    self.navigationController.navigationBar.hidden = NO;
    SelectCityViewController *seleCtiyVC = [[SelectCityViewController alloc] init];
    [self.navigationController pushViewController:seleCtiyVC animated:NO];
    seleCtiyVC.searchHostBlock = ^(NSString *cityName, NSString *cityCode) {
        [_listTopView.btnCity setTitle:cityName forState:UIControlStateNormal];
        self.cityCode = cityCode;
        self.cityName = cityName;
        [self requestNetHotelListWithCityCode:cityCode sort:@"low"];
    };
}

- (void)btnDateClick
{
    self.navigationController.navigationBar.hidden = NO;
    BDCalendarViewController *calendarVC = [[BDCalendarViewController alloc] init];
    [self.navigationController pushViewController:calendarVC animated:NO];
}

- (void)btnAreaClick:(UIButton *)sendBtn
{
    self.popListView.listDataArray = _arrayTown;
    [self isHiddenPopListView:sendBtn];
}

- (void)btnTaxisClick:(UIButton *)sendBtn
{
    self.popListView.listDataArray = @[@"从高到低",@"从低到高"];
    [self isHiddenPopListView:sendBtn];
}

- (void)isHiddenPopListView:(UIButton *)sendBtn
{
    [self.popListView.tableView reloadData];
    sendBtn.selected = !sendBtn.selected;
    if (sendBtn.selected == YES) {
        self.popListView.hidden = NO;
    }else {
        self.popListView.hidden = YES;
    }
    BDWeakSelf();
    self.popListView.popListSelectViewCellBlock = ^(NSString *selectName, NSString *selectCode) {
        [sendBtn setTitle:selectName forState:UIControlStateNormal];
        weakSelf.popListView.hidden = YES;
        NSString *sort = @"low";
        NSString *code = @"";
        if ([selectName isEqualToString:@"从高到低"]) {
            sort = @"higt";
        }
        if (selectCode.length > 4) {
            code = selectCode;
        }
        [weakSelf requestNetHotelListWithCityCode:code sort:sort];
    };
    
}

// 酒店日期选择改变
- (void)changeOrderRoomDate:(NSNotification *)notif
{
    NSDate *startDate = notif.userInfo[@"startDate"];
    NSDate *endDate = notif.userInfo[@"endDate"];
    self.startDate = startDate;
    self.endDate = endDate;
    NSString *startStr = notif.userInfo[@"changeStartDate"];
//    NSString *endStr = notif.userInfo[@"changeEndDateStr"];
    NSString *nuberDays = notif.userInfo[@"numberDays"];
    NSString *dateStr = [NSString stringWithFormat:@"%@(%@)", startStr, nuberDays];
    [_listTopView.btnDate setTitle:dateStr forState:UIControlStateNormal];
}

// NSString转NSDate
- (NSDate *)dateFromString:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM月dd日"];
    NSDate *dastDate = [dateFormatter dateFromString:dateString];
    return dastDate;
}

#pragma mark -网络请求
- (void)requestNetHotelListWithCityCode:(NSString *)cityCode sort:(NSString *)sort
{
    [[BDNetworkTools sharedInstance] getHotelSeekWithFromDate:_startDate ThruDate:_endDate City:nil County:nil CityCode:cityCode Sort:sort PageIndex:0 PageSize:1000 Tag:nil Blcok:^(NSDictionary *responseObject, NSError *error) {
        NSArray *dataArray = [responseObject objectForKey:@"hotelList"];
        NSMutableArray *tempArray = [NSMutableArray array];
        for (NSDictionary *dict in dataArray) {
            [tempArray addObject:[HotelSeekListModel hotelSeekListModelWithDict:dict]];
        }
        self.seekListArray = tempArray;
        [self.tableView reloadData];
    }];
}

- (void)requestNetLocationTownWithCity:(NSString *)city
{
    [[BDNetworkTools sharedInstance] getLocationTownWithCity:city Block:^(NSDictionary *responseObject, NSError *error) {
        NSArray *arrayTown = [responseObject objectForKey:@"townList"];
        self.arrayTown = arrayTown;
        [self.tableView reloadData];
    }];
}

#pragma mark -懒加载
- (PopListSelectView *)popListView
{
    if (!_popListView) {
        CGFloat y = _listTopView.bd_height + 20;
        _popListView = [[PopListSelectView alloc] initWithFrame:CGRectMake(0, y, kScreenW, kScreenH - y)];
        _popListView.alpha = 0.9;
        [self.view addSubview:_popListView];
    }
    return _popListView;
}





@end
