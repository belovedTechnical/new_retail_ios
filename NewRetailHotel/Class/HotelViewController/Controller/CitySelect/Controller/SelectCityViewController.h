//
//  SelectCityViewController.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/18.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SearchHostlistBlock)(NSString *cityName, NSString *cityCode);

@interface SelectCityViewController : UITableViewController

@property (nonatomic, weak) UILabel *userAddress;
@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) SearchHostlistBlock searchHostBlock;

@end
