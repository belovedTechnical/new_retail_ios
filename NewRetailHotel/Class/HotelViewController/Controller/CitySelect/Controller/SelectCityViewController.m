//
//  SelectCityViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/18.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "SelectCityViewController.h"
#import "SelectCitySectionView.h"
#import "SelectCityCell.h"
#import "SelectCityModel.h"

@interface SelectCityViewController ()<SelectCityCellDelegate>

@property (nonatomic, strong) SelectCityModel *selectCityModel;

@end

static NSString * const selectCityCellID = @"SelectCityCell";
@implementation SelectCityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"选择城市";
    self.selectCityModel = [[SelectCityModel alloc] init];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.tableView registerNib:[UINib nibWithNibName:@"SelectCityCell" bundle:nil] forCellReuseIdentifier:selectCityCellID];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return _selectCityModel.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    NSDictionary *dict = _selectCityModel.dataArray[section];
    return [[dict objectForKey:@"Arr"] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        SelectCityCell *cell = [tableView dequeueReusableCellWithIdentifier:selectCityCellID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        return cell;
    }else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        NSDictionary *dic = _selectCityModel.dataArray[indexPath.section];
        NSArray *citysArr = dic[@"Arr"];
        cell.textLabel.text = [citysArr[indexPath.row] objectForKey:@"name"];
        cell.textLabel.font = [UIFont systemFontOfSize:12];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        SelectCitySectionView *headerView = [[SelectCitySectionView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 60)];
        [headerView.BtnCurrentCity setTitle:@" 深圳市" forState:UIControlStateNormal];
        return headerView;
    }else {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 30)];
        UILabel *LBTitle = [[UILabel alloc] initWithFrame:CGRectMake(11, 4, 110, 24)];
        LBTitle.font = [UIFont systemFontOfSize:13];
        LBTitle.text = [_selectCityModel.dataArray[section] objectForKey:@"Title"];
        [headerView addSubview:LBTitle];
        return headerView;
    }
}

- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    tableView.sectionIndexColor = [UIColor darkGrayColor];
    NSMutableArray *array = [NSMutableArray array];
    for (NSUInteger i = 1; i < _selectCityModel.dataArray.count; i++) {
        [array addObject:[[_selectCityModel.dataArray objectAtIndex:i] objectForKey:@"Title"]];
    }
    return array;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 45;
    }else {
        return 30;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 130;
    }else {
        return 30;
    }
}

#pragma mark -UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        return;
    }
    
    NSDictionary *dict = _selectCityModel.dataArray[indexPath.section];
    NSArray *cityArray = dict[@"Arr"];
    NSString *cityName = [cityArray[indexPath.row] objectForKey:@"name"];
    NSString *cityCode = [cityArray[indexPath.row] objectForKey:@"code"];
    if (self.searchHostBlock) {
        self.searchHostBlock(cityName,cityCode);
    }
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -SelectCityCellDelegate
- (void)selectedHoteCity:(NSDictionary *)cityDict
{
    NSString *cityName = [cityDict objectForKey:@"cityName"];
    NSString *cityCode = [cityDict objectForKey:@"cityCode"];
    if (self.searchHostBlock) {
        self.searchHostBlock(cityName,cityCode);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end
