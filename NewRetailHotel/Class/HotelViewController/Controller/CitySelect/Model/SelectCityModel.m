//
//  SelectCityModel.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/18.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "SelectCityModel.h"

@implementation SelectCityModel

- (NSMutableArray *)allCityDataArray
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"china_pct" ofType:@"data"];
    NSString *utf8Str = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSArray *json = [NSJSONSerialization JSONObjectWithData:[utf8Str dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments | NSJSONReadingMutableLeaves | NSJSONReadingMutableContainers error:nil];
    
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    for (char i = 'A'; i <= 'Z'; i++) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:[NSString stringWithFormat:@"%c",i] forKey:@"Title"];
        [dict setObject:[[NSMutableArray alloc] init] forKey:@"Arr"];
        [dataArray addObject:dict];
    }
    
    for (NSDictionary *dictItem in json) {
        if ([[dictItem objectForKey:@"cities"] count] > 2) {
            NSArray *cititesArray = [dictItem objectForKey:@"cities"];
            for (NSDictionary *dicts in cititesArray) {
                [self addObjectNameAndCode:dicts dataArray:dataArray];
            }
        }else {
            [self addObjectNameAndCode:dictItem dataArray:dataArray];
        }
    }
    NSMutableDictionary *hotDict = [[NSMutableDictionary alloc] init];
    [hotDict setObject:@"当前城市:深圳" forKey:@"Title"];
    [hotDict setObject:@[@""] forKey:@"Arr"];
    [dataArray insertObject:hotDict atIndex:0];

    return dataArray;
}

- (void)addObjectNameAndCode:(NSDictionary *)dict dataArray:(NSMutableArray *)dataArray
{
    NSString *cityName = [dict objectForKey:@"geoNameLocal"];
    NSString *cityCode = [dict objectForKey:@"geoCodeNumeric"];
    NSString *firstLetter = [self getFirstLetter:cityName];
    // 字母索引编号
    NSUInteger asciiCode = [firstLetter characterAtIndex:0] - 65;
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:[dataArray objectAtIndex:asciiCode]];
    NSMutableArray *array = [dic objectForKey:@"Arr"];
    [array addObject:@{@"name":cityName, @"code":cityCode}];
}

- (NSString *)getFirstLetter:(NSString *)sourceStr
{
    NSMutableString *source = [sourceStr mutableCopy];
    CFStringTransform((__bridge CFMutableStringRef)source, NULL, kCFStringTransformMandarinLatin, NO);
    CFStringTransform((__bridge CFMutableStringRef)source, NULL, kCFStringTransformStripDiacritics, NO);
    return [[source substringToIndex:1] uppercaseString];
}

- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [self allCityDataArray];
    }
    return _dataArray;
}



























@end
