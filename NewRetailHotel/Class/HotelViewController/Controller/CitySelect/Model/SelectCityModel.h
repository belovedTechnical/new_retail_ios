//
//  SelectCityModel.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/18.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SelectCityModel : NSObject

@property (nonatomic, strong) NSMutableArray *dataArray;

@end
