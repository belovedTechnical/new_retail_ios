//
//  SelectCitySectionView.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/18.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "SelectCitySectionView.h"

@implementation SelectCitySectionView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    UILabel *LBTitle = [[UILabel alloc] init];
    LBTitle.frame = CGRectMake(10, 5, kScreenW, 15);
    LBTitle.textColor = [UIColor blackColor];
    LBTitle.font = [UIFont systemFontOfSize:13];
    LBTitle.text = @"定位城市";
    [self addSubview:LBTitle];
    
    UIButton *BtnCurrentCity = [[UIButton alloc] init];
    BtnCurrentCity.frame = CGRectMake(30, LBTitle.bd_bottom + 5, 200, 18);
    [BtnCurrentCity setImage:[UIImage imageNamed:@"icon_location_small.png"] forState:UIControlStateNormal];
    [BtnCurrentCity setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    BtnCurrentCity.titleLabel.font = [UIFont systemFontOfSize:13];
    BtnCurrentCity.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self addSubview:BtnCurrentCity];
    self.BtnCurrentCity = BtnCurrentCity;
}






















@end
