//
//  SelectCitySectionView.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/18.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectCitySectionView : UIView

@property (nonatomic, strong) UIButton *BtnCurrentCity;

@end
