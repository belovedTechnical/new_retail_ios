//
//  SelectCityCell.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/18.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectCityCellDelegate<NSObject>
@optional
- (void)selectedHoteCity:(NSDictionary *)cityDict;

@end

@interface SelectCityCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *hoteCityTitle;
@property (nonatomic, assign) id <SelectCityCellDelegate> delegate;


@end
