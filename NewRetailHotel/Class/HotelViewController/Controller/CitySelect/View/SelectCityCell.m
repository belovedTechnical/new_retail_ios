//
//  SelectCityCell.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/18.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "SelectCityCell.h"

@interface SelectCityCell()

@property (nonatomic, strong) NSArray *hoteCityArray;
@property (nonatomic, strong) NSArray *hoteCityCodeArray;

@end

@implementation SelectCityCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    self.hoteCityArray = @[@"北京市",@"上海市",@"广州市",@"深圳市",
                           @"杭州市",@"南京市",@"天津市",@"武汉市",
                           @"重庆市"];
    // 直辖市需要默认选择下一级区域
    self.hoteCityCodeArray = @[@"110100",@"310100",@"440100",@"440300",
                               @"330100",@"320100",@"120100",@"420100",
                               @"500100"];
    NSUInteger count = _hoteCityArray.count;
    if (count > 12) {
        count = 12;
    }
    if (count != 12) {
        for (NSUInteger i = count; i < 12; i++) {
            UIButton *btn = [(UIButton *)self viewWithTag:i+1];
            btn.hidden = YES;
        }
    }
    for (NSUInteger i=0; i < count; i++) {
        UIButton *btn = [(UIButton *)self viewWithTag:i+1];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setTitle:[_hoteCityArray objectAtIndex:i] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        
        [btn addTarget:self action:@selector(setupUserLocationCity:) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)setupUserLocationCity:(UIButton *)sender{
    
    if([self.delegate respondsToSelector:@selector(selectedHoteCity:)]){
        NSDictionary *cityDict = @{@"cityName":[_hoteCityArray objectAtIndex:(sender.tag-1)],@"cityCode":[_hoteCityCodeArray objectAtIndex:(sender.tag-1)]};
        [self.delegate selectedHoteCity:cityDict];
    }
}


@end
