//
//  WebHotelSearchListViewController.h
//  NewRetailHotel
//
//  Created by lwz on 2018/9/15.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "UIWebBaseViewController.h"

@interface WebHotelSearchListViewController : UIWebBaseViewController

@property (nonatomic, strong) NSString *hotelSearchListURL;

@end
