//
//  SeekListCell.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/21.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HotelSeekListModel;
@interface SeekListCell : UITableViewCell

@property (nonatomic, strong) HotelSeekListModel *seekListModel;

@end
