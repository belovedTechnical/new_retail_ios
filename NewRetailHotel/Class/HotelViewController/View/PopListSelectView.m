//
//  PopListSelectView.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/22.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "PopListSelectView.h"

@interface PopListSelectView()<UITableViewDataSource, UITableViewDelegate>


@end

@implementation PopListSelectView

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    _tableView = [[UITableView alloc] init];
    _tableView.frame = self.bounds;
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.estimatedRowHeight = 41;
    _tableView.tableFooterView = [[UIView alloc] init];
    [self addSubview:_tableView];
}

#pragma mark -UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listDataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    if ([_listDataArray containsObject:@"从高到低"]) {
        cell.textLabel.text = _listDataArray[indexPath.row];
    }else {
        cell.textLabel.text = [_listDataArray[indexPath.row] objectForKey:@"geoNameLocal"];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
}

#pragma mark -UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /* lastUpdatedStamp:2017-12-29T05:57:44+0000,geoId:SZX1956,geoName:Nanshan Qu,geoCodeNumeric:440305,geoNameLocal:南山区,geoCodeAlpha2:<null>}
     */
    NSString *selectName = @"";
    NSString *selectCode = @"";
    if ([_listDataArray containsObject:@"从高到低"]) {
        selectName = _listDataArray[indexPath.row];
    }else {
        selectName = [_listDataArray[indexPath.row] objectForKey:@"geoNameLocal"];
        selectCode = [_listDataArray[indexPath.row] objectForKey:@"geoCodeNumeric"];
    }
    if (self.popListSelectViewCellBlock) {
        self.popListSelectViewCellBlock(selectName, selectCode);
    }
}













@end
