//
//  HotelSeekView.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/14.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelSeekView : UIView

@property (nonatomic, strong) UIButton *btnDestination;
@property (nonatomic, strong) UIButton *btnInputDate;
@property (nonatomic, strong) UIButton *btnLocation;
@property (nonatomic, strong) UIButton *btnSeek;
@property (nonatomic, strong) UILabel *dateStart;
@property (nonatomic, strong) UILabel *dateDeparture;
@property (nonatomic, strong) UILabel *lbGross;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;

@end
