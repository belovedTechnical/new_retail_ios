//
//  OccupantNameCell.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/9.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OccupantModel;
@interface OccupantNameCell : UITableViewCell

@property (nonatomic, strong) OccupantModel *occupantModel;
@property (nonatomic, strong) UIButton *selectBtn;

@end
