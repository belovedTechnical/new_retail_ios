//
//  SeekListTopView.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/21.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "SeekListTopView.h"
#import "RightBaseButton.h"

@interface SeekListTopView()

@end

@implementation SeekListTopView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    CGFloat backX = 0;
    CGFloat backY = 0;
    CGFloat backW = 60;
    CGFloat backH = 35;
    UIButton *btnBack = [[UIButton alloc] init];
    btnBack.frame = CGRectMake(backX, backY, backW, backH);
    [btnBack setImage:[UIImage imageNamed:@"return"] forState:UIControlStateNormal];
    [self addSubview:btnBack];
    self.btnBack = btnBack;
    
    CGFloat searchX = backW;
    CGFloat searchY = 0;
    CGFloat searchW = kScreenW-backW*2;
    CGFloat searchH = backH;
    UIButton *btnSearch = [[UIButton alloc] init];
    btnSearch.frame = CGRectMake(searchX, searchY, searchW, searchH);
    [btnSearch setTitle:@" 搜索你想要的内容" forState:UIControlStateNormal];
    [btnSearch setImage:[UIImage imageNamed:@"icon-search"] forState:UIControlStateNormal];
    [btnSearch setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
    btnSearch.backgroundColor = [UIColor colorWithHexString:@"ededed"];
    btnSearch.alpha = 0.8;
    btnSearch.layer.cornerRadius = 5;
    btnSearch.layer.masksToBounds = YES;
    btnSearch.titleLabel.font = [UIFont systemFontOfSize:13];
    [self addSubview:btnSearch];
    self.btnSearch = btnSearch;
    
    CGFloat cityW = 60;
    CGFloat cityH = backH;
    CGFloat cityX = kScreenW-cityW;
    CGFloat cityY = 0;
    UIButton *btnCity = [[UIButton alloc] init];
    btnCity.frame = CGRectMake(cityX, cityY, cityW, cityH);
    [btnCity setTitleColor:[UIColor colorWithHexString:@"#444444"] forState:UIControlStateNormal];
    [btnCity setTitle:@"城市" forState:UIControlStateNormal];
    btnCity.titleLabel.font = [UIFont systemFontOfSize:16];
    [self addSubview:btnCity];
    self.btnCity = btnCity;
    
    CGFloat btnW = self.frame.size.width/2 - 60;
    _btnDate = [self topTitleButtonWithtitle:@"" titleX:10 titleW:btnW+10];
    _btnArea = [self topTitleButtonWithtitle:@"区域" titleX:self.center.x-20 titleW:70];
    _btnTaxis = [self topTitleButtonWithtitle:@"智能排序" titleX:self.bd_right-btnW titleW:btnW];
}

- (UIButton *)topTitleButtonWithtitle:(NSString *)title titleX:(CGFloat)titleX titleW:(CGFloat)titleW
{
    RightBaseButton *titleBtn = [[RightBaseButton alloc] init];
    titleBtn.frame = CGRectMake(titleX, 35, titleW, 44);
    [titleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBtn setImage:[UIImage imageNamed:@"icon_arrow_down"] forState:UIControlStateNormal];
    titleBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [titleBtn setTitle:title forState:UIControlStateNormal];
    [self addSubview:titleBtn];
    return titleBtn;
}


@end
