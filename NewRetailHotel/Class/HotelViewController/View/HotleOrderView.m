//
//  HotleOrderView.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/23.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "HotleOrderView.h"
#import "RightBaseButton.h"
#import "RightButton.h"
#import "SaveTool.h"
#import "BDMobileNumberTools.h"

@interface HotleOrderView()

@property (nonatomic, strong) UIView *hotleNameView;
@property (nonatomic, strong) UIView *occupantInformationView;
@property (nonatomic, strong) UIView *remarksView;
@property (nonatomic, strong) UIView *invoiceView;
@property (nonatomic, strong) UILabel *lbExplanatory;

@end

#define leftSpace 15
#define titleHeight 50
@implementation HotleOrderView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    UIView *hotleNameView = [[UIView alloc] init];
    hotleNameView.frame = CGRectMake(0, 0, self.bd_width, 135);
    hotleNameView.backgroundColor = [UIColor whiteColor];
    [self addSubview:hotleNameView];
    self.hotleNameView = hotleNameView;
    
    UIView *occupantInformationView = [[UIView alloc] init];
    occupantInformationView.frame = CGRectMake(0, hotleNameView.bd_bottom+10, self.bd_width, 201);
    occupantInformationView.backgroundColor = [UIColor whiteColor];
    [self addSubview:occupantInformationView];
    self.occupantInformationView = occupantInformationView;
    
    CGFloat invoiceY = occupantInformationView.bd_bottom+10;
    UIView *invoiceView = [[UIView alloc] init];
    invoiceView.frame = CGRectMake(0, invoiceY, self.bd_width, titleHeight*4+1);
    invoiceView.backgroundColor = [UIColor whiteColor];
    invoiceView.hidden = YES;
    [self addSubview:invoiceView];
    self.invoiceView = invoiceView;
    
    CGFloat remarksY = occupantInformationView.bd_bottom+10;
    UIView *remarksView = [[UIView alloc] init];
    remarksView.frame = CGRectMake(0, remarksY, self.bd_width, titleHeight);
    remarksView.backgroundColor = [UIColor whiteColor];
    [self addSubview:remarksView];
    self.remarksView = remarksView;
    
    UILabel *lbExplanatory = [[UILabel alloc] init];
    lbExplanatory.frame = CGRectMake(leftSpace, remarksView.bd_bottom+10, self.bd_width-20, 50);
    lbExplanatory.text = @"请于入住中午12:00后办理入住,如提前到店,视酒店空房详情安排。";
    lbExplanatory.textColor = [UIColor colorWithHexString:@"#b1b1b1"];
    lbExplanatory.font = [UIFont systemFontOfSize:13];
    lbExplanatory.numberOfLines = 0;
    [self addSubview:lbExplanatory];
    self.lbExplanatory = lbExplanatory;

    UILabel *lbHotleName = [[UILabel alloc] init];
    lbHotleName.frame = CGRectMake(leftSpace, 0, self.bd_width-15-titleHeight, titleHeight);
    lbHotleName.textColor = [UIColor colorWithHexString:@"#333333"];
    lbHotleName.font = [UIFont systemFontOfSize:15];
    lbHotleName.text = @"深圳至爱空间商务酒店(1号店)";
    [hotleNameView addSubview:lbHotleName];
    self.lbHotleName = lbHotleName;
    
    UIButton *btnPhone = [[UIButton alloc] init];
    btnPhone.frame = CGRectMake(hotleNameView.bd_right - titleHeight, 0, titleHeight, titleHeight);
    [btnPhone setImage:[UIImage imageNamed:@"contact"] forState:UIControlStateNormal];
    [btnPhone setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [hotleNameView addSubview:btnPhone];
    self.btnPhone = btnPhone;
    
    [self setUplineSegmentingWithLineY:lbHotleName.bd_bottom AddView:hotleNameView];
    
    UILabel *lbDateEnter = [self setUpHotleDateTitleWithDateX:leftSpace DateW:75];
//    NSDate *today = [[NSDate alloc] init];
//    lbDateEnter.text = [self achieveCurrentlyDate:today];
    lbDateEnter.text = [SaveTool objectForKey:checkInStrKey];
    self.lbDateEnter = lbDateEnter;
    
    UIView *lingDate = [[UIView alloc] init];
    lingDate.frame = CGRectMake(lbDateEnter.bd_right, lbDateEnter.center.y, 20, 0.5);
    lingDate.backgroundColor = [UIColor colorWithHexString:@"#444444"];
    [hotleNameView addSubview:lingDate];
    
    UILabel *lbDateOut = [self setUpHotleDateTitleWithDateX:lingDate.bd_right+5 DateW:75];
//    NSDate *tomorrow = [[NSDate date] dateByAddingTimeInterval:3600*24];
//    lbDateOut.text = [self achieveCurrentlyDate:tomorrow];
    lbDateOut.text = [SaveTool objectForKey:checkOutStrKey];
    self.lbDateOut = lbDateOut;
    
    UIButton *btnDateClick = [[UIButton alloc] init];
    btnDateClick.frame = CGRectMake(0, lbDateEnter.bd_y, lbDateOut.bd_right, lbDateEnter.bd_height);
    [hotleNameView addSubview:btnDateClick];
    self.btnDateClick = btnDateClick;
    
    UILabel *lbRoomForm = [self setUpHotleDateTitleWithDateX:hotleNameView.bd_right - 135 DateW:130];
    lbRoomForm.text = @"豪华大床房(含早)";
    
    UILabel *lbEnter = [self setUpHotleLodgingTitleWithLodgX:leftSpace];
    lbEnter.text = @"入住";
    UILabel *lbOut = [self setUpHotleLodgingTitleWithLodgX:lbDateOut.bd_left];
    lbOut.text = @"离店";
    UILabel *lbDayAll = [self setUpHotleLodgingTitleWithLodgX:hotleNameView.bd_right-60];
    lbDayAll.textAlignment = NSTextAlignmentRight;
//    lbDayAll.text = @"共1晚";
    lbDayAll.text = [SaveTool objectForKey:numberDaysKey];
    self.lbDayAll = lbDayAll;
    
    UILabel *lbNumberT = [self setUpTitleLabelWithTitleY:0 AddView:occupantInformationView];
    lbNumberT.text = @"房间数";
    UILabel *lbUserT = [self setUpTitleLabelWithTitleY:lbNumberT.bd_bottom+0.5 AddView:occupantInformationView];
    lbUserT.text = @"入住人";
    UILabel *lbPhoneT = [self setUpTitleLabelWithTitleY:lbUserT.bd_bottom+0.5 AddView:occupantInformationView];
    lbPhoneT.text = @"手机号";
    UILabel *lbInvoiceT = [self setUpTitleLabelWithTitleY:lbPhoneT.bd_bottom+0.5 AddView:occupantInformationView];
    lbInvoiceT.text = @"发票";
    UILabel *lbRemarksT = [self setUpTitleLabelWithTitleY:0 AddView:remarksView];
    lbRemarksT.text = @"备注";
    [self setUplineSegmentingWithLineY:lbNumberT.bd_bottom AddView:occupantInformationView];
    [self setUplineSegmentingWithLineY:lbUserT.bd_bottom AddView:occupantInformationView];
    [self setUplineSegmentingWithLineY:lbPhoneT.bd_bottom AddView:occupantInformationView];
    
    CGFloat btnNumberW = 80;
    RightBaseButton *btnNumber = [[RightBaseButton alloc] init];
    btnNumber.frame = CGRectMake(occupantInformationView.bd_right-btnNumberW-10, 0, btnNumberW, titleHeight);
    [btnNumber setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnNumber setImage:[UIImage imageNamed:@"icon_rightarrow"] forState:UIControlStateNormal];
    [btnNumber setTitle:@"1 " forState:UIControlStateNormal];
    btnNumber.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    btnNumber.titleLabel.textAlignment = NSTextAlignmentLeft;
    [occupantInformationView addSubview:btnNumber];
    self.btnSelectRoom = btnNumber;
    
    CGFloat btnAddW = 94;
    UIButton *btnAdd = [[UIButton alloc] init];
    btnAdd.frame = CGRectMake(occupantInformationView.bd_right-btnAddW-10,btnNumber.bd_bottom+10, btnAddW, 30);
    [btnAdd setTitleColor:[UIColor colorWithHexString:@"#a6926b"] forState:UIControlStateNormal];
    [btnAdd setTitle:@"添加入住人" forState:UIControlStateNormal];
    btnAdd.titleLabel.font = [UIFont systemFontOfSize:13];
    btnAdd.layer.cornerRadius = 15;
    btnAdd.layer.masksToBounds = YES;
    btnAdd.layer.borderWidth = 1;
    btnAdd.layer.borderColor = [UIColor colorWithHexString:@"#a6926b"].CGColor;
    [occupantInformationView addSubview:btnAdd];
    self.btnAddOccupant = btnAdd;
    
    UILabel *nameLb = [[UILabel alloc] init];
    nameLb.frame = CGRectMake(lbUserT.bd_right, lbUserT.bd_y, occupantInformationView.bd_width-lbUserT.bd_width-btnAddW-15, lbUserT.bd_height);
    nameLb.textColor = [UIColor colorWithHexString:@"#333333"];
    nameLb.textAlignment = NSTextAlignmentCenter;
    nameLb.font = [UIFont systemFontOfSize:13];
    [occupantInformationView addSubview:nameLb];
    self.nameLb = nameLb;
    
    CGFloat tfPhoneW = 150;
    UITextField *tfPhone = [[UITextField alloc] init];
    tfPhone.frame = CGRectMake(occupantInformationView.bd_right-tfPhoneW-10, lbUserT.bd_bottom, tfPhoneW, titleHeight);
    tfPhone.placeholder = @"请输入手机号";
    tfPhone.font = [UIFont systemFontOfSize:14];
    tfPhone.textAlignment = NSTextAlignmentRight;
    [tfPhone addTarget:self action:@selector(phoneTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
//    tfPhone.textColor = [UIColor colorWithHexString:@"@b1b1b1"];
    [occupantInformationView addSubview:tfPhone];
    self.tfPhone = tfPhone;
    
    CGFloat scInvoiceW = 50;
    UISwitch *scInvoice = [[UISwitch alloc] init];
    scInvoice.frame = CGRectMake(occupantInformationView.bd_right-scInvoiceW-10, lbPhoneT.bd_bottom+10, scInvoiceW, 30);
    [scInvoice addTarget:self action:@selector(scInvoiceClick:) forControlEvents:UIControlEventTouchUpInside];
    [occupantInformationView addSubview:scInvoice];
    
    CGFloat tfRemarksW = 150;
    UITextField *tfRemarks = [[UITextField alloc] init];
    tfRemarks.frame = CGRectMake(remarksView.bd_right-tfRemarksW-10, 0, tfRemarksW, titleHeight);
    tfRemarks.placeholder = @"请填写其他要求";
    tfRemarks.font = [UIFont systemFontOfSize:14];
    tfRemarks.textAlignment = NSTextAlignmentRight;
    tfRemarks.textColor = [UIColor colorWithHexString:@"#b1b1b1"];
    [remarksView addSubview:tfRemarks];
    self.tfRemarks = tfRemarks;
    
    UILabel *lbRise = [self setUpTitleLabelWithTitleY:0 AddView:invoiceView];
    lbRise.text = @"发票抬头";
    UILabel *lbRecognitionNumber = [self setUpTitleLabelWithTitleY:lbRise.bd_bottom+0.5 AddView:invoiceView];
    lbRecognitionNumber.text = @"识别号";
    UILabel *lbOrigin = [self setUpTitleLabelWithTitleY:lbRecognitionNumber.bd_bottom+0.5 AddView:invoiceView];
    lbOrigin.text = @"发票明细";
    UILabel *lbGet = [self setUpTitleLabelWithTitleY:lbOrigin.bd_bottom+0.5 AddView:invoiceView];
    lbGet.text = @"领取方式";
    [self setUplineSegmentingWithLineY:lbRise.bd_bottom AddView:invoiceView];
    [self setUplineSegmentingWithLineY:lbRecognitionNumber.bd_bottom AddView:invoiceView];
    [self setUplineSegmentingWithLineY:lbOrigin.bd_bottom AddView:invoiceView];
    
    CGFloat tfRiseW = 100;
    UITextField *tfRise = [[UITextField alloc] init];
    tfRise.frame = CGRectMake(invoiceView.bd_right-tfRiseW-10, 0, tfRiseW, titleHeight);
//    tfRise.placeholder = @"个人";
    tfRise.text = @"个人";
    tfRise.font = [UIFont systemFontOfSize:14];
    tfRise.textAlignment = NSTextAlignmentRight;
    tfRise.textColor = [UIColor colorWithHexString:@"#b1b1b1"];
    [invoiceView addSubview:tfRise];
    self.tfRise = tfRise;
    
    UITextField *tfNumber = [[UITextField alloc] init];
    tfNumber.frame = CGRectMake(invoiceView.bd_right-tfRiseW-10, lbRecognitionNumber.bd_y, tfRiseW, titleHeight);
    tfNumber.placeholder = @"纳税人识别号";
    tfNumber.font = [UIFont systemFontOfSize:14];
    tfNumber.textAlignment = NSTextAlignmentRight;
    tfNumber.textColor = [UIColor colorWithHexString:@"#b1b1b1"];
    [invoiceView addSubview:tfNumber];
    self.tfNumber = tfNumber;
    
    CGFloat lbRelaxW = 100;
    UILabel *lbRelax = [[UILabel alloc] init];
    lbRelax.frame = CGRectMake(invoiceView.bd_right-lbRelaxW-10, lbRecognitionNumber.bd_bottom+0.5, lbRelaxW, titleHeight);
    lbRelax.textAlignment = NSTextAlignmentRight;
    lbRelax.font = [UIFont systemFontOfSize:14];
    lbRelax.text = @"住宿费";
    [invoiceView addSubview:lbRelax];
    
    CGFloat lbCounterW = 180;
    UILabel *lbCounter = [[UILabel alloc] init];
    lbCounter.frame = CGRectMake(invoiceView.bd_right-lbCounterW-10, lbRelax.bd_bottom+0.5, lbCounterW, titleHeight);
    lbCounter.textAlignment = NSTextAlignmentRight;
    lbCounter.font = [UIFont systemFontOfSize:14];
    lbCounter.text = @"请至前台自行领取";
    [invoiceView addSubview:lbCounter];
}

- (UILabel *)setUpTitleLabelWithTitleY:(CGFloat)titleY AddView:(UIView *)addView
{
    UILabel *lb = [[UILabel alloc] init];
    lb.frame = CGRectMake(leftSpace, titleY, 80, titleHeight);
    lb.textColor = [UIColor colorWithHexString:@"#333333"];
    lb.textAlignment = NSTextAlignmentLeft;
    lb.font = [UIFont systemFontOfSize:15];
    [addView addSubview:lb];
    return lb;
}

- (UIView *)setUplineSegmentingWithLineY:(CGFloat)lineY AddView:(UIView *)addView
{
    UIView *line = [[UIView alloc] init];
    line.frame = CGRectMake(leftSpace, lineY, self.bd_width-leftSpace, 0.5);
    line.backgroundColor = [UIColor colorWithHexString:@"#e1e1e1"];
    [addView addSubview:line];
    return line;
}

- (UILabel *)setUpHotleLodgingTitleWithLodgX:(CGFloat)lodgX
{
    UILabel *lb = [[UILabel alloc] init];
    lb.frame = CGRectMake(lodgX, _hotleNameView.center.y, 50, 30);
    lb.font = [UIFont systemFontOfSize:13];
    lb.textColor = [UIColor colorWithHexString:@"#666666"];
    [_hotleNameView addSubview:lb];
    return lb;
}

- (UILabel *)setUpHotleDateTitleWithDateX:(CGFloat)dateX DateW:(CGFloat)dateW
{
    UILabel *lb = [[UILabel alloc] init];
    lb.frame = CGRectMake(dateX, _hotleNameView.center.y + 30, dateW, 30);
    lb.font = [UIFont systemFontOfSize:16];
    lb.textColor = [UIColor colorWithHexString:@"#444444"];
    [_hotleNameView addSubview:lb];
    return lb;
}

- (void)scInvoiceClick:(UISwitch *)sender
{
    if (sender.isOn == YES) {
        self.invoiceView.hidden = NO;
        self.remarksView.bd_y = _invoiceView.bd_bottom+10;
        self.lbExplanatory.bd_y = _remarksView.bd_bottom+10;
    }else {
        self.invoiceView.hidden = YES;
        self.remarksView.bd_y = _occupantInformationView.bd_bottom+10;
        self.lbExplanatory.bd_y = _remarksView.bd_bottom+10;
    }
}

- (NSString *)achieveCurrentlyDate:(NSDate *)date
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MM月dd日"];
    NSString *currentlyDate = [format stringFromDate:date];
    return currentlyDate;
}

- (void)phoneTextFieldDidChange:(UITextField *)textField
{
    [BDMobileNumberTools textFieldDidChange:textField selfField:self.tfPhone];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
}


@end
