//
//  OccupantNameCell.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/9.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "OccupantNameCell.h"
#import "OccupantModel.h"

@interface OccupantNameCell()

@property (nonatomic, strong) UILabel *nameLabel;

@end
@implementation OccupantNameCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setOccupantModel:(OccupantModel *)occupantModel
{
    _occupantModel = occupantModel;
    self.nameLabel.text = occupantModel.checkInPersonName;
    if (occupantModel.isSelect == YES) {
        [self.selectBtn setImage:[UIImage imageNamed:shopingCartSelecdIcon] forState:UIControlStateNormal];
    }else {
        [self.selectBtn setImage:[UIImage imageNamed:shopingCartIcon] forState:UIControlStateNormal];
    }
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:@"OccupantNameCell"]) {
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    UIButton *BtnSelect = [[UIButton alloc] init];
    BtnSelect.frame = CGRectMake(10, 0, 30, self.bd_height);
    [BtnSelect setImage:[UIImage imageNamed:shopingCartIcon] forState:UIControlStateNormal];
    [BtnSelect setImage:[UIImage imageNamed:shopingCartSelecdIcon] forState:UIControlStateSelected];
    [BtnSelect addTarget:self action:@selector(imageSelectClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:BtnSelect];
    self.selectBtn = BtnSelect;
    
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.frame = CGRectMake(BtnSelect.bd_right, 0, 150, self.bd_height);
    nameLabel.textColor = [UIColor blackColor];
    [self addSubview:nameLabel];
    self.nameLabel = nameLabel;
    
//    CGFloat deleteW = 60;
//    UIButton *btnDelete = [[UIButton alloc] init];
//    btnDelete.frame = CGRectMake(self.bd_width-deleteW , 0, deleteW, self.bd_height);
//    [btnDelete setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [btnDelete setTitle:@"删除" forState:UIControlStateNormal];
//    btnDelete.titleLabel.font = [UIFont systemFontOfSize:15];
//    [self addSubview:btnDelete];
}

- (void)imageSelectClick:(UIButton *)sender
{
//    sender.selected = !sender.selected;
}



@end
