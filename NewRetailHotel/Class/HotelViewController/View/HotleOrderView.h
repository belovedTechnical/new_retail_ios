//
//  HotleOrderView.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/23.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotleOrderView : UIView

// 添加入住人
@property (nonatomic, strong) UIButton *btnAddOccupant;
// 选择房间数
@property (nonatomic, strong) UIButton *btnSelectRoom;
@property (nonatomic, strong) UIButton *btnDateClick;
@property (nonatomic, strong) UIButton *btnPhone;
@property (nonatomic, strong) UILabel  *nameLb;
@property (nonatomic, strong) UILabel *lbDayAll;
@property (nonatomic, strong) UILabel *lbDateEnter;
@property (nonatomic, strong) UILabel *lbDateOut;
@property (nonatomic, strong) UILabel *lbHotleName;
@property (nonatomic, strong) UITextField *tfRise;
@property (nonatomic, strong) UITextField *tfNumber;
@property (nonatomic, strong) UITextField *tfPhone;
@property (nonatomic, strong) UITextField *tfRemarks;

@end
