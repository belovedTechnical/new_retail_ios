//
//  HotelTopView.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/14.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "HotelTopView.h"
#import "BottomButton.h"

@interface HotelTopView()

@end

@implementation HotelTopView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    CGFloat topCenterY = (self.frame.size.height - 20) / 2;
    CGFloat space = 10;
    CGFloat locationBtnX = space;
    CGFloat locationBtnY = topCenterY;
    CGFloat locationBtnW = 40;
    CGFloat locationBtnH = 35;
//    BottomButton *locationBtn = [[BottomButton alloc] initWithFrame:CGRectMake(locationBtnX, locationBtnY, locationBtnW, locationBtnH)];
////    locationBtn.backgroundColor = randomColor;
////    [locationBtn setTitle:@"定位中" forState:UIControlStateNormal];
////    [locationBtn setTitleColor:[UIColor colorWithHexString:@"#666666"] forState:UIControlStateNormal];
////    [locationBtn setImage:[UIImage imageNamed:@"icon_position"] forState:UIControlStateNormal];
////    locationBtn.titleLabel.font = [UIFont systemFontOfSize:10];
////    locationBtn.contentMode = UIViewContentModeCenter;
////    [self addSubview:locationBtn];
//    self.locationBtn = locationBtn;
    
    UIView *locationView = [[UIView alloc] init];
    locationView.frame = CGRectMake(locationBtnX, locationBtnY, locationBtnW, locationBtnH);
    [self addSubview:locationView];
    UIImageView *locaIcon = [[UIImageView alloc] init];
    locaIcon.frame = CGRectMake(8.5, 0, 23, 23);
    locaIcon.image = [UIImage imageNamed:@"icon_position"];
    locaIcon.image = [UIImage imageOriginalWhitImageName:@"icon_position"];
    [locationView addSubview:locaIcon];
    UILabel *lbLocation = [[UILabel alloc] init];
    lbLocation.frame = CGRectMake(0, locaIcon.bd_bottom+2, locationView.bd_width, locationView.bd_height-locaIcon.bd_height);
    lbLocation.textColor = [UIColor colorWithHexString:@"#666666"];
    lbLocation.textAlignment = NSTextAlignmentCenter;
    lbLocation.font = [UIFont systemFontOfSize:10];
    lbLocation.text = @"定位中";
    [locationView addSubview:lbLocation];
    self.lbLocation = lbLocation;
    self.locationView = locationView;
    
    CGFloat searchBtnX = locationView.frame.origin.x + locationBtnW + space;
    CGFloat searchBtnY = topCenterY;
    CGFloat searchBtnW = kScreenW - searchBtnX * 2;
    CGFloat searchBtnH = locationBtnH;
    UIButton *searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(searchBtnX, searchBtnY, searchBtnW, searchBtnH)];
    [searchBtn setTitle:@" 搜索你想要的内容" forState:UIControlStateNormal];
    [searchBtn setImage:[UIImage imageNamed:@"icon-search"] forState:UIControlStateNormal];
    [searchBtn setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
    searchBtn.backgroundColor = [UIColor groupTableViewBackgroundColor];
    searchBtn.alpha = 0.8;
    searchBtn.layer.cornerRadius = 2.5;
    searchBtn.layer.masksToBounds = YES;
    searchBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    //    searchBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self addSubview:searchBtn];
    self.searchBtn = searchBtn;
    
    CGFloat scanBtnX = searchBtnX + searchBtnW + space;
    CGFloat scanBtnY = topCenterY;
    CGFloat scanBtnW = locationBtnW;
    CGFloat scanBtnH = locationBtnH;
    UIButton *scanBtn = [[UIButton alloc] initWithFrame:CGRectMake(scanBtnX, scanBtnY, scanBtnW, scanBtnH)];
    [scanBtn setImage:[UIImage imageNamed:@"qrcode_borderIcon"] forState:UIControlStateNormal];
    scanBtn.contentMode = UIViewContentModeCenter;
    [self addSubview:scanBtn];
    self.scanBtn = scanBtn;
}


@end
