//
//  SeekListTopView.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/21.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeekListTopView : UIView

@property (nonatomic, strong) UIButton *btnDate;
@property (nonatomic, strong) UIButton *btnArea;
@property (nonatomic, strong) UIButton *btnTaxis;
@property (nonatomic, strong) UIButton *btnBack;
@property (nonatomic, strong) UIButton *btnCity;
@property (nonatomic, strong) UIButton *btnSearch;

@end
