//
//  HotelTopView.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/14.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelTopView : UIView

@property (nonatomic, strong) UIButton *searchBtn;
@property (nonatomic, strong) UIButton *scanBtn;
@property (nonatomic, strong) UIView *locationView;
@property (nonatomic, strong) UILabel *lbLocation;

@end
