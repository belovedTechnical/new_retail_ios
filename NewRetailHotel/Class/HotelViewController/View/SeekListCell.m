//
//  SeekListCell.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/21.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "SeekListCell.h"
#import "UIImageView+WebCache.h"
#import "HotelSeekListModel.h"

@interface SeekListCell()

@property (weak, nonatomic) IBOutlet UIImageView *preViewImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *addressBtn;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@end

@implementation SeekListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self initUI];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}

- (void)setSeekListModel:(HotelSeekListModel *)seekListModel
{
    _seekListModel = seekListModel;
    [self.addressBtn setTitle:seekListModel.address forState:UIControlStateNormal];
    [self.preViewImageView sd_setImageWithURL:[NSURL URLWithString:seekListModel.imageUrl]];
    self.nameLabel.text = [NSString stringWithFormat:@" %@", seekListModel.hotelName];
    self.priceLabel.text = seekListModel.price;
    UIImage *image = self.preViewImageView.image;
    self.preViewImageView.image = [self graphicsImageClipWithImage:image];
}

- (void)initUI
{
    self.backgroundColor = [UIColor colorWithHexString:@"#f6f6f6"];
    [self.addressBtn setTitleColor:[UIColor colorWithHexString:@"#666666"] forState:UIControlStateNormal];
    self.bottomView.backgroundColor = [UIColor colorWithHexString:@"#ffffff"];
    self.pointLabel.textColor = [UIColor colorWithHexString:@"#666666"];
    self.priceLabel.textColor = [UIColor colorWithHexString:@"#b4282d"];
    self.addressBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    [self graphicsViewClipWithView:self.bottomView];
}

- (UIImage *)graphicsImageClipWithImage:(UIImage *)image
{
    // 开启图形上下文
    UIGraphicsBeginImageContext(image.size);
    // 绘制想要裁剪的地方
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, image.size.width, image.size.height) byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(5, 5)];
    // 将路径设置为裁剪区域
    [path addClip];
    // 把图片绘制到上下文中
    [image drawAtPoint:CGPointZero];
    // 从上下文中生成一张新的图片
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    // 关闭上下文
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)graphicsViewClipWithView:(UIView *)clipView
{
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:clipView.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(5, 5)];
    CAShapeLayer *layer = [CAShapeLayer layer];
    layer.frame = clipView.bounds;
    layer.path = path.CGPath;
    clipView.layer.mask = layer;
}







@end
