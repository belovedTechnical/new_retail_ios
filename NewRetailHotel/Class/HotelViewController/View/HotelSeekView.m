//
//  HotelSeekView.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/14.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "HotelSeekView.h"
#import "Masonry.h"

@interface HotelSeekView()

@end

@implementation HotelSeekView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    CGFloat locaX = 20;
    CGFloat locaY = 0;
    CGFloat locaW = self.bd_width - locaX*2;
    CGFloat locaH = 50;
    UIView *locationView = [[UIView alloc] init];
    locationView.frame = CGRectMake(locaX, locaY, locaW, locaH);
    UIView *line = [[UIView alloc] init];
    line.frame = CGRectMake(0, locationView.bd_height, locationView.bd_width, 0.5);
    line.backgroundColor = [UIColor colorWithHexString:@"#e1e1e1"];
    [locationView addSubview:line];
    [self addSubview:locationView];
    
    CGFloat btnW = 50;
    CGFloat locationViewH = locationView.bd_height;
    
    UIButton *btnDestination = [[UIButton alloc] init];
    btnDestination.frame = CGRectMake(0, 0, locationView.bd_width - btnW, locationViewH);
    [btnDestination setTitleColor:[UIColor colorWithHexString:@"#444444"] forState:UIControlStateNormal];
    [btnDestination setImage:[UIImage imageNamed:@"bh_b_position"] forState:UIControlStateNormal];
    btnDestination.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btnDestination setTitle:@" 目的地" forState:UIControlStateNormal];
    btnDestination.titleLabel.font = [UIFont systemFontOfSize:16];
    [locationView addSubview:btnDestination];
    self.btnDestination = btnDestination;
    
    UIButton *btnLocation = [[UIButton alloc] init];
    btnLocation.frame = CGRectMake(btnDestination.bd_right, 0, btnW, locationViewH);
    [btnLocation setTitleColor:[UIColor colorWithHexString:@"#444444"] forState:UIControlStateNormal];
    [btnLocation setImage:[UIImage imageNamed:@"icon_locate"] forState:UIControlStateNormal];
    btnLocation.titleLabel.font = [UIFont systemFontOfSize:10];
    [locationView addSubview:btnLocation];
    self.btnLocation = btnLocation;
    
    CGFloat dateX = locaX;
    CGFloat dateY = locationView.bd_bottom + 1;
    CGFloat dateW = locaW;
    CGFloat dateH = locaH;
    UIView *dateView = [[UIView alloc] init];
    dateView.frame = CGRectMake(dateX, dateY, dateW, dateH);
    UIView *dateLine = [[UIView alloc] init];
    dateLine.frame = CGRectMake(0, dateView.bd_height, dateView.bd_width, 0.5);
    dateLine.backgroundColor = [UIColor colorWithHexString:@"#e1e1e1"];
    [dateView addSubview:dateLine];
    [self addSubview:dateView];
    
    UIImageView *imgCalendar = [[UIImageView alloc] init];
    imgCalendar.image = [UIImage imageNamed:@"bh_b_calendar"];
    [dateView addSubview:imgCalendar];
    
    // 开始时间
    UILabel *dateStart = [self foundDateLabelWithView:dateView];
    NSDate *today = [[NSDate alloc] init];
    _startDate = today;
    dateStart.text = [self achieveCurrentlyDate:today];
    self.dateStart = dateStart;
    
    // 结束时间
    UILabel *dateDeparture = [self foundDateLabelWithView:dateView];
    NSDate *tomorrow = [[NSDate date] dateByAddingTimeInterval:3600*24];
    _endDate = tomorrow;
    dateDeparture.text = [self achieveCurrentlyDate:tomorrow];
    self.dateDeparture = dateDeparture;

    // 入住
    UILabel *lbStart = [self foundCheckAndDepartureWithView:dateView];
    lbStart.text = @"入住";
    
    // 离店
    UILabel *lbDeparture = [self foundCheckAndDepartureWithView:dateView];
    lbDeparture.text = @"离店";
    
    UIView *lineGross = [[UIView alloc] init];
    lineGross.backgroundColor = [UIColor colorWithHexString:@"#a6926b"];
    [dateView addSubview:lineGross];
    
    UILabel *lbGross = [[UILabel alloc] init];
    lbGross.textColor = [UIColor colorWithHexString:@"#a6926b"];
    lbGross.textAlignment = NSTextAlignmentCenter;
    lbGross.font = [UIFont systemFontOfSize:11];
    lbGross.text = @"共1晚";
    [dateView addSubview:lbGross];
    self.lbGross = lbGross;
    
    CGFloat btnSeekX = locaX;
    CGFloat btnSeekY = dateView.bd_bottom + 20;
    CGFloat btnSeekW = locaW;
    CGFloat btnSeekH = 44;
    UIButton *btnSeek = [[UIButton alloc] init];
    btnSeek.frame = CGRectMake(btnSeekX, btnSeekY, btnSeekW, btnSeekH);
    [btnSeek setTitle:@"查询酒店" forState:UIControlStateNormal];
    [btnSeek setTitleColor:[UIColor colorWithHexString:@"#ffffff"] forState:UIControlStateNormal];
    btnSeek.backgroundColor = [UIColor colorWithHexString:@"#a6926b"];
    btnSeek.titleLabel.font = [UIFont systemFontOfSize:17];
    [btnSeek.layer setCornerRadius:2.5];
    [btnSeek.layer setMasksToBounds:YES];
    [self addSubview:btnSeek];
    self.btnSeek = btnSeek;
    
    UIButton *btnInputDate = [[UIButton alloc] init];
    btnInputDate.frame = dateView.bounds;
    [dateView addSubview:btnInputDate];
    self.btnInputDate = btnInputDate;
    
    [imgCalendar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(dateView.mas_centerY);
        make.left.equalTo(dateView.mas_left);
        make.height.equalTo(@23);
        make.width.equalTo(@23);
    }];
    
    [dateStart mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(dateView.mas_top).offset(0);
        make.bottom.equalTo(dateView.mas_bottom).offset(0);
        make.left.equalTo(imgCalendar.mas_right).offset(0);
        make.right.equalTo(lbStart.mas_left).offset(-6);
    }];
    
    CGFloat sp = -10;
    CGFloat lineGrossW = 46;
    if (Device_iPhone4 || Device_iPhone5) {
        sp = -8;
        lineGrossW = 30;
    }
    [lbStart mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(dateView.mas_top).offset(0);
        make.bottom.equalTo(dateView.mas_bottom).offset(0);
        make.left.equalTo(dateStart.mas_right).offset(6);
        make.right.equalTo(lineGross.mas_left).offset(sp);
    }];
    
    [lineGross mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(dateView.mas_centerX).offset(10);
        make.centerY.equalTo(dateView.mas_centerY);
        make.height.equalTo(@0.5);
        make.width.equalTo(@(lineGrossW));
    }];
    
    [lbGross mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(dateView.mas_top).offset(0);
        make.bottom.equalTo(lineGross.mas_top).offset(0);
        make.width.equalTo(lineGross);
        make.left.equalTo(lineGross);
    }];
    
    [dateDeparture mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(dateView.mas_top).offset(0);
        make.bottom.equalTo(dateView.mas_bottom).offset(0);
        make.left.equalTo(lineGross.mas_right).offset(10);
        make.right.equalTo(lbDeparture.mas_left).offset(0);
    }];
    
    [lbDeparture mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(dateView.mas_top).offset(0);
        make.bottom.equalTo(dateView.mas_bottom).offset(0);
        make.left.equalTo(dateDeparture.mas_right).offset(0);
        make.right.equalTo(dateView.mas_right).offset(0);
    }];
}

- (UILabel *)foundDateLabelWithView:(UIView *)superView
{
    UILabel *dateLabel = [[UILabel alloc] init];
    dateLabel.textColor = [UIColor colorWithHexString:@"#444444"];
    dateLabel.textAlignment = NSTextAlignmentCenter;
    if (Device_iPhone5 || Device_iPhone4) {
     dateLabel.font = [UIFont systemFontOfSize:12];
    }else {
     dateLabel.font = [UIFont systemFontOfSize:16];
    }
    [dateLabel sizeToFit];
    [superView addSubview:dateLabel];
    return dateLabel;
}

- (UILabel *)foundCheckAndDepartureWithView:(UIView *)superView
{
    UILabel *lbStart = [[UILabel alloc] init];
    lbStart.textColor = [UIColor colorWithHexString:@"#333333"];
    lbStart.textAlignment = NSTextAlignmentRight;
    lbStart.font = [UIFont systemFontOfSize:11];
    [lbStart sizeToFit];
    [superView addSubview:lbStart];
    return lbStart;
}

- (NSString *)achieveCurrentlyDate:(NSDate *)date
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MM月dd日"];
    NSString *currentlyDate = [format stringFromDate:date];
    return currentlyDate;
}

@end
