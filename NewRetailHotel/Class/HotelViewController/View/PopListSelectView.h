//
//  PopListSelectView.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/22.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^popListSelectViewCellBlcok)(NSString *selectName, NSString *selectCode);
@interface PopListSelectView : UIView

@property (nonatomic, strong) NSArray *listDataArray;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) popListSelectViewCellBlcok popListSelectViewCellBlock;

@end
