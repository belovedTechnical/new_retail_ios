//
//  HotelSeekListModel.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/2.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelSeekListModel : NSObject

@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *hotelName;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *hotelPartyId;
@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, copy) NSString *hotelLevel;

+ (instancetype)hotelSeekListModelWithDict:(NSDictionary *)dict;

@end
