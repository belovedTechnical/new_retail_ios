//
//  HotelSeekListModel.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/2.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "HotelSeekListModel.h"

@implementation HotelSeekListModel

+ (instancetype)hotelSeekListModelWithDict:(NSDictionary *)dict
{
    HotelSeekListModel *hotelSeekListModel = [[self alloc] init];
    [hotelSeekListModel setValuesForKeysWithDictionary:dict];
    return hotelSeekListModel;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}

@end
