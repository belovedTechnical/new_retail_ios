//
//  HotelSearchData.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/20.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelSearchData : NSObject

//用户选择入住起始日期
@property (strong,nonatomic) NSDate *OrderRoomStartDate;

//用户选择离店的日期
@property (strong,nonatomic) NSDate *OrderRoomEndDate;

//入住天数统计
@property (assign,nonatomic) NSUInteger CountDay;

+ (HotelSearchData *)sharedHotelSearchData;

- (void)setOrderRoomEndDate:(NSDate *)OrderRoomEndDate;

//显示为 07月14日(1天)这种格式
- (NSString *)showStartDateAndCountDays;

- (NSString *)showStartDateForStr;

- (NSString *)showEndDateForStr;


@end
