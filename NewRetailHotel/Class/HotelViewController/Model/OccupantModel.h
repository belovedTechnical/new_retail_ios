//
//  OccupantModel.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/10.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OccupantModel : NSObject

@property (nonatomic, strong) NSString *checkInPersonId;
@property (nonatomic, strong) NSString *checkInPersonName;
@property (nonatomic, strong) NSString *selectName;
@property (nonatomic, assign) BOOL isSelect;

+ (instancetype)occupantModelWithDict:(NSDictionary *)dict;

@end
