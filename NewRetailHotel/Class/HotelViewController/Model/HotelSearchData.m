//
//  HotelSearchData.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/20.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "HotelSearchData.h"

@interface HotelSearchData()

@property (strong,nonatomic) NSDateFormatter *formatter;

@end

@implementation HotelSearchData

+ (HotelSearchData *)sharedHotelSearchData{
    static HotelSearchData *instance = nil;
    
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}


- (void)setOrderRoomEndDate:(NSDate *)OrderRoomEndDate{
    _OrderRoomEndDate = OrderRoomEndDate;
    if(_OrderRoomStartDate != nil){
        NSTimeInterval interval = [_OrderRoomEndDate timeIntervalSinceDate:_OrderRoomStartDate];
        NSUInteger resultDays = ((NSInteger)interval)/(3600*24);
        _CountDay = resultDays;
    }
}



//显示为 07月14日(1天)这种格式
- (NSString *)showStartDateAndCountDays{
    if(self.OrderRoomStartDate != nil){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setDateFormat:@"MM月dd日"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Shanghai"]];
        
        NSString *roomStartspStr = [formatter stringFromDate:self.OrderRoomStartDate];
        
        NSTimeInterval interval = [self.OrderRoomEndDate timeIntervalSinceDate:self.OrderRoomStartDate];
        NSUInteger resultDays = ((NSInteger)interval)/(3600*24);
        return [NSString stringWithFormat:@"%@(%lu天)",roomStartspStr,(unsigned long)resultDays];
    }else{
        return @"";
    }
}


- (NSString *)showStartDateForStr{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setDateFormat:@"MM月dd日"]; //HH:mm:ss ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Shanghai"]];
    return [formatter stringFromDate:self.OrderRoomStartDate];
}

- (NSString *)showEndDateForStr{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setDateFormat:@"MM月dd日"]; //HH:mm:ss ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Shanghai"]];
    return [formatter stringFromDate:self.OrderRoomEndDate];
}







@end
