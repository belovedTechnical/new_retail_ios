//
//  Macro.h
//  BelovedHotel
//
//  Created by 王保栋 on 2017/5/25.
//  Copyright © 2017年 BeloverdSpace. All rights reserved.
//

#ifndef Macro_h
#define Macro_h
#import <Foundation/Foundation.h>

// 支付
//#define kWeChatURLScheme @"wx4b2322692fe85433"
#define kWeChatURLScheme @"wxe6960de30bac5534"
#define kAliPayURLScheme @"AliPay2016080501709547"

// 支付成功通知
#define PAY_NOTIFICATION  @"ORDER_PAY_NOTIFICATION"
// apikey
#define WeChatLogin                 @"wxe6960de30bac5534"                                       // 微信登录
#define userPhoneCodeKey            @"userPhoneCodeKey"                                         // 用户手机号
#define WeChatSecret                @"e68aa768fbd0fd1ce45e891eb5284c77"                         // 第三方平台的appSecret
//通知定义常量
#define kChangeOrderRoomDate        @"changeOrderRoomDate"                                      // 酒店预订日期选择通知
#define succeedLoginNoti            @"succeedLoginNoti"                                         // 登陆成功通知
#define thirdpartyLoginNoti         @"thirdpartyLoginNoti"                                      // 第三方 微信登陆成功通知
#define LogoutNoti                  @"LogoutNoti"                                               // 注销登陆
#define kbackCouponIDNoti       @"backCouponIDNoti"                     // 返回优惠券ID通知
#define kNoBackCouponIDNoti     @"kNoBackCouponIDNoti"                  // 不使用优惠劵按钮点击通知


///////////////////
// NSUserDefault的Key，命名以Key结尾
///////////////////
static NSString * superapi_key =  @"superapiKey";
static NSString * apikey = @"apiKey";
static NSString * checkInStrKey = @"checkInStrKey";
static NSString * checkOutStrKey = @"checkOutStrKey";
static NSString * checkInDateKey = @"checkInDateKey";
static NSString * checkOutDateKey = @"checkOutDateKey";
static NSString * numberDaysKey = @"numberDaysKey";
#pragma mark -购物车选择按钮图片
static NSString *shopingCartIcon = @"icon_checkbox.png";
static NSString *shopingCartSelecdIcon = @"icon_checkbox_selected.png";

#define URL_GlobalPurchaseID    @"102602"                                        // 全球购店铺ID


// web界面网址
#define urlWebWelfare        @"welfare.html?lovehuodong"             //星星公益
#define urlWebMyWelfare      @"users/mywelfare.html"           //我的公益
#define urlWebFindVideo      @"c-video.html"    //发现
#define urlWebShopDetails    @"c-shopDetails/"  //购物车
#define urlWebHotelDetails   @"c-hotelDetails.html?hotelPartyId="  //酒店





#endif /* Macro_h */


