//
//  BDTabBarController.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/1.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "BDTabBarController.h"
#import "ExperienceHallViewController.h"
#import "ShoppingCartViewController.h"
#import "BDNavigationController.h"
#import "HotelViewController.h"
#import "MinesViewController.h"
#import "FindViewController.h"
#import "WelfareWebViewController.h"
#import "UIImage+Image.h"

@interface BDTabBarController ()

@end

@implementation BDTabBarController

+ (void)load
{
    UITabBarItem *tabItem = [UITabBarItem appearanceWhenContainedInInstancesOfClasses:@[self]];
    NSMutableDictionary *arrtC = [NSMutableDictionary dictionary];
    arrtC[NSForegroundColorAttributeName] = [UIColor blackColor];
    [tabItem setTitleTextAttributes:arrtC forState:UIControlStateSelected];
    
    NSMutableDictionary *arrtF = [NSMutableDictionary dictionary];
    arrtF[NSFontAttributeName] = [UIFont systemFontOfSize:11];
    [tabItem setTitleTextAttributes:arrtF forState:UIControlStateNormal];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupAllChildController];
    [self setupAllTitleButton];
}

- (void)setupAllTitleButton
{
    UIViewController *vc = self.childViewControllers[0];
    vc.tabBarItem.title = @"体验馆";
    vc.tabBarItem.image = [UIImage imageOriginalWhitImageName:@"iconbottom_index"];
    vc.tabBarItem.selectedImage = [UIImage imageOriginalWhitImageName:@"iconbottom_index_selected"];
    
    UIViewController *vc1 = self.childViewControllers[1];
    vc1.tabBarItem.title = @"酒店";
    vc1.tabBarItem.image = [UIImage imageOriginalWhitImageName:@"iconbottom_hotel"];
    vc1.tabBarItem.selectedImage = [UIImage imageOriginalWhitImageName:@"iconbottom_hotel_selected"];
    
//    UIViewController *vc2 = self.childViewControllers[2];
//    vc2.tabBarItem.title = @"发现";
//    vc2.tabBarItem.image = [UIImage imageOriginalWhitImageName:@"iconbottom_discoverl"];
//    vc2.tabBarItem.selectedImage = [UIImage imageOriginalWhitImageName:@"iconbottom_discoverl_selected"];
    
    UIViewController *vc2 = self.childViewControllers[2];
    vc2.tabBarItem.title = @"星星公益";
    vc2.tabBarItem.image = [UIImage imageOriginalWhitImageName:@"iconbottom_welfare"];
    vc2.tabBarItem.selectedImage = [UIImage imageOriginalWhitImageName:@"iconbottom_welfare_selected"];
    
    UIViewController *vc3 = self.childViewControllers[3];
    vc3.tabBarItem.title = @"购物车";
    vc3.tabBarItem.image = [UIImage imageOriginalWhitImageName:@"iconbottom_cart"];
    vc3.tabBarItem.selectedImage = [UIImage imageOriginalWhitImageName:@"iconbottom_cart_selected"];
    
    UIViewController *vc4 = self.childViewControllers[4];
    vc4.tabBarItem.title = @"我的";
    vc4.tabBarItem.image = [UIImage imageOriginalWhitImageName:@"iconbottom_my"];
    vc4.tabBarItem.selectedImage = [UIImage imageOriginalWhitImageName:@"iconbottom_my_selected"];
    
}

- (void)setupAllChildController
{
    ExperienceHallViewController *oneVC = [[ExperienceHallViewController alloc] init];
    BDNavigationController *nav = [[BDNavigationController alloc] initWithRootViewController:oneVC];
    [self addChildViewController:nav];
    
    HotelViewController *twoVC = [[HotelViewController alloc] init];
    BDNavigationController *nav1 = [[BDNavigationController alloc] initWithRootViewController:twoVC];
    [self addChildViewController:nav1];
    
    //FindViewController *threeVC = [[FindViewController alloc] init];
    WelfareWebViewController *threeVC = [[WelfareWebViewController alloc] init];
    BDNavigationController *nav2 = [[BDNavigationController alloc] initWithRootViewController:threeVC];
    [self addChildViewController:nav2];
    
    ShoppingCartViewController *fourVC = [[ShoppingCartViewController alloc] init];
    BDNavigationController *nav3 = [[BDNavigationController alloc] initWithRootViewController:fourVC];
    [self addChildViewController:nav3];
    
    MinesViewController *mineVC = [[MinesViewController alloc] init];
    BDNavigationController *nav4 = [[BDNavigationController alloc] initWithRootViewController:mineVC];
    [self addChildViewController:nav4];
}


@end

