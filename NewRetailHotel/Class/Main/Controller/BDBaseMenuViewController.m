//
//  BDBaseMenuViewController.m
//  BDBaseMenuVC
//
//  Created by BDSir on 2017/6/14.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "BDBaseMenuViewController.h"
#import "UIView+Frame.h"

static NSString *const ID = @"cell";
@interface BDBaseMenuViewController ()<UICollectionViewDataSource, UICollectionViewDelegate>

@property(nonatomic, weak) UICollectionView *collectionView;
@property(nonatomic, weak) UIButton *selectButton;
@property(nonatomic, weak) UIView *underLineView;
@property(nonatomic, strong) NSMutableArray *buttons;
@property(nonatomic, assign) BOOL isInitial;
@end

@implementation BDBaseMenuViewController

#define BDScreenW [UIScreen mainScreen].bounds.size.width
- (NSMutableArray *)buttons
{
    if (!_buttons) {
        _buttons = [NSMutableArray array];
    }
    return _buttons;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (_isInitial == NO) {
        [self setupAllTitleButton];
        _isInitial = YES;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 添加底部内容View
    [self setupBottomView];
    
    // 添加顶部标题View
    [self setupTopView];

    // 不需要自动添加额外滚动区域
    self.automaticallyAdjustsScrollViewInsets = NO;
}

#pragma mark -设置所有标题
- (void)setupAllTitleButton
{
    NSUInteger count = self.childViewControllers.count;
    CGFloat btnX = 0;
    CGFloat btnY = 0;
    CGFloat btnW = BDScreenW / count;
    CGFloat btnH = _topTitleView.bd_height;
    for (int i = 0; i < count; i++) {
        UIButton *titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        titleBtn.tag = i;
        UIViewController *vc = self.childViewControllers[i];
        [titleBtn setTitle:vc.title forState:UIControlStateNormal];
        [titleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [titleBtn setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
        titleBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        btnX = i * btnW;
        titleBtn.frame = CGRectMake(btnX, btnY, btnW, btnH);
        
        [_topTitleView addSubview:titleBtn];
        [self.buttons addObject:titleBtn];
        
        // 监听按钮
        [titleBtn addTarget:self action:@selector(titleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        if (i == 0) {
            
            // 添加下划线
            UIView *underLineView = [[UIView alloc] init];
            underLineView.backgroundColor = [UIColor redColor];
            [_topTitleView addSubview:underLineView];
            _underLineView = underLineView;
            
            //中心点
            underLineView.bd_height = 2;
            underLineView.bd_y = _topTitleView.bd_height - underLineView.bd_height;
            [titleBtn.titleLabel sizeToFit];
            underLineView.bd_width = titleBtn.bd_width;
            underLineView.bd_centerX = titleBtn.bd_centerX;
            
            [self titleBtnClick:titleBtn];
        }
    }
}

#pragma mark -点击标题
- (void)titleBtnClick:(UIButton *)button
{
    NSInteger i = button.tag;
    // 1.选中按钮
    [self selButton:button];
    
    // 2.设置collectionView偏移量
    CGFloat offsetX = i * BDScreenW;
    _collectionView.contentOffset = CGPointMake(offsetX, 0);
}

#pragma mark -选中按钮
- (void)selButton:(UIButton *)button
{
    _selectButton.selected = NO;
    button.selected = YES;
    _selectButton = button;
    
    // 修改下划线位置
    [UIView animateWithDuration:0.25 animations:^{
        _underLineView.bd_centerX = button.bd_centerX;
        
    }];
}

#pragma mark -UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.childViewControllers.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ID forIndexPath:indexPath];
    
    // 移除其他子控制器view
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    // 添加对应的子控制器View 到对应cell
    UIViewController *vc = self.childViewControllers[indexPath.row];
    
    // 默认控制器frame有y值, 每次添加的时候，必须重新设置子控制器的frame
    vc.view.frame = [UIScreen mainScreen].bounds;
    
    [cell.contentView addSubview:vc.view];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate
// 滚动完成的时候就会调用
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // 获取页码
    NSInteger page = scrollView.contentOffset.x / BDScreenW;
    
    // 获取标题按钮
    UIButton *titleButton = self.buttons[page];
    
    // 选中标题
    [self selButton:titleButton];
}

#pragma mark -添加顶部标题View
- (void)setupTopView
{
    CGFloat x = 0;
    CGFloat y = SafeAreaTopHeight;
    CGFloat w = self.view.frame.size.width;
    CGFloat h = 35;
    
    UIScrollView *topTitleView = [[UIScrollView alloc] initWithFrame:CGRectMake(x, y, w, h)];
    topTitleView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:topTitleView];
    _topTitleView = topTitleView;
}

#pragma mark -添加底部内容View
- (void)setupBottomView
{
    // 布局
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = [UIScreen mainScreen].bounds.size;
    // 设置滚动方向
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 0;
    
    // UICollectionView
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:[UIScreen mainScreen].bounds collectionViewLayout:layout];
    collectionView.backgroundColor = [UIColor colorWithRed:(215)/255.0 green:(215)/255.0 blue:(215)/255.0 alpha:1];
    [self.view addSubview:collectionView];
    
    collectionView.showsHorizontalScrollIndicator = NO;
    collectionView.pagingEnabled = YES;
    
    _collectionView = collectionView;
    collectionView.dataSource = self;
    collectionView.delegate = self;
    
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:ID];
}


@end
