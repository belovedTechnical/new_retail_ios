//
//  BDNavigationController.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/1.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "BDNavigationController.h"
@interface BDNavigationController ()<UIGestureRecognizerDelegate>

@end

@implementation BDNavigationController

+(void)load
{
    UINavigationBar *navBar = [UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[self]];
    
    NSMutableDictionary *arrt = [NSMutableDictionary dictionary];
    arrt[NSFontAttributeName] = [UIFont boldSystemFontOfSize:17];
    arrt[NSForegroundColorAttributeName] = [UIColor colorWithHexString:@"#444444"];
    [navBar setTitleTextAttributes:arrt];
    
    [navBar setBarTintColor:[UIColor colorWithHexString:@"#f6f6f6"]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    id target = self.interactivePopGestureRecognizer.delegate;
//    // 创建全屏滑动手势
//    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:target action:@selector(handleNavigationTransition:)];
//    pan.delegate = self;
//    [self.view addGestureRecognizer:pan];
//
//    // 限制系统边缘滑动手势
//    self.interactivePopGestureRecognizer.enabled = NO;
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return self.childViewControllers.count > 1;
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.childViewControllers.count > 0) {
        UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [backBtn setImage:[UIImage imageNamed:@"icon_return"] forState:UIControlStateNormal];
        [backBtn setImage:[UIImage imageNamed:@"icon_return"] forState:UIControlStateSelected];
        [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [backBtn sizeToFit];
        backBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
}

- (void)backBtnClick
{
    [self popViewControllerAnimated:YES];
}


@end

