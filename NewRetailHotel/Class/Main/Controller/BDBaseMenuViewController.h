//
//  BDBaseMenuViewController.h
//  BDBaseMenuVC
//
//  Created by BDSir on 2017/6/14.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BDBaseMenuViewController : UIViewController

@property(nonatomic, weak) UIScrollView *topTitleView;

@end
