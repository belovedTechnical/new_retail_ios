//
//  RootTool.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/1.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RootTool : NSObject

// 选择根控制器
+ (UIViewController *)choosRootVC;

@end
