//
//  RootTool.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/1.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "RootTool.h"
#import "SaveTool.h"
#import "NewFeatureViewController.h"
#import "BDTabBarController.h"

#define Version @"version"
@implementation RootTool

// 选择根控制器
+ (UIViewController *)choosRootVC
{
    // 获取上一次版本号
    NSString *preV = [SaveTool objectForKey:Version];
    // 获取当前版本号
    NSString *curV = [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"];
    // 判断两次是否相同
    if ([curV isEqualToString:preV]) {
        
        BDTabBarController *tabVC = [[BDTabBarController alloc] init];
        return tabVC;
        
    }else {
        
        
        // 保存当前版本号
        [SaveTool setObject:curV forKey:Version];
        
        BDTabBarController *tabVC = [[BDTabBarController alloc] init];
        return tabVC;
        
//        NewFeatureViewController *newFVC = [[NewFeatureViewController alloc] init];
//        return newFVC;
    }
    
}

@end
