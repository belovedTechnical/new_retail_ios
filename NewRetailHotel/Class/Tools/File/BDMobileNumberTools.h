//
//  BDMobileNumberTools.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/3/21.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BDMobileNumberTools : NSObject

/** 判断是否是手机号 */
+ (BOOL)isMobileNumber:(NSString *)mobileNum;

+ (void)textFieldDidChange:(UITextField *)textField selfField:(UITextField *)field;

@end
