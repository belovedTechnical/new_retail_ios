//
//  BDFileManager.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//  作用: 处理文件

#import <Foundation/Foundation.h>

@interface BDFileManager : NSObject

/**
 *  指定一个文件夹路径,获取文件夹尺寸
 *
 *  @param directoryPath 文件夹全路径
 *
 *  retur 文件夹尺寸
 */
+ (void)getDirectorySizeOfDirectoryPath:(NSString *)directoryPath completion:(void(^)(NSInteger totalSize))completionBlcok;

/**
 *  删除文件夹
 *
 *  @param directoryPath 文件夹路径
 */
+ (void)removeDirectoryPath:(NSString *)directoryPath;

@end
