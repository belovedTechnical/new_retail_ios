//
//  BDFilePathImage.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/9.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "BDFilePathImage.h"

@implementation BDFilePathImage

+ (NSString *)getImagePathWithIamge:(UIImage *)image {
    NSData *data = nil;
    NSString *filePath = nil;
    if (UIImagePNGRepresentation(image) == nil) {
        data = UIImageJPEGRepresentation(image, 1.0);
    }else {
        data = UIImagePNGRepresentation(image);
    }
    NSString *documentsPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager createDirectoryAtPath:documentsPath withIntermediateDirectories:YES attributes:nil error:nil];
    NSString *imagePath = [[NSString alloc] initWithFormat:@"/theFirstImage.png"];
    [fileManager createFileAtPath:[documentsPath stringByAppendingString:imagePath] contents:data attributes:nil];
    filePath = [[NSString alloc] initWithFormat:@"%@%@", documentsPath,imagePath];
    return filePath;
}

@end
