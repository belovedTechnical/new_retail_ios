//
//  BDFileManager.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "BDFileManager.h"

@implementation BDFileManager

// 异步任务，不需要返回值，没有意义，异步操作延迟，并不能马上返回,搞个blcok回调出去
// 获取文件夹尺寸
+ (void)getDirectorySizeOfDirectoryPath:(NSString *)directoryPath completion:(void(^)(NSInteger))completionBlock
{
    // 计算大的文件夹比较耗时，开启异步线程
    __block NSInteger totalSize = 0;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
       //  1.创建文件管理者
        NSFileManager *mgr = [NSFileManager defaultManager];
        
        BOOL isDirectory;
        BOOL exist = [mgr fileExistsAtPath:directoryPath isDirectory:&isDirectory];
        if (!exist || !isDirectory) { // 不存在并且不是文件夹
            // 报错
            NSException *excp = [NSException exceptionWithName:@"fileError" reason:@"传入路径错误，请检查" userInfo:nil];
            [excp raise];
        }
        
        // 2.遍历文件夹中所有文件
        NSArray *subpaths = [mgr subpathsAtPath:directoryPath];
        for (NSString *subpath in subpaths) {
            // 3.拼接完整文件名
            NSString *filePath = [directoryPath stringByAppendingPathComponent:subpath];
            
            // 3.1判断是否是隐藏文件
            if ([filePath containsString:@".DS_Store"]) continue;
            
            // 3.2判断下是否是文件夹
            BOOL isDirectory;
            [mgr fileExistsAtPath:directoryPath isDirectory:&isDirectory];
//            if (isDirectory) continue;
            
            // 4.attributesOfItemAtPath: 传入一个文件路径就能获取文件信息
            NSDictionary *attr = [mgr attributesOfItemAtPath:filePath error:nil];
            // 5.获取文件尺寸，添加到总尺寸
            totalSize += [attr fileSize];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completionBlock) {
                completionBlock(totalSize);
            }
        });
    });
}

+ (void)removeDirectoryPath:(NSString *)directoryPath
{
    // 1.创建文件管理者
    NSFileManager *mgr = [NSFileManager defaultManager];
    
    BOOL isDirectory;
    BOOL exist = [mgr fileExistsAtPath:directoryPath isDirectory:&isDirectory];
    if (!exist || !isDirectory) { // 不存在并且不是文件夹
        // 报错
        NSException *excp = [NSException exceptionWithName:@"fileError" reason:@"传入路径错误，请检查" userInfo:nil];
        [excp raise];
    }
    
    // 删除Cache文件夹
    [[NSFileManager defaultManager] removeItemAtPath:directoryPath error:nil];
    
    // 再创建Cache文件夹
    [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:nil];
}

@end
