//
//  BDFilePathImage.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/4/9.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BDFilePathImage : NSObject

/** 照片获取本地路径转换 */
+ (NSString *)getImagePathWithIamge:(UIImage *)image;

@end
