//
//  BDShowHUD.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/29.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "BDShowHUD.h"

@implementation BDShowHUD

static UIWindow *_keyWindow = nil;

+ (void)showSVP
{
    [SVProgressHUD show];
}
+ (void)showSVPMaskTypeClear
{
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD show];
}
+ (void)showSVPMaskType:(SVProgressHUDMaskType)maskType
{
    [SVProgressHUD setDefaultMaskType:maskType];
    [self showSVP];
}
+ (void)dismissSVP
{
    [SVProgressHUD dismiss];
}
+ (void)showSVPMaskWithStatus:(NSString *)status
{
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:status];
}
+ (void)showSVPMaskWithtime:(NSTimeInterval)time SuccessStatus:(NSString *)status
{
    [SVProgressHUD setMaximumDismissTimeInterval:time];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showSuccessWithStatus:status];
}
+ (void)showSVPMaskWithtime:(NSTimeInterval)time maskType:(SVProgressHUDMaskType)maskType SuccessStatus:(NSString *)status
{
    [SVProgressHUD setMaximumDismissTimeInterval:time];
    [SVProgressHUD showSuccessWithStatus:status];
    [SVProgressHUD setDefaultMaskType:maskType];
}

+ (void)showSVPMaskWithtime:(NSTimeInterval)time faileStatus:(NSString *)status
{
    [SVProgressHUD setMaximumDismissTimeInterval:time];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showErrorWithStatus:status];
}
+ (void)showSVPMaskWithtime:(NSTimeInterval)time maskType:(SVProgressHUDMaskType)maskType faileStatus:(NSString *)status
{
    [SVProgressHUD setMaximumDismissTimeInterval:time];
    [SVProgressHUD showErrorWithStatus:status];
    [SVProgressHUD setDefaultMaskType:maskType];
}
+ (void)showSVPMaskWithtime:(NSTimeInterval)time status:(NSString *)status
{
    [SVProgressHUD setMaximumDismissTimeInterval:time];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:status];
}
+ (void)showSVPMaskWithtime:(NSTimeInterval)time maskType:(SVProgressHUDMaskType)maskType status:(NSString *)status
{
    [SVProgressHUD setMaximumDismissTimeInterval:time];
    [SVProgressHUD setDefaultMaskType:maskType];
    [SVProgressHUD showWithStatus:status];
}
+ (void)showSVPMaskWithtime:(NSTimeInterval)time statusString:(NSString *)status
{
    [SVProgressHUD setMaximumDismissTimeInterval:time];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showImage:[UIImage imageNamed:@""] status:status];
}
+ (void)showSVPMaskWithtime:(NSTimeInterval)time maskType:(SVProgressHUDMaskType)maskType statusString:(NSString *)status
{
    [SVProgressHUD setMaximumDismissTimeInterval:time];
    [SVProgressHUD setDefaultMaskType:maskType];
    [SVProgressHUD showImage:[UIImage imageNamed:@""] status:status];
}
#pragma mark --<MBProgressHUD.h>
+ (void)showMBPShowTime:(CGFloat)time stateLabel:(NSString *)labelText;
{
    [self dismissMBP];
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:_keyWindow];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = labelText;
    hud.removeFromSuperViewOnHide = YES;
    [hud showAnimated:YES];
    [_keyWindow addSubview:hud];
    [hud hideAnimated:YES afterDelay:time];
}
+ (void)showMBP
{
    if (_keyWindow == nil) {
        _keyWindow = [[UIApplication sharedApplication].delegate window];
    }
    [MBProgressHUD showHUDAddedTo:_keyWindow animated:YES];
}
+ (void)dismissMBP
{
    if (_keyWindow == nil) {
        _keyWindow = [[UIApplication sharedApplication].delegate window];
    }
    [MBProgressHUD hideHUDForView:_keyWindow animated:YES];
}


@end
