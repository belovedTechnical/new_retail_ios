//
//  BDShowHUD.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/29.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SVProgressHUD.h"
#import "MBProgressHUD.h"

@interface BDShowHUD : NSObject

#pragma mark --<SVProgressHUD.h>
/** 显示一个圆圈在转 无蒙版 */
+ (void)showSVP;
+ (void)showSVPMaskTypeClear;

/** 显示一个圆圈在转 有蒙版 */
+ (void)showSVPMaskType:(SVProgressHUDMaskType)maskType;

/** 转圆圈 加 文字 默认 maskClear */
+ (void)showSVPMaskWithStatus:(NSString *)status;

/** 消失 */
+ (void)dismissSVP;

/** 对勾 加 字体 默认 maskClear */
+ (void)showSVPMaskWithtime:(NSTimeInterval)time SuccessStatus:(NSString *)status;
+ (void)showSVPMaskWithtime:(NSTimeInterval)time maskType:(SVProgressHUDMaskType)maskType SuccessStatus:(NSString *)status;

/** 叉叉 加 字体 默认 maskClear */
+ (void)showSVPMaskWithtime:(NSTimeInterval)time faileStatus:(NSString *)status;
+ (void)showSVPMaskWithtime:(NSTimeInterval)time maskType:(SVProgressHUDMaskType)maskType faileStatus:(NSString *)status;

/** 转圈 加 字体 默认 maskClear */
+ (void)showSVPMaskWithtime:(NSTimeInterval)time status:(NSString *)status;
+ (void)showSVPMaskWithtime:(NSTimeInterval)time maskType:(SVProgressHUDMaskType)maskType status:(NSString *)status;

/** 单独字体模式 默认 maskClear  */
+ (void)showSVPMaskWithtime:(NSTimeInterval)time statusString:(NSString *)status;
+ (void)showSVPMaskWithtime:(NSTimeInterval)time maskType:(SVProgressHUDMaskType)maskType statusString:(NSString *)status;

#pragma mark --<MBProgressHUD.h>
/** 背景透明 不可点击 */
+ (void)showMBP;

/** 消失 */
+ (void)dismissMBP;

/** 单独文字模式 背景透明 不可点击 time后消失 */
+ (void)showMBPShowTime:(CGFloat)time stateLabel:(NSString *)labelText;

@end
