//
//  SaveTool.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/1.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "SaveTool.h"

@implementation SaveTool

// 储存
+ (void)setObject:(id)object forKey:(NSString *)key
{
    NSUserDefaults *udf = [NSUserDefaults standardUserDefaults];
    [udf setObject:object forKey:key];
    [udf synchronize];
}

// 读取
+ (id)objectForKey:(NSString *)key
{
    NSUserDefaults *udf = [NSUserDefaults standardUserDefaults];
    return [udf objectForKey:key];
}

@end
