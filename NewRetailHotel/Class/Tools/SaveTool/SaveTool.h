//
//  SaveTool.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/1.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SaveTool : NSObject

// 储存
+ (void)setObject:(id)object forKey:(NSString *)key;

// 读取
+ (id)objectForKey:(NSString *)key;

@end
