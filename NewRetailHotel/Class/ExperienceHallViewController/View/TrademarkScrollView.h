//
//  TrademarkScrollView.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/11.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrademarkScrollView : UIView

@property (nonatomic, strong) NSArray *imageNames;

@end
