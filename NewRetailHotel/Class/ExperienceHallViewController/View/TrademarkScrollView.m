//
//  TrademarkScrollView.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/11.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "TrademarkScrollView.h"

@interface TrademarkScrollView()

@property (nonatomic, strong) UIScrollView *scrollView;

@end
@implementation TrademarkScrollView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}

- (void)initUI
{
    
}

- (void)setImageNames:(NSArray *)imageNames
{
    _imageNames = imageNames;
    [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSInteger count = imageNames.count;
    CGFloat scrollViewW = self.scrollView.frame.size.width;
    CGFloat scrollViewH = self.scrollView.frame.size.height;
    for (int i = 0; i < count; i ++) {
        UIButton *trademarkBtn = [[UIButton alloc] init];
        [trademarkBtn setImage:[UIImage imageNamed:imageNames[i]] forState:UIControlStateNormal];
        trademarkBtn.frame = CGRectMake(i * scrollViewW, 0, scrollViewW, scrollViewH);
        [self.scrollView addSubview:trademarkBtn];
    }
    self.scrollView.contentSize = CGSizeMake(count * scrollViewW, 0);
}

#pragma mark -懒加载
- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.frame = self.bounds;
        _scrollView.pagingEnabled = NO;
        [self addSubview:_scrollView];
    }
    return _scrollView;
}




@end
