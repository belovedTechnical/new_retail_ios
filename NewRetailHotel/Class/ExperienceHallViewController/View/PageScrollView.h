//
//  PageScrollView.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/9.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageScrollView : UIView

@property (nonatomic, strong) NSArray *imageNames;

@end
