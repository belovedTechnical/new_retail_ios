//
//  TopContView.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/8.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "TopContView.h"

@interface TopContView()

@end

@implementation TopContView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    CGFloat sanX = 0;
    CGFloat sanY = 0;
    CGFloat sanW = 60;
    CGFloat sanH = self.bd_height/2;
    UIButton *btnSan = [[UIButton alloc] init];
    btnSan.frame = CGRectMake(sanX, sanY, sanW, sanH);
    btnSan.backgroundColor = randomColor;
    [self addSubview:btnSan];
    
    CGFloat searchX = btnSan.bd_y+btnSan.bd_width;
    CGFloat searchY = 0;
    CGFloat searchW = kScreenW-btnSan.bd_width*2;
    CGFloat searchH = self.bd_height/2;
    UIButton *btnSearch = [[UIButton alloc] init];
    btnSearch.frame = CGRectMake(searchX, searchY, searchW, searchH);
    btnSearch.backgroundColor = randomColor;
    [self addSubview:btnSearch];
    
    CGFloat sortW = 60;
    CGFloat sortH = self.bd_height/2;
    CGFloat sortX = kScreenW-sortW;
    CGFloat sortY = 0;
    UIButton *btnSort = [[UIButton alloc] init];
    btnSort.frame = CGRectMake(sortX, sortY, sortW, sortH);
    btnSort.backgroundColor = randomColor;
    [self addSubview:btnSort];
    
    CGFloat btnX = self.frame.size.width/4;
    _btnNominate = [self topTitleButtonWithtitle:@"首推" titleX:0];
    _btnSuperexcellence = [self topTitleButtonWithtitle:@"百里挑一" titleX:btnX];
    _btnNewGoods = [self topTitleButtonWithtitle:@"新品先品" titleX:btnX*2];
    _btnRebate = [self topTitleButtonWithtitle:@"堪折须折" titleX:btnX*3];
}

- (UIButton *)topTitleButtonWithtitle:(NSString *)title titleX:(CGFloat)titleX
{
    UIButton *titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    titleBtn.frame = CGRectMake(titleX, self.bd_height/2, kScreenW/4, self.bd_height/2);
    titleBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [titleBtn setTitle:title forState:UIControlStateNormal];
    [titleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self addSubview:titleBtn];
    return titleBtn;
}


@end
