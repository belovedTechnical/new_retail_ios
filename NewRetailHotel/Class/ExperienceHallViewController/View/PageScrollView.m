//
//  PageScrollView.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/9.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "PageScrollView.h"

@interface PageScrollView()<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, weak) NSTimer *timer;

@end

@implementation PageScrollView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self == [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}

- (void)initUI
{
    // 1.设置pageControl单页的时候是否隐藏
    self.pageControl.hidesForSinglePage = YES;
    
//    // 2.设置pageControl显示的图片(KVC)
//    [self.pageControl setValue:[UIImage imageNamed:@"current"] forKeyPath:@"_currentPageImage"];
//    [self.pageControl setValue:[UIImage imageNamed:@"other"] forKeyPath:@"_pageImage"];
    
    // 3.开启定时器
    [self startTimer];
}

- (void)setImageNames:(NSArray *)imageNames
{
    _imageNames = imageNames;
    [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSInteger count = imageNames.count;
    CGFloat scrollViewW = self.scrollView.frame.size.width;
    CGFloat scrollViewH = self.scrollView.frame.size.height;
    for (int i = 0; i < count; i ++) {
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.image = [UIImage imageNamed:imageNames[i]];
        imageView.frame = CGRectMake(i * scrollViewW, 0, scrollViewW, scrollViewH);
        [self.scrollView addSubview:imageView];
    }
    self.scrollView.contentSize = CGSizeMake(count * scrollViewW, 0);
    self.pageControl.numberOfPages = count;
}

#pragma mark -定时器相关方法
- (void)startTimer
{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(nextPage:) userInfo:@"123" repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

- (void)stopTimer
{
    [self.timer invalidate];
}

- (void)nextPage:(NSTimer *)timer
{
    NSInteger page = self.pageControl.currentPage + 1;
    if (page == _imageNames.count) {
        page = 0;
    }
    [self.scrollView setContentOffset:CGPointMake(page * self.scrollView.frame.size.width, 0) animated:YES];
}

#pragma mark -懒加载
- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.frame = self.bounds;
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate = self;
        [self addSubview:_scrollView];
    }
    return _scrollView;
}

- (UIPageControl *)pageControl
{
    CGFloat bottomSpace = 20;
    CGFloat pageW = 150;
    CGFloat pageH = 30;
    CGFloat pageX = self.frame.size.width - pageW;
    CGFloat pageY = self.frame.size.height - pageH - bottomSpace;
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] init];
        _pageControl.frame = CGRectMake(pageX, pageY, pageW, pageH);
        [self addSubview:_pageControl];
    }
    return _pageControl;
}

#pragma mark -UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // 计算页码
    int page = (int)(scrollView.contentOffset.x / self.scrollView.frame.size.width + 0.5);
    
    // 修改页码
    self.pageControl.currentPage = page;
}

/**
 * 用户即将拖拽scrollView时候,停止定时器
 */
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self stopTimer];
}

/**
 * 用户已经停止拖拽scrollView时候,开启定时器
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self startTimer];
}


@end
