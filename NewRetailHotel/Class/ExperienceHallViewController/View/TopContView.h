//
//  TopContView.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/8.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopContView : UIView

@property (nonatomic, strong) UIButton *btnNominate;
@property (nonatomic, strong) UIButton *btnSuperexcellence;
@property (nonatomic, strong) UIButton *btnNewGoods;
@property (nonatomic, strong) UIButton *btnRebate;

@end
