//
//  WebSearchViewController.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/18.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebSearchViewController : UIViewController

@property (nonatomic, strong) NSString *listURL;

@end
