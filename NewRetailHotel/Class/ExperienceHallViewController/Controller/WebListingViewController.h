//
//  WebListingViewController.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/15.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebChildViewController.h"

@interface WebListingViewController : WebChildViewController

@property (nonatomic, strong) NSString *listURL;

@end
