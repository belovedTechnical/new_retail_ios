//
//  WebListingViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/15.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "WebListingViewController.h"

@interface WebListingViewController ()

@end

@implementation WebListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (NSURL *)webURL
{
    NSURL *url = [NSURL URLWithString:_listURL];
    return url;
}



@end
