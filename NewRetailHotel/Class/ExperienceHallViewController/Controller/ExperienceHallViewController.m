//
//  ExperienceHallViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/7.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "ExperienceHallViewController.h"
#import "TopContView.h"

@interface ExperienceHallViewController ()

@property (nonatomic, strong) UIScrollView *trademarkScrollView;

@end

@implementation ExperienceHallViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
//    self.view.backgroundColor = randomColor;
    UIView *topView = [[UIView alloc] init];
    topView.frame = CGRectMake(0, 0, kScreenW, StatusBarHeight);
    topView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:topView];
}

- (NSURL *)webURL
{
    NSString *urlStr = [NSString stringWithFormat:@"%@c-index.html",HTML_URL_Base];
    NSLog(@"体验馆url: %@", urlStr);
    NSURL *url = [NSURL URLWithString:urlStr];
    return url;
}












@end
