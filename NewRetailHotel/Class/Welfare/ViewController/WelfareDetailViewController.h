//
//  WelfareDetailViewController.h
//  NewRetailHotel
//
//  Created by lwz on 2018/8/6.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelfareDetailViewController : UIViewController

@property (nonatomic, strong) NSString *welfareDetailsURL;

@end
