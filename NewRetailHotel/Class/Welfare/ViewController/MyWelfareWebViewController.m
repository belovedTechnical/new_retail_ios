//
//  MyWelfareWebViewController.m
//  NewRetailHotel
//
//  Created by lwz on 2018/8/4.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "MyWelfareWebViewController.h"

@interface MyWelfareWebViewController ()

@end

@implementation MyWelfareWebViewController

- (CGRect)webViewFrame
{
    CGRect frame = CGRectMake(0, 0, kScreenW, kScreenH);
    return frame;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"我的公益";
    self.view.backgroundColor = randomColor;
    
}

- (NSURL *)webURL
{
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",HTML_URL_Base, urlWebMyWelfare];
    NSLog(@"我的公益：%@", urlStr);
    NSURL *url = [NSURL URLWithString:urlStr];
    return url;
}

@end
