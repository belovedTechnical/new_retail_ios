//
//  OCAndJSBaseModel.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "OCAndJSBaseModel.h"
#import "SaveTool.h"

@implementation OCAndJSBaseModel

- (NSString *)getKey {
    NSString *userKey = [SaveTool objectForKey:apikey];
    if (![userKey isEqualToString:@"(null)"] && userKey.length > 0) {// 登录了
        
    }else{// 没有登录
        [self gotoLogin];
    }
    return userKey;
}

- (void)goBack {
    
}

- (void)gotoCar {
    if ([self isUserLogin] == YES) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.jumpOcFuncDelegate respondsToSelector:@selector(webShopingCart)]) {
                [self.jumpOcFuncDelegate webShopingCart];
            }
        });
    }
}

- (void)gotoLogin {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.jumpOcFuncDelegate respondsToSelector:@selector(webLogin)]) {
            [self.jumpOcFuncDelegate webLogin];
        }
    });
}

- (void)openShortVideo {
    if ([self isUserLogin] == YES) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.jumpOcFuncDelegate respondsToSelector:@selector(webOpenShortVideo)]) {
                [self.jumpOcFuncDelegate webOpenShortVideo];
            }
        });
    }
}

- (void)gotoBuy:(NSString *)json {
    if ([self isUserLogin] == YES) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.jumpOcFuncDelegate respondsToSelector:@selector(webImmediatelyBuy:)]) {
                [self.jumpOcFuncDelegate webImmediatelyBuy:json];
            }
        });
    }
}

- (void)gotoMMBuy:(NSString *)json {
    if ([self isUserLogin] == YES) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.jumpOcFuncDelegate respondsToSelector:@selector(webImmediatelyBuy:)]) {
                [self.jumpOcFuncDelegate webImmediatelyBuy:json];
            }
        });
    }
}

- (void)gotoSpecialCar:(NSString *)storeId {
    if ([self isUserLogin] == YES) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.jumpOcFuncDelegate respondsToSelector:@selector(webSpecialCart:)]) {
                [self.jumpOcFuncDelegate webSpecialCart:storeId];
            }
        });
    }
}

- (void)createHotelOrder:(NSString *)json {
    if ([self isUserLogin] == YES) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.jumpOcFuncDelegate respondsToSelector:@selector(webCreateHotelOrder:)]) {
                [self.jumpOcFuncDelegate webCreateHotelOrder:json];
            }
        });
    }
}

- (void)openScan {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.jumpOcFuncDelegate respondsToSelector:@selector(webOpenScan)]) {
            [self.jumpOcFuncDelegate webOpenScan];
        }
    });
}
- (BOOL)isUserLogin {
    NSString *userKey = [SaveTool objectForKey:apikey];
    if (userKey.length > 8) {
        return YES;
    }else {
        return NO;
    }
}


- (void)callOC {
    
    //获取HTML中所定义的JS就去jsFunc，然后调用它  // 调用JS的方法
    JSValue *jsFunc = self.jsContext[@"javacalljs"];
    [jsFunc callWithArguments:nil];
}

- (void)callOCWithStr:(NSString *)str {
    
    //    NSLog(@"str== %@",str);
    // 调用JS的方法
    JSValue *jsFunc = self.jsContext[@"javacalljswith"];
    [jsFunc callWithArguments:@[@"888"]];
}

@end
