//
//  OCAndJSBaseModel.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JavaScriptCore/JavaScriptCore.h>
#import <UIKit/UIKit.h>

@protocol javaScriptTransferObjectiveDeleage <JSExport>
- (NSString *)getKey;
- (void)openScan;
- (void)gotoCar;
- (void)gotoLogin;
- (void)openShortVideo;
- (void)gotoBuy:(NSString *)json;
- (void)gotoMMBuy:(NSString *)json;
- (void)gotoSpecialCar:(NSString *)storeId;
- (void)createHotelOrder:(NSString *)json;

@end

@protocol jumpOCViewControllerDelegate <NSObject>
@optional
- (void)webLogin;
- (void)webOpenScan;
- (void)webShopingCart;
- (void)webOpenShortVideo;
- (void)webImmediatelyBuy:(NSString *)json;
- (void)webSpecialCart:(NSString *)storeId;
- (void)webCreateHotelOrder:(NSString *)json;

@end

@interface OCAndJSBaseModel : NSObject <javaScriptTransferObjectiveDeleage>

@property (nonatomic, weak) UIWebView *webView;
@property (nonatomic, weak) JSContext *jsContext;

@property (nonatomic, strong) id <jumpOCViewControllerDelegate> jumpOcFuncDelegate;

@end
