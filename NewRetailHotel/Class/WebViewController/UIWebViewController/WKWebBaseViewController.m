//
//  WKWebBaseViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/15.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "WKWebBaseViewController.h"
#import "WebShopDetailsViewController.h"
#import "HotelOrderListViewController.h"
#import "WebListingViewController.h"
#import "SaveTool.h"

@interface WKWebBaseViewController ()<
                                        WKUIDelegate,
                                        WKNavigationDelegate,
                                        WKScriptMessageHandler
                                        >
@property(nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong) WKNavigation *backNavigation;
@end

@implementation WKWebBaseViewController
- (NSURL *)webURL
{
    NSURL *url = [NSURL URLWithString:@""];
    return url;
}
- (CGRect)webViewFrame
{
    CGRect frame = CGRectMake(0, 0, kScreenW, kScreenH+49);
    return frame;
}

- (WKWebView *)webView
{
    if (!_webView) {
        _webView = [[WKWebView alloc] initWithFrame:[self webViewFrame]];
        [self.webView loadRequest:[NSURLRequest requestWithURL:[self webURL]]];
        self.webView.scrollView.bounces = NO;
        [self.view addSubview:_webView];
        [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
//        [_webView addObserver:self forKeyPath:@"canGoBack" options:NSKeyValueObservingOptionNew context:nil];
        [self.view insertSubview:_webView belowSubview:self.progressView];
    }
    return _webView;
}

// 进度条
- (UIProgressView *)progressView
{
    if (!_progressView) {
        if (self.navigationController.navigationBarHidden == NO || self.navigationController.navigationBar.hidden == NO) {
            _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, kScreen_Width, 2)];
        }else {
            _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, StatusBarHeight, kScreen_Width, 2)];
        }
        self.progressView.tintColor = [UIColor darkGrayColor];
        self.progressView.trackTintColor = [UIColor whiteColor];
        [self.view addSubview:self.progressView];
    }
    return _progressView;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.webView goBack];
//    self.backNavigation = [_webView goBack];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView.UIDelegate = self;
    self.webView.navigationDelegate = self;
    [self setupHttpCookie];
}

#pragma mark - 计算wkWebView进度条
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == self.webView && [keyPath isEqualToString:@"estimatedProgress"]) {
        CGFloat newprogress = [[change objectForKey:NSKeyValueChangeNewKey] doubleValue];
        if (newprogress == 1) {
            self.progressView.hidden = YES;
            [self.progressView setProgress:0 animated:NO];
        }else {
            self.progressView.hidden = NO;
            [self.progressView setProgress:newprogress animated:YES];
        }
    }
}

// 取消监听
- (void)dealloc {
    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
//    [self.webView removeObserver:self forKeyPath:@"canGoBack"];
}

- (void)setupHttpCookie
{
    NSString *userKey = [SaveTool objectForKey:apikey];
    if (userKey.length > 6) {
        NSMutableDictionary *cookieProperties = [NSMutableDictionary dictionary];
        [cookieProperties setObject:@"userkey" forKey:NSHTTPCookieName];
        [cookieProperties setObject:userKey forKey:NSHTTPCookieValue];
        [cookieProperties setObject:HTML_HOST forKey:NSHTTPCookieDomain];
        [cookieProperties setObject:HTML_HOST forKey:NSHTTPCookieOriginURL];
        [cookieProperties setObject:@"/" forKey:NSHTTPCookiePath];
        [cookieProperties setObject:@"0" forKey:NSHTTPCookieVersion];
        
        NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
        NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        for (NSHTTPCookie *cookie in [cookieJar cookies]) {
//            BDLog(@"%@", cookie);
        }
    }
}

#pragma mark - WKNavigationDelegate

/**
 *  页面开始加载时调用
 *
 *  @param webView    实现该代理的webview
 *  @param navigation 当前navigation
 */
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    
    [BDShowHUD showSVPMaskWithStatus:@"正在加载"];
}

/**
 *  当内容开始返回时调用
 *
 *  @param webView    实现该代理的webview
 *  @param navigation 当前navigation
 */
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    
    
}

/**
 *  页面加载完成之后调用
 *
 *  @param webView    实现该代理的webview
 *  @param navigation 当前navigation
 */
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [BDShowHUD dismissSVP];
    self.title = self.webView.title;
//    if ([_backNavigation isEqual:navigation]) {
//        // 这次的加载是点击返回产生的，刷新
//        [_webView reload];
//        _backNavigation = nil;
//    }
}

/**
 *  加载失败时调用
 *
 *  @param webView    实现该代理的webview
 *  @param navigation 当前navigation
 *  @param error      错误
 */
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    
    
}

/**
 *  接收到服务器跳转请求之后调用
 *
 *  @param webView      实现该代理的webview
 *  @param navigation   当前navigation
 */
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation {
    
    
}

/**
 *  在收到响应后，决定是否跳转
 *
 *  @param webView            实现该代理的webview
 *  @param navigationResponse 当前navigation
 *  @param decisionHandler    是否跳转block
 */
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
    
    NSString *url = navigationResponse.response.URL.absoluteString;
//    BDLog(@"%@", url);
    if ([url containsString:@"shopDetails"]) {
        [self.webView stopLoading];
        WebShopDetailsViewController *shopVC = [[WebShopDetailsViewController alloc] init];
        NSString *shopId = [url substringFromIndex:url.length-6];
        shopVC.shopDetailsURL = [NSString stringWithFormat:@"%@%@%@", HTML_URL_Base,urlWebShopDetails,shopId];
        self.navigationController.navigationBar.hidden = NO;
        [self.navigationController pushViewController:shopVC animated:YES];
    }
    
    if ([url containsString:@"hotelOrderList"]) {
        HotelOrderListViewController *hotelOrderVC = [[HotelOrderListViewController alloc] init];
        [self.navigationController pushViewController:hotelOrderVC animated:YES];
    }
    
    if ([url containsString:@"shop/MALL"] || [url isEqualToString:HTML_URL_Base]) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
//    if ([url containsString:@"brand"] || [url containsString:@"category"] || [url containsString:@"search"]) {
//        [self.webView stopLoading];
//        WebListingViewController *listVC = [[WebListingViewController alloc] init];
//        listVC.listURL = url;
//        self.navigationController.navigationBar.hidden = NO;
//        [self.navigationController pushViewController:listVC animated:YES];
//    }
    
    //    // 不允许跳转
//        decisionHandler(WKNavigationResponsePolicyCancel);
    // 允许跳转
    decisionHandler(WKNavigationResponsePolicyAllow);
    return;
}

/**
 *  在发送请求之前，决定是否跳转
 *
 *  @param webView          实现该代理的webview
 *  @param navigationAction 当前navigation
 *  @param decisionHandler  是否调转block
 */
//- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
//
////    // 如果请求的是百度地址，则延迟5s以后跳转
////    if ([navigationAction.request.URL.host.lowercaseString isEqual:@"www.baidu.com"]) {
////
////        //        // 延迟5s之后跳转
////        //        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
////        //
////        //            // 允许跳转
////        //            decisionHandler(WKNavigationActionPolicyAllow);
////        //        });
////
////        // 允许跳转
////        decisionHandler(WKNavigationActionPolicyAllow);
////        return;
////    }
////    // 不允许跳转
////    decisionHandler(WKNavigationActionPolicyCancel);
//    // 允许跳转
//    decisionHandler(WKNavigationActionPolicyAllow);
//    return;
//}

#pragma mark - WKUIDelegate

/**
 *  web界面中有弹出警告框时调用
 *
 *  @param webView           实现该代理的webview
 *  @param message           警告框中的内容
 *  @param frame             主窗口
 *  @param completionHandler 警告框消失调用
 */

- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler {
    [[[UIAlertView alloc] initWithTitle:@"温馨提示" message:message delegate:nil cancelButtonTitle:@"确认" otherButtonTitles: nil] show];
    
    completionHandler(YES);
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString *))completionHandler {
    
    
}

// 从web界面中接收到一个脚本时调用
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    
//    BDLog(@"%@", message);
}




@end
