//
//  WKNavBarHideViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/3/26.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "WKNavBarHideViewController.h"

@interface WKNavBarHideViewController ()

@end

@implementation WKNavBarHideViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *btnCustomBack = [[UIButton alloc] initWithFrame:CGRectMake(0, StatusBarHeight, 36, 35)];
    [btnCustomBack addTarget:self action:@selector(webBackBtnClick) forControlEvents:UIControlEventTouchUpInside];
    btnCustomBack.backgroundColor = [UIColor clearColor];
    [self.view addSubview:btnCustomBack];
}

- (NSURL *)webURL
{
    NSURL *url = [NSURL URLWithString:_navBarHideURL];
    return url;
}

- (void)webBackBtnClick {
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
