//
//  WebThemeViewController.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/3/16.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WKWebBaseViewController.h"

@interface WebThemeViewController : WKWebBaseViewController

@property (nonatomic, strong) NSString *themeURL;

@end
