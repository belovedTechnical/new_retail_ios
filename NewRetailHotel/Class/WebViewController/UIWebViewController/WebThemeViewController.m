//
//  WebThemeViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/3/16.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "WebThemeViewController.h"

@interface WebThemeViewController ()

@end

@implementation WebThemeViewController

//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBar.hidden = YES;
//}
//
//- (void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:animated];
//    self.navigationController.navigationBar.hidden = NO;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
  
}

- (NSURL *)webURL
{
    NSURL *url = [NSURL URLWithString:_themeURL];
    return url;
}


@end
