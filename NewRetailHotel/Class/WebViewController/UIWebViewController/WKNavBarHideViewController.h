//
//  WKNavBarHideViewController.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/3/26.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "WKWebBaseViewController.h"

@interface WKNavBarHideViewController : WKWebBaseViewController

@property (nonatomic, strong) NSString *navBarHideURL;

@end
