//
//  UIWebBaseViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "UIWebBaseViewController.h"
#import "WebShopDetailsViewController.h"
#import "OrderCofirmViewController.h"
#import "WebRoomDetailsViewController.h"
#import "ShoppingCartViewController.h"
#import "WebSearchViewController.h"
#import "ScanningViewController.h"
#import "NoteLoginViewController.h"
#import "HotelOrderListViewController.h"
#import "WebThemeViewController.h"
#import "WKNavBarHideViewController.h"
#import "WelfareDetailViewController.h"

#import "OCAndJSBaseModel.h"
#import "SaveTool.h"

@interface UIWebBaseViewController ()<UIWebViewDelegate, jumpOCViewControllerDelegate>

@property (nonatomic, strong) JSContext *jsContext;

@end

@implementation UIWebBaseViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [self.webView goBack];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self setupHttpCookie];
    // 监听成功登陆
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(succeedLoginFun) name:succeedLoginNoti object:nil];
}

- (NSURL *)webURL
{
    if (self.webURLStr) {
        return [NSURL URLWithString:self.webURLStr];
    }
    return [NSURL URLWithString:@""];
}

- (CGRect)webViewFrame
{
    CGRect frame = CGRectMake(0, 0, kScreenW, kScreenH);
    return frame;
}

- (void)initUI
{
    self.webView = [[UIWebView alloc] initWithFrame:[self webViewFrame]];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[self webURL]]];
    self.webView.scalesPageToFit = YES;
    self.webView.scrollView.bounces = NO;
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
    self.webView.backgroundColor = [UIColor whiteColor];
    self.webView.scrollView.backgroundColor = [UIColor whiteColor];
    self.webView.opaque = false;
}

#pragma mark -UIWebViewDelegate
// 开始加载时调用
-(void)webViewDidStartLoad:(UIWebView *)webView {
    [BDShowHUD showSVPMaskWithStatus:@"正在加载"];
}

// 加载完成时调用
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    self.jsContext = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    OCAndJSBaseModel *model  = [[OCAndJSBaseModel alloc] init];
    self.jsContext[@"app"] = model;
    model.jsContext = self.jsContext;
    model.webView = self.webView;
    model.jumpOcFuncDelegate = self;
    NSString *NavTitle = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.navigationItem.title = NavTitle;
    
    _jsContext.exceptionHandler = ^(JSContext *context, JSValue *exceptionValue) {
        context.exception = exceptionValue;
        //        NSLog(@"异常信息：%@", exceptionValue);
    };
    
    if ([webView.request.URL.absoluteString containsString:@"welfare.html"]) {
        [webView stringByEvaluatingJavaScriptFromString:@"hidenav()"];
    }
    
    [BDShowHUD dismissSVP];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *url = request.URL.absoluteString;
    
    if ([url containsString:@"welfareDetail"]) {
        WelfareDetailViewController *vc = [[WelfareDetailViewController alloc] init];
        vc.welfareDetailsURL = url;
        [self.navigationController pushViewController:vc animated:YES];
        return NO;
    }
    
    if ([url containsString:@"welChild/schId"]) {
        WelfareDetailViewController *vc = [[WelfareDetailViewController alloc] init];
        vc.welfareDetailsURL = url;
        [self.navigationController pushViewController:vc animated:YES];
        return NO;
    }
    
    
//    BDLog(@"%@", url);
    if ([url containsString:@"shopDetails"]) {
        WebShopDetailsViewController *shopVC = [[WebShopDetailsViewController alloc] init];
        NSString *shopId = [url substringFromIndex:url.length-6];
        shopVC.shopDetailsURL = [NSString stringWithFormat:@"%@%@%@", HTML_URL_Base, urlWebShopDetails,shopId];
        [self.navigationController pushViewController:shopVC animated:YES];
        return NO;
    }
    if ([url containsString:@"roomDetail"] || [url containsString:@"/brands.html"]) {
        WebRoomDetailsViewController *roomDetailsVC = [[WebRoomDetailsViewController alloc] init];
        roomDetailsVC.roomDetailsURL = url;
        [self.navigationController pushViewController:roomDetailsVC animated:YES];
        return NO;
    }
    
    if ([url containsString:@"hotelOrderList"] || [url containsString:@"roomServiceList"]) {
        HotelOrderListViewController *hotelOrderVC = [[HotelOrderListViewController alloc] init];
        [self.navigationController pushViewController:hotelOrderVC animated:YES];
        return NO;
    }
    
    if ([url containsString:@"category"] || [url containsString:@"spec"]) {
        WebThemeViewController *themeVC = [[WebThemeViewController alloc] init];
        themeVC.themeURL = url;
        [self.navigationController pushViewController:themeVC animated:YES];
        return NO;
    }
    
    if ([url containsString:@"/brand/"] || [url containsString:@"/shop/"] || [url isEqualToString:HTML_URL_Base]) {
        if ([url containsString:@"shop/MALL"] || [url isEqualToString:HTML_URL_Base]) {
            self.navigationController.tabBarController.selectedIndex = 0;
            [self.navigationController popToRootViewControllerAnimated:YES];
            return NO;
        } else {
            WKNavBarHideViewController *nabBarHVC = [[WKNavBarHideViewController alloc] init];
            nabBarHVC.navBarHideURL = url;
            [self.navigationController pushViewController:nabBarHVC animated:YES];
            return NO;
        }
    }
    
    /*
    if (UIWebViewNavigationTypeLinkClicked == navigationType) {
        if ([url containsString:@"/lobby/"]) {
            
            UIWebBaseViewController *vc = [[UIWebBaseViewController alloc] init];
            vc.webURLStr = url;
            [self.navigationController pushViewController:vc animated:YES];
            return NO;
        }
        
    }
     */
    

    
    if ([url containsString:@"shopkeyword-search"] || [url containsString:@"shop-search"]) {
        WebSearchViewController *searchVC = [[WebSearchViewController alloc] init];
        [self.navigationController pushViewController:searchVC animated:YES];
        return NO;
    }
    
    return YES;
}

- (void)setupHttpCookie
{
    NSString *userKey = [SaveTool objectForKey:apikey];
    if (userKey.length > 6) {
        NSMutableDictionary *cookieProperties = [NSMutableDictionary dictionary];
        [cookieProperties setObject:@"userkey" forKey:NSHTTPCookieName];
        [cookieProperties setObject:userKey forKey:NSHTTPCookieValue];
        [cookieProperties setObject:HTML_HOST forKey:NSHTTPCookieDomain];
        [cookieProperties setObject:HTML_HOST forKey:NSHTTPCookieOriginURL];
        [cookieProperties setObject:@"/" forKey:NSHTTPCookiePath];
        [cookieProperties setObject:@"0" forKey:NSHTTPCookieVersion];
        
        NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
        NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        for (NSHTTPCookie *cookie in [cookieJar cookies]) {
//            BDLog(@"%@", cookie);
        }
    }
}

#pragma mark -jumpOCViewControllerDelegate
- (void)webOpenScan
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Scanning" bundle:[NSBundle mainBundle]];
    ScanningViewController *scaningVC = [story instantiateViewControllerWithIdentifier:@"Scanning"];
    [self.navigationController pushViewController:scaningVC animated:YES];
}

- (void)webLogin {
    NoteLoginViewController *loginVC = [[NoteLoginViewController alloc] init];
    [self.navigationController pushViewController:loginVC animated:YES];
}

- (void)webShopingCart {
    ShoppingCartViewController *cartVC = [[ShoppingCartViewController alloc] init];
    cartVC.isRootVC = YES;
    [self.navigationController pushViewController:cartVC animated:YES];
}

- (void)webImmediatelyBuy:(NSString *)json {
    NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:(NSJSONReadingMutableContainers) error:nil];
    NSInteger quantity = [[dic objectForKey:@"quantity"] integerValue];
    NSString *isCart = [NSString stringWithFormat:@"%@",[dic objectForKey:@"isCart"]];
    NSString *idStr = [NSString stringWithFormat:@"%@",[dic objectForKey:@"productId"]];
    NSString *productStoreId = [NSString stringWithFormat:@"%@",[dic objectForKey:@"storeId"]];
    OrderCofirmViewController *OrderVC = [[OrderCofirmViewController alloc]init];
    OrderVC.idStr = idStr;
    OrderVC.isCart = isCart;
    OrderVC.quantity = quantity;
    OrderVC.productStoreId = productStoreId;
    [self.navigationController pushViewController:OrderVC animated:YES];
}

- (void)webSpecialCart:(NSString *)storeId {
    ShoppingCartViewController *cartVC = [[ShoppingCartViewController alloc] init];
    cartVC.isRootVC = YES;
    cartVC.storeID = storeId;
    [self.navigationController pushViewController:cartVC animated:YES];
}

// 登录成功通知监听方法
- (void)succeedLoginFun {
    [self setupHttpCookie];
}

@end
