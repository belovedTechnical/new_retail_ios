//
//  WebChildViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/15.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import "WebChildViewController.h"
#import "OrderCofirmViewController.h"
#import "ShoppingCartViewController.h"
#import "HotleOrderViewController.h"
#import "WebSearchViewController.h"
#import "ShoppingCartViewController.h"
#import "OrderCofirmViewController.h"
#import "HotelOrderListViewController.h"
#import "NoteLoginViewController.h"
#import "WebThemeViewController.h"
#import "WKNavBarHideViewController.h"
#import "WebShopDetailsViewController.h"

#import "OCAndJSBaseModel.h"
#import "SaveTool.h"

@interface WebChildViewController ()<UIWebViewDelegate, jumpOCViewControllerDelegate>

@property (nonatomic, strong) JSContext *jsContext;
@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, assign) BOOL isLogin;
@end

@implementation WebChildViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.webView goBack];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
    [self setupHttpCookie];
    // 监听成功登陆
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(succeedLoginFun) name:succeedLoginNoti object:nil];
}

- (NSURL *)webURL
{
    NSURL *url = [NSURL URLWithString:@""];
    return url;
}
- (CGRect)webViewFrame
{
    CGRect frame = CGRectMake(0, 0, kScreenW, kScreenH);
    return frame;
}

- (void)initUI
{
    self.webView = [[UIWebView alloc] initWithFrame:[self webViewFrame]];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[self webURL]]];
    self.webView.scalesPageToFit = YES;
    self.webView.scrollView.bounces = NO;
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
}

#pragma mark -UIWebViewDelegate
// 开始加载时调用
-(void)webViewDidStartLoad:(UIWebView *)webView {
    [BDShowHUD showSVPMaskWithStatus:@"正在加载"];
}

// 加载完成时调用
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    self.jsContext = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    OCAndJSBaseModel *model  = [[OCAndJSBaseModel alloc] init];
    self.jsContext[@"app"] = model;
    model.jsContext = self.jsContext;
    model.webView = self.webView;
    model.jumpOcFuncDelegate = self;
    NSString *NavTitle = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.title = NavTitle;
    
    _jsContext.exceptionHandler = ^(JSContext *context, JSValue *exceptionValue) {
        context.exception = exceptionValue;
        //        NSLog(@"异常信息：%@", exceptionValue);
    };
//    NSString *userKey = [SaveTool objectForKey:apikey];
//    if (userKey.length > 6) {
//        if (_isLogin == NO) {
//            _isLogin = YES;
//            NSString *addkey = [NSString stringWithFormat:@"addKey('%@');", userKey];
//            [self.webView stringByEvaluatingJavaScriptFromString:addkey];
//            [_webView reload];
//        }
//    }
    [BDShowHUD dismissSVP];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *url = request.URL.absoluteString;
    if ([url containsString:@"shopDetails"]) {
        WebShopDetailsViewController *shopVC = [[WebShopDetailsViewController alloc] init];
        NSString *shopId = [url substringFromIndex:url.length-6];
        shopVC.shopDetailsURL = [NSString stringWithFormat:@"%@%@%@", HTML_URL_Base, urlWebShopDetails,shopId];
        [self.navigationController pushViewController:shopVC animated:YES];
    }
    if ([url containsString:@"hotelOrderList"]) {
        HotelOrderListViewController *hotelOrderVC = [[HotelOrderListViewController alloc] init];
        [self.navigationController pushViewController:hotelOrderVC animated:YES];
    }
    if ([url containsString:@"-search"]) {
        WebSearchViewController *searchVC = [[WebSearchViewController alloc] init];
        [self.navigationController pushViewController:searchVC animated:YES];
    }
    
    if ([url containsString:@"/brand/"] || [url containsString:@"/shop/"] || [url isEqualToString:HTML_URL_Base]) {
        if ([url containsString:@"shop/MALL"] || [url isEqualToString:HTML_URL_Base]) {
            self.navigationController.tabBarController.selectedIndex = 0;
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else {
            WKNavBarHideViewController *nabBarHVC = [[WKNavBarHideViewController alloc] init];
            nabBarHVC.navBarHideURL = url;
            [self.navigationController pushViewController:nabBarHVC animated:YES];
        }
    }
    
    return YES;
}

- (void)setupHttpCookie {
    NSString *userKey = [SaveTool objectForKey:apikey];
    if (userKey.length > 6) {
        NSMutableDictionary *cookieProperties = [NSMutableDictionary dictionary];
        [cookieProperties setObject:@"userkey" forKey:NSHTTPCookieName];
        [cookieProperties setObject:userKey forKey:NSHTTPCookieValue];
        [cookieProperties setObject:HTML_HOST forKey:NSHTTPCookieDomain];
        [cookieProperties setObject:HTML_HOST forKey:NSHTTPCookieOriginURL];
        [cookieProperties setObject:@"/" forKey:NSHTTPCookiePath];
        [cookieProperties setObject:@"0" forKey:NSHTTPCookieVersion];
        
        NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
        NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        for (NSHTTPCookie *cookie in [cookieJar cookies]) {
//            BDLog(@"%@", cookie);
        }
    }
}

#pragma mark -jumpOCViewControllerDelegate
- (void)webLogin {
    NoteLoginViewController *loginVC = [[NoteLoginViewController alloc] init];
    [self.navigationController pushViewController:loginVC animated:YES];
}

- (void)webShopingCart {
    ShoppingCartViewController *cartVC = [[ShoppingCartViewController alloc] init];
    cartVC.isRootVC = YES;
    [self.navigationController pushViewController:cartVC animated:YES];
}

- (void)webSpecialCart:(NSString *)storeId {
    ShoppingCartViewController *cartVC = [[ShoppingCartViewController alloc] init];
    cartVC.isRootVC = YES;
    cartVC.storeID = storeId;
    [self.navigationController pushViewController:cartVC animated:YES];
}

// {"isCart":"N","productId":"100307_100307","quantity":"1","storeId":"100063"}
- (void)webImmediatelyBuy:(NSString *)json {
    NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:(NSJSONReadingMutableContainers) error:nil];
    NSInteger quantity = [[dic objectForKey:@"quantity"] integerValue];
    NSString *isCart = [NSString stringWithFormat:@"%@",[dic objectForKey:@"isCart"]];
    NSString *idStr = [NSString stringWithFormat:@"%@",[dic objectForKey:@"productId"]];
    NSString *productStoreId = [NSString stringWithFormat:@"%@",[dic objectForKey:@"storeId"]];
    OrderCofirmViewController *OrderVC = [[OrderCofirmViewController alloc]init];
    OrderVC.idStr = idStr;
    OrderVC.isCart = isCart;
    OrderVC.quantity = quantity;
    OrderVC.productStoreId = productStoreId;
    [self.navigationController pushViewController:OrderVC animated:YES];
}

/*
 String hotelName 酒店名称
 String hotelPartyId 酒店id
 String roomTypeId 房型id
 String room_type 房型类型
 String payType 支付类型 到店付/在线付CashPayment/PrepaidPayment
 */
- (void)webCreateHotelOrder:(NSString *)json {
    NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:(NSJSONReadingMutableContainers) error:nil];
    NSString *payType = [NSString stringWithFormat:@"%@",[dic objectForKey:@"payType"]];
    NSString *room_type = [NSString stringWithFormat:@"%@",[dic objectForKey:@"room_type"]];
    NSString *hotelName = [NSString stringWithFormat:@"%@",[dic objectForKey:@"hotelName"]];
    NSString *roomTypeId = [NSString stringWithFormat:@"%@",[dic objectForKey:@"roomTypeId"]];
    NSString *hotelPartyId = [NSString stringWithFormat:@"%@",[dic objectForKey:@"hotelPartyId"]];
//    BDLog(@"payType--%@ room_type--%@ hotelName--%@ roomTypeId--%@ hotelPartyId--%@", payType, room_type, hotelName, roomTypeId, hotelPartyId);
    HotleOrderViewController *hotleOrderVC = [[HotleOrderViewController alloc] init];
    hotleOrderVC.payType = payType;
    hotleOrderVC.room_type = room_type;
    hotleOrderVC.hotelName = hotelName;
    hotleOrderVC.roomTypeId = roomTypeId;
    hotleOrderVC.hotelPartyId = hotelPartyId;
    [self.navigationController pushViewController:hotleOrderVC animated:YES];
}

// 登录成功通知监听方法
- (void)succeedLoginFun {
    [self setupHttpCookie];
}

@end
