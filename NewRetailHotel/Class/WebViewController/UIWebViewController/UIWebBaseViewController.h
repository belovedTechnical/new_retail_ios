//
//  UIWebBaseViewController.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/11.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWebBaseViewController : UIViewController

@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, copy) NSString *webURLStr;


@end
