//
//  WKWebBaseViewController.h
//  NewRetailHotel
//
//  Created by BDSir on 2018/1/15.
//  Copyright © 2018年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface WKWebBaseViewController : UIViewController

@property(nonatomic,strong) WKWebView *webView;
- (NSURL *)webURL;

@end
