//
//  NewFeatureCell.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/1.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "NewFeatureCell.h"
#import "BDTabBarController.h"

@interface NewFeatureCell()

@property (nonatomic, weak) UIImageView *imageV;

/** 开始按钮 */
@property (nonatomic, weak) UIButton *startBtn;

@end
@implementation NewFeatureCell

/// 控制开始按钮显示或隐藏
- (void)setStartBtn:(NSIndexPath *)indexPath count:(int)count
{
    if (indexPath.item == count - 1) {
        self.startBtn.hidden = NO;
    }else {
        self.startBtn.hidden = YES;
    }
}

#pragma mark -懒加载
- (UIButton *)startBtn
{
    if (_startBtn == nil) {
        UIButton *startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //        [startBtn setImage:[UIImage imageNamed:@"iconbottom_cart_selected"] forState:UIControlStateNormal];
        [startBtn setTitle:@"开启全新的体验" forState:UIControlStateNormal];
        startBtn.layer.cornerRadius = 25;
        startBtn.layer.masksToBounds = YES;
        startBtn.layer.borderColor = [UIColor whiteColor].CGColor;
        startBtn.layer.borderWidth = 1;
        [startBtn setTintColor:[UIColor whiteColor]];
        [startBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //        [startBtn sizeToFit];
        startBtn.frame =
        CGRectMake(kScreenW/ 4, kScreenH - 60 - 65, kScreenW /2, 50);
        //        startBtn.center = CGPointMake(self.width * 0.5, self.heigth * 0.8);
        [self.contentView addSubview:startBtn];
        _startBtn = startBtn;
        
        [startBtn addTarget:self action:@selector(startBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _startBtn;
}

/// 控制按钮点击
- (void)startBtnClick
{
    BDTabBarController *tabVC = [[BDTabBarController alloc] init];
    [UIApplication sharedApplication].keyWindow.rootViewController = tabVC;
}

- (UIImageView *)imageV
{
    if (_imageV == nil) {
        UIImageView *imageV = [[UIImageView alloc] init];
        imageV.frame = self.bounds;
        [self.contentView addSubview:imageV];
        _imageV = imageV;
    }
    return _imageV;
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    self.imageV.image = image;
}
@end
