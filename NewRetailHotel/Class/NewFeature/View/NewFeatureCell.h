//
//  NewFeatureCell.h
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/1.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewFeatureCell : UICollectionViewCell

@property (nonatomic, strong) UIImage *image;

/// 控制开始按钮显示或隐藏
- (void)setStartBtn:(NSIndexPath *)indexPath count:(int)count;

@end
