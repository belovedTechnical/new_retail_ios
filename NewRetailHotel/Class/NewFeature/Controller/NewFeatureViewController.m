//
//  NewFeatureViewController.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/1.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import "NewFeatureViewController.h"
#import "NewFeatureCell.h"

@interface NewFeatureViewController ()

@end

#define Count 4
@implementation NewFeatureViewController

static NSString * const reuseIdentifier = @"Cell";

- (instancetype)init
{
    UICollectionViewFlowLayout *flowF = [[UICollectionViewFlowLayout alloc] init];
    flowF.itemSize = CGSizeMake(kScreenW, kScreenH);
    flowF.minimumLineSpacing = 0;
    flowF.minimumInteritemSpacing = 0;
    flowF.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    return [super initWithCollectionViewLayout:flowF];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.collectionView registerClass:[NewFeatureCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    self.collectionView.bounces = NO;
    self.collectionView.pagingEnabled = YES;
    
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return Count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NewFeatureCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    NSString *imageN = [NSString stringWithFormat:@"NewFeature%ld", indexPath.item + 1];
    cell.image = [UIImage imageNamed:imageN];
    
    cell.backgroundColor = randomColor;
    [cell setStartBtn:indexPath count:Count];
    
    return cell;
}


@end

