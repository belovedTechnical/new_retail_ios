//
//  main.m
//  NewRetailHotel
//
//  Created by BDSir on 2017/12/1.
//  Copyright © 2017年 BDSir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
